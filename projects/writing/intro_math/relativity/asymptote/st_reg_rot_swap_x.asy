// :::: settings
import math;
settings.outformat = "pdf";
defaultpen(fontsize(20pt));
size(20cm);
arrowbar axisarrow = Arrow(TeXHead);
arrowbar axisarrow = Arrow;

pair orig = (0,0);
int xmin = -1;
int xmax =  6;

int ymin = -1;
int ymax =  6;

// :::: axes
// lines
path pxaxis = (xmin, 0) -- (xmax, 0);
draw(pxaxis, arrow = axisarrow);

path pyaxis = (0, ymin) -- (0, ymax);
draw(pyaxis, arrow = axisarrow);

// origin
dot("$O$", orig, align = W + S);

// points
// x axis
for (int i = xmin + 1; i <= (xmax-1); ++i){
  if (i<=0){continue;}
  dot((i,0));
  Label L = Label((string) i);
  label(L, (i,0), align = (0, -5));
}
label("$x$", (xmax, 0), align = E);

// y axis
for (int i = ymin + 1; i <= (ymax-1); ++i){
  if (i<=0){continue;}
  dot((0,i));
  Label L = Label((string) i);
  label(L, (0,i), align = (-5, 0));
}
label("$y$", (0, ymax), align = N);

// :::: initial dots
real ax = 3;
real ay = 1;
real bx = 4;
real by = 3;
pair a = (ax, ay);
pair b = (bx, by);

real dtheta = 45;
real atheta = aTan(ay/ax);
real btheta = aTan(by/bx);
real athetaprime = atheta + dtheta;
real bthetaprime = btheta + dtheta;

real ar = sqrt(ax*ax + ay*ay);
real br = sqrt(bx*bx + by*by);


real axprime = ar*Cos(athetaprime);
real ayprime = ar*Sin(athetaprime);
real bxprime = br*Cos(bthetaprime);
real byprime = br*Sin(bthetaprime);

pair aprime = (axprime, ayprime);
pair bprime = (bxprime, byprime);

// :::: arcs
path aarc = arc(orig, ar, atheta, athetaprime);
path barc = arc(orig, br, btheta, bthetaprime);
 
draw(aarc, red, arrow = axisarrow);
draw(barc, blue, arrow = axisarrow);

dot(a, red, L = "$A$");
dot(b, blue, L = "$B$");
dot(aprime, red, L = "$A'$", align = N);
dot(bprime, blue, L = "$B'$", align = N);

// :::: altitudes and labels
draw((axprime, ayprime) -- (axprime, 0), red);
draw((ax     , ay     ) -- (ax     , 0), red);
draw((bxprime, byprime) -- (bxprime, 0), blue);
draw((bx     , by     ) -- (bx     , 0), blue);

Label lax = Label("$x_a$", red);
label(lax, (ax, 0), align = S);

Label laxprime = Label("$x'_a$", red);
label(laxprime, (axprime, 0), align = S);

Label lbx = Label("$x_b$", blue);
label(lbx, (bx, 0), align = S);

Label lbxprime = Label("$x'_b$", blue);
label(lbxprime, (bxprime, 0), align = S);

// :::: angles
path paangle = aprime -- orig -- a;
draw(paangle, Dotted() + red);
path pbangle = bprime -- orig -- b;
draw(pbangle, Dotted() + blue);

path asarc = arc(orig, 0.1*ar, atheta, athetaprime);
path bsarc = arc(orig, 0.1*br, btheta, bthetaprime);
 
draw(asarc, red);
draw(bsarc, blue);

// :::: angle labels
pair aaloc = (0.1*ar*Cos((atheta + athetaprime)/2), 0.1*ar*Cos((atheta + athetaprime)/2));
Label aalabel = Label("$45^{\circ}$", red);
label(aalabel, aaloc, align = N + E);

pair baloc = (0.1*br*Cos((btheta + bthetaprime)/2), 0.1*br*Cos((btheta + bthetaprime)/2));
Label balabel = Label("$45^{\circ}$", blue);
label(balabel, baloc, align = 3N + E);




// // :::: tilted lines
// real tiltdirx = 9;
// real tiltdiry = 5;
// pair tiltpoint = (tiltdirx, tiltdiry);
// path tiltx = orig -- tiltpoint;
// draw(tiltx, arrow = axisarrow, p = RGB(200,0,0));

// path tilty = orig -- (-5, 9);
// draw(tilty, arrow = axisarrow, p = RGB(200,0,0));

// // :::: vertical line
// real xcoord = 3;
// path vertline = (xcoord,-4) -- (xcoord, 9);
// draw(vertline, p = Dotted());

// // :::: dots on vertical line
// int y1 = 4;
// int y2 = 6;
// pair P = (xcoord, y1);
// dot("$P$", P);
// pair Q = (xcoord, y2);
// dot("$Q$", Q);

// // :::: lines perpendicular to tilted lines
// pair perpdir = (-tiltdiry, tiltdirx);
// pair pprime = extension(orig, tiltpoint, P, P + perpdir);
// dot("$P'$", pprime);
// pair qprime = extension(orig, tiltpoint, Q, Q + perpdir);
// dot("$Q'$", qprime);

// draw(P--pprime, Dotted() + red);
// draw(Q--qprime, Dotted() + red);





// // :::: arc
// real rarc   = 5;
// real xstart = 4.75;
// real ystart = sqrt(rarc*rarc - xstart*xstart);
  
// path uarc = arc( c  = (0,0),
// 		 z1 = (xstart, -ystart),
// 		 z2 = (-ystart, xstart));
// draw(uarc, Dotted());

// // :::: triangle
// path rtriangle = (0,0) -- (4,0) -- (4,3) -- cycle;
// draw(rtriangle, linewidth(3));

// Label alabel = Label("A", position = BeginPoint, p = RGB(200, 0,0));
// label(alabel, rtriangle, align = 2N + 5E);

// Label blabel = Label("B", position = Relative(0.3333), p = RGB(200,0,0));
// label(blabel, rtriangle, align = N + W);

// Label clabel = Label("C", position = Relative(7/12), p = RGB(200,0,0));
// label(clabel, rtriangle, align = 4S + 2W);

// Label calabel = Label("len(AC) = 5", position = Relative(9.5/12));
// label(calabel, rtriangle);

// Label ablabel = Label("len(AB) = 4", position = Relative(2/12));
// label(ablabel, rtriangle, align = N);

// Label bclabel = Label("len(BC) = 3", position = Relative(5.5/12));
// label(bclabel, rtriangle, align = E);

