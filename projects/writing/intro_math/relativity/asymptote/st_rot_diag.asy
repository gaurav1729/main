// :::: settings
import math;
settings.outformat = "pdf";
defaultpen(fontsize(20pt));
size(12cm);
arrowbar axisarrow = Arrow(TeXHead);
arrowbar axisarrow = Arrow;

pair orig = (0,0);
int xmin = -1;
int xmax =  6;

int ymin = -1;
int ymax =  6;

// :::: axes
// lines
path pxaxis = (xmin, 0) -- (xmax, 0);
draw(pxaxis, arrow = axisarrow);

path pyaxis = (0, ymin) -- (0, ymax);
draw(pyaxis, arrow = axisarrow);

// origin
dot("$O$", orig, align = W + S);

// points
// x axis
for (int i = xmin + 1; i <= (xmax-1); ++i){
  if (i<=0){continue;}
  dot((i,0));
  Label L = Label((string) i);
  label(L, (i,0), align = (0, -5));
}
label("$x$", (xmax, 0), align = E);

// y axis
for (int i = ymin + 1; i <= (ymax-1); ++i){
  if (i<=0){continue;}
  dot((0,i));
  Label L = Label((string) i);
  label(L, (0,i), align = (-5, 0));
}
label("$y$", (0, ymax), align = N);

// :::: initial dots
real ax = 3;
real ay = 1;
pair a = (ax, ay);


real dtheta = 45;
real atheta = aTan(ay/ax);
real athetaprime = atheta + dtheta;

real ar = sqrt(ax*ax + ay*ay);

real axprime = ar*Cos(athetaprime);
real ayprime = ar*Sin(athetaprime);

pair aprime = (axprime, ayprime);

// :::: arcs
path aarc = arc(orig, ar, atheta, athetaprime);
 
draw(aarc, red, arrow = axisarrow);

dot(a, red, L = "$A = (x,y)$");
dot(aprime, red, L = "$A' = (x',y')$", align = N);

// // :::: altitudes and labels
// draw((axprime, ayprime) -- (axprime, 0), red);
// draw((ax     , ay     ) -- (ax     , 0), red);

// Label lax = Label("$x_a$", red);
// label(lax, (ax, 0), align = S);

// Label laxprime = Label("$x'_a$", red);
// label(laxprime, (axprime, 0), align = S);


// :::: angles
path paangle = aprime -- orig -- a;
draw(paangle, Dotted() + red);

path asarc = arc(orig, 0.1*ar, atheta, athetaprime);
 
draw(asarc, red);

// :::: angle labels
pair aaloc = (0.1*ar*Cos((atheta + athetaprime)/2), 0.1*ar*Cos((atheta + athetaprime)/2));
Label aalabel = Label("$\theta^{\circ}$", red);
label(aalabel, aaloc, align = N + E);


