// :::: settings
import math;
import graph;

settings.outformat = "pdf";
defaultpen(fontsize(20pt));
size(20cm);
arrowbar axisarrow = Arrow(TeXHead);
arrowbar axisarrow = Arrow;

pair orig = (0,0);

// :::: hyperbola functions
pair thbola1(real t){
  real y = cosh(t);
  real x = sinh(t);
  return (x,y);
}
pair thbola2(real t){
  real y = 2*cosh(t);
  real x = 2*sinh(t);
  return (x,y);
}


// :::: axes
int xmin = -2;
int xmax =  4;

int ymin = -2;
int ymax =  4;

// lines
path pxaxis = (xmin, 0) -- (xmax, 0);
draw(pxaxis, arrow = axisarrow);

path pyaxis = (0, ymin) -- (0, ymax);
draw(pyaxis, arrow = axisarrow);

// origin
dot("$O$", orig, align = W + S);

// axis points
// x axis
for (int i = xmin + 1; i <= (xmax-1); ++i){
  if (i==0){continue;}
  dot((i,0));
  Label L = Label((string) i);
  label(L, (i,0), align = (0, -5));
}
label("$x$", (xmax, 0), align = E);

// y axis
for (int i = ymin + 1; i <= (ymax-1); ++i){
  if (i==0){continue;}
  dot((0,i));
  Label L = Label((string) i);
  label(L, (0,i), align = (-5, 0));
}
label("$y$", (0, ymax), align = N);


// hyperbolas
path uhbola1 = graph(thbola1, -2, 2);
draw(uhbola1, blue);
path uhbola2 = graph(thbola2, -1.5, 1.5);
draw(uhbola2, red);

// vertical line
path vertl = (1, ymin) -- (1, ymax);
draw(vertl, Dotted());

// intersections
pair [] ps = intersectionpoints(uhbola1, vertl);
pair p = ps[0];
dot(p, L = "$P$", linewidth(8) + blue);

pair [] qs = intersectionpoints(uhbola2, vertl);
pair q = qs[0];
dot(q, L = "$Q$", linewidth(8) + red);

// find the celerity (?) and the new points
real delta = 0.5;
real cp = atanh(p.x/p.y);
pair pprime = (sinh(cp + delta), cosh(cp + delta));
dot(pprime, L = "$P'$", linewidth(8) + blue);

real cq = atanh(q.x/q.y);
pair qprime = (2*sinh(cq + delta), 2*cosh(cq + delta));
dot(qprime, L = "$Q'$", linewidth(8) + red);

// drop the altitudes
path palt = pprime -- (pprime.x, 0);
draw(palt, Dotted() + blue);

path qalt = qprime -- (qprime.x, 0);
draw(qalt, Dotted() + red);

dot((pprime.x, 0), linewidth(8) + blue);
dot((qprime.x, 0), linewidth(8) + red);


// // :::: tilted lines
// real tiltdirx = 9;
// real tiltdiry = 5;
// pair tiltpoint = (tiltdirx, tiltdiry);
// path tiltx = orig -- tiltpoint;
// draw(tiltx, arrow = axisarrow, p = RGB(200,0,0));

// path tilty = orig -- (-5, 9);
// draw(tilty, arrow = axisarrow, p = RGB(200,0,0));

// // :::: vertical line
// real xcoord = 3;
// path vertline = (xcoord,-4) -- (xcoord, 9);
// draw(vertline, p = Dotted());

// // :::: dots on vertical line
// int y1 = 4;
// int y2 = 6;
// pair P = (xcoord, y1);
// dot("$P$", P);
// pair Q = (xcoord, y2);
// dot("$Q$", Q);

// // :::: lines perpendicular to tilted lines
// pair perpdir = (-tiltdiry, tiltdirx);
// pair pprime = extension(orig, tiltpoint, P, P + perpdir);
// dot("$P'$", pprime);
// pair qprime = extension(orig, tiltpoint, Q, Q + perpdir);
// dot("$Q'$", qprime);

// draw(P--pprime, Dotted() + red);
// draw(Q--qprime, Dotted() + red);





// // :::: arc
// real rarc   = 5;
// real xstart = 4.75;
// real ystart = sqrt(rarc*rarc - xstart*xstart);
  
// path uarc = arc( c  = (0,0),
// 		 z1 = (xstart, -ystart),
// 		 z2 = (-ystart, xstart));
// draw(uarc, Dotted());

// // :::: triangle
// path rtriangle = (0,0) -- (4,0) -- (4,3) -- cycle;
// draw(rtriangle, linewidth(3));

// Label alabel = Label("A", position = BeginPoint, p = RGB(200, 0,0));
// label(alabel, rtriangle, align = 2N + 5E);

// Label blabel = Label("B", position = Relative(0.3333), p = RGB(200,0,0));
// label(blabel, rtriangle, align = N + W);

// Label clabel = Label("C", position = Relative(7/12), p = RGB(200,0,0));
// label(clabel, rtriangle, align = 4S + 2W);

// Label calabel = Label("len(AC) = 5", position = Relative(9.5/12));
// label(calabel, rtriangle);

// Label ablabel = Label("len(AB) = 4", position = Relative(2/12));
// label(ablabel, rtriangle, align = N);

// Label bclabel = Label("len(BC) = 3", position = Relative(5.5/12));
// label(bclabel, rtriangle, align = E);

