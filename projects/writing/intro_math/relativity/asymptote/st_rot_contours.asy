// :::: settings
import math;
import graph;

settings.outformat = "pdf";
defaultpen(fontsize(20pt));
size(20cm);
arrowbar axisarrow = Arrow(TeXHead);
arrowbar axisarrow = Arrow;

pair orig = (0,0);

// :::: hyperbola function
pair hbola(real t){
  real x = cosh(t);
  real y = sinh(t);
  // label((string) x, (1,1));
  // label((string) y, (1,2));
  // label((string) t, (1,3));
  return (x,y);
}


// :::: axes
int xmin = -4;
int xmax =  4;

int ymin = -4;
int ymax =  4;

// lines
path pxaxis = (xmin, 0) -- (xmax, 0);
draw(pxaxis, arrow = axisarrow);

path pyaxis = (0, ymin) -- (0, ymax);
draw(pyaxis, arrow = axisarrow);

// origin
dot("$O$", orig, align = W + S);

// axis points
// x axis
for (int i = xmin + 1; i <= (xmax-1); ++i){
  if (i==0){continue;}
  dot((i,0));
  Label L = Label((string) i);
  label(L, (i,0), align = (0, -5));
}
label("$x$", (xmax, 0), align = E);

// y axis
for (int i = ymin + 1; i <= (ymax-1); ++i){
  if (i==0){continue;}
  dot((0,i));
  Label L = Label((string) i);
  label(L, (0,i), align = (-5, 0));
}
label("$y$", (0, ymax), align = N);


path c1 = circle(orig, 1);
draw(c1, Dotted() + blue);
path c2 = circle(orig, 2);
draw(c2, Dotted() + blue);
path c3 = circle(orig, 2.5);
draw(c3, Dotted() + blue);




// path uhbola = graph(hbola, -1, 1.5);
// draw(uhbola);

// dot(hbola(0), L = "$P$", p = linewidth(5)+ red);
// dot(hbola(1), L = "$P'$", p = linewidth(5)+ red);

// path uhbola = graph(hbola, 0, 1);
// draw(uhbola, linewidth(2) + red, arrow = axisarrow);



// // :::: tilted lines
// real tiltdirx = 9;
// real tiltdiry = 5;
// pair tiltpoint = (tiltdirx, tiltdiry);
// path tiltx = orig -- tiltpoint;
// draw(tiltx, arrow = axisarrow, p = RGB(200,0,0));

// path tilty = orig -- (-5, 9);
// draw(tilty, arrow = axisarrow, p = RGB(200,0,0));

// // :::: vertical line
// real xcoord = 3;
// path vertline = (xcoord,-4) -- (xcoord, 9);
// draw(vertline, p = Dotted());

// // :::: dots on vertical line
// int y1 = 4;
// int y2 = 6;
// pair P = (xcoord, y1);
// dot("$P$", P);
// pair Q = (xcoord, y2);
// dot("$Q$", Q);

// // :::: lines perpendicular to tilted lines
// pair perpdir = (-tiltdiry, tiltdirx);
// pair pprime = extension(orig, tiltpoint, P, P + perpdir);
// dot("$P'$", pprime);
// pair qprime = extension(orig, tiltpoint, Q, Q + perpdir);
// dot("$Q'$", qprime);

// draw(P--pprime, Dotted() + red);
// draw(Q--qprime, Dotted() + red);





// // :::: arc
// real rarc   = 5;
// real xstart = 4.75;
// real ystart = sqrt(rarc*rarc - xstart*xstart);
  
// path uarc = arc( c  = (0,0),
// 		 z1 = (xstart, -ystart),
// 		 z2 = (-ystart, xstart));
// draw(uarc, Dotted());

// // :::: triangle
// path rtriangle = (0,0) -- (4,0) -- (4,3) -- cycle;
// draw(rtriangle, linewidth(3));

// Label alabel = Label("A", position = BeginPoint, p = RGB(200, 0,0));
// label(alabel, rtriangle, align = 2N + 5E);

// Label blabel = Label("B", position = Relative(0.3333), p = RGB(200,0,0));
// label(blabel, rtriangle, align = N + W);

// Label clabel = Label("C", position = Relative(7/12), p = RGB(200,0,0));
// label(clabel, rtriangle, align = 4S + 2W);

// Label calabel = Label("len(AC) = 5", position = Relative(9.5/12));
// label(calabel, rtriangle);

// Label ablabel = Label("len(AB) = 4", position = Relative(2/12));
// label(ablabel, rtriangle, align = N);

// Label bclabel = Label("len(BC) = 3", position = Relative(5.5/12));
// label(bclabel, rtriangle, align = E);

