// :::: settings
settings.outformat = "pdf";
defaultpen(fontsize(20pt));
size(20cm);
arrowbar axisarrow = Arrow(TeXHead);
arrowbar axisarrow = Arrow;

int xmin = -2;
int xmax = 6;

int ymin = -2;
int ymax = 6;

// :::: axes
// lines
path pxaxis = (xmin, 0) -- (xmax, 0);
draw(pxaxis, arrow = axisarrow);

path pyaxis = (0, ymin) -- (0, ymax);
draw(pyaxis, arrow = axisarrow);

// points
// x axis
for (int i = xmin + 1; i <= (xmax-1); ++i){
  if (i<=0){continue;}
  dot((i,0));
  Label L = Label((string) i);
  label(L, (i,0), align = (0, -5));
}
label("$x$", (xmax, 0), align = E);

// y axis
for (int i = ymin + 1; i <= (ymax-1); ++i){
  if (i<=0){continue;}
  dot((0,i));
  Label L = Label((string) i);
  label(L, (0,i), align = (-5, 0));
}
label("$y$", (0, ymax), align = N);

// :::: arc
real rarc   = 5;
real xstart = 4.75;
real ystart = sqrt(rarc*rarc - xstart*xstart);
  
path uarc = arc( c  = (0,0),
		 z1 = (xstart, -ystart),
		 z2 = (-ystart, xstart));
draw(uarc, Dotted());

// :::: triangle
path rtriangle = (0,0) -- (4,0) -- (4,3) -- cycle;
draw(rtriangle, linewidth(3));

Label alabel = Label("A", position = BeginPoint, p = RGB(200, 0,0));
label(alabel, rtriangle, align = 2N + 5E);

Label blabel = Label("B", position = Relative(0.3333), p = RGB(200,0,0));
label(blabel, rtriangle, align = N + W);

Label clabel = Label("C", position = Relative(7/12), p = RGB(200,0,0));
label(clabel, rtriangle, align = 4S + 2W);

Label calabel = Label("len(AC) = 5", position = Relative(9.5/12));
label(calabel, rtriangle);

Label ablabel = Label("len(AB) = 4", position = Relative(2/12));
label(ablabel, rtriangle, align = N);

Label bclabel = Label("len(BC) = 3", position = Relative(5.5/12));
label(bclabel, rtriangle, align = E);

