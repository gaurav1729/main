"""Find all ways that you can have a 6 numbers from {1,2,3} sum up to 10.
Initially, consider them distinguishable.  Later, we will look at bosons.
"""
from collections import Counter
req_sum = 23


all_parts = [ (i0, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12)
                  for i0 in range(1,4)
                  for i1 in range(1,4)
                  for i2 in range(1,4)
                  for i3 in range(1,4)
                  for i4 in range(1,4)
                  for i5 in range(1,4)
                  for i6 in range(1,4)
                  for i7 in range(1,4)
                  for i8 in range(1,4)
                  for i9 in range(1,4)
                  for i10 in range(1,4)
                  for i11 in range(1,4)
                  for i12 in range(1,4)
]

              
some_parts = [x for x in all_parts if sum(x) == req_sum]

distribs = [((sum([t==1 for t in x])), (sum([t==2 for t in x])), (sum([t==3 for t in x])))
           for x in some_parts]
distrib_types = dict([(x, distribs.count(x)) for x in set(distribs)])
print(distrib_types)

def partition_string(inTuple, i):
    sum_tup, dist_tup = inTuple
    out_str = f'{i+1} & ' + ' + '.join([str(x) for x in sum_tup]) + f' = {sum(sum_tup) } & '
    out_str += f'   {dist_tup[0]} & {dist_tup[1]} & {dist_tup[2]}'
    out_str += "\\\\ \\hline"
    return out_str
    
part_strs = [partition_string(x,i ) for i, x in enumerate(zip(some_parts, distribs))]
