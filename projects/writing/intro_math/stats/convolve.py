start_arr = [1,1]
normalize = True
out_exps  = [0, 1, 4, 7]

import numpy as np
from matplotlib import pyplot as plt

start_arr = np.array(start_arr)
out_dict  = dict()

if normalize:
    start_arr = start_arr/sum(start_arr)

it_arr = start_arr


##gsdebug: scale them to have the same mean (0) and std dev
for itnum in range(1, max(out_exps)+ 1):
    it_arr = np.convolve(it_arr, it_arr)
    ## save to a dictionary
    if itnum in out_exps or itnum == 0:
        ## numbers to scale the plots to have height and width
        max_ind = len(it_arr)
        ave_ind = (max_ind-1)/2
        it_range = max(it_arr) - min(it_arr)
        min_val = min(it_arr)
        max_val = max(it_arr)
        width   = ave_ind - min([i for i in range(max_ind)
                                 if it_arr[i] > max_val/2 ])
        if width == 0:
            width = 1
        
        
        ## scaling
        out_dict[itnum]        = dict()
        #out_dict[itnum]['arr'] = [(x-min_val)/it_range for x in it_arr]
        out_dict[itnum]['arr'] = [x/max_val for x in it_arr]
        out_dict[itnum]['ind'] = [(x - ave_ind)/width for x in range(max_ind)]

out_dict[0] = dict()
out_dict[0]['arr'] = (1, 1)
out_dict[0]['ind'] = (-1, 1)
## plot
f = plt.figure()

# itnum = 0
# scale_fac = 1.0
# inds = out_dict[itnum]['ind']
# vals = out_dict[itnum]['arr']
# inds = [x*scale_fac for x in inds]
# vals = [x*scale_fac for x in vals]
# width = (inds[1] - inds[0])*0.9
# plt.bar(inds, vals, width, fill = True)

itnum = 1
scale_fac = 1.0
inds = out_dict[itnum]['ind']
vals = out_dict[itnum]['arr']
##inds = [x*scale_fac for x in inds]
vals = [x*scale_fac for x in vals]
width = (inds[1] - inds[0])*0.9
h1 = plt.bar(inds, vals, width, fill = True, color="yellow", label = "Row number 3")

print(itnum)
print(len(inds))
print("_____")

itnum = 4
scale_fac = 0.9
inds = out_dict[itnum]['ind']
vals = out_dict[itnum]['arr']
inds = [x*scale_fac for x in inds]
##vals = [x*scale_fac for x in vals]
width = (inds[1] - inds[0])*0.9
h4 = plt.bar(inds, vals, width, fill = False, linewidth=3, label = "Row number 17")

print(itnum)
print(len(inds))
print("_____")


itnum = 7
inds = out_dict[itnum]['ind']
vals = out_dict[itnum]['arr']
h7 = plt.plot(inds, vals, 'g-o', label = "Row number 129")


print(itnum)
print(len(inds))
print("_____")


plt.legend()
#f.legend([h1, h4, h7])
plt.title("Plot of the 3rd, 17th, 129th rows of Pascal's Triangle")
plt.xlim(-3, 3)
plt.show()

