Reunion

You're back in your hostel -- in the lounge, of course, watching tv, of course (Chhaya Geet?  A Test match?).  You're sitting on one of those wicker chairs -- remember how the mosquitoes could creep up from behind and bite you in the butt?  Shift your gaze a little to the right from the television, along the shelves.  Do you see that locked little drawer?  Keep your focus on that for a little while, please.

One year, we realized that that locked drawer was, well, locked.  And that it had been locked ever since anyone could remember.  We broke the lock, and out of the vault came: a tape recorder and radio boombox.  All reunion long, this memory kept annoying me -- itching like a mosquito bite on the butt.  And it took a little while, a little scratching,  to figure out why this memory was so persistent.

Who were these people who left it in there, locked it up, and walked away?  What were they thinking?  We don't know anything, except that they did indeed do it and then vanished.  This was their Stonehenge, their Easter Island.

What about the kids in the hostels today?  What if we had done that, what would they be thinking when they saw the artefact?  Actually, that's an easy question to answer.  They'd be saying, what the fuck is is a "tape recorder", because I've spent the last three hours trying to get it to talk to the wifi.

We are the ghosts who walk their corridors.  But it's more than that.  For too long, we've been ghosts to each other too.  Not in touch; or even if we have, we've been nothing more than the tiny tug on an electron in a web server in Lithuania, sending a pulse to a monitor a thousand kilometres away.  We're more than that, we're flesh and blood, perhaps flesh and blood and a little bit of alcohol for those few days.  The reunion is our reminder of this.

But it's more than just that, too.  As we see it reflected in our friends' eyes, it is a reminder to ourselves most of all of our own existence as full human beings.  We forget that in our routine; and we don't even know we forget it.  We need our own reminders.

But what of the kids we met, today's kids, who only knew us as their ghosts?  What do they think of us now they've seen us in person?  That's another easy answer.  The  bastards think we're old; and they're right.  Wallace Stevens said that death is the mother of beauty.  Nothing would matter if not for the prospect of its loss.  The reminder of our flesh and blood reality is our opportunity to look around with sharper eyes at the wonder around us.
