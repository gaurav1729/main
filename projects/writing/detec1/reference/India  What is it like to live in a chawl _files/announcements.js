define(['jquery','libs/jquery-plugins/jquery.cookie'], function($, unused) {
  var mod = {
    closeBanner: function(bannerName, cookieName) {
      $.cookie(cookieName, 99,
               { expires: 720, domain: 'slate.com', path: '/' });
      $("#" + bannerName + "_announcement").slideUp();      
    },
    showBanner: function(bannerName, cookieName,
                         announcementText, maxDisplay) {
      var cookieVal = parseInt($.cookie(cookieName),10);
      var showIt = false;
      var closeFunctionName;
      
      if (isNaN(cookieVal)) {
        showIt = true;
        $.cookie(cookieName, 1,
                 { expires: 720, domain: 'slate.com', path: '/' });
      } else if (cookieVal < maxDisplay) {
        showIt = true;
        $.cookie(cookieName, (cookieVal+1),
                 { expires: 720, domain: 'slate.com', path: '/' });
      } else {
        showIt = false;
      }

      if (showIt) {
        $('<header id="'+bannerName+'_announcement" class="notification">'
          + announcementText
          + '<a href="javascript:void(0)" class="close">X</a>'
          + '</header>').hide().prependTo('body').slideDown('slow');
        closeFunctionName = "close" + bannerName[0].toUpperCase() + bannerName.substr(1) + "Banner";
        $("#"+bannerName+"_announcement .close").click(mod[closeFunctionName]);

      }

      return showIt;
    },
    deleteOldCookies: function() {
      var oldCookies = [
        "betaIntro",
        "overlayShown",
        "redesignIntro",
        "Origin_Type",
        "persistTreatment"
      ];
      for (c in oldCookies) {
        $.removeCookie(oldCookies[c], { domain: 'slate.com', path: '/' });
      }
    },
    closePolicyBanner: function() {
      mod.closeBanner("policy", "policyChange");
      return false;
    },
    showPolicyBanner: function() {
      var announcementText;

      announcementText = 'On February 28, 2014, <strong><em>Slate</em></strong> '
        + 'published an updated version of its <a target="_blank" '
        + 'href="/articles/briefing/slate_user_agreement_and_privacy_policy/2012/12/slate_s_privacy_policy.html">Privacy '
        + 'Policy</a> and <a target="_blank" '
        + 'href="/articles/briefing/slate_user_agreement_and_privacy_policy/2012/12/slate_s_terms_of_service.html">Terms '
        + 'of Service</a>.  By continuing to use '
        + '<strong><em>Slate</em></strong>&rsquo;s online properties, you '
        + 'agree to these revised documents and agree that they may apply to '
        + 'information that has already been collected by '
        + '<strong><em>Slate</em></strong>.';

      return mod.showBanner("policy", "policyChange", announcementText, 2);
    },
    openOverlay: function(force) {
      var cookieName = "overlayShown";
      var alreadyShown = $.cookie(cookieName);
      var showOverlay = false;
      var overlayPath;

      if (force != undefined ||
          alreadyShown != "true" ||
          window.location.search.indexOf("showDocent=t") != -1) {
        showOverlay = true;
      }
      if (!showOverlay || mobileMode()) {
        return false;
      }

      var overlayHTML;
      var imageWidth, imageHeight;
      var browserWidth = $('body').width();
      var leftOffset, backgroundSize;
      if (tabletMode()) {
        if (Modernizr.mq('(orientation: portrait)')) {
          overlayPath = 'http://www.slate.com/features/docent/SlateHP-tablet-portrait.jpg';
        } else {
          overlayPath = 'http://www.slate.com/features/docent/SlateHP-tablet-landscape.jpg';
        }
        leftOffset = "0";
        backgroundSize = "background-size: 100%;";

      } else {
        overlayPath = 'http://www.slate.com/features/docent/SlateHP-desktop.jpg';
        imageWidth = 1440;
        if (browserWidth < imageWidth) {
          leftOffset = (imageWidth - browserWidth)/2;
          leftOffset = "-" + leftOffset + "px";
        } else {
          leftOffset = "0";
        }
        if ($('body').width() > 1440) {
          backgroundSize = "background-size: 100% 100%;";
        } else {
          backgroundSize = "";
        }
            
      }

      overlayHTML = '<style type="text/css">#redesign_overlay {'
        + 'position:absolute;top:0;left:0;'
        + 'width:100%;height:100%;z-index:9999;'
        + 'background-position:'+leftOffset+' 0;'
        + backgroundSize
        + 'background-image:url("'+overlayPath+'")}</style> '
        + '<div id="redesign_overlay"></div>';
      $('body').prepend(overlayHTML);

      $('#redesign_overlay').click(function() {
        $("#redesign_overlay").hide();
        return false;
      });

      $.cookie(cookieName, "true",
               { expires: 3650, domain: 'slate.com', path: '/' });
      return true;
    },
    closeOverlay: function() {
      console.log('closing overlay');
    },
    isHomePage: function() {
      var requestPath = window.location.pathname;
      var debug = false;
      var isHP = false;

      if (window.location.search.indexOf("showDocent=t") != -1) {
        debug = true;
      }

      if (debug || requestPath == "/" ||
          requestPath == "/index.html" ||
          requestPath == "/content/slate/index.html") {
        isHP = true;
      }
      return isHP;
    },
    showRedesignBanner: function() {
      var announcementText;
      var showOverlay = false;
      var result;
      var articleLink = '/articles/briefing/slate_fare/2013/09/slate_s_redesign_what_we_changed_and_why.html';

      showOverlay = mod.isHomePage() && !mobileMode();

      if (showOverlay) {
        announcementText = 'Welcome to <strong><em>Slate</em></strong>\'s redesigned site! '
          + '<a href="#" class="redesign_tour">Take a quick tour</a>. '
          + '<a target="_blank" href="'+articleLink+'">Learn more and send your feedback</a>.';
      } else {
        announcementText = 'Welcome to <strong><em>Slate</em></strong>\'s redesigned site! '
          + '<a target="_blank" href="'+articleLink+'">Learn more and send your feedback</a>.';
      }

      if (mobileMode()) {
        announcementText = announcementText.replace('! ', '<br>');
      }

      result = mod.showBanner("redesign", "redesignIntro", announcementText, 20);

      if (showOverlay) {
        $("#redesign_announcement .redesign_tour").click(function() {
          mod.openOverlay("t");
          return false;
        });
      }

      return result ;
    },
    closeRedesignBanner: function() {
      mod.closeBanner("redesign", "redesignIntro");
      return false;
    },
    showRedesignIntro: function() {
      mod.showRedesignBanner();
    }
  };
  return mod;
});
