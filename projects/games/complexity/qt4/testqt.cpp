/*
1. Push button
2. Push button of fixed shape
3. Push button that changes colours

4. grid of push buttons
5. grid of push buttons with other buttons

class of hex:
  occupied (white, black, none)
  is_target
  neighbours
  position
  index (may not be part of the class)
  edges_drawn

class of grid:
  takes in size of grid, percentage filled
  collection of hexes
 */

#include <QtGui>

int main(int argv, char **args)
{
  QApplication app(argv, args);
  
  QTextEdit textEdit;
  QPushButton quitButton("Dammit I quit");

  QObject::connect(&quitButton, SIGNAL(clicked()),
		   qApp, SLOT(quit()));

  QVBoxLayout layout;
  layout.addWidget(&textEdit);
  layout.addWidget(&quitButton);
  QWidget window;
  window.setLayout(&layout);
  window.show();

  return app.exec();
} // int main(int argv, char **args)
