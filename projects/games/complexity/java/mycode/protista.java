// Code written by me to get the game working.  

import java.awt.*;
import javax.swing.*;
import java.awt.event.*; 


public class protista {

    //:::::::: class variables
    // gsdebug -- need to read from the command line, make this
    //  get initialized in constructor (with defaults)
    private static final int WIDTH = 5; // num cells
    private static final int LENGTH = 7; // num cells
    private static final int CELL_SIZE = 40; // pixels
    private static final int NVERTICES = 6; // hexagon
    // frac cells populated initially
    private static final float DENSITY = (float) 0.35;
    
    Grid grid;

    
    //::::::::

    //MAIN
    public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
		    new protista();
		}}); // new runnable
    } // main

    //::::::::

    //CONSTRUCTOR
    private protista() {
	initGame();
	runGui();
    } // private protista

    //::::::::

    private void initGame(){
	grid = new Grid(WIDTH, LENGTH, DENSITY);
    } // initialize

    //::::::::

    
    private void runGui() {
    } //rungui
    
} // public class protista

//:::::::::::::
class Cell {
    // This class represents one cell
    
    
} // public class cell

//:::::::::::::

class Grid {
    private 
} //class Grid

