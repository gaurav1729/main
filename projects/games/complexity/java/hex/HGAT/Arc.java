/* 
   Hexagonal Grid Algorithm Testbed source file 
   (c) Copyright 1996 Clark Verbrugge (clump@cs.mcgill.ca)
   This code may be freely distributed and modified. 
   */

import java.awt.*;

// Arcs are the basis for deciding what is visible.  Each bubble is 
// subdivided into arcs, where every hexagon in an arc is at least
// partially visible.  Arcs must be less than pi degrees.
public class Arc {
   public int hs;            // starting hexagon
   public double cx,cy;      // clockwise-most arm 
   public int he;            // ending hexagon
   public double ccx,ccy;    // counter-clockwise-most arm 
   public Arc next;          // for linked lists
   
   // returns the square of the distance between the two points
   public static double euclideanDistance2(double x1,double y1,double x2,double y2) {
      return ((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
   }
   
   // for copying arcs
   public Arc(Arc a) { 
      this(a.hs,a.cx,a.cy,a.he,a.ccx,a.ccy);
   }
   public Arc(int nhs,double ncx,double ncy,int nhe,double nccx,double nccy) {
      hs = nhs; cx = ncx; cy = ncy;
      he = nhe; ccx = nccx; ccy = nccy;
      next = null;
   }
   
   public String toString() {
      return ("Arc:[" + hs + "," + he + "]");
   }
   
   // Contracts the clockwise arm of the arc to exclude the given hexagon 
   // Note that left & right are reversed because our y-coordinates
   // increase downward
   public void contractC(Hexagon hc,Hexagon h) {
      int i;
      double bestcx,bestcy;
      bestcx = cx; bestcy = cy;
      for (i=0;i<6;i++) {
         if (Hexagon.turns(hc.cx,hc.cy,bestcx,bestcy,h.x[i],h.y[i])==Hexagon.RIGHT) {
            bestcx = h.x[i];
            bestcy = h.y[i];
         }
      }
      cx = bestcx;
      cy = bestcy;
   }
   
   // Contracts the counter-clockwise arm of the arc to exclude the given hexagon.
   // Again, left & right are reversed.
   public void contractCC(Hexagon hc,Hexagon h) {
      int i;
      double bestccx,bestccy;
      bestccx = ccx; bestccy = ccy;
      for (i=0;i<6;i++) {
         if (Hexagon.turns(hc.cx,hc.cy,bestccx,bestccy,h.x[i],h.y[i])==Hexagon.LEFT) {
            bestccx = h.x[i];
            bestccy = h.y[i];
         }
      }
      ccx = bestccx;
      ccy = bestccy;
   }
   
   // returns true if the arc can contain nothing.  To handle numerical
   // imprecision/error, this also rejects very thin arcs.
   public boolean emptyArc(HexGrid hg) {
      if (hs>he || Hexagon.turns(hg.hc.cx,hg.hc.cy,cx,cy,ccx,ccy)!=Hexagon.RIGHT)
         return true;
      // to deal with numerical error, we should also purge any arcs
      // which are very small (angle).  The code below purges arcs
      // with interior angles of less than about 0.01 degrees.  This doesn't
      // have to be done, and the below calculation is probably not
      // the best way to do it anyway, but it looks better with it.
      double cosA,b2,c2,a2,d;
      b2 = euclideanDistance2(hg.hc.cx,hg.hc.cy,cx,cy);
      c2 = euclideanDistance2(hg.hc.cx,hg.hc.cy,ccx,ccy);
      a2 = euclideanDistance2(cx,cy,ccx,ccy);
      d = Math.sqrt(b2)*Math.sqrt(c2)*2;
      if (d==0) return true;    // degenerate case--an arm lies on the centre
      cosA = (b2+c2-a2)/d;
      if (cosA>0.99999998) return true;
      return false;
   }
   
   // breaks up the arc into a list of arcs according to any obstacles 
   // which may be contained in the hexes defined by the arc.
   public Arc breakArc(HexGrid hg,int r) {
      Hexagon h;
      int i;
      Arc a,anext;
      // first contract the Arc on the most-clockwise end
      while (hs<=he) {
         h = hg.hexOnCircle(r,hs);
         if (h==null || !(h.obstacle)) break;
         contractC(hg.hc,h);
         hs++;
      }
      // now contract on the most-counter-clockwise end
      while (he>=hs) {
         h = hg.hexOnCircle(r,he);
         if (h==null || !(h.obstacle)) break;
         contractCC(hg.hc,h);
         he--;
      }
      if (emptyArc(hg)) return null;   // no arc left!
      
      // now, the arc has been appropriately contracted on both ends.
      // We also have to run through interior points and split the arc
      // if we find obstacles.
      for (i = hs+1; i< he; i++) {  // note we only go through the interior
         h = hg.hexOnCircle(r,i);
         if (h!=null && h.obstacle) {
            // split arc into two pieces; the first is our original arc
            // and it stops just before this obstacle.  The second is the
            // is a newly allocated arc for the rest of the original arc.
            // We terminate this arc, and then call this method recursively
            // on the new arc, but only after contracting both away from
            // the obstacle separating them.  This may cause either to
            // become an empty arc.
            a = new Arc(i+1,h.x[0],h.y[0],he,ccx,ccy);
            he = i-1;
            a.contractC(hg.hc,h);
            contractCC(hg.hc,h);
            if (!a.emptyArc(hg)) {
               a.next = next;
               next = a;
               a.breakArc(hg,r);
            }
            break;
         }
      }
      if (emptyArc(hg)) return next;
      return this;
   }
   
   // given a HexGrid, this expands the current arc out one radius unit,
   // respecting the arc limits (r is presumed to be the existing radius)
   void expandArc(HexGrid hg,int r) {
      Hexagon h;
      int i,d,e,newhs,newhe;
      boolean istop;
      
      // we have to calculate the new indices of the next outer bubble.
      // to accomodate partially-visible hexagons, we have to verify the
      // starting and ending hexagon.
      // Note that we switch calls to leftOfArc and rightOfArc.  This is
      // because our y-coord increases downward
      d = hs/r;  // which of the 6 sides hs is on
      e = hs%r;  // where along the side hs is on
      if (e==0 && (d==0 || d==3)) // hack to ensure arcs are <= pi rads
         newhs = d*(r+1);
      else {
         if (e!=0)  // along a side, but not on a corner
            newhs = d*(r+1) + e+1;
         else
            newhs = d*(r+1)+1;
      }
      // found a starting point, now go backwards until we find a hex
      // that the clockwise arm intersects; this will be the first hex
      // index in the new arc.
      istop = (ccy<=hg.hc.cy && cy<=hg.hc.cy); // true if upper-half of circle
      if (istop) { // upper half-circle
         while (newhs>0) {
            h = hg.hexOnCircle(r+1,newhs-1);
            if (h==null) { // arc expanded beyond the grid
               h = hg.fakeHexOnCircle(r,hs);  // get a temporary invalid hex
            }
            if (!h.rightOfArc(hg.hc,cx,cy)) {
               break;
            }
            newhs--;
         }
      } else {  // lower half-circle
         while (newhs>(r+1)*3) {
            h = hg.hexOnCircle(r+1,newhs-1);
            if (h==null) { // arc expanded beyond the grid
               h = hg.fakeHexOnCircle(r,hs);
            }
            if (!h.rightOfArc(hg.hc,cx,cy)) break;
            newhs--;
         }
      }
      
      // now do the same sort of thing for the ending hexagon
      d = he/r;  // which of the 6 sides he is on
      e = he%r;  // where along the side he is on
      if (e==0 && (d==0 || d==3)) // hack to ensure arcs are <= pi rads
         newhe = d*(r+1);
      else {
         if (e!=0)  // along a side, but not on a corner
            newhe = d*(r+1) + e-1;
         else
            newhe = d*(r+1)-1;
      }
      // found a starting point, now go backwards until we find a hex
      // that the counter-clockwise arm intersects; this will be the last hex
      // index in the new arc.
      if (istop) { // upper half-circle
         while (newhe<(r+1)*3) {
            h = hg.hexOnCircle(r+1,newhe+1);
            if (h==null) { // arc expanded beyond the grid
               h = hg.fakeHexOnCircle(r,hs);
            }
            if (!h.leftOfArc(hg.hc,ccx,ccy)) break;
            newhe++;
         }
      } else {  // lower half-circle
         while (newhe<(r+1)*6) {
            h = hg.hexOnCircle(r+1,newhe+1);
            if (h==null) { // arc expanded beyond the grid
               h = hg.fakeHexOnCircle(r,hs);
            }
            if (!h.leftOfArc(hg.hc,ccx,ccy)) break;
            newhe++;
         }
      }
      hs = newhs;
      he = newhe;
   }
   
   // Marks the hexes for the arc assuming a radius of r.
   // This routine is only necessary for displaying this info
   // on the grid.
   public void markHexes(HexGrid hg,int r) {
      int i;
      Hexagon h;
      for (i=hs;i<=he;i++) {
         h = hg.hexOnCircle(r,i);
         if (h!=null) {
            h.mark = i;
            h.radius = r;
         }
      }
   }
   
   // paint routine to draw arcs.
   public void render(Graphics g,Hexagon hc,Arc anext) {
      int px[]=new int[4],py[]=new int[4];
      px[0] = (int)(hc.cx); 
      py[0] = (int)(hc.cy);
      px[1] = (int)(cx);
      py[1] = (int)(cy);
      px[2] = (int)(ccx);
      py[2] = (int)(ccy);
      px[3] = px[0]; py[3] = py[0];
      g.setColor(Color.white);
      g.setXORMode(Color.green);
      g.fillPolygon(px,py,4);
      g.setPaintMode();
      g.setColor(Color.blue);
      g.drawLine(px[0],py[0],px[1],py[1]); // c-arm
      g.drawLine(px[0],py[0],px[2],py[2]); // cc-arm
      g.setColor(Color.black);
   }
}
