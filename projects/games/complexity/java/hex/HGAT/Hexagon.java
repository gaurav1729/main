/* 
   Hexagonal Grid Algorithm Testbed source file 
   (c) Copyright 1996 Clark Verbrugge (clump@cs.mcgill.ca)
   This code may be freely distributed and modified. 
   */

import java.awt.*;

public class Hexagon {
   // geometric lengths for drawing hexagons
   public static final double H_SIDE = 16.0;  // length of a hex side
   public static final double H_WIDTH = (H_SIDE*1.7320508);  // horiz. width 
   public static final double H_WIDTH2 = (H_WIDTH/2.0);
   public static final double H_PEAK = (H_SIDE/2.0); 
   public static final double BORDER = (H_WIDTH+3.0);
   
   // kinds of hexagon marks.  Used when drawing.
   public static final int CENTRE = -1;
   public static final int NONE = -2;
   
   // return values of turns() method
   public static final int LEFT = 1;
   public static final int RIGHT = -1;
   public static final int STRAIGHT = 0;
   
   // returns one of the 3 above constants, depending on whether the
   // three vertices constitute a left turn or a right turn.
   public static final int turns(double x0,double y0,double x1,double y1,
                                 double x2,double y2) {
      double cross;
      cross = (x1-x0)*(y2-y0) - (x2-x0)*(y1-y0);
      return((cross>0.0) ? LEFT : ((cross==0.0) ? STRAIGHT : RIGHT));
   }
   
   // the next two are not necessary; they're just for debugging info
   public int radius;         // bubble radius
   public int mark;           // index in latest bubble
   
   public boolean obstacle;   // whether this hex blocks sight
   public boolean visible;    // whether this hex can be seen
   // orientation of the hexagon is with point-up, so vertices are at
   // 4 o'clock, 2 o'clock, 12 o'clock, etc.  The 0-vertex is at 4 o'clock,
   // and order is counter-clockwise.
   public double x[];
   public double y[];
   public double cx,cy;  // center of hexagon (average of vertices)
   
   // hexes are oriented as a rectangular patch, with y increasing "downward"
   public Coord h;     // coordinate position (hex-coords)
   
   // given array coordinates, this produces a hexagon for that location
   public Hexagon(int ax,int ay) { 
      double eks,why;
      
      x = new double[6];
      y = new double[6];
      
      if (ay%2!=0) eks = H_WIDTH2+BORDER;
      else eks = BORDER;
      why = BORDER+ay * (H_PEAK+H_SIDE)+H_PEAK;
      
      x[3] = eks + ax * H_WIDTH; y[3] = why;
      x[2] = x[3] + H_WIDTH2;    y[2] = y[3] - H_PEAK;
      x[1] = x[3] + H_WIDTH;     y[1] = y[3];
      x[0] = x[1];               y[0] = y[1] + H_SIDE;
      x[5] = x[2];               y[5] = y[3] + H_SIDE + H_PEAK;
      x[4] = x[3];               y[4] = y[0];
      cx = (x[0] + x[1] + x[2] + x[3] + x[4] + x[5]) / 6.0;
      cy = (y[0] + y[1] + y[2] + y[3] + y[4] + y[5]) / 6.0;
      
      h = new Coord(ax,ay,true);
      h.toHex();
      mark = NONE;
   }
   
   // returns true if this hexagon intersects the line passing through
   // the given two points.
   public boolean intersects(double x0,double y0,double x1,double y1) {
      int side1,i,j;
      side1 = turns(x0,y0,x1,y1,x[0],y[0]);
      if (side1==STRAIGHT) return true;
      for (i=1;i<6;i++) {
         j = turns(x0,y0,x1,y1,x[i],y[i]);
         if (j==STRAIGHT || j!=side1) return true;
      }
      return false;
   }
   
   // A routine to tell if a hexagon has any part of it is "left" of a given
   // arc boundary (clockwise arm of the cone)
   boolean leftOfArc(Hexagon hc,double acx,double acy) {
      int i,carm;
      for (i=0;i<6;i++) {
         carm = turns(hc.cx,hc.cy,acx,acy,x[i],y[i]);
         if (carm==LEFT) return true;
      }
      return false;
   }
   
   // A routine to tell if a hexagon has any part of it is "right" of a given
   // arc boundary (counter-clockwise arm of the cone)
   boolean rightOfArc(Hexagon hc,double accx,double accy) {
      int i,ccarm;
      for (i=0;i<6;i++) {
         ccarm = turns(hc.cx,hc.cy,accx,accy,x[i],y[i]);
         if (ccarm==RIGHT) return true;
      }
      return false;
   }
   
   // paint routine to draw this hexagon
   public void render(Graphics g) {
      int px[]=new int[7],py[] = new int[7];
      String s;
      for (int i=0;i<6;i++) {
         px[i] = (int)(x[i]);
         py[i] = (int)(y[i]);
      }
      px[6] = px[0]; py[6] = py[0];
      if (obstacle) {
         g.setColor(Color.white);
         g.fillPolygon(px,py,7);
         g.setXORMode(Color.darkGray);
         g.fillPolygon(px,py,7);
         g.setColor(Color.black);
         g.setPaintMode();
      } 
      g.drawPolygon(px,py,7);
      if (visible) {
         if (mark==CENTRE) s = "C";  // centre vertex is marked
         else if (mark>=0) s = radius + "." + mark;
         else s = "";
         if (obstacle)
            g.setColor(Color.lightGray);
         g.drawString(s,(int)(cx-H_WIDTH2/2.0),(int)(cy+H_PEAK/2.0));
      }
      g.setColor(Color.black);
   }
}

