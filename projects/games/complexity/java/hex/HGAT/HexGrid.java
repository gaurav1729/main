/* 
   Hexagonal Grid Algorithm Testbed source file 
   (c) Copyright 1996 Clark Verbrugge (clump@cs.mcgill.ca)
   This code may be freely distributed and modified. 
   */

import java.awt.*;

// An entire hexagonal grid
public class HexGrid { 
   public static int dirs[][] = {  // dx,dy for moving in diff. directions
      {0,-1}, {-1,-1}, {-1,0},      // in hex-space
      {0,1}, {1,1}, {1,0}
   };
   
   int w,h;              // dimensions of grid (in hexagons)
   Hexagon hc;           // centre hexagon (where LOS starts)
   Hexagon grid[];
   Arc alist;            // list of arcs
   
   public HexGrid(HexGrid hg) {  // for copying a grid
      this(hg.w,hg.h);
      int i,j;
      for (j=0;j<h;j++)
         for (i=0;i<w;i++)
            if (hg.grid[j*w+i].obstacle)
               grid[j*w+i].obstacle = true;
            else
               grid[j*w+i].obstacle = false;    
   }
   
   // Generates a new grid of the specified dimensions (in hexagons)
   public HexGrid(int nw,int nh) {
      w = nw;  h = nh;
      grid = new Hexagon[w*h];
      int i,j;
      for (j=0;j<h;j++)
         for (i=0;i<w;i++) {
            grid[j*w+i] = new Hexagon(i,j);
            if (((int)(Math.random()*6))==0)
               grid[j*w+i].obstacle = true;
         }
      int ctx = w/2;  // make our point-of-view the approximate centre
      int cty = h/2;
      hc = grid[cty*w+ctx];
      hc.visible = true;  // it's assumed we can see ourselves
      hc.obstacle = false;  // and if hc is obstacle, then can't see anything
      hc.mark = Hexagon.CENTRE;
      initialArcs();
   }
   
   // reconstructs the grid without allocating new hexagons
   public void remakeGrid() {
      int j,i;
      for (j=0;j<h;j++)
         for (i=0;i<w;i++) {
            if (((int)(Math.random()*6))==0)
               grid[j*w+i].obstacle = true;
            else
               grid[j*w+i].obstacle = false;
            grid[j*w+i].visible = false;
         }
      hc.visible = true;
      hc.obstacle = false;
      hc.mark = Hexagon.CENTRE;
      initialArcs();
   }
   
   // Arc setup.  We begin with 4 arcs corresponding to the four
   // quadrants of cartesian coordinates.  We could probably get
   // away with 3 and maybe 2, but why push it.
   private void initialArcs() {
      alist = new Arc(0,hc.x[0],hc.cy,1,hc.cx,hc.y[2]);
      alist.next = new Arc(2,hc.cx,hc.y[2],3,hc.x[3],hc.cy);
      alist.next.next = new Arc(3,hc.x[3],hc.cy,4,hc.cx,hc.y[5]);
      alist.next.next.next = new Arc(5,hc.cx,hc.y[5],6,hc.x[0],hc.cy);
      // mark the initial ring of arcs
      markArcs(1);
   }
   
   // given pixel coordinates, this returns a hexagon under those coords
   public Hexagon whichHex(int mx,int my) {
      int row,col,c;
      double hh = Hexagon.H_SIDE + Hexagon.H_PEAK;
      if (((double)my)<=hh) {
         row = 0;
         col = (int)(((double)mx)/Hexagon.H_WIDTH);
      } else {
         row = (int)(((double)my)/hh);  // either this row or the previous
         col = (int)(((double)mx)/Hexagon.H_WIDTH2); // number of half-hexes horizontally
         c = col/2;
         if (c%2==1) {
            if (col%2==1) {
               // OBVIOUSLY THIS IS NOT FINISHED!
            }
         }
      }
      return null;
   }
   
   // Hexagons of a bubble are ordered by counter-clockwise
   // indexing.  This returns the hexagon for the given index and radius.
   // Returns null if the hex is not within our rectangular grid.
   public Hexagon hexOnCircle(int r,int i) {
      int d,e;
      Coord c = new Coord(hc.h);
      c.toHex();
      d = (i/r)%6;  // find which side it's on
      e = i%r;
      c.x = c.x+r*(dirs[(d+4)%6][0]); // locate first hex on that side
      c.y = c.y+r*(dirs[(d+4)%6][1]);
      c.x += e*(dirs[d][0]);
      c.y += e*(dirs[d][1]);
      c.toArray();
      if (c.x>=0 && c.x<w && c.y>=0 && c.y<h)
         return grid[c.y*w+c.x];
      return null;
   }
   
   // When our bubble expands past the grid in some areas, but not others,
   // we will end up wanting to look at hexagons which are not in our grid.
   // This routine manufactures hexagons to order.
   public Hexagon fakeHexOnCircle(int r,int i) {
      int d,e;
      Coord c = new Coord(hc.h);
      c.toHex();
      d = (i/r)%6;  // find which side it's on
      e = i%r;
      c.x = c.x+r*(dirs[(d+4)%6][0]); // locate first hex on that side
      c.y = c.y+r*(dirs[(d+4)%6][1]);
      c.x += e*(dirs[d][0]);
      c.y += e*(dirs[d][1]);
      c.toArray();
      return new Hexagon(c.x,c.y);
   }
   
   // Assuming the current arcs are at radius r, this computes the
   // next set of arcs at radius r+1
   void nextArc(int r) {
      Arc a,ahead,atail,anext;
      Hexagon h;
      int i;
      
      // mark all hexagons in current arc as visible
      System.out.println("Expanding arcs...");
      System.out.print("Current radius " + r + " set: {");
      a = alist;
      while (a!=null) {
         System.out.print(a + ((a.next==null) ? "" : ","));
         for (i=a.hs; i<=a.he;i++) {
            h = hexOnCircle(r,i);
            if (h!=null) h.visible = true;
         }
         a = a.next;
      }
      markArcs(r);
      System.out.println("}");
      
      // split up arcs according to obstacles
      ahead = null;
      atail = null;
      a = alist;
      while (a!=null) {
         anext = a.next;
         a.next = null;
         System.out.print("Splitting " + a + " into {");
         a = a.breakArc(this,r);
         if (a==null) {
            // nothing visible here---need to merge adjacent arcs...
            System.out.print("<deleted>");
         } else {
            if (atail!=null) atail.next = a;
            if (ahead==null) ahead = a;
            atail = a;
            System.out.print(a);
            while (atail.next!=null && atail.next!=anext) {
               System.out.print("," + atail.next);
               atail = atail.next;
            }
         }
         System.out.println("}");
         a = anext;
      } 
      alist = ahead;
      
      // now to expand our bubble out one radius unit
      a = alist;
      while(a!=null) {
         System.out.print("Expanding " + a + " into ");
         a.expandArc(this,r);
         System.out.println(a);
         a = a.next;
      }
      r++;
      System.out.println("- Done computing nextArc -");
   }
   
   // Does the first part of expansion
   void splitArc(int r) {
      Arc a,ahead,atail,anext;
      Hexagon h;
      int i;
      
      // split up arcs according to obstacles
      ahead = null;
      atail = null;
      a = alist;
      while (a!=null) {
         anext = a.next;
         a.next = null;
         System.out.print(" Splitting " + a + " into {");
         a = a.breakArc(this,r);
         if (a==null) {
            // nothing visible here---need to merge adjacent arcs...
            System.out.print("<deleted>");
         } else {
            if (atail!=null) atail.next = a;
            if (ahead==null) ahead = a;
            atail = a;
            System.out.print(a);
            while (atail.next!=null && atail.next!=anext) {
               System.out.print("," + atail.next);
               atail = atail.next;
            }
         }
         System.out.println("}");
         a = anext;
      } 
      alist = ahead;
   }
   
   // For spitting out all arcs
   public void printArcs(Arc aa) {
      Arc a;
      a = aa;
      while(a!=null) {
         System.out.println(a);
         a = a.next;
      }
   }
   
   // Marks all hexagons in all arcs
   public void markArcs(int r) {
      for (Arc a=alist; a!=null; a=a.next)
         a.markHexes(this,r);
   }
   
   // paint routine
   public void draw(Graphics g) {
      int i,j;
      g.setColor(Color.black);
      for (j=0;j<h;j++)
         for (i=0;i<w;i++)
            grid[j*w+i].render(g);
      for (Arc a=alist; a!=null; a=a.next)
         a.render(g,hc,((a.next==null) ? alist : a.next));
   }
}
