/* 
   Hexagonal Grid Algorithm Testbed source file 
   (c) Copyright 1996 Clark Verbrugge (clump@cs.mcgill.ca)
   This code may be freely distributed and modified. 
   */

public class Coord {
   // macros to make sure floor and ceiling deal properly with negative
   // numbers
   private static int FLOOR2(int x) { return ((x>=0) ? (x>>1) : ((x-1)/2)); }
   private static int CEIL2(int x) { return ((x>=0) ? ((x+1)>>1) : (x/2)); }
   
   // coords can be maintained in either hex- or array-space.  There are
   // routines here for conversion between.  Default is hex-space.
   boolean isarray;            // true if array coord, false if hex coord
   public int x,y;
   
   public Coord() { this(0,0,false); }
   public Coord(Coord c) { this(c.x,c.y,c.isarray); }
   public Coord(int nx,int ny) { this(nx,ny,false); }
   public Coord(int nx,int ny,boolean nisarray) {
      x = nx; y = ny;
      isarray = nisarray;       // default is hex coords
   }
   
   public void move(int dx,int dy) {
      x += dx; y += dy;
   }
   
   // Convert coordinates to hex-space 
   public void toHex() { 
      if (isarray) {
         int hx,hy;
         hx = x - FLOOR2(y);
         hy = x + CEIL2(y);
         x = hx;  y = hy;
         isarray = false;
      }
   }
   
   // Convert coordinates to array-space 
   public void toArray() { 
      if (!isarray) {
         int ax,ay;
         ax = FLOOR2(x+y);
         ay = y - x;
         x = ax; y = ay;
         isarray = true;
      }
   }
}
