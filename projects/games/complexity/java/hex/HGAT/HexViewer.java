/* 
   Hexagonal Grid Algorithm Testbed source file 
   (c) Copyright 1996 Clark Verbrugge (clump@cs.mcgill.ca)
   This code may be freely distributed and modified. 
   */

import java.awt.*;

public class HexViewer extends Frame {
   static final int W = 680;  // size in pixels
   static final int H = 640;
   static final int HW = 22;  // size in hexagons
   static final int HH = 22;
   
   // HGAT hgat;   // just because we need to communicate with the parent applet
   HexGrid hg;
   int r = 0;
   
   // public HexViewer() { this(null); }
   // public HexViewer(HGAT nhgat) {
    public HexViewer(){
      super("Hexagonal Grid Algorithm Testbed");
      
      hg = new HexGrid(HW,HH);
      // hgat = nhgat;
      
      setFont(new Font("TimesRoman",Font.PLAIN,10));
      setBackground(Color.white);
      setForeground(Color.black);
      setLayout(new BorderLayout());
      
      Panel p = new Panel();
      p.add(new Button("Quit"));
      p.add(new Button("New"));
      p.add(new Button("Restart"));
      p.add(new Button("Expand"));
      
      add("South",p);
      
      pack();
      resize(W,H);
      show();
   }
   
   public void paint(Graphics g) {
      hg.draw(g);
   }
   
   public void quit() {
       System.exit(0);
      // if (hgat==null)
      //    System.exit(0);
      // else {
      //    hgat.isRunning = false;
      //    hgat.goButton.enable();
      //    dispose();
      // }
   }
   
   public boolean handleEvent(Event evt) {
      switch(evt.id) {
      case Event.WINDOW_DESTROY:
         quit();
         return true;
      default:
         return super.handleEvent(evt);
      }
   }
   
   public boolean action(Event evt,Object arg) {
      if(evt.target instanceof Button) {
         if ("Quit".equals(arg)) {
            quit();
            return true;
         } else if ("Expand".equals(arg)) {
            r++;
            hg.nextArc(r);
            repaint();
            return true;
         } else if ("Restart".equals(arg)) {
            r=0;
            hg = new HexGrid(hg);
            repaint();
            return true;
         } else if ("New".equals(arg)) {
            r=0;
            hg.remakeGrid();
            repaint();
            return true;
         }
      }
      return false; // super.handleEvent(evt);
   }
   
   // Use this main and delete HGAT.* if you want a stand-alone
   // application instead of an applet
   public static void main(String argsp[]) {
     new HexViewer();
     }
}

