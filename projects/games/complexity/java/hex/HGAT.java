/* 
   Hexagonal Grid Algorithm Testbed source file 
   (c) Copyright 1996 Clark Verbrugge (clump@cs.mcgill.ca)
   This code may be freely distributed and modified. 
   
   */

import java.awt.*;

public class HGAT extends java.applet.Applet {
   Button goButton;
   boolean isRunning = false;
   
   public void start() {
	  if (goButton==null) {
		 goButton = new Button("Hex Grid Algorithm Testbed");
		 if(isRunning)
			goButton.disable();
		 add(goButton);
	  }   
   }
   
   public boolean handleEvent(Event e) {
	  if((e.target instanceof Button) && e.id==Event.ACTION_EVENT) {
		 ((Button) e.target).disable();
		 isRunning = true;
		 new HexViewer(this);
	  }
      return super.handleEvent(e);
   }
   
   public void paint(Graphics g) { }
   
   public void stop() {
	  if(goButton!=null) {
		 remove(goButton);
		 goButton = null;
		 isRunning = false;
	  }
      System.gc();
   }
}

