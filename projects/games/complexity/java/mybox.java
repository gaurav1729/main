import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class mybox
{
    public static void main(String[] args)
    {
        Window win = new Window();
    }
}


class Window
    extends JFrame
{
    public Window()
    {
        //::::::::::::::
        Box bv = Box.createVerticalBox();
        for (int i = 0; i < 5; i++)
        {
            bv.add(new JButton("bv " + i));
            bv.add(Box.createVerticalStrut(i*10));
        }

        
        //::::::::::::::
        Box bh = Box.createHorizontalBox();
        for (int i = 0; i < 5; i++)
            bh.add(new JButton("bh " + i));
        
        //::::::::::::::
        Box bigger = Box.createVerticalBox();

        Box bd1 = Box.createHorizontalBox();
        bd1.add(new JButton("Top"));
        bd1.add(Box.createRigidArea(new Dimension(200,20)));
        bd1.add(new JButton("Bottom"));

        //::::::::::::::
        Box bd2 = Box.createHorizontalBox();
        bd2.add(Box.createRigidArea(new Dimension(200, 50)));
        bd2.add(new JButton("Left"));
        bd2.add(Box.createRigidArea(new Dimension(160,80)));
        bd2.add(new JButton("Right"));

        
        bigger.add(bd1);
        bigger.add(Box.createVerticalGlue());
        bigger.add(bd2);

        //::::::::::::::
        Container cp = getContentPane();
        cp.add(BorderLayout.EAST, bv);
        cp.add(BorderLayout.SOUTH, bh);
        cp.add(BorderLayout.NORTH, bigger);

        setSize(500,400);
        setVisible(true);
    }     // public void init()

} //public class mybox