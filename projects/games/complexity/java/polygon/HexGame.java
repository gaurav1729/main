// This was code written by me earlier.  I don't think this has the graphics
// working, but it does have some of the logic.


//package com.rush;
// comes from http://gdreflections.com/2011/02/hexagonal-grid-math.html; see hex.html
// javac HexLightsOut.java; appletviewer hex.html
import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;

/*
  Quick note: there are three kinds of coordinates used: (i,j) usually refers to
  the integer coordinates of the individual hexagons, where i goes from (0, n-1)
  or (0, n-2) depending on whether the row is odd or even.

  (x,y) refer to the coordinates of a point in pixels (also integer).

  (a,b) refer to the transformed coordinates from x,y to the grid defined on the
  centres of the hexagons.  They are like (i,j) but 'a' starts from floor(-b/2)
  (and, of course, goes up by n or n-1, depending on whether the row is odd or
  even).

 */

public class HexGame extends Applet implements MouseListener {
    private static final int BOARD_WIDTH = 5;
    private static final int BOARD_LENGTH = 7;
    private static final int SIDE_LENGTH = 40; // pixels
    private static final int x0 = 40; // offset in pixels of 
    private static final int y0 = 40; // center of first hexagon.
    private static final int nVertices = 6; // hexagon
    private static final float init_density = (float) 0.35; // fraction of cells occupied initially

    private HexGridClass hexGrid; 



    //::::::::::::::

    @Override
	public void init() {
	addMouseListener(this);
	hexGrid = new HexGridClass(BOARD_WIDTH, BOARD_LENGTH, SIDE_LENGTH);
        hexGrid.calcHexGrid(); // gsdebug: maybe put this in the constructor?
    } // init

    //::::::::::::::

    @Override
	public void paint(Graphics g) {
        for (SingleHexCell shc : hexGrid.values() ) {
            if ( shc.isSelected ) {
                g.setColor(Color.ORANGE);
            } else {
                g.setColor(Color.GRAY);
            }
            g.fillPolygon(shc.vertexCoords.xcoords,
                          shc.vertexCoords.ycoords,
                          nVertices);
            g.setColor(Color.BLACK);
            g.drawPolygon(shc.vertexCoords.xcoords,
                          shc.vertexCoords.ycoords,
                          nVertices);
        } // shc : hexGrid.keySet()
        System.out.println("in paint");
    } // public void paint

    //::::::::::::::

    @Override
	public void update(Graphics g) {
        System.out.println("in update");
	paint(g);
    }

    //::::::::::::::

    @Override
	public void mouseReleased(MouseEvent arg0) {
        SingleHexCell shc = hexGrid.calcWhichCell(arg0.getX(), arg0.getY());
        //hereiam
    } // mousereleased
    
    //::::::::::::::

    @Override
        public void mouseClicked(MouseEvent arg0) {
    }

    @Override
        public void mouseEntered(MouseEvent arg0) {
    }

    @Override
        public void mouseExited(MouseEvent arg0) {
    }

    @Override
        public void mousePressed(MouseEvent arg0) {
    }

    //::::::::::::::
    //::::::::::::::



} // public class HexGame extends Applet implements MouseListener


//::::::::::::::
//gsdebug: note that pixels are integers too!  merge them into 
// one xypair class.
/*
class XYIntPair {
    public int h; // the coordinates of the single cell (integers, not pixels)
    public int v; // starting from top left
    XYIntPair(int x_, int y_) {
	x = x_;
	y = y_;
    }

    @Override
        public int hashCode

}
*/

//::::::::::::::

// from
// http://stackoverflow.com/questions/156275/what-is-the-
// equivalent-of-the-c-pairl-r-in-java
/*
class Pair {
    public final int first;
    public final int second;
    public final static int base; //hereiam
    private final String str; // "ij" or "xy" or "ab"

    // CONSTRUCTOR
    public Pair(int first, int second, String str) {
    	super();
    	this.first  = first;
    	this.second = second;
        this.str    = str;
    }

    @Override
    public int hashCode() {
    	int hashFirst = first != null ? first.hashCode() : 0;
    	int hashSecond = second != null ? second.hashCode() : 0;

    	return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    public boolean equals(Object other) {
    	if (other instanceof Pair) {
    		Pair otherPair = (Pair) other;
    		return 
    		((  this.first == otherPair.first ||
    			( this.first != null && otherPair.first != null &&
    			  this.first.equals(otherPair.first))) &&
    		 (	this.second == otherPair.second ||
    			( this.second != null && otherPair.second != null &&
    			  this.second.equals(otherPair.second))) );
    	}

    	return false;
    }

    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }

    public int getFirst() {
    	return first;
    }

    public int getSecond() {
    	return second;
    }
    } 

*/

class Pair {
    private static int large_num1 = 10;
    private static int large_num2 = 10000;

    public static int create_pair(int x1, int x2, String s) {
        int type = -1; // bad value
        if (s == "ij") {
            type = 0;
        } else if (s == "xy") {
            type = 1;
        } else if (s == "ab") {
            type = 2;
        }
        
        return x1 * large_num2 + x2 * large_num1 + type ;
    } // create_pair

    public static int x1(int comb) {
        return (comb - comb%large_num2)/large_num2;
    }

    public static int x2(int comb) {
        comb = comb % large_num1;
        return (comb - comb % large_num1)/large_num1;
    }

    public static String type(int comb) {
        int intType = comb % large_num1;
        if (intType == 0)
            return "ij";
        if (intType == 1)
            return "xy";
        if (intType == 2)
            return "ab";
        return "badvalue";
    }

} // class Pair {


//::::::::::::::

// gsdebug: put these into the single hex cell, instead of creating a new
// class. or maybe not.

class NeighboursClass {
    // this assumes a horizontal arrangement of rows (rather than columns)
    public int  topLeft    ;
    public int  topRight   ;
    public int  left       ;
    public int  right      ;
    public int  bottomLeft ;
    public int  bottomRight;

    // we're using calcNeighbours instead
    // public void setNeighbours(double topLeft_,    double topRight_,
    //                           double left_,       double right_,
    //                           double bottomLeft_, double bottomRight_) {
    //     topLeft     = topLeft_;
    //     topRight    = topRight_;
    //     left        = left_;
    //     right       = right_;
    //     bottomLeft  = bottomLeft_;
    //     bottomRight = bottomRight_;
    // };

    public int [] getNeighbourInds() {
        int  [] outArray = {
            left, topLeft,topRight, right, bottomLeft, bottomRight};
	return outArray;
    };
}

//::::::::::::::

class VertexCoords {
    public int[] xcoords; // pixels (xy)
    public int[] ycoords;
}

//::::::::::::::

class SingleHexCell {
    public NeighboursClass neighbours  = new NeighboursClass();
    public VertexCoords vertexCoords   = new VertexCoords();
    public int  ij;

    private double xcenter;
    private double ycenter;
    private double sideLength;
    private double radius;

    private boolean longLine;
    public boolean isTargetA   = false;
    public boolean isTargetB   = false;
    public boolean isOccupiedA = false;
    public boolean isOccupiedB = false;
    public boolean isSelected  = false;

    private static final double sqrt3 = 1.73205;
    private  double [] dx;

    private  double [] dy;

    // constructor:
    public SingleHexCell(int i, int j, int boardWidth, 
                         int boardLength, double sideLength_) {

        ij = Pair.create_pair(i, j, "ij");

	sideLength = sideLength_;
	radius     = sideLength * sqrt3 / 2.0;
	longLine   = (j%2 == 0);

        // from top left vertex:
        dx = new double [] {-radius, 0, radius, radius, 0, -radius };
        dy = new double [] { sideLength/2.0,
               radius,
               sideLength/2.0,
               -sideLength/2.0,
               -radius,
               -sideLength/2.0
        }; // from top left vertex

	calcNeighbours(i, j, boardWidth, boardLength);
	calcVertexCoords(i, j, sideLength);
    } // public SingleHexCell

    // should we do this in HexGrid instead?
    void calcNeighbours( int i, int j, int boardWidth, int boardLength) {
	boolean topRow    = (j == 0);
	boolean bottomRow = (j == boardLength - 1);
	boolean leftCol   = ( i == 0);
	boolean rightCol  = (longLine && i == boardWidth-1
                             || ! longLine && i == boardWidth-2 );

	if ( ! topRow && ! leftCol ) {
	    if (longLine) {
		neighbours.topLeft = Pair.create_pair (i-1, j-1, "ij");
	    } else {
		neighbours.topLeft = Pair.create_pair (i, j-1, "ij");
	    }
	} // if ( ! topRow && ! leftCol)

	if ( ! topRow && ! rightCol ) {
	    if (longLine) {
		neighbours.topRight = Pair.create_pair (i, j-1, "ij");
	    } else {
		neighbours.topRight = Pair.create_pair (i+1, j-1, "ij");
	    }
	}

	if ( ! bottomRow && ! leftCol )  {
	    if (longLine) {
		neighbours.bottomLeft = Pair.create_pair (i-1, j+1, "ij");
	    } else {
		neighbours.bottomLeft = Pair.create_pair (i, j+1, "ij");
	    }
	}

	if ( ! bottomRow && ! rightCol ) {
	    if (longLine) {
		neighbours.bottomRight = Pair.create_pair (i, j+1, "ij");
	    } else {
		neighbours.bottomRight = Pair.create_pair (i+1, j+1, "ij");
	    }
	}

	if ( ! leftCol ) {
	    neighbours.left = Pair.create_pair (i-1, j, "ij");
	}

	if ( ! rightCol ) {
	    neighbours.right = Pair.create_pair (i+1, j, "ij");
	}


    } // void calcNeighbours

    //::::::::::::::
    void calcVertexCoords(int i, int j, double sideLength_) {
	// first, calculate xcenter and ycenter
	double shortLineOffset = radius;
	xcenter = 2.0 * radius * i;
	if ( ! longLine) {
	    xcenter += shortLineOffset;
	}
	
	ycenter = sideLength * 1.5 * j + sideLength;

	// now the vertices
        int [] temp = {
	    (int) (xcenter + dx[0]*sideLength),
	    (int) (xcenter + dx[1]*sideLength),
	    (int) (xcenter + dx[2]*sideLength),
	    (int) (xcenter + dx[3]*sideLength),
	    (int) (xcenter + dx[4]*sideLength),
	    (int) (xcenter + dx[5]*sideLength)
	};
	vertexCoords.xcoords = temp;

        int [] temp1 = {
	    (int) (ycenter + dy[0]*sideLength),
	    (int) (ycenter + dy[1]*sideLength),
	    (int) (ycenter + dy[2]*sideLength),
	    (int) (ycenter + dy[3]*sideLength),
	    (int) (ycenter + dy[4]*sideLength),
	    (int) (ycenter + dy[5]*sideLength)
	};
	vertexCoords.ycoords = temp1;

    } // void calcVertexCoords(int i, int j, double sideLength_) {

    //::::::::::::::


}; // class SingleHexCell

//::::::::::::::

class HexGridClass extends HashMap <Integer , SingleHexCell> 
{
    public int boardWidth;
    public int boardLength;
    public int sideLength;
    
    // offset of the center of the first hexagon, in pixels
    private int x0;
    private int y0;

    private final double tan30 = 0.57735;
    private final double sin30 = 0.5;
    private final double cos30 = 0.866;
    // constructor
    HexGridClass( int boardWidth_, int boardLength_, int sideLength_) {
        boardWidth  = boardWidth_;
        boardLength = boardLength_;
        sideLength  = sideLength_;
        
        x0 = sideLength;
        y0 = sideLength;
    }     // HexGridClass( int boardWidth, int boardLength, double sideLength)

    public void calcHexGrid() {
	for (int iBoardWidth = 0; iBoardWidth < boardWidth; ++iBoardWidth) {
	    for (int iBoardLength = 0; iBoardLength < boardLength; ++iBoardLength) {
		if (iBoardWidth %2 == 1 && iBoardLength == boardLength - 1) {
		    continue;
		}
		int  xy = Pair.create_pair (iBoardWidth, iBoardLength, "ij");
		this.put(new Integer(xy),
                         new SingleHexCell(iBoardWidth,
                                           iBoardLength,
                                           boardWidth,
                                           boardLength,
                                           sideLength));
	    } // for (int iBoardLength = 0; iBoardLength < BOARD_LENGTH; ++iBoardLength)
	} // 	for (int iBoardWidth = 0; i < BOARD_WIDTH; ++i) {
        
    }     // void CalcHexGrid( int boardWidth, int boardLength, double sideLength) 
    


    //::::::::::::::

    public SingleHexCell calcWhichCell(int x, int y)
    {
        // x and y are pixels in integers
        double a, b; // transformed coordinates; see below
        int amin, amax;
        int bmin, bmax;
        double adelta, bdelta;
        double dsquared;

        double xcenter, ycenter;
        int    icenter, jcenter;

        int minCorner;

        double m11, m22, m12, m21;
        double t11, t22, t12, t21;
        double min_dsquared;
        // remove the offset.  at this point, the coordinates of the center of
        // the first hexagon are (0,0)
        x -= x0;
        y -= y0;

        // v1 = (2*cos30*sideLength,              0)
        // v2 = (  cos30*sideLength, 1.5*sideLength)
        // we now switch from (x,y) to (a,b) using v1, v2 as unit vectors.
        m11 = -1.0/(2.0 * cos30 * sideLength);
        m12 = -1.0/(3.0 * sideLength);
        m21 =  0.0;
        m22 =  2.0/(3.0 * sideLength);

        a = m11 * x + m12 * y;
        b = m21 * x + m22 * y;
        amin = (int) Math.round(Math.floor(a));
        amax = (int) Math.round(Math.ceil(a));
        bmin = (int) Math.round(Math.floor(b));
        bmax = (int) Math.round(Math.ceil(b));

        int [] corners = {
            Pair.create_pair(amin, bmin, "ab"),
            Pair.create_pair(amin, bmax, "ab"),
            Pair.create_pair(amax, bmin, "ab"),
            Pair.create_pair(amax, bmax, "ab")
        };

        min_dsquared = -1.0;
        minCorner = corners[0]; // to avoid the warning
        for (int corner : corners)
        {
            adelta = Pair.x1(corner)  - a;
            bdelta = Pair.x2(corner) - b;

            // the actual formula actually multiplies the expression below by
            // 3*sideLength*sideLength, but that's a constant so is ignored.
            dsquared = adelta*adelta + bdelta*bdelta + adelta*bdelta;

            if (min_dsquared < 0 || dsquared < min_dsquared) {
                min_dsquared = dsquared;
                minCorner   = corner;
            }

        }   // for (XYCoord corner : corners)
        
        // at this point, min_corner contains the (a,b) coordinates of the
        // center of the closest hexagon, so transform back to (x,y)

        t11 = 2 * cos30 * sideLength;
        t12 =     cos30 * sideLength;
        t21 =                      0;
        t22 =       1.5 * sideLength;
        
        xcenter = t11 * Pair.x1(minCorner) + t12 * Pair.x2(minCorner);
        ycenter = t21 * Pair.x1(minCorner) + t22 * Pair.x2(minCorner);

        icenter = (int) Math.round(xcenter);
        jcenter = (int) Math.round(ycenter);

        return (this.get( Pair.create_pair (icenter, jcenter, "ij") ));


    }     // private SingleHexCell calcWhichCell(double x, double y)

    //::::::::::::::::::

    int  ab(SingleHexCell shc) {
        int a = Pair.x1(shc.ij) - (int)Math.round(Math.floor(Pair.x2(shc.ij)/2.0));
        int b = Pair.x2(shc.ij);
        int  out = Pair.create_pair  (a, b, "ab");
        return (out);
    }


} // class HexGridClass extends TreeMap <Pair , SingleHexCell> 

//::::::::::::::
