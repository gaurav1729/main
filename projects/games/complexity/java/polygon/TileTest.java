import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
 
class Tile extends JButton {
    Polygon poly; // define for object
     
    // we want to name our button.  More on this later
    public Tile(String name) {
        super(name);
        int size = 30;
        // set up here so you don't have to do setup every single paint
        poly = new Polygon();
        double factor = 2.0 * Math.PI / 6.0;
        for (int i = 0; i < 6; i++) {
            int x = size + (int)(size * Math.cos(i * factor + Math.PI/6.0));
            int y = size + (int)(size * Math.sin(i * factor + Math.PI/6.0));
            poly.addPoint(x,y);
        }
         
        // give the button size, based on our image
        setPreferredSize(poly.getBounds().getSize());
    }
     
    @Override
    public void paint(java.awt.Graphics g) {
        // g.drawPolygon(poly); // don't need this
        g.fillPolygon(poly);
    }

    @Override
    public boolean contains(Point p)
    {
        return poly.contains(p);
    }
 
    @Override
    public boolean contains(int x, int y)
    {
        return poly.contains(x, y);
    }
 
    @Override
    protected void processMouseEvent(MouseEvent e)
    {
        if ( contains(e.getPoint()) )
            super.processMouseEvent(e);
    }
 
}
 
// always extend JFrame of JPanel, rather doing everything in main
// put the listener in the parent of the button
public class TileTest extends JFrame implements ActionListener {
    private JButton button1, button2; // going to test two buttons.
     
    public TileTest() {
        setLayout(new FlowLayout());
         
        button1 = new Tile("B1");
        // IMPORTANT! nothing listens to a button unless you tell it to
        button1.addActionListener(this);
        add(button1);
         
        button2 = new Tile("B2");
        button2.addActionListener(this);
        add(button2);
         
        setSize(400,200);
        setTitle("Erfworld: the Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
     
    // here's the method
    public void actionPerformed(ActionEvent e){
        String cmd = e.getActionCommand();
        System.out.println(cmd + " - Click!");
    }
 
     
    public static void main(String[] args) {
        // look how little we do in static.  This is good.
        JFrame frame = new TileTest();
        frame.setVisible(true);
    }
 
}
