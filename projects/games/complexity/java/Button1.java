import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Button1
{
    public static void main(String[] args)
    {
        Window win = new Window();
    }
}

class Window
    extends JFrame
    implements ActionListener
{
    JButton start = new JButton("Start it");
    JButton stop = new JButton("Stop it");
    public Window()
    {
        super ("Window Title");
        setSize(500, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        Container ca = getContentPane();
        ca.setBackground(Color.green);

        FlowLayout flm = new FlowLayout();
        ca.setLayout(flm);


        start.setBackground(Color.magenta);
        start.addActionListener(this);
        ca.add(start);


        ca.add(stop);

        setContentPane(ca);

    }     // public Window()

    public void actionPerformed(ActionEvent event)
    {
        start.setBackground(Color.red);
    }     // public void actionPerformed(ActionEvent event)

} // class Window extends JFrame