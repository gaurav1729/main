import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class button1_box
{
    public static void main(String[] args)
    {
        Window win = new Window();
    }
}

class Window
    extends JFrame
    implements ActionListener
{
    JButton start = new JButton("Start it");
    JButton stop = new JButton("Stop it");
    
    public Window()
    {
        super ("Window Title");
        JPanel jpv = new JPanel();
        setSize(500, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        Container ca = getContentPane();
        ca.setBackground(Color.green);

        BoxLayout blm = new BoxLayout(jpv, BoxLayout.X_AXIS);
        jpv.setLayout(blm);


        start.setBackground(Color.magenta);
        start.addActionListener(this);
        jpv.add(start);


        jpv.add(stop);

        setContentPane(jpv);

    }     // public Window()

    public void actionPerformed(ActionEvent event)
    {
        start.setBackground(Color.red);
    }     // public void actionPerformed(ActionEvent event)

} // class Window extends JFrame