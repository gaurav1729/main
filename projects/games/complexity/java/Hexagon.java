public Hexagon(float pX, float pY, float pSize) {
        super(pX, pY, pSize, pSize);
//      setColor(1, 0, 0);
        setAlpha(0);

        float x1, x2, y1, y2;
        float lineWidth = 3;

        x1 = 0; y1 = pSize / 2;
        x2 = pSize / 4; y2 = (pSize * ((2 - (float)Math.sqrt(3)) / 4)); // Done
        Line line = new Line(x1, y1, x2, y2);
        line.setLineWidth(lineWidth);
        attachChild(line);

        x1 = x2; y1 = y2;
        x2 = pSize * .75f; // Done
        line = new Line(x1, y1, x2, y2);
        line.setLineWidth(lineWidth);
        attachChild(line);

        x1 = x2; y1 = y2;
        x2 = pSize; y2 = pSize / 2; // Done
        line = new Line(x1, y1, x2, y2);
        line.setLineWidth(lineWidth);
        attachChild(line);

        x1 = x2; y1 = y2;
        x2 = pSize * .75f; y2 = pSize - (pSize * ((2 - (float)Math.sqrt(3)) / 4)); // Done
        line = new Line(x1, y1, x2, y2);
        line.setLineWidth(lineWidth);
        attachChild(line);

        x1 = x2; y1 = y2;
        x2 = pSize / 4; // Done
        line = new Line(x1, y1, x2, y2);
        line.setLineWidth(lineWidth);
        attachChild(line);

        x1 = x2; y1 = y2;
        x2 = 0; y2 = pSize / 2; // Done
        line = new Line(x1, y1, x2, y2);
        line.setLineWidth(lineWidth);
        attachChild(line);

        touchableArea = new Rectangle(pSize / 4, pSize / 4, pSize * .75f, pSize * .75f);
        touchableArea.setAlpha(0);
        attachChild(touchableArea);
    }