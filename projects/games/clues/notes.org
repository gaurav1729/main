* Overall
Not clear if it's a cooperative game, or a competitive one
If it's competitive, then either
- there are partners assigned randomly as part of the game, but the players
  don't know who they are.
- the players decide who they want to partner with.  Then the partners would
  divide the points gained by winning.  Part of it might then be bluffing
  whether they need help to win or whether they can win themselves.  They could
  then divide the points based on negotiations, rather than equally.

** Turns
Then each individual has a list of information that's legal to give.  They take
turns giving one item of information at each point.  A big part of the strategy
is deciding how much information to give.

Deciding whether to bluff is a big part of the game.
