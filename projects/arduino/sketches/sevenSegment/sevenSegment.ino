/*
seven segment display

*/

const int abcdef[10][7] = {
  {1,1,1,1,1,1,0},
  {0,1,1,0,0,0,0},
  {1,1,0,1,1,0,1},
  {1,1,1,1,0,0,1},
  {0,1,1,0,0,1,1},
  {1,0,1,1,0,1,1},
  {1,0,1,1,1,1,1},
  {1,1,1,0,0,0,0},
  {1,1,1,1,1,1,1},
  {1,1,1,1,0,1,1}};

//pins for a, b, c, d, e, f in the seven segment
// display:
const int abcdefPin[7] = {3,4,5,6,7,8,9};

const int minPin = 13;
const int tensecPin = 12;
const int secPin = 11;

//:::::::::::::::::::::::::

void pushNumber(int num) {
  const int * table = abcdef[num];
  for (int i = 0; i < 7; ++i) {
    if (table[i] == 1) {
      digitalWrite(abcdefPin[i], HIGH);
    }
    else {
      digitalWrite(abcdefPin[i], LOW);
    }
  }
}
//:::::::::::::::::::::::::
void setup() {
  for (int i = 0; i < 7; ++i) {
    pinMode(abcdefPin[i], OUTPUT);
  }
  
}

void loop() {
  for (int i = 0; i < 10; ++i) {
    pushNumber(i);
    delay(1000);
  }
}
  
