/*

Uses a push button press to toggle the state of the LED.
Triggers only on upward edges -- when the push button is
released, the light stays on or off.

Does debouncing.


The circuit:
 * LED attached from pin 13 to ground
 * pushbutton attached from pin 2 to +5V
 * 10K resistor attached from pin 2 to ground
*/

const int buttonPin =  3;
const int ledPin    = 13; 

int ledState = HIGH;
int buttonState;
int lastButtonState = LOW;

boolean upTriggerDetected = false;

long lastDebounceTime = 0;
long debounceDelay = 50; //milliseconds

void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  int reading = digitalRead(buttonPin);
  if (lastButtonState == LOW && reading == HIGH) {
    upTriggerDetected = true;
    lastDebounceTime = millis();
  }
  if (reading == LOW) {
    upTriggerDetected = false;
  }
  
  if ( upTriggerDetected && (millis() - lastDebounceTime > debounceDelay)) {
    upTriggerDetected = false;
    if (buttonState == HIGH) {
      buttonState = LOW;
    } 
    else if (buttonState == LOW) {
      buttonState = HIGH;
    }
  }
  digitalWrite(ledPin, buttonState);
  lastButtonState = reading;
}
      
