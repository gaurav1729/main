// time and beeper for exercise


//:::::::::: setup
const int num_output_pins = 12;
const int num_analog_input_pins = 6;

int ssd_pin_0 = 7;
int ssd_pin_1 = 8;
int input_pin = 12;

int count;
int prev_state;
int curr_state;
int recheck_state;

enum State {
  READ_TIME,
  READ_RATIO,
  RUN,
  DO_NOTHING
};
State s;
byte seven_seg_digits[10][7] = {
   { 1,1,1,1,1,1,0 },  // = 0
   { 0,1,1,0,0,0,0 },  // = 1
   { 1,1,0,1,1,0,1 },  // = 2
   { 1,1,1,1,0,0,1 },  // = 3
   { 0,1,1,0,0,1,1 },  // = 4
   { 1,0,1,1,0,1,1 },  // = 5
   { 1,0,1,1,1,1,1 },  // = 6
   { 1,1,1,0,0,0,0 },  // = 7
   { 1,1,1,1,1,1,1 },  // = 8
   { 1,1,1,0,0,1,1 }   // = 9
};

int abcdefg[7] = {0, 1, 2, 3, 4, 5, 6};

void Exec_Read_Time();
void Exec_Read_Ratio();
void Exec_Run();
void Write_SSD(int count);
void Two_Digit_Display(int count, int ssd_pin_0, int ssd_pin_1, int* abcdefg, long n_ms);

void setup() {
  for (int i = 0; i < num_output_pins; i++) {
    pinMode(i, OUTPUT);
  } // output pins
  pinMode(input_pin, INPUT);
  s = DO_NOTHING;
  digitalWrite(7, LOW);
  digitalWrite(8, LOW);
  count = 0;

  prev_state    = LOW;
  curr_state    = LOW;
  recheck_state = LOW;

  //Serial.begin(9600);
};  // setup


//:::::::::::::::: main loop
void loop() {
  
  // Read a bouncy switch
  curr_state = digitalRead(input_pin);
  if (prev_state == LOW && curr_state == HIGH) {
    delay(3);
    recheck_state = digitalRead(input_pin);
    if (recheck_state == HIGH)
      count++;
  }
  prev_state = curr_state;


  Two_Digit_Display(count, ssd_pin_0, ssd_pin_1, abcdefg, 500);    

  //count++;
  if (count == 100) {
    count = 0;
  }
   

  if (s == READ_TIME) {
    Exec_Read_Time();
    s = READ_RATIO;
  }
  
  if (s == READ_RATIO) {
    Exec_Read_Ratio();
    s = RUN;
  }
  
  if (s == RUN) {
    Exec_Run();
  }
} // loop

//:::::::::::::: subroutines

void Write_SSD(int digit, int* abcdefg) {
  for (int i_pin = 0; i_pin < 7; i_pin++) {
    digitalWrite(abcdefg[i_pin], seven_seg_digits[digit][i_pin]);
  } // i_pin loop
} // Write_SSD

void Two_Digit_Display(int input, int ssd_pin_0, int ssd_pin_1, int* abcdefg, long n_ms) {
  long etime = 0;
  int delta = 10; // in milliseconds
  
  int d0 = input % 10;
  int d1 = ((input - d0)/ 10) %10;

  for(;;) {
    digitalWrite(ssd_pin_0, HIGH);
    digitalWrite(ssd_pin_1, LOW);
    Write_SSD(d0, abcdefg);
    delay(delta);
    
    digitalWrite(ssd_pin_0, LOW);
    digitalWrite(ssd_pin_1, HIGH);
    Write_SSD(d1, abcdefg);
    delay(delta);
    
    if (n_ms > 0) {
      etime += 2 * delta;
      if (etime >= n_ms)
        break;
    } //if (n_ms > 0)
  }  // for(;;)
} // Two_Digit_Display

void Exec_Read_Time() {
}

void Exec_Read_Ratio() {
}

void Exec_Run() {
}
