// sum7.cpp

// gsdebug -- add asserts
// gsdebug -- add const's to end of all member functions that do not change val
// gsdebug -- check all 'new' have deletes
// gsdebug -- organize into subroutines
// gsdebug -- use const_iterators if needed
// gsdebug -- whenever we return a reference, make sure we don't return the address of a local variable!!!!
// gsdebug -- exception handling for new and mallocs
// gsdebug -- why are we including functional and math.h? try without them.
// gsdebug -- replace loops with iterators?
// gsdebug -- have more systematic names (Numbers, ints, etc?)

// gsdebug -- define a constructor for numbers from a vector, that handles each
// "digit" not being less than ten, and use that constructor for operator*,
// operator += (both kinds) etc.

// gsdebug -- make sure it's always Number*int, not int*Number
// gsdebug -- change pn_next to pnum_next, ps_next to psum_next, etc. or tally or something.
// gsdebug -- change the vector in Number to template class T, try using list, etc.

// gsdebug -- note that the shift is now a member function, either make it a
// friend or change the format (see what works better for less copying of data).

// gsdebug -- define a copy constructor
// gsdebug -- when assigning memory for the array, do try catch


/*::::::::::::::::::::::::::
  ::::::::::::::::::::::::::

sum7_p.cpp is code that calculates the sum of all numbers between 1 and 10^n that are
divisible by 7 and have their decimal reverse also divisible by 7.

It operates in polynomial time.


::::::::::::::::::::::::::
::::::::::::::::::::::::::*/


// the code starts here

#include <iostream>
#include <vector>
#include <math.h> //gsdebug why this one?
#include <time.h>
#include <functional> //defines multiplies functor
#include <stdlib.h> // gsdebug do we need this for exit?
#include <assert>

using namespace std;


//:::::::  classes used in the code ::::::::


// this is a class of numbers, represented as a vector of digits.

// in_size is the size of the vector initially, 'base'
// is the base of the number system

class Number
{
private:
  vector <int> val;
  int base;

public:
  friend ofstream & operator<<(ofstream &outs, const Number & num);

  // some constructors:
  Number(int in_size, int in_base)
  {
    val.resize(in_size, 0);
    base = in_base;
  }
  Number()
  {
    val.resize(1, 0);
    base = 10;
  }



  //::::::::::::::: a bunch of overloaded arithmetic operators:::::::::
  const Number& operator+(Number n) const
  {
    Number sum;
    int dig_sum;
    int i;
    int carry =0;
    Number *longer, *shorter;

    if (base != n.base)
      {
        print "trying to add two numbers with different base";
        exit(1);
      }

    if (n.val.size() < val.size())
      {
        longer = this;
        shorter = &n;
      }
    else
      {
        longer = &n;
        shorter = this;
      }

    for (i = 0; i < shorter->val.size(); i++)
      {
        dig_sum = longer->val[i] + shorter->val[i] + carry;
        sum.val[i] = (dig_sum >= base)? (dig_sum-base):dig_sum;
        carry = int(dig_sum >= base);
      }

    for ( ; i < longer->val.size(); i++)
      {
        dig_sum = longer->val[i] + carry;
        sum.val[i] = (dig_sum >= base)? (dig_sum-base):dig_sum;
        carry = int(dig_sum >= base);
      }

    if (carry > 0)
      {
        sum.val.push_back(carry);
      }

    return sum;
  } //   const Number& operator+(Number n, Number n2)


  const Number& operator+=(Number n)
  {
    int dig_sum;
    int i;
    int carry =0;
    Number *longer, *shorter;

    if (base != n.base)
      {
        print "trying to add two numbers with different base";
        exit(1);
      }

    if (val.size() < n.val.size())
      val.resize(n.val.size(), 0);

    for (i=0; i< n.val.size(); i++)
      {
        val[i] += n.val[i] + carry;
        if (val[i] >= base)
          {
            val[i]-= base;
            carry  = 1;
          }
      }
    
    while( i < val.size() && carry >= 1)
      {
        val[i] += carry;
        if (val[i] >= base)
          {
            val[i] -= base;
            carry   = 1;
          }
        i++;
      }

    while (carry > 0)
      {
        val.push_back(carry%base);
        carry /= base;
      }

    return *this;
  } //    const Number& operator+=(Number n)


  const Number& operator+=(int n)
  {
    int i = 0;
    int carry = 0;
    val[0] += n;
    do //gsdebug this and other do-whiles should probably be a while loop
      {
        val[i]+= carry;
        if (val[i] >= base)
          {
            carry   = val[i]/base;
            val[i] %= base;
          }
        i++;
      }
    while (carry >= 1 && i < val.size());

    while (carry > 0)
      {
        val.push_back(carry%base);
        carry /= base;
      }

    return *this;
  }

  const Number& operator*(int n)
  {
    Number product(0, base);
    int carry = 0;
    for (int i = 0; i < val.size(); i++)
      {
        product.val[i] = val[i] * n;
      }

    for (vector<int>::iterator it = product.val.begin(); it != product.val.end(); it++)
      {
        *it += carry;
        carry = *it/base;
        *it %= base;
      }

    for (int i=0; carry >= 1; i++)
      {
        product.val.push_back(carry % base);
        carry /= base;
      } //     for (int i=0; carry >0; i++)

    return product;
  } //   const Number& operator*(int n)


  const Number& shift(int n) const // multiply by BASE^n
  {
    Number result = *this;
    result.val.insert(result.val.begin(), n, 0); //gsdebug if val is a list, we can use push_forward
    return result;
  }

} // class Number

ofstream & operator<<(ofstream &outs, const Number & num)
{
  for (int i =num.val.size()-1; i >= 0; i--)
    {
      if (num.val[i] == 0) continue;
      outs << num.val[i];
    }
  return outs;
}

inline int mod7(int in)
{
  return (in%7);
}

template<class T>
void swap(T& a, T& b)
{
  T temp;
  temp = a;
  a    = b;
  b    = temp;
}


//:::::::::::::::::::::::::::::::::::::::::::

int main(int argc, char * argv[])
{
  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::::: preliminary setup and checking::::::::::::::
  //::::::::::::::::::::::::::::::::::::::::::::::::::::

  // convert the arguments to integers and check on the range

  int num_digits;
  if (argc == 0)
    num_digits = 11;
  else if (argc == 1)
    {
      num_digits = atoi(argv[1]);
      if (num_digits < 3 || num_digits > 200)
        {
          cout <<"Usage: sum7 num_digits\n";
          cout << "num_digits is out of range: "<< num_digits << endl;
          exit(1);
        }
    }
  else // argc >= 2
    {
      cout <<"Usage: sum7 num_digits\n";
      cout << "You have supplied " << argc << "parameters." << endl;
      exit(1);
    }

  cout << "The number of digits is " << num_digits << "." << endl;

  // gsdebug make these global?
  // create lookup tables for modulo 7 arithmetic

  int times_7[7][7];
  int plus_7[7][7];
  int divide_7[7][6];
  int minus_7[7][7];
  const int neg_7[7] = {0, 6, 5, 4, 3, 2, 1};
  const int inv_7[7] = {0, 1, 4, 5, 2, 3, 6};

  for (int i=0; i<7; i++)
    for (int j=0; j<7; j++)
      {
        times_7[i][j] = (i*j)%7;
        plus_7[i][j]  = (i+j)%7;
        minus_7[i][j] = (i + neg_7[j])%7;
        jprime[i][j]  = (5*i + 2*j)%7;
      }

  for (int i=0; i<7; i++)
    for (int j=1; j<7; j++)
      {
        divide_7[i][j] = (i * inv_7[j]) %7;
      }

  // create an array of 3^i for i = 0...num_digits.
  // (note that 3 == 10 (modulo 7))
  int * three_power = new int[num_digits];
  three_power[0] = 1;
  for (int i=1; i<num_digits; i++)
    {
      three_power[i] = times_7[3][three_power[i-1]];
    }

  // create data structure to hold the information at each step.

  int  na[7][7];
  int  nb[7][7];
  Number  sa[7][7]; //gsdebug make this 7x7 pointers to Numbers rather than Numbers
  Number  sb[7][7];
  
  int *pn_prev, *pn_next;
  Number *ps_prev, *ps_next;

  pn_prev = na;
  pn_next = nb;
  ps_prev = sa;
  ps_next = sb;

  Number sum(1, 10);

  // create tables for first digit
  const int n1[]      = {2, 2, 2, 1, 1, 1, 1};
  const int n1_rest[] = {1, 2, 2, 1, 1, 1, 1}; // first digit restricted to be non-zero
  const int s1[]      = {7, 9,11, 3, 4, 5, 6}; 

  sum += s1[0]; // the sum of all 1-digit numbers satisfying the requirements

  // do calculations for the 2nd digit (this is a special case because it can be
  // done faster than other digits). (k = 1)

  for (int i = 0; i < 7; i++)
    {
      for (int j = 0 ; j < 7; j++)
        {
          int ind1 = mod7(3*i -   j);
          int ind2 = mod7(6*i + 3*j)
          pn_next->[i][j] = n1[ind1] * n1[ind2];
          ps_next->[i][j] = shift(s1(ind1)*n1(ind2), 1) + s1(ind2)*n1(ind1);
        } //       for (int j=0; j< 7; j++)
    } //   for (int i =0; i < 7; i++)

  sum += shift(s1[0]*n1[0], 1) + s1[0]*n1_rest[0]; // the sum of all appropriate 2-digit numbers
  swap(pn_prev, pn_next);
  swap(ps_prev, ps_next);
  //gsdebug if num_digits == 2, exit here (after printing out relevant data)


  // do calculations for all the digits except the last
  int k;
  for (k = 2; k <= (num_digits-2); k++) // each value of k corresponds to (k+1) digit numbers.  if k = 3, this loop gets skipped.
    {
      sum += Partial_Sum(pn_prev, ps_prev, k, jprime, times_7, plus_7, n1, n1_rest, s1); //gsdebug see what we really need out of these, and have a bunch of them as static const local variables inside Partial_Sum?
      Update_Matrices(pn_prev, ps_prev, k, pn_next, ps_next, jprime, times_7, plus_7, n1, n1_rest, s1); // gsdebug in this, create an iprime table similar to jprime, but for the correct value of k

      swap(pn_prev, pn_next);
      swap(ps_prev, ps_next);
    }

  // do calculations for the last digit

  sum += Partial_Sum(pn_prev, ps_prev, k, jprime, times_7, plus_7, n1, n1_rest, s1);

} // int main(int argc, char * argv[])
