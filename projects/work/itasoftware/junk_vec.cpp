#include <iostream>
#include <exception>
using namespace std;

template <class T>
class Array
{

private:
  T **pp;
  int nrows;
  int ncols;

public:
  Array(int in_n, int in_m)
  {
    n = in_n;
    m = in_m;
    pp = new T*[n];
    try
      {
        for (int i = 0; i < n; i++)
          {
            pp[i] = new T[m];
            if (pp[i] == NULL) throw i;
          }
      }
    catch (int e)
      {
        cout << "Memory allocation exception." ;
        for (int i = e; i >=0; i--)
          {
            delete [] pp[i];
          }
        delete [] pp;
        exit(1);
      }
    /*
      alternatively, no throw statement
      catch(exception& e)
      {
        cout << "exception:  " << e.what();
      }
      
     */

  }


  ~Array()
  {
    for (int i = 0; i<n; i++)
      delete [] pp[i];
    delete [] pp;
  }


}; //  class Array

template<class T>
void change_arr(T& testarr)
{
  cout << " in change_arr " << endl;
  for (int i = 0; i < testarr.n; i++)
    {
      for (int j = 0; j < testarr.m; j++)
        {
          testarr.pp[i][j] += 100;
          cout << testarr.pp[i][j]<< " ";
        }
      cout << endl;
    }
}


int main(int argc, char * argv[])
{
  Array<int> testarr = Array<int>(5,6);
  cout << "in main 0 : " << endl;
  for (int i = 0; i < testarr.n; i++)
    for (int j = 0; j < testarr.m; j++)
      testarr.pp[i][j] = i+j;

  for (int i = 0; i < testarr.n; i++)
    {
      for (int j = 0; j < testarr.m; j++)
        cout << testarr.pp[i][j]<< " ";
      cout << endl;
    }

  cout << "_____________________" << endl;

  change_arr<Array<int> >(testarr);

  cout << "_____________________" << endl;

  cout << "in main 1 : " << endl;
  for (int i = 0; i < testarr.n; i++)
    {
      for (int j = 0; j < testarr.m; j++)
        cout << testarr.pp[i][j]<< " ";
      cout << endl;
    }

}
