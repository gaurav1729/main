// sum7.cpp

// gsdebug -- do we have to define an array to represent the numbers, or is the sum sufficient?
// gsdebug -- how do we defined int(Number) conversions?
// gsdebug -- given a int** a; how do we reshape it into a double array?
// gsdebug -- create a struct for profiling, do timing, create ansatz_breakup
// gsdebug -- add asserts
// gsdebug -- add const's to end of all member functions that do not change val
// gsdebug -- check all 'new' have deletes
// gsdebug -- for Numbers, change N and BASE from templates to constructor parameters
// gsdebug -- organize into subroutines
// gsdebug -- use const_iterators if needed
// gsdebug -- whenever we return a reference, make sure we don't return the address of a local variable!!!!
// gsdebug -- exception handling for new and mallocs
// gsdebug -- why are we including functional and math.h? try without them.
// gsdebug -- replace loops with iterators?

/*::::::::::::::::::::::::::
  ::::::::::::::::::::::::::

sum7.cpp is code that calculates the sum of all numbers between 1 and 10^n that are
divisible by 7 and have their decimal reverse also divisible by 7.

mod7.h is code that does some of the work of mod 7 arithmetic.

.......................
The algorithm is as follows:

1) Divide the n digits into a certain number of blocks as evenly as possible
   (symmetrically?).  For example, 11 digits could be 4+3+4.  Calculate the
   remainder mod 7 for each block of code (e.g.: 1, rem(10^4,7), rem(10^7,7)).

2) In a loop from one to each different sized block, for each number calculate
   the remainder when divided by 7 and the remainder when the reverse is divided
   by 7.  Add that number to a matrix keyed by remainder and reverse remainder.
   Keep the track of the count of each of these numbers also.

3) Construct the linear equation from the remainders of each block to ensure
   that the whole number is divisible by 7.  Do the same with the reverse
   remainders.

4) For each allowable value of the remainders, search for all the allowable
   values of the reverse remainders.  For each value that satisfies the both
   equations, for a given block add to the running sum the sum from the matrix
   for those values of remainder and reverse remainder multiplied by the product
   of the counts for the remaining blocks.

5) Add the sums for each block multiplied by the appropriate weight (Sigma 10^ni*si)

........................

The structure of the code is as follows:

1) A struct (call it remainder_data) that has two fields -- count and sum.

2) An array of these structs (7x7).  It would be nice to have the number of
   dimensions be flexible, but that might not be easy.

3) Multiple instances of this array, one for each sized block (say one each for
   10^3, 10^4)

4) Mod 7 arithmetic type: Takes an int and give the mod 7 remainder, mod 7
   reverse remainder.  Take an int and make a constructor.  Add, subtract and
   multiply mod sevens.  Perhaps use them for indexing into the "array" of
   remainder_data.  Check for equality of mod sevens, and equality of mod sevens
   and ints.  Define mod7++.

5) An array of ints to represent the numbers.  Define adding, multiplying by a
   factor of 10^n, being able to check if it's divisible by seven.
 
6) An array of 3^n base seven, useful for part 5 above and for checking to see
   if the combination of the clumps is divisible by 7.

::::::::::::::::::::::::::
::::::::::::::::::::::::::*/


// the code starts here

#include <iostream>
#include <list>
#include <vector>
#include <math.h>
#include <time.h>
#include <functional> //defines multiplies functor
#include <stdlib.h> // gsdebug do we need this for exit?
#include <assert>

using namespace std;


//:::::::  classes used in the code ::::::::


// this is a class of numbers, represented as a vector of base BASE digits.
// gsdebug implement += -- this should be faster than +
// in_size is the size of the vector initially, 'base'
// is the base of the number system (10 or 7 in this case)

class Number
{
private:
  vector <int> val;
  int base;

public:
  friend ofstream & operator<<(ofstream &outs, const Number<N, BASE> & num);

  // some constructors:
  Number(int in_size, int in_base)
  {
    val.resize(in_size, 0);
    base = in_base;
  }
  Number(int in_size, int in_base, unsigned long n)
  {
    val = vector;
    base = in_base;
    int r;
    do
      {
        r = n%base;
        n /= base;
        val.push_back(r);
      }
    while (n > 0);

    val.resize(in_size,0);
  }
  Number(int in_size, int in_base, vector<int> v)
  {
    val = v;
    base = in_base;
    val.resize(in_size);
  }


  //::::::::::::::: a bunch of overloaded arithmetic operators:::::::::
  const Number& operator+(Number n) const
  {
    Number sum;
    int dig_sum;
    int i;
    int carry =0;
    Number *longer, *shorter;

    if (base != n.base)
      {
        print "trying to add two numbers with different base";
        exit(1);
      }

    if (n.val.size() < val.size())
      {
        longer = this;
        shorter = &n;
      }
    else
      {
        longer = &n;
        shorter = this;
      }

    for (i = 0; i < shorter->val.size(); i++)
      {
        dig_sum = longer->val[i] + shorter->val[i] + carry;
        sum.val[i] = (dig_sum >= base)? (dig_sum-base):dig_sum;
        carry = int(dig_sum >= base);
      }

    for ( ; i < longer->val.size(); i++)
      {
        dig_sum = longer->val[i] + carry;
        sum.val[i] = (dig_sum >= base)? (dig_sum-base):dig_sum;
        carry = int(dig_sum >= base);
      }

    if (carry > 0)
      {
        sum.val.push_back(carry);
      }

    return sum;
  } //   const Number& operator+(Number n, Number n2)


  const Number& operator+=(Number n)
  {
    int dig_sum;
    int i;
    int carry =0;
    Number *longer, *shorter;

    if (base != n.base)
      {
        print "trying to add two numbers with different base";
        exit(1);
      }

    if (val.size() < n.val.size())
      val.resize(n.val.size(), 0);

    for (i=0; i< n.val.size(); i++)
      {
        val[i] += n.val[i] + carry;
        if (val[i] >= base)
          {
            val[i]-= base;
            carry  = 1;
          }
      }
    
    while( i < val.size() && carry >= 1)
      {
        val[i] += carry;
        if (val[i] >= base)
          {
            val[i] -= base;
            carry   = 1;
          }
        i++;
      }

    if (carry > 0)
      val.push_back(carry);

    return *this;
  } //    const Number& operator+=(Number n)


  const Number& operator++(int) // a++
  {
    int i = 0;
    int carry = 0;
    do
      {
        val[i]++;
        if (val[i] >= base)
          {
            carry   = 1;
            val[i] -= base;
          }
        i++;
      }
    while (carry >= 1 && i < val.size());

    if (carry == 1)
      val.push_back(carry);
    return *this;
  }

  const bool is_max() const // returns true if all the elements of the vector are equal to BASE-1
  {
    const i_max = base-1;
    return ( find_if(val.begin(), val.end(), bind2nd(not_equal_to<int>(), i_max)) == val.end() );
  }


  const Number& shift_digits(int n) const // multiply by BASE^n
  {
    Number result = *this;
    result.val.insert(result.val.begin(), n, 0);
    return result;
  }

  const int& rem7(const int& offset, const vector <int>& three_power) const
  {
    int sum = 0;
    for (int i=0; i< val.size(); i++)
      {
        sum += val[i]*three_power.at(offset+i);
      }
    return sum%7;
  }

  const int& reverse_rem7(const int& offset, const vector<int> & three_power) const
  {
    int sum = 0;
    int len = val.size();
    for (int i=0; i < len; i++)
      {
        sum += val[len-i-1]*three_power.at(offset+i);
      }
    return sum%7;
  }

} // class Number

ofstream & operator<<(ofstream &outs, const Number & num)
{
  for (int i =num.val.size()-1; i >= 0; i--)
    {
      if (num.val[i] == 0) continue;
      outs << num.val[i];
    }
  return outs;
}


// this class contains the data for a given value of remainder mod 7 and 
// remainder of the reverse mod 7.  It will be used in a 7x7 array.
class Rem_Data
{
public:
  Number<1,10> sum = Number<1,10>(0); //gsdebug is this appropriate for defaults?
  int count = 0;
} // class Rem_Data

class Rem_Data_Array
{
public:
  int size_x;
  int size_y;
  Rem_Data* data;
  Rem_Data_Array(int n1, int n2)
  {
    data = new Rem_Data[n1][n2];
    size_x = n1;
    size_y = n2;
  }
  ~Rem_Data_Array()
  {
    delete [] data;
  }
} // class Rem_Data_Array


//:::::::::::::::::::::::::::::::::::::::::::

int main(int argc, char * argv[])
{
  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::::: preliminary setup and checking::::::::::::::
  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  list <int> clump_size;
  vector<int> offset;
  vector<int> reverse_offset;
  // convert the arguments to integers and store them in a list
  //gsdebug if there is only one input argument, create the others.
  int num_digits = atoi(argv[1]);

  for (int i =2; i < argc; i++)
    clump_size.push_back(atoi(argv[i]));

  int num_clumps = clump_size.size();

  offset[0] = 0;
  for (int i=1; i< num_clumps; i++)
    offset[i] = offset[i-1] + clump_size[i-1];

  reverse_offset[num_clumps-1] = 0;
  for (int i = num_clumps-2; i > 0; i--)
    reverse_offset[i] = reverse_offset[i+1] + clump_size[i+1];

  // check to see if the arguments are correct -- the first arg must be the sum
  // of the rest.
  int clump_size_sum = accumulate(clump_size.begin(), clump_size.end(), 0);

  if (num_digits != clump_size_sum)
    {
      print("wrong breakup of digits\n");
      exit(1);
    }

  // create lookup tables for modulo 7 arithmetic

  int times_7[7][7];
  int plus_7[7][7];
  int divide_7[7][6];
  int minus_7[7][7];
  int neg_7[7] = {0, 6, 5, 4, 3, 2, 1};
  int inv_7[7] = {0, 1, 4, 5, 2, 3, 6};

  for (int i=0; i<7; i++)
    for (int j=0; j<7; j++)
      {
        times_7[i][j] = (i*j)%7;
        plus_7[i][j]  = (i+j)%7;
        minus_7[i][j] = (i + neg_7[j])%7;
      }

  for (int i=0; i<7; i++)
    for (int j=1; j<7; j++)
      {
        divide_7[i][j] = (i * inv_7[j]) %7;
      }

  // create an array of 3^i for i = 0...num_digits.
  // (note that 3 == 10 (modulo 7))
  vector three_power(num_digits);
  three_power[0] = 1;
  for (int i=1; i<num_digits; i++)
    {
      three_power[i] = times_7[3][three_power[i-1]];
    }


  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::: tabulate remainders for relevant numbers::::::
  //::::::::::::::::::::::::::::::::::::::::::::::::::::

  // :::::::: create a 7x7 array of remainder_data structs, indexed by remainder
  // and reverse remainder.  each element contains sum of all elements that have
  // these values of remainder and reverse remainder and the number of such
  // elements.
  // do this once for each unique number in the breakup of the digits.

  //    find the unique numbers in the breakup.

  list <int> unique_clump_sizes(clump_size);
  unique_clump_sizes.unique();


  //    for each unique number x, loop from 0 to (10^x-1) 
  map <int, Rem_Data_Array*> rem_data_lookup;
  for (int i = 0; i < unique_clump_sizes.size(); i++)
    {
      int clump_len = unique_clump_size[i];
      Rem_Data_Array table;
      for (Number<clump_len, 10> n = Number<clump_len, 10>(0); !n.is_max(); n++)
        {
          int n_rem7 = n.rem7(0, three_power);
          int n_rev_rem7 = n.reverse_rem7(0, three_power);
          table.data[n_rem7][n_rev_rem7].sum += n;
          table.data[n_rem7][n_rev_rem7].count++;
        } //       for (Number<clump_len, 10> n = Number<clump_len, 10>(0); !n.is_max(); n++)
      rem_data_lookup[clump_len] = &table;
    } //   for (int i = 0; i < unique_clump_sizes_vals.size(); i++)
    

  //:::::::::::::::::::::::::::::::::::::::::::::::::::::
  //:::::: calculate the sum of all appropriate numbers::
  //:::::::::::::::::::::::::::::::::::::::::::::::::::::

  // Create a base 7 number which has num_clumps-1 parts, each of two digits
  // (which correspond to rem7 and reverse_rem7).  Loop that up to max value.
  // For each value, separate it into num_clumps-1 constituents.  Calculate the
  // num_clumps-th values of rem7 and reverse_rem7.  For each of the
  // constituents in turn, multiply the sum by the product of the other counts,
  // and add that to the running sum for that particular clump.


  unsigned long count_product = clump_size.accumulate( clump_size.begin(), clump_size.end(), 1, multiplies<int>() );

  Number<2*(num_clumps-1), 7> remainders = Number<2*(num_clumps-1), 7>(0);
  vector<vector<int>::iterator> indiv_pointers = vector<vector<int>::iterator>(num_clumps-1);
  for (int i=0; i < num_clumps-1; i++)
    indiv_pointers[i] = remainders.begin() + 2*i;

  vector<Number<1, 10>> clump_sums = vector<Number<1, 10>>(num_clumps);


  for ( ; !remainders.is_max(); remainders++)
    {
      for (int i=0; i< num_clumps-1; i++)
        {
          clump_len = clump_size[i];
          p_table = rem_data_lookup[clump_len];
          
          
        }
    } //    for ( ; !remainders.is_max(); remainders++)
  
  
} // int main(int argc, char * argv[])
