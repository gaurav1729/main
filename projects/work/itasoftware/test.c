#include <stdio.h>
#include <math.h>
void flipString(char *r, char *s) {
	int i=0;
	int l = strlen(s); 
	r[l-1] = (char) NULL;
	for(i=0; i< l; i++) {
		r[l-i-1] = s[i];
	}//for
}
unsigned int flipBits10(unsigned int i) {
	char s[80] ;
	char is[80];
	unsigned int r; 
	bzero(s,80);
	bzero(is,80);
	sprintf(s, "%0u", i);
	flipString(is,s); 
	sscanf(is,"%0u", &r);  // Note: Scanf like this works only up to 1<<30
	// printf("%s = %s = %u\n", s,is,r); 
	return(r);
} // Flips the bits 
main()
{
unsigned int i; 
unsigned int is;
unsigned int sum=0;
unsigned int prev =0 ;
for(i=0; i<(1000000); i+=7) {
	if (i%7 != 0 ) continue; 
	is = flipBits10(i); 	
	if ( (is%7) != 0) continue;
	sum+= i;
	/*printf("Foundone: %u %u %u (%u) \n", i, is, sum, (i-prev)); */
	prev = i ;
	}
	printf("FinalSum: %u %u\n", i,  sum);
}

