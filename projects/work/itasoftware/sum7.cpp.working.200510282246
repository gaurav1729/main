// sum7_vec.cpp, comes from sum7_p.cpp, with change to vectors instead of arrays.

// gsdebug -- add asserts
// gsdebug -- add const's to end of all member functions that do not change val
// gsdebug -- check all 'new' have deletes
// gsdebug -- organize into subroutines
// gsdebug -- use const_iterators if needed
// gsdebug -- whenever we return a reference, make sure we don't return the address of a local variable!!!!
// gsdebug -- why are we including functional and math.h? try without them.  do we need stdlib.h for exit?
// gsdebug -- have more systematic names (Numbers, ints, etc?)
// gsdebug -- make sure it's always Number*int, not int*Number
// gsdebug -- change pn_next to pnum_next, ps_next to psum_next, etc. or tally or something.
// gsdebug -- take out lots of the inline functions
// gsdebug -- make sure all push_back's can handle larger size vectors with leading zeros

// gsdebug -- change the vector in Number to template class T, try using list, etc.
// gsdebug -- replace loops with iterators?
// gsdebug -- implement << with reverse iterator

/*::::::::::::::::::::::::::
  ::::::::::::::::::::::::::


Usage: ./sum7 [n] where n is the maximum number of digits required (default 11)

sum7_p.cpp is code that calculates the sum of all numbers between 1 and 10^n that are
divisible by 7 and have their decimal reverse also divisible by 7.

It operates in polynomial time.


::::::::::::::::::::::::::
::::::::::::::::::::::::::*/


// the code starts here

#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <assert.h>

//  #include <math.h>
//  #include <time.h>
//  #include <functional> //defines multiplies functor
//  #include <stdlib.h>


using namespace std;


//:::::::  classes used in the code ::::::::


// this is a class of numbers, represented as a collection of digits.

class Number
{
private:
  list <int> val;
  int base;

public:
  friend ostream & operator<<(ostream &outs, const  Number & num);

  // some constructors:
  Number()
  {
    val.resize(0, 0);
    base = 10;
  }
  Number(const Number& num)
  {
    val  = num.val;
    base = num.base;
  }
  Number(unsigned long n)
  {
    val.resize(0,0);
    base = 10;

    do
      {
        val.push_back(n%base);
        n /= base;
      }
    while (n > 0);

//     val[0] = n%base;
//     n /= base;
//     while (n > 0)
//       {
//         val.push_back(n%base);
//         n /= base;
//       }

  }



  ~Number() { };

  //::::::::::::::: a bunch of overloaded arithmetic operators:::::::::
//   Number operator=(Number n)
//   {
//     val = n.val;
//     base = n.base;
//   }

  const Number operator+(const Number& n) const
  {
    Number sum;
    int dig_sum;
    list<int>::const_iterator i;
    list<int>::const_iterator j;
    int carry =0;
    const Number * longer;
    const Number * shorter;

    if (base != n.base)
      {
        printf("trying to add two numbers with different base\n");
        exit(1);
      }

    if (n.val.size() < val.size())
      {
        longer = this;
        shorter = &n;
      }
    else
      {
        longer = &n;
        shorter = this;
      }

    for (i = shorter->val.begin(), j = longer->val.begin();
         i != shorter->val.end(); 
         i++, j++)
      {
        dig_sum = (*i) + (*j) + carry;
        sum.val.push_back((dig_sum >= base)? (dig_sum-base):dig_sum);
        carry = int(dig_sum >= base);
      }

    for ( ; j != longer->val.end(); j++)
      {
        dig_sum = (*j) + carry;
        sum.val.push_back((dig_sum >= base)? (dig_sum-base):dig_sum);
        carry = int(dig_sum >= base);
      }

    if (carry > 0)
      {
        sum.val.push_back(carry);
      }

    return sum;
  } //   const Number& operator+(Number n, Number n2)

  const Number operator+(unsigned long n) const
  {
    Number sum;
    sum.val = val;
    unsigned long carry = 0;
    list<int>::iterator i = sum.val.begin();
    *i += n;
    for ( ; i != sum.val.end(); i++)
      {
        *i += carry;
        carry = *i/base;
        *i %= base;
        if (carry == 0)
          break;
      }

    while (carry > 0)
      {
        sum.val.push_back(carry%base);
        carry /= base;
      }


    return sum;
  }


  void operator+=(const Number& n) // we will not be using this as an rvalue, so no need to return (*this)
  {
    int dig_sum;
    list<int>::const_iterator i;
    list<int>::iterator j;
    int carry =0;


    if (base != n.base)
      {
        printf("trying to add two numbers with different base\n");
        exit(1);
      }

    if (val.size() < n.val.size())
      val.resize(n.val.size(), 0);

    for (i=n.val.begin(), j = val.begin();
         i != n.val.end(); 
         i++, j++)
      {
        (*j) += (*i) + carry;

        carry  =  (*j)/base;
        (*j) %= base;
      }
    
    while( j != val.end() && carry >= 1)
      {
        (*j) += carry;

        carry  =  (*j)/base;
        (*j) %= base;

        j++;
      }

    while (carry > 0)
      {
        val.push_back(carry%base);
        carry /= base;
      }

  } //    const Number& operator+=(Number n)


  void operator+=(const unsigned long& n) // we will not be using this as an rvalue, so no need to return (*this)
  {
    list<int>::iterator i = val.begin();
    unsigned long carry = 0;
    (*i) += n;
    do
      {
        (*i)+= carry;
        if ((*i) >= base)
          {
            carry   = (*i)/base;
            (*i) %= base;
          }
        i++;
      }
    while (carry >= 1 && i != val.end());

    while (carry >= 1)
      {
        val.push_back(carry%base);
        carry /= base;
      }

  }

  const Number operator*(unsigned long n) const
  {
    Number product(*this);
    unsigned long carry = 0;
    list<int>::const_iterator i;
    list<int>::iterator j;
    for ( i = val.begin(),  j = product.val.begin();
         i != val.end();
         i++, j++)
      {
        (*j) = (*i) * n;
      }

    //gsdebug inconsistent naming
    for (list<int>::iterator it = product.val.begin(); it != product.val.end(); it++)
      {
        *it += carry;
        carry = *it/base;
        *it %= base;
      }

//     for (int i=0; carry >= 1; i++)
    while (carry >= 1)
      {
        product.val.push_back(carry % base);
        carry /= base;
      }

    return product;
  } //   const Number& operator*(int n)


  const Number shift(int n) const // multiply by BASE^n
  {
    Number result = *this;
    for (int i = 0; i < n; i++)
      result.val.push_front(0);
    return result;
  }

}; // class Number

ostream & operator<<(ostream &outs, const Number & num)
{

//:::::::::::::::::::::::::::::::::::

  list<int>::const_reverse_iterator ir;
  for (ir =num.val.rbegin(); ir != num.val.rend(); ++ir)
    {
      if (*ir == 0) continue;
      else break;
    }

  if (ir == num.val.rend())
    {
      outs << 0;
      return outs;
    }

  for ( ; ir != num.val.rend(); ++ir)
    outs << *ir;
  return outs;

//:::::::::::::::::::::::::::::::::::

//   int i;
//   for (i = num.val.size() -1; i>=0; --i)
//     {
//       if (num.val[i] == 0) continue;
//       else break;
//     }

// //   ++i;
//   if (i == -1)
//     {
//       outs << 0;
//       return outs;
//     }

//   for ( ; i >=0; --i)
//     outs << num.val[i];
//   return outs;

//:::::::::::::::::::::::::::::::::::

}

inline int mod7(int in)
{
  return (in%7);
}

//  template<class T>
//  void swap(T& a, T& b)
//  {
//    T temp;
//    temp = a;
//    a    = b;
//    b    = temp;
//  };


template <typename T>
class Array
{

private:
  vector<vector <T> > data;
  int nrows;
  int ncols;

public:
  Array<T>(int in_nrows, int in_ncols):data(in_nrows)
  {
    nrows = in_nrows;
    ncols = in_ncols;

    for (int i = 0; i < nrows; i++)
      {
        data[i].resize(ncols);
      }
  }
//   Array()
//   {
//     data = NULL;
//     nrows = 0;
//     ncols = 0;
//   }
  Array<T>(const Array<T>& in)
  {
    data = in.data; // we want the pointers to point to the same array
    nrows = in.nrows;
    ncols = in.ncols;
  }



  ~Array()  {  }

  const int get_nrows() const
  {
    return nrows;
  }

  const int get_ncols() const
  {
    return ncols;
  }

  const T& operator()( int irow,  int icol) const
  {
    return data[irow][icol];
  }

  T& operator()( int irow,  int icol)
  {
    return data[irow][icol];
  }


}; //  class Array

// function declarations
const Number Partial_Sum(const Array<unsigned long> *n, const Array<Number> *s, int k, int ten_exp_k,
                         const int n1_rest[], const int s1[]);

void Update_Matrices(const Array<unsigned long> *n_prev, const Array<Number> *s_prev, int k, int ten_exp_k,
                     Array<unsigned long> *n_next, Array<Number> *s_next,  const Array<int>& jprime_arr,
                     const Array<int>& x7, const Array<int>& s7, const int n1[], const int s1[]);

//:::::::::::::::::::::::::::::::::::::::::::

int main(int argc, char * argv[])
{
  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::::: preliminary setup and checking::::::::::::::
  //::::::::::::::::::::::::::::::::::::::::::::::::::::

  // convert the arguments to integers and check on the range

  int num_digits;
  if (argc == 1)
    num_digits = 11;
  else if (argc == 2)
    {
      num_digits = atoi(argv[1]);
      if (num_digits < 2 || num_digits > 200)
        {
          cout <<"Usage: sum7 num_digits\n";
          cout << "num_digits is out of range 2-200: "<< num_digits << endl;
          exit(1);
        }
    }
  else // argc >= 2
    {
      cout <<"Usage: sum7 num_digits\n";
      cout << "You have supplied " << argc << "parameters." << endl;
      exit(1);
    }

  cout << "The number of digits is " << num_digits << "." << endl;

  // create lookup tables for modulo 7 arithmetic

  Array <int> x7   (7,7);  // multiplication
  Array <int> p7   (7,7);  // plus
  Array <int> d7   (7,7);  // division
  Array <int> s7   (7,7);  // subtraction
  Array <int> jprime_arr(7,7);

  const int neg_7[7] = {0, 6, 5, 4, 3, 2, 1};
  const int inv_7[7] = {0, 1, 4, 5, 2, 3, 6}; // the zero element is a place-holder and should never get called

  for (int i=0; i<7; i++)
    for (int j=0; j<7; j++)
      {
        x7(i,j)         = mod7(i*j);
        p7(i,j)         = mod7(i+j);
        s7(i,j)         = mod7(i + neg_7[j]);
        jprime_arr(i,j) = mod7(5*i + 2*j); // for explanation of the 'magic numbers' 5 and 2, see writeup
      }

  for (int i=0; i<7; i++)
    for (int j=1; j<7; j++) // note j starts at 1!
      {
        d7(i,j) = mod7(i * inv_7[j]);
      }

  // create data structure to hold the information at each step.

  Array<unsigned long>       na(7,7);
  Array<unsigned long>       nb(7,7);
  Array<Number>    sa(7,7);
  Array<Number>    sb(7,7);
  
  Array<unsigned long>      *pn_prev;
  Array<unsigned long>      *pn_next;
  Array<Number>   *ps_prev;
  Array<Number>   *ps_next;

  pn_prev = &na;
  pn_next = &nb;
  ps_prev = &sa;
  ps_next = &sb;


  int ten_exp_k = 1; // 10^0, mod 7
  Number sum(0);
  // create tables for first digit (please see writeup for explanations)
  const int n1[]      = {2, 2, 2, 1, 1, 1, 1};
  const int n1_rest[] = {1, 2, 2, 1, 1, 1, 1}; // first digit restricted to be non-zero
  const int s1[]      = {7, 9,11, 3, 4, 5, 6}; 

  sum += s1[0]; // the sum of all 1-digit numbers satisfying the requirements



  // do calculations for the 2nd digit (this is a special case because it can be
  // done faster than other digits). (k = 1)

  ten_exp_k = x7(3,ten_exp_k); //10^1, mod 7

  for (int i = 0; i < 7; i++)
    {
      for (int j = 0 ; j < 7; j++)
        {
          int ind1 = mod7(3*i + 6*j);
          int ind2 = mod7(6*i + 3*j);
          (*pn_next)(i,j) = n1[ind1] * n1[ind2];
          (*ps_next)(i,j) = Number(  s1[ind1]*n1[ind2]*10 + s1[ind2]*n1[ind1]  );
//           cout << "i,j are " << i << " " << j << endl;
//           cout << "ind1, ind2 are " << ind1 << " " << ind2 << endl;
//           cout << " s is " << (*ps_next)(i,j) << endl;
//           cout << " num is " <<  s1[ind1]*n1[ind2]*10 + s1[ind2]*n1[ind1] << endl;
//           cout << "___________" << endl;
        }
    }

  sum += Number(s1[0]*n1[0]).shift(1) + s1[0]*n1_rest[0]; // the sum of all appropriate 2-digit numbers
  swap(pn_prev, pn_next);
  swap(ps_prev, ps_next);
  if (num_digits == 2)
    {
      cout << "2 digit sum is " << sum << endl<< endl;
//        Print_Results();
      exit(0);
    }

  // do calculations for all the digits except the last
  int k;  // k = (number of digits)-1
  for (k = 2; k <= (num_digits-2); k++)
    {
      ten_exp_k = x7(3,ten_exp_k); // 10^k mod 7
      cout << " k = " << k <<"; partial sum so far is " << sum << endl;

      sum += Partial_Sum(pn_prev, ps_prev, k, ten_exp_k, n1_rest, s1);
      Update_Matrices(pn_prev, ps_prev, k, ten_exp_k, pn_next, ps_next, jprime_arr, x7, s7, n1, s1);

      swap(pn_prev, pn_next);
      swap(ps_prev, ps_next);
    }

  // do calculations for the last digit
  ten_exp_k = x7(3,ten_exp_k);
  cout << " (last) k = " << k <<"; partial sum so far is " << sum << endl;
  sum += Partial_Sum(pn_prev, ps_prev, k, ten_exp_k, n1_rest, s1);

//    Print_Results();

  cout << "final sum is : " << sum << endl;
} // int main(int argc, char * argv[])


const Number Partial_Sum(const Array<unsigned long> *n, const Array<Number> *s, int k, int ten_exp_k,
                   const int n1_rest[], const int s1[])
{
  Number sum(0);

  for (int m = 0; m < 7; m++)
    {
      int iprime = mod7(6 * ten_exp_k * m);
      int jprime = mod7(2 * m);

//       cout << "m is " << m << endl;
//       cout << "iprime, jprime are " << iprime << " " << jprime << endl;
//       cout << " s is " << (*s)(iprime, jprime) << endl;

//       Number temp, temp1, temp2, temp3, temp4, temp5, temp6;
//       temp = Number(s1[m] * (*n)(iprime,jprime));
//       cout << "temp = " << temp << endl;
//       temp = Number(s1[m] * (*n)(iprime,jprime)).shift(k);
//       cout << "temp = " << temp << endl<< endl;
//       temp1 = (*s)(iprime,jprime);
//       cout << "temp1 = " << temp1 << endl;
//       temp3 = temp1.operator*( n1_rest[m]);
//       cout << "temp3 = " << temp3 << endl;
//       temp1 + temp;
//       temp2 = temp1 + temp;
//       cout << "temp2 = " << temp2 << endl;
//       sum += temp2;
//       cout << "m =" << m << "sum so far is" << sum << endl;
//       cout << "________________" << endl;
      sum += Number(s1[m] * (*n)(iprime,jprime)).shift(k) +  (*s)(iprime,jprime) * n1_rest[m];
//       cout << "sum is " << sum << endl;
//       cout << "_________________" << endl;
    }

  return sum;
} // Number Partial_Sum

void Update_Matrices(const Array<unsigned long> *n_prev, const Array<Number> *s_prev, int k, int ten_exp_k,
                     Array<unsigned long> *n_next, Array<Number> *s_next,  const Array<int>& jprime_arr,
                     const Array<int>& x7, const Array<int>& s7, const int n1[], const int s1[])
{


  int jprime, iprime;

  for (int i = 0; i < 7; i++)
    for (int j = 0; j < 7; j++)
      {
        (*n_next)(i,j) = 0;
        (*s_next)(i,j) = Number(0);
        for (int m = 0; m < 7; m++)
          {
            iprime = s7(i,x7(ten_exp_k, m));
            jprime = jprime_arr(j,m);
            
            (*n_next)(i,j) += n1[m] * (*n_prev)(iprime,jprime);
            (*s_next)(i,j) += Number(s1[m] * (*n_prev)(iprime,jprime)).shift(k) + (*s_prev)(iprime,jprime) * n1[m];
          } //    for (int m = 0; m < 7; m++)
      }


} //void Update_Matrices
