// sum7_vec.cpp, comes from sum7_p.cpp, with change to vectors instead of arrays.

// gsdebug -- is it meaningful to have const Number as a return type?

// gsdebug -- make n a Number instead of unsigned long? add a operator*(const Number&) const
// gsdebug -- write one regularize() member for the Number class and use it in all the other ones.


/*::::::::::::::::::::::::::
  ::::::::::::::::::::::::::

  (To compile, $> g++ sum7.cpp -o sum7)

  Usage: ./sum7 [n] where n is the maximum number of digits required (default 11)

  sum7.cpp is code that calculates the sum of all numbers between 1 and 10^n that are
  divisible by 7 and have their decimal reverse also divisible by 7.

  It operates in polynomial time (that is, linear in the number of digits n).

::::::::::::::::::::::::::
::::::::::::::::::::::::::*/

#define NDEBUG // if this line is present, assert statements are dropped

// the code starts here

#include <iostream>
#include <vector>
#include <list>
#include <assert.h>

using namespace std;


//:::::::  classes used in the code ::::::::


// this is a class of numbers, represented as a collection of digits.

class Number
{
private:
  list <int> val;
  const static int base = 10;

public:
  friend ostream & operator<<(ostream &outs, const  Number & num);

  // some constructors
  Number::Number();
  Number::Number(const Number& num);
  Number::Number(unsigned long n);

  ~Number() { };

  //::::::::::::::: a bunch of overloaded arithmetic operators:::::::::
  const Number operator+(const Number& n) const;
  const Number operator+(unsigned long n) const;
  void operator+=(const Number& n); // we will not be using this as an rvalue, so no need to return (*this)
  void operator+=(const unsigned long& n); // we will not be using this as an rvalue, so no need to return (*this)
  const Number operator*(unsigned long n) const;
  const Number shift(int n) const; // multiply by BASE^n


}; // class Number


// some constructors:
Number::Number()
{
  val.resize(0, 0);
}
Number::Number(const Number& num)
{
  val  = num.val;
}
Number::Number(unsigned long n)
{
  val.resize(0,0);
  
  do
    {
      val.push_back(n%base);
      n /= base;
    }
  while (n > 0);
}


const Number Number::operator+(const Number& n) const
{
  Number sum;
  int dig_sum;
  list<int>::const_iterator i;
  list<int>::const_iterator j;
  int carry =0;
  const Number * longer;
  const Number * shorter;

  if (base != n.base)
    {
      printf("trying to add two numbers with different base\n");
      exit(1);
    }

  if (n.val.size() < val.size())
    {
      longer = this;
      shorter = &n;
    }
  else
    {
      longer = &n;
      shorter = this;
    }

  for (i = shorter->val.begin(), j = longer->val.begin();
       i != shorter->val.end(); 
       i++, j++)
    {
      dig_sum = (*i) + (*j) + carry;
      sum.val.push_back(dig_sum%base);
      carry = dig_sum/base;
    }

  for ( ; j != longer->val.end(); j++)
    {
      dig_sum = (*j) + carry;
      sum.val.push_back(dig_sum%base);
      carry = dig_sum/base;
    }

  while (carry > 0)
    {
      sum.val.push_back(carry%base);
      carry /= base;
    }

  return sum;
} //   const Number& operator+(Number n, Number n2)

const Number Number::operator+(unsigned long n) const
{
  Number sum;
  sum.val = val;
  unsigned long carry = 0;
  list<int>::iterator i = sum.val.begin();
  *i += n;
  for ( ; i != sum.val.end(); i++)
    {
      *i += carry;
      carry = *i/base;
      *i %= base;
      if (carry == 0)
        break;
    }

  while (carry > 0)
    {
      sum.val.push_back(carry%base);
      carry /= base;
    }

  return sum;
}


void Number::operator+=(const Number& n) // we will not be using this as an rvalue, so no need to return (*this)
{
  int dig_sum;
  list<int>::const_iterator i;
  list<int>::iterator j;
  int carry =0;
  

  if (base != n.base)
    {
      printf("trying to add two numbers with different base\n");
      exit(1);
    }

  if (val.size() < n.val.size())
    val.resize(n.val.size(), 0);
  
  for (i=n.val.begin(), j = val.begin();
       i != n.val.end(); 
       i++, j++)
    {
      (*j) += (*i) + carry;
      
      carry  =  (*j)/base;
      (*j) %= base;
    }
  
  while( j != val.end() && carry >= 1)
    {
      (*j) += carry;
      
      carry  =  (*j)/base;
      (*j) %= base;
      
      j++;
    }
  
  while (carry > 0)
    {
      val.push_back(carry%base);
      carry /= base;
    }
  
} //    const Number& operator+=(Number n)


void Number::operator+=(const unsigned long& n) // we will not be using this as an rvalue, so no need to return (*this)
{
  list<int>::iterator i = val.begin();
  unsigned long carry = 0;
  (*i) += n;
  do
    {
      (*i)+= carry;
      if ((*i) >= base)
        {
          carry   = (*i)/base;
          (*i) %= base;
        }
      i++;
    }
  while (carry >= 1 && i != val.end());
  
  while (carry >= 1)
    {
      val.push_back(carry%base);
      carry /= base;
    }
  
}

const Number Number::operator*(unsigned long n) const
{
  Number product(*this);
  unsigned long carry = 0;
  list<int>::const_iterator i;
  list<int>::iterator j;
  for ( i = val.begin(),  j = product.val.begin();
        i != val.end();
        i++, j++)
    {
      (*j) = (*i) * n;
    }
  
  for (j = product.val.begin(); j != product.val.end(); j++)
    {
      (*j) += carry;
      carry = (*j)/base;
      (*j) %= base;
    }
  
  while (carry >= 1)
    {
      product.val.push_back(carry % base);
      carry /= base;
    }

  return product;
} //   const Number& operator*(int n)


const Number Number::shift(int n) const // multiply by BASE^n
{
  Number result = *this;
  for (int i = 0; i < n; i++)
    result.val.push_front(0);
  return result;
}



ostream & operator<<(ostream &outs, const Number & num)
{
  list<int>::const_reverse_iterator ir;
  for (ir =num.val.rbegin(); ir != num.val.rend(); ++ir)
    {
      if (*ir == 0) continue;
      else break;
    }

  if (ir == num.val.rend())
    {
      outs << 0;
      return outs;
    }

  for ( ; ir != num.val.rend(); ++ir)
    outs << *ir;
  return outs;
}

inline int mod7(int in)
{
  assert(in >= 0);
  return (in%7);
}

//::::::::::::::::::::::::::::::::::::::::::::::::::::


//  A 2 dimensional array class

template <typename T>
class Array
{

private:
  vector<vector <T> > data;
  int num_rows;
  int num_cols;
public:
  Array<T>(int nrows, int ncols);
  ~Array()  {  }


  const T& operator()( int irow,  int icol) const
  {
    assert(irow < num_rows && icol < num_cols);
    return data[irow][icol];
  }

  T& operator()( int irow,  int icol)
  {
    assert(irow < num_rows && icol < num_cols);
    return data[irow][icol];
  }


}; //  class Array

template<typename T>
Array<T>::Array(int nrows, int ncols):data(nrows)
{
  num_rows = nrows;
  num_cols = ncols;
  for (int i = 0; i < nrows; i++)
    {
      data[i].resize(ncols);
    }
}


//::::::::::::::::::::::::::::::::::::::::::::::::::::
// function declarations
//::::::::::::::::::::::::::::::::::::::::::::::::::::
const Number Partial_Sum(const Array<unsigned long> *n, const Array<Number> *s, int k, int ten_exp_k,
                         const int n1_rest[], const int s1[]);

void Update_Matrices(const Array<unsigned long> *n_prev, const Array<Number> *s_prev, int k, int ten_exp_k,
                     Array<unsigned long> *n_next, Array<Number> *s_next,  const Array<int>& jprime_arr,
                     const Array<int>& x7, const Array<int>& s7, const int n1[], const int s1[]);
void Print_Results(int num_digits, const Number sum);

//:::::::::::::::::::::::::::::::::::::::::::

int main(int argc, char * argv[])
{
  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::::: preliminary setup and checking::::::::::::::
  //::::::::::::::::::::::::::::::::::::::::::::::::::::

  // convert the arguments to integers and check on the range

  int num_digits;
  if (argc == 1)
    num_digits = 11;
  else if (argc == 2)
    {
      num_digits = atoi(argv[1]);
      if (num_digits < 2 || num_digits > 200)
        {
          cout <<"Usage: sum7 num_digits\n";
          cout << "num_digits is out of range 2-200: "<< num_digits << endl;
          exit(1);
        }
    }
  else // argc >= 2
    {
      cout <<"Usage: sum7 num_digits\n";
      cout << "You have supplied " << argc << "parameters." << endl;
      exit(1);
    }

  cout << "The number of digits is " << num_digits << "." << endl;

  // create lookup tables for modulo 7 arithmetic

  Array <int> x7   (7,7);  // multiplication
  Array <int> s7   (7,7);  // subtraction
  Array <int> jprime_arr(7,7);

  const int neg_7[7] = {0, 6, 5, 4, 3, 2, 1};

  for (int i=0; i<7; i++)
    for (int j=0; j<7; j++)
      {
        x7(i,j)         = mod7(i*j);
        s7(i,j)         = mod7(i + neg_7[j]);
        jprime_arr(i,j) = mod7(5*i + 2*j); // for explanation of the 'magic numbers' 5 and 2, see writeup
      }

  // create data structure to hold the information at each step.  there are two sets because at each step
  // we calculate the next values from the previous ones.

  Array<unsigned long>       na(7,7); // na(i,j) and nb(i,j) is the count of numbers so far such that
  Array<unsigned long>       nb(7,7); // their remainder modulo 7 is i and the remainder of their reverse is j

  Array<Number>    sa(7,7); //sa is the sum of all such numbers
  Array<Number>    sb(7,7);

  // we create pointers to these array to make it easier to swap them at each step without excess copying
  Array<unsigned long>      *pn_prev = &na;
  Array<unsigned long>      *pn_next = &nb;
  Array<Number>   *ps_prev = &sa;
  Array<Number>   *ps_next = &sb;

  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  // Start the calculations, digit by digit
  //::::::::::::::::::::::::::::::::::::::::::::::::::::

  // do the calculations for step one (one digit numbers)

  int ten_exp_k = 1; // 10^0, mod 7
  Number sum(0);
  // create tables for first digit (please see writeup for explanations)
  const int n1[]      = {2, 2, 2, 1, 1, 1, 1};
  const int n1_rest[] = {1, 2, 2, 1, 1, 1, 1}; // first digit restricted to be non-zero
  const int s1[]      = {7, 9,11, 3, 4, 5, 6}; 

  sum += s1[0]; // the sum of all 1-digit numbers satisfying the requirements


  // do calculations for the 2nd digit (this is a special case because it can be
  // done faster than other digits). (k = 1)

  ten_exp_k = x7(3,ten_exp_k); //10^1, mod 7

  for (int i = 0; i < 7; i++)
    {
      for (int j = 0 ; j < 7; j++)
        {
          int ind1 = mod7(3*i + 6*j);
          int ind2 = mod7(6*i + 3*j);
          (*pn_next)(i,j) = n1[ind1] * n1[ind2];
          (*ps_next)(i,j) = Number(  s1[ind1]*n1[ind2]*10 + s1[ind2]*n1[ind1]  );
        }
    }

  sum += Number(s1[0]*n1[0]).shift(1) + s1[0]*n1_rest[0]; // the sum of all appropriate 2-digit numbers
  swap(pn_prev, pn_next);
  swap(ps_prev, ps_next);
  if (num_digits == 2)
    {
      cout << "2 digit sum is " << sum << endl<< endl;
      Print_Results(num_digits, sum);
      exit(0);
    }

  // do calculations for all the digits except the last

  int k;  // k = (number of digits)-1
  for (k = 2; k <= (num_digits-2); k++)
    {
      ten_exp_k = x7(3,ten_exp_k); // 10^k mod 7
//       cout << " k = " << k <<"; partial sum so far is " << sum << endl;

      sum += Partial_Sum(pn_prev, ps_prev, k, ten_exp_k, n1_rest, s1);
      Update_Matrices(pn_prev, ps_prev, k, ten_exp_k, pn_next, ps_next, jprime_arr, x7, s7, n1, s1);

      swap(pn_prev, pn_next);
      swap(ps_prev, ps_next);
    }

  // do calculations for the last digit
  ten_exp_k = x7(3,ten_exp_k);
//   cout << " k = " << k <<"; partial sum so far is " << sum << endl;
  sum += Partial_Sum(pn_prev, ps_prev, k, ten_exp_k, n1_rest, s1);

  Print_Results(num_digits, sum);

  cout << "final sum is : " << sum << endl;
} // int main(int argc, char * argv[])


const Number Partial_Sum(const Array<unsigned long> *n, const Array<Number> *s, int k, int ten_exp_k,
                   const int n1_rest[], const int s1[])
{
  Number sum(0);

  for (int m = 0; m < 7; m++)
    {
      int iprime = mod7(6 * ten_exp_k * m);
      int jprime = mod7(2 * m);

      sum += Number(s1[m] * (*n)(iprime,jprime)).shift(k) +  (*s)(iprime,jprime) * n1_rest[m];
    }

  return sum;
} // Number Partial_Sum

void Update_Matrices(const Array<unsigned long> *n_prev, const Array<Number> *s_prev, int k, int ten_exp_k,
                     Array<unsigned long> *n_next, Array<Number> *s_next,  const Array<int>& jprime_arr,
                     const Array<int>& x7, const Array<int>& s7, const int n1[], const int s1[])
{


  int jprime, iprime;

  for (int i = 0; i < 7; i++)
    for (int j = 0; j < 7; j++)
      {
        (*n_next)(i,j) = 0;
        (*s_next)(i,j) = Number(0);
        for (int m = 0; m < 7; m++)
          {
            iprime = s7(i,x7(ten_exp_k, m));
            jprime = jprime_arr(j,m);
            
            (*n_next)(i,j) += n1[m] * (*n_prev)(iprime,jprime);
            (*s_next)(i,j) += Number(s1[m] * (*n_prev)(iprime,jprime)).shift(k) + (*s_prev)(iprime,jprime) * n1[m];
          } //    for (int m = 0; m < 7; m++)
      }


} //void Update_Matrices

void Print_Results(int num_digits, const Number sum)
{
  cout << "::::::::::::::::::::::::::::::::::::" << endl;
  cout << "For " << num_digits <<" digits, the sum is :" << endl;
  cout << sum << endl << endl;
  cout << "::::::::::::::::::::::::::::::::::::" << endl;
} // void Print_Results(int k, const Number sum)
