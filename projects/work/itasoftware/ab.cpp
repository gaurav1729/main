//ab.cpp

#include <iostream>
#include <string>
#include <vector>
#include <map>
using namespace std;

#define CONSTRUCTOR
#define DESTRUCTOR
#define FUNCTOR
#define VIRTUAL

const int ALIKE            =  0; // for strcmp
const int NOT_IN_FOCUS     = -1; // which member of the vector is in focus
const int NULL             =  0;

enum Email_Type {home_email, work_email};
enum Phone_Type {cell_phone, home_phone, work_phone};

class Super;

//::::::::::::::::::::::::::::::::::::::::::::::::

VIRTUAL class Base
{
protected:
  bool selected;
  bool in_focus_chain;
  bool in_buffer;

  string data_type; // perhaps make this an enum (email, phone, etc.)
  Super *parent;
  int indent;
  string val; // also the key if this is part of a map
public:
  virtual bool contains(string in);
  virtual bool is_last_focus()=0;
  virtual void print()=0;
  virtual void display(Screen &)=0;
  virtual void edit()=0;
  virtual void user_initialize()=0; // ask the right questions to the user
  // select *this* class, get parent to do book-keeping:
  void select()
  {
    selected = true;
    *parent->select_response();
  } //    void select()

  // the parent class has chosen this one for focus; do what needs to be done.
  void focus_response()
  {
    in_focus_chain = true;
  }

  void buffer() { in_buffer = true; }
  void unbuffer() { in_buffer = false; }
  void is_in_buffer() { return in_buffer; }
} // class Base

bool Base::contains(const string search_string) const
{
  string lower_search_string = lower_case(search_string);
  string lower_val = lower_case(val);
  if (lower_val.find(lower_search_string, 0) == string::npos)
    return false;
  else
    return true;

}

FUNCTOR class to_lower // will be used in most implementations of 'contains'
{
public:
  char operator() (char c) const
  {
    return ('A' <= c && c <= 'Z') ? (c-'A'+'a') : c  ;
  } //    char operator() (char c) const
}; //  FUNCTOR class to_lower


string lower_case(const string in) // will be used in most implementations of 'contains'
{
  string out = in;
  transform(in.begin(), in.end(), out.begin(), to_lower());

  return out;
} //  string lower_case(string in)

//::::::::::::::::::::::::::::::::::::::::::::::::

// this class has children
VIRTUAL class Super:class Base
{
protected:
  map<string, Base *> child_classes;
  vector<string> selected_children;
  Base * p_focused_sub; // or rather, make this an iterator; address of sub that is focused; initialized to NULL
  bool expanded; // do we display this in expanded format?
public:
  void select_response(string); // the lower class has selected itself; update selected_children
  //choose the lower class keyed by string
  void focus(string in)
  {
    child_class[in].focus_response();
    p_focused_sub = Get_Iterator(in);
  }
  //write get_iterator(instring)


  virtual void add_field(); // each instance has to know what the possible additions
                            // are and present a menu


  bool is_last_focus()
  {
    return (  (p_focused_sub == NULL) && in_focus_chain   );
  }

  string next_child(); // return key of next child to be in focus

} // class Super:class Base




//::::::::::::::::::::::::::::::::::::::::::::::::

// this class has no children
VIRTUAL class Field:class Base
{
public:
  bool is_last_focus()
  {
    return in_focus_chain; // there're no children --if we're in focus, we're last.
  }
} // class Field:class Base

//::::::::::::::::::::::::::::::::::::::::::::::::

class Family::class Super
{
protected:
  map<string, Super *> people; // could be base *?
  map<string, Base *> common; // common phones, emails, addresses, etc.

public:
  void display(Screen &);


  // needs a display, edit, user_initialize, etc.
} // class Family

//::::::::::::::::::::::::::::::::::::::::::::::::

// this class for each line in the output/printout

class Line
{
protected:
  string val;
  bool header;
  bool footer;
  bool data;
  bool selected;
  bool in_focus;
public:
  string get_val() { return val; }
  bool is_header() { return header; }
  bool is_footer() { return footer; }
  bool is_data() { return data; }
  bool is_selected() { return selected; }
  bool is_in_focus() { return in_focus; }
} //  class Line

template <class container>
class Screen
{
protected:
  container<Line> lines;
  container::iterator print_to; // points to where in the data we are printing
  int max_lines;
  container::iterator display_start; // which lines in the data are going to be 
  container::iterator display_end;   // displayed.
public:
  void change_print_to(int){};
  // need to be able to append data lines, show only max_lines at a time
} //  class Screen
