/*::::::::::::::::::::::::::
  sum7_bignum.cpp
  written by Gaurav Shah
  ::::::::::::::::::::::::::

  sum7_bignum.cpp calculates the sum of all numbers between 1 and 10^n that are
  divisible by 7 and have their decimal reverse also divisible by 7.

  I've tried to make it as user friendly as possible.  However, there are
  aspects to the algorithm that cannot be easily explained in comments.  Please
  see the documentation sent alongside for full explanations.

  (To compile from the Linux command line, $> g++ sum7_bignum.cpp -o sum7_bignum)

  Usage: ./sum7_bignum [n] where n is the maximum number of digits required
  (default 11)

  This version allows the calculation of arbitrarily large numbers of digits
  (for example, it can calculate the relevant sum up to 10^40 in less than half
  a second).  This was achieved by creating a custom Number class that has no
  limits on the number of digits.

  This code operates in POLYNOMIAL TIME (that is, polynomial in the number of
  digits n).

  For convenience, the header and implementation have been combined into one
  file.

::::::::::::::::::::::::::
::::::::::::::::::::::::::*/

#define NDEBUG // if this line is present, assert statements are dropped

#include <iostream>
#include <vector>
#include <list>
#include <assert.h>
#include <time.h>
#include <stdlib.h>

using namespace std;


//:::::::  classes used in the code ::::::::


// this is a class of numbers, represented as a collection of digits.

class Number
{
private:
  list <int> val;
  const static int base = 10;

public:
  friend ostream & operator<<(ostream &outs, const  Number & num);
  friend void regularize(Number& num, int base);

  // some constructors
  Number();
  Number(const Number& num);
  Number(unsigned long n);

  // at present, no need to make the destructor virtual, which would 
  // probably slow things down.
  ~Number() { };

  //::::::::::::::: a bunch of overloaded arithmetic operators:::::::::
  //::: We want to be able to add and multiply Number objects with each other,
  //::: and with ordinary integers.
  const Number operator+(const Number& n) const;
  const Number operator*(const Number& n) const;
  const Number operator+(unsigned long n) const;
  // we will not be using += as an rvalue, so no need to return (*this)
  void operator+=(const Number& n); 
  void operator+=(const unsigned long& n);
  const Number operator*(unsigned long n) const;
  const Number shift(int n) const; // multiply by BASE^n (10^n in practise)


}; // class Number


// some constructors:
Number::Number()
{
  val.resize(0, 0);
}
Number::Number(const Number& num)
{
  val  = num.val;
}
Number::Number(unsigned long n)
{
  val.resize(0,0);
  
  do
    {
      val.push_back(n%base);
      n /= base;
    }
  while (n > 0);
}

// add two Numbers, digit by digit, taking care of carry overs.
const Number Number::operator+(const Number& n) const
{
  Number sum;
  int dig_sum;
  list<int>::const_iterator i;
  list<int>::const_iterator j;
  int carry =0;
  const Number * longer;
  const Number * shorter;

  if (base != n.base)
    {
      cout << "trying to add two numbers with different base\n";
      exit(1);
    }

  if (n.val.size() < val.size())
    {
      longer = this;
      shorter = &n;
    }
  else
    {
      longer = &n;
      shorter = this;
    }

  // this loop lasts as long as both numbers have digits
  for (i = shorter->val.begin(), j = longer->val.begin();
       i != shorter->val.end(); 
       i++, j++)
    {
      dig_sum = (*i) + (*j) + carry;
      sum.val.push_back(dig_sum%base);
      carry = dig_sum/base;
    }

  // this loop is for significant digits found on only the longer Number
  for ( ; j != longer->val.end(); j++)
    {
      dig_sum = (*j) + carry;
      sum.val.push_back(dig_sum%base);
      carry = dig_sum/base;
    }

  // if there is any carry over after that, convert it into digits
  while (carry > 0)
    {
      sum.val.push_back(carry%base);
      carry /= base;
    }

  return sum;
} //   const Number& operator+(Number n, Number n2)

// Number + long
const Number Number::operator+(unsigned long n) const
{
  Number sum;
  sum.val = val;
  unsigned long carry = 0;
  list<int>::iterator i = sum.val.begin();
  *i += n;
  for ( ; i != sum.val.end(); i++)
    {
      *i += carry;
      carry = *i/base;
      *i %= base;
      if (carry == 0)
        break;
    }

  while (carry > 0)
    {
      sum.val.push_back(carry%base);
      carry /= base;
    }

  return sum;
}

// we will not be using += as an rvalue, so no need to return (*this)
// Note that this could also have been written using the + operator
void Number::operator+=(const Number& n)
{
  int dig_sum;
  list<int>::const_iterator i;
  list<int>::iterator j;
  int carry =0;
  

  if (base != n.base)
    {
      cout << "trying to add two numbers with different base\n";
      exit(1);
    }

  if (val.size() < n.val.size())
    val.resize(n.val.size(), 0);
  
  for (i=n.val.begin(), j = val.begin();
       i != n.val.end(); 
       i++, j++)
    {
      (*j) += (*i) + carry;
      
      carry  =  (*j)/base;
      (*j) %= base;
    }
  
  while( j != val.end() && carry >= 1)
    {
      (*j) += carry;
      
      carry  =  (*j)/base;
      (*j) %= base;
      
      j++;
    }
  
  while (carry > 0)
    {
      val.push_back(carry%base);
      carry /= base;
    }
  
} //    const Number& operator+=(Number n)

// we will not be using this as an rvalue, so no need to return (*this)
void Number::operator+=(const unsigned long& n) 
{
  list<int>::iterator i = val.begin();
  unsigned long carry = 0;
  (*i) += n;
  do
    {
      (*i)+= carry;
      if ((*i) >= base)
        {
          carry   = (*i)/base;
          (*i) %= base;
        }
      i++;
    }
  while (carry >= 1 && i != val.end());
  
  while (carry >= 1)
    {
      val.push_back(carry%base);
      carry /= base;
    }
  
}

// Number * long int
const Number Number::operator*(unsigned long n) const
{
  Number product(*this);
  unsigned long carry = 0;
  list<int>::const_iterator i;
  list<int>::iterator j;
  for ( i = val.begin(),  j = product.val.begin();
        i != val.end();
        i++, j++)
    {
      (*j) = (*i) * n;
    }
  
  for (j = product.val.begin(); j != product.val.end(); j++)
    {
      (*j) += carry;
      carry = (*j)/base;
      (*j) %= base;
    }
  
  while (carry >= 1)
    {
      product.val.push_back(carry % base);
      carry /= base;
    }

  return product;
} //   const Number& operator*(int n)


// multiply by BASE^n
const Number Number::shift(int n) const
{
  Number result = *this;
  for (int i = 0; i < n; i++)
    result.val.push_front(0);
  return result;
}

// Number * Number
const Number Number::operator*(const Number& n) const
{
  Number product;
  list<int>::const_iterator l_low, l_high, s_low, s_high;
  list<int>::const_iterator l, s;
  list<int>::const_iterator l_last, s_last;

  const Number* longer;
  const Number* shorter;

  product.val.resize(0,0);

  if (n.val.size() < val.size())
    {
      longer = this;
      shorter = &n;
    }
  else
    {
      longer = &n;
      shorter = this;
    }
  
  /*
    We multiply and then add all digits whose place numbers add up to two (i.e.,
    the units digits).  Then multiply and add all digits whose place numbers add
    up to three, etc.  Iterate until we have multiplied and added all pairs of
    digits, giving their product the correct place value in the final sum.
   */
  l_low = l_high = longer->val.begin();
  s_low = s_high = shorter->val.begin();

  // l_last and s_last point to the last element of the list  
  l_last = longer->val.end();  --l_last;
  s_last = shorter->val.end(); --s_last;

  int sum;
  // the outer loop is the one that iterates over the sum of the place values
  for (;;)
    {
      sum = 0;
      // this loop multiples and adds all pairs of digits, the sum of whose
      // place values is fixed.
      for (l = l_high, s = s_low;
           ;
           --l, ++s)
        {
          sum += (*l) * (*s);
          if ( (l==l_low) || (s == s_high) )
            break;
        }
      product.val.push_back(sum);

      // now we advance to the next most significant place in the product
      if (s_low == s_last)
        {
          break;
        }
      else if (l_high == l_last)
        {
          ++l_low;
          ++s_low;
        }
      else if (s_high == s_last)
        {
          ++l_high;
        }
      else
        {
          ++s_high;
          ++l_high;
        }

    } // for (;;)
      
  regularize(product, base); // handle elements of the vector that are greater
                             // than "base"
  return product;
} // const Number Number::operator*(const Number& n) const

// print a Number
ostream & operator<<(ostream &outs, const Number & num)
{
  list<int>::const_reverse_iterator ir;
  for (ir =num.val.rbegin(); ir != num.val.rend(); ++ir)
    {
      if (*ir == 0) continue;
      else break;
    }

  if (ir == num.val.rend())
    {
      outs << 0;
      return outs;
    }

  for ( ; ir != num.val.rend(); ++ir)
    outs << *ir;
  return outs;
}

// given a list of integers, transform it into a list of digits
// by carrying over (each resulting digits is less than BASE)
void regularize(Number& num, int base)
{
  int carry = 0;
  list<int>::iterator i = num.val.begin();

  for (i = num.val.begin(); i != num.val.end(); ++i)
    {
      carry = (*i)/base;
      (*i) %= base;
    }

  while (carry > 0)
    {
      num.val.push_back(carry % base);
      carry /= base;
    }
}

// Just what it says!  Returns the remainder modulo 7 of the input.
// (Inlined because it's small and gets called a lot -- let's hope 
// the compiler actually cares!)
inline int mod7(int in)
{
  assert(in >= 0);
  return (in%7);
}

//::::::::::::::::::::::::::::::::::::::::::::::::::::


//  A 2 dimensional array class.  We will use it later to
// store intermediate data.

template <typename T>
class Array
{

private:
  vector<vector <T> > data;
  int num_rows;
  int num_cols;
public:
  Array<T>(int nrows, int ncols);
  ~Array()  {  }

  // data access operators (they come in pairs, for setting and getting!)
  const T& operator()( int irow,  int icol) const
  {
    assert(irow < num_rows && icol < num_cols);
    return data[irow][icol];
  }

  T& operator()( int irow,  int icol)
  {
    assert(irow < num_rows && icol < num_cols);
    return data[irow][icol];
  }

}; //  class Array


// Constructor
template<typename T>
Array<T>::Array(int nrows, int ncols):data(nrows)
{
  num_rows = nrows;
  num_cols = ncols;
  for (int i = 0; i < nrows; i++)
    {
      data[i].resize(ncols);
    }
}


//::::::::::::::::::::::::::::::::::::::::::::::::::::
// function declarations
//::::::::::::::::::::::::::::::::::::::::::::::::::::
const Number Partial_Sum(const Array<Number> *n, const Array<Number> *s, int k,
                         int ten_exp_k, const int n1_rest[], const int s1[]);

void Update_Matrices(const Array<Number> *n_prev, const Array<Number> *s_prev,
                     int k, int ten_exp_k, Array<Number> *n_next,
                     Array<Number> *s_next,  const Array<int>& jprime_arr,
                     const Array<int>& x7, const Array<int>& s7, const int n1[],
                     const int s1[]);
void Print_Results(int num_digits, const Number sum);

//:::::::::::::::::::::::::::::::::::::::::::

int main(int argc, char * argv[])
{
  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::::: preliminary setup and checking::::::::::::::
  //::::::::::::::::::::::::::::::::::::::::::::::::::::

  // convert the arguments to integers and check on the range

  int num_digits;
  if (argc == 1)
    num_digits = 11;
  else if (argc == 2)
    {
      num_digits = atoi(argv[1]);
      if (num_digits < 2 || num_digits > 200)
        {
          cout <<"Usage: sum7 num_digits\n";
          cout << "num_digits is out of range 2-200: "<< num_digits << endl;
          exit(1);
        }
    }
  else // argc >= 2
    {
      cout <<"Usage: sum7 num_digits\n";
      cout << "You have supplied " << argc << "parameters." << endl;
      exit(1);
    }

  // create lookup tables for faster modulo 7 arithmetic

  Array <int> x7   (7,7);  // multiplication
  Array <int> s7   (7,7);  // subtraction
  Array <int> jprime_arr(7,7); // please refer to documentation

  const int neg_7[7] = {0, 6, 5, 4, 3, 2, 1}; // e.g. -1 = 6 modulo 7

  // populate the tables
  for (int i=0; i<7; i++)
    for (int j=0; j<7; j++)
      {
        x7(i,j)         = mod7(i*j);
        s7(i,j)         = mod7(i + neg_7[j]);
        // for explanation of the 'magic numbers' 5 and 2 below, see writeup
        jprime_arr(i,j) = mod7(5*i + 2*j); 
      }

  // create data structure to hold the information at each step.  there are TWO
  // sets because at each step we calculate the next values from the previous
  // ones.

  // na(i,j) and nb(i,j) is the count of numbers so far such that
  // their remainder modulo 7 is i and the remainder of their reverse is j
  Array<Number>    na(7,7); 
  Array<Number>    nb(7,7);

  //sa and sb are the sum of all such numbers
  Array<Number>    sa(7,7);
  Array<Number>    sb(7,7);

  // we create pointers to these array to make it easier to swap them at each
  // step without excess copying
  Array<Number>   *pn_prev = &na;
  Array<Number>   *pn_next = &nb;
  Array<Number>   *ps_prev = &sa;
  Array<Number>   *ps_next = &sb;

  //::::::::::::::::::::::::::::::::::::::::::::::::::::
  // Start the calculations, digit by digit
  //::::::::::::::::::::::::::::::::::::::::::::::::::::

  // do the calculations for step one (one digit numbers)

  int ten_exp_k = 1; // 10^0, mod 7
  Number sum(0);

  //:::::: create tables for first digit (please see writeup for explanations)
  // number of one digit numbers whose remainder mod 7 is m
  const int n1[]      = {2, 2, 2, 1, 1, 1, 1};
  // same, except that first digit restricted to be non-zero
  const int n1_rest[] = {1, 2, 2, 1, 1, 1, 1};
  // sum of these numbers
  const int s1[]      = {7, 9,11, 3, 4, 5, 6};

  sum += s1[0]; // the sum of all 1-digit numbers satisfying the requirements


  // do calculations for the 2nd digit (this is a special case because it can be
  // done faster than other digits). (k = 1)

  ten_exp_k = x7(3,ten_exp_k); //10^1, mod 7

  for (int i = 0; i < 7; i++)
    {
      for (int j = 0 ; j < 7; j++)
        {
          // if the 2 digit number has remainder mod 7 of i and reversed
          // remainder j, then its digits are ind1 and ind2.
          int ind1 = mod7(3*i + 6*j);
          int ind2 = mod7(6*i + 3*j);
          (*pn_next)(i,j) = n1[ind1] * n1[ind2];
          (*ps_next)(i,j) = Number( s1[ind1]*n1[ind2]*10 + s1[ind2]*n1[ind1] );
        }
    }

  // the sum of all appropriate 2-digit numbers:
  sum += Number(s1[0]*n1[0]).shift(1) + s1[0]*n1_rest[0];

  // the next round of calculations are based on the matrices computed this
  // time.
  swap(pn_prev, pn_next);
  swap(ps_prev, ps_next);
  if (num_digits == 2)
    {
      Print_Results(num_digits, sum);
      exit(0);
    }

  // do calculations for all the digits except the last

  int k;  // k = (number of digits)-1
  for (k = 2; k <= (num_digits-2); k++)
    {
      ten_exp_k = x7(3,ten_exp_k); // 10^k mod 7

      sum += Partial_Sum(pn_prev, ps_prev, k, ten_exp_k, n1_rest, s1);
      Update_Matrices(pn_prev, ps_prev, k, ten_exp_k, pn_next, ps_next,
                      jprime_arr, x7, s7, n1, s1);

      swap(pn_prev, pn_next);
      swap(ps_prev, ps_next);
    }

  // do calculations for the last digit
  ten_exp_k = x7(3,ten_exp_k);
  sum += Partial_Sum(pn_prev, ps_prev, k, ten_exp_k, n1_rest, s1);

  Print_Results(num_digits, sum);

} // int main(int argc, char * argv[])


// Calculate the sum of numbers that have k digits that satisfy the conditions
const Number Partial_Sum(const Array<Number> *n, const Array<Number> *s, int k,
                         int ten_exp_k, const int n1_rest[], const int s1[])
{
  Number sum(0);

  for (int m = 0; m < 7; m++)
    {
      int iprime = mod7(6 * ten_exp_k * m);
      int jprime = mod7(2 * m);

      sum += Number((*n)(iprime,jprime) * s1[m]).shift(k) 
        +  ((*s)(iprime,jprime) * n1_rest[m]);
    }

  return sum;
} // Number Partial_Sum

// Update the sum and count matrices (n_next, s_next) which will be used
// for the next step.  For full explanation of the algorithm, please
// see the writeup.
void Update_Matrices(const Array<Number> *n_prev, const Array<Number> *s_prev,
                     int k, int ten_exp_k, Array<Number> *n_next,
                     Array<Number> *s_next,  const Array<int>& jprime_arr,
                     const Array<int>& x7, const Array<int>& s7,
                     const int n1[], const int s1[])
{


  int jprime, iprime;

  for (int i = 0; i < 7; i++)
    for (int j = 0; j < 7; j++)
      {
        (*n_next)(i,j) = 0;
        (*s_next)(i,j) = Number(0);
        for (int m = 0; m < 7; m++)
          {
            iprime = s7(i,x7(ten_exp_k, m)); // i - m*10^k
            jprime = jprime_arr(j,m);        // 5*j + 2*m
            
            (*n_next)(i,j) += (*n_prev)(iprime,jprime) * n1[m];
            (*s_next)(i,j) += Number((*n_prev)(iprime,jprime) * s1[m]).shift(k)
              + (*s_prev)(iprime,jprime) * n1[m];
          }
      }


} //void Update_Matrices

// pretty printing
void Print_Results(int num_digits, const Number sum)
{
  cout << "::::::::::::::::::::::::::::::::::::" << endl;
  cout << "For " << num_digits <<" digits, the sum is :" << endl;
  cout << sum << endl << endl;
  cout << "The time taken was " << double(clock())/CLOCKS_PER_SEC 
       << " seconds." << endl;
  cout << "::::::::::::::::::::::::::::::::::::" << endl;
} // void Print_Results(int k, const Number sum)
