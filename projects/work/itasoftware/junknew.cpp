#include <iostream>
#include <vector>

using namespace std;
 
 template<typename T>  // See section on templates for more
 class Matrix {
 public:
   Matrix(int nrows, int ncols);
   // Throws a BadSize object if either size is zero
   class BadSize { };
 
   // No need for any of The Big Three!
 
   // Access methods to get the (i,j) element:
   T&       operator() (int i, int j);       
   const T& operator() (int i, int j) const;
   // These throw a BoundsViolation object if i or j is too big
   class BoundsViolation { };
 
   int nrows() const;  // #rows in this matrix
   int ncols() const;  // #columns in this matrix
 
 private:
   vector<vector<T> > data_;
 };
 
 template<typename T>
 inline int Matrix<T>::nrows() const
 { return data_.size(); }
 
 template<typename T>
 inline int Matrix<T>::ncols() const
 { return data_[0].size(); }
 
 template<typename T>
 inline T& Matrix<T>::operator() (int row, int col)
 {

   return data_[row][col];
 }
 
 template<typename T>
 inline const T& Matrix<T>::operator() (int row, int col) const
 {

   return data_[row][col];
 }
 
 template<typename T>
 Matrix<T>::Matrix(int nrows, int ncols)
   : data_ (nrows)
 {
   for (int i = 0; i < nrows; ++i)
     data_[i].resize(ncols);
 } 

int main(int argc, char* argv[])
{
  Matrix<int> temp(5,5);

  for (int i=0; i<5; i++)
    for (int j=0; j<5; j++)
      temp(i,j) = i+j;

  for (int i=0; i<5; i++)
    {
      for (int j=0; j<5; j++)
        cout << temp(i,j);
      cout << endl;
    }


} // void main(int argc, char* argv[])
