// sum7.cpp

// gsdebug -- do we have to define an array to represent the numbers, or is the sum sufficient?
// gsdebug -- how do we defined int(Number) conversions?
// gsdebug -- given a int** a; how do we reshape it into a double array?

/*::::::::::::::::::::::::::
  ::::::::::::::::::::::::::

sum7.cpp is code that calculates the sum of all numbers between 1 and 10^n that are
divisible by 7 and have their decimal reverse also divisible by 7.

mod7.h is code that does some of the work of mod 7 arithmetic.

.......................
The algorithm is as follows:

1) Divide the n digits into a certain number of blocks as evenly as possible
   (symmetrically?).  For example, 11 digits could be 4+3+4.  Calculate the
   remainder mod 7 for each block of code (e.g.: 1, rem(10^4,7), rem(10^7,7)).

2) In a loop from one to each different sized block, for each number calculate
   the remainder when divided by 7 and the remainder when the reverse is divided
   by 7.  Add that number to a matrix keyed by remainder and reverse remainder.
   Keep the track of the count of each of these numbers also.

3) Construct the linear equation from the remainders of each block to ensure
   that the whole number is divisible by 7.  Do the same with the reverse
   remainders.

4) For each allowable value of the remainders, search for all the allowable
   values of the reverse remainders.  For each value that satisfies the both
   equations, for a given block add to the running sum the sum from the matrix
   for those values of remainder and reverse remainder multiplied by the product
   of the counts for the remaining blocks.

5) Add the sums for each block multiplied by the appropriate weight (Sigma 10^ni*si)

........................

The structure of the code is as follows:

1) A struct (call it remainder_data) that has two fields -- count and sum.

2) An array of these structs (7x7).  It would be nice to have the number of
   dimensions be flexible, but that might not be easy.

3) Multiple instances of this array, one for each sized block (say one each for
   10^3, 10^4)

4) Mod 7 arithmetic type: Takes an int and give the mod 7 remainder, mod 7
   reverse remainder.  Take an int and make a constructor.  Add, subtract and
   multiply mod sevens.  Perhaps use them for indexing into the "array" of
   remainder_data.  Check for equality of mod sevens, and equality of mod sevens
   and ints.  Define mod7++.

5) An array of ints to represent the numbers.  Define adding, multiplying by a
   factor of 10^n, being able to check if it's divisible by seven.
 
6) An array of 3^n base seven, useful for part 5 above and for checking to see
   if the combination of the clumps is divisible by 7.

::::::::::::::::::::::::::
::::::::::::::::::::::::::*/


// the code starts here

#include <iostream>
#include <list>
#include <vector>
#include <math.h>
#include <time.h>
#include <functional>

using namespace std;

//:::::::: parameters :::::::::::::::::::



//:::::::  classes used in the code ::::::::
// this is a class of base 7 numbers; need to be able to convert from base 10 to base 7, 
// add, subtract, convert from base 7 to base 10, check for equality, equality between mod7 and base 10.
// we try to inline as much as possible.

class mod7
{

private:
  static int mod7_inverse[7] = {0, 1, 4, 5, 2, 3, 6};
  // note that the zero in the first place, while wrong, is merely a place
  // holder and should never get called
public:
  int val;
  // some constructors
  mod7()         { val = 0; }
  mod7(int n10)  { val = n10 % 7; }
  mod7(mod7 n7)  { val = n7.val; }

  // some overloaded operators
  bool operator== (int n10)
  { return (val == (n10%7)); }

  bool operator== (mod7 n7)
  { return (val == (n7.val)); }

  const mod7& operator+(mod7 n)
  { return mod7((n.val + val)%7); }

  const mod7& operator-(mod7 n)
  { return mod7((val - n.val)%7); }

  const mod7& operator*(mod7 n)
  { return mod7((n.val*val)%7);  }

  const mod7& operator/(mod7 n)
  { return mod7(val*mod7_inverse[n.val]); }

} // class mod7

ofstream & operator<<(ofstream &outs, const mod7 & num)
{
  outs << num.val;
  return outs;
}


// this is a class of numbers, represented as a vector of base BASE digits.

// need: constructor, addition, ++, streaming, ==

// T is the class of the elements, N is the size of the vector initially, BASE
// is the base of the number system (10 or 7 in this case)
template <int N=1, int BASE=10>
class Number
{
private:
  vector <int> val;
  static Number <N, BASE>number_one = Number<N,BASE>(1);
public:
  friend ofstream & operator<<(ofstream &outs, const Number<N, BASE> & num);
  // some constructors:
  Number<N, BASE>() { val.resize(N); }
  Number<N, BASE>(unsigned long n)
  {
    val = vector(1);
    int r;
    do
      {
        r = n%BASE;
        n = n/BASE;
        val.push_back(r);
      }
    while (n > 0);
  }


  const Number<N, BASE>& operator+(Number<N, BASE> n) // gsdebug put this outside so not inlined!
  {
    Number <N, BASE>sum;
    int dig_sum;
    int i;
    int carry =0;
    Number<N, BASE> *longer, *shorter;
    if (n.val.size() < val.size())
      {
        longer = this;
        shorter = &n;
      }
    else
      {
        longer = &n;
        shorter = this;
      }

    for (i = 0; i < shorter->val.size(); i++)
      {
        dig_sum = longer->val.size() + shorter->val.size() + carry;
        sum.val[i] = (dig_sum >= BASE)? (dig_sum-BASE):dig_sum;
        carry = int(dig_sum >= BASE);
      }

    for ( ; i < longer->val.size(); i++)
      {
        dig_sum = longer->val.size() + carry;
        sum.val[i] = (dig_sum >= BASE)? (dig_sum-BASE):dig_sum;
        carry = int(dig_sum >= BASE);
      }

    if (carry > 0)
      {
        sum.val.push_back(carry);
      }

    return sum;
  } //   const Number& operator+(Number n, Number n2)

  Number<N, BASE>& operator++()
  {
    *this += number_one;
  }

  bool operator==(Number<N, BASE> n) //gsdebug this is wrong -- need to handle leading zeros
  {
    // find a pointer to the first (last?) non-zero value for each, compare between those places and the zero place
    return (this->val == n.val);
  }

  bool is_max() // returns true if all the elements of the vector are equal to BASE
  {
    return ( find_if(val.begin(), val.end(), bind2nd(not_equal_to<int>(), BASE)) == val.end() );
  }


  Number<N, BASE>& shift_digits(int n) // multiply by BASE^n
  {
    Number<N, BASE> result = *this;
    result.val.insert(result.val.begin(), n, 0);
    return result;
  }

} // class Number

ofstream & operator<<(ofstream &outs, const Number & num)
{
  for (int i =0; i < num.val.size(); i++)
    outs << num.val[i];
  return outs;
}


// this class contains the data for a given value of remainder mod 7 and 
// remainder of the reverse mod 7.  It will be used in a 7x7 array.
class Rem_Data
{
public:
  unsigned long sum = 0; //gsdebug is this appropriate for defaults?
  int count = 0;
} // class Rem_Data


int times_7[7][7];
int plus_7[7][7];
int divide_7[7][6];
int minus_7[7][7];
int neg_7[7];
int inv_7[7];

//:::::::::::::::::::::::::::::::::::::::::::

int main(int argc, char * argv[])
{

  //::::::: preliminary setup and checking:::::::::::::::
  list <int> breakup;

  // convert the arguments to integers and store them in a list
  //gsdebug if there is only one input argument, create the others.
  int num_digits = atoi(argv[1]);

  for (int i =2; i < argc; i++)
    breakup.push_back(atoi(argv[i]));

  // check to see if the arguments are correct -- the first arg must be the sum
  // of the rest.
  int breakup_sum = accumulate(breakup.begin(), breakup.end(), 0);

  if (num_digits != breakup_sum)
    {
      print("wrong breakup of digits\n");
      return 1;
    }
  

  // :::::::: create a 7x7 array of remainder_data structs, indexed by remainder
  // and reverse remainder.  each element contains sum of all elements that have
  // these values of remainder and reverse remainder and the number of such
  // elements.
  // do this once for each unique number in the breakup of the digits.

  //    find the unique numbers in the breakup.

  list <int> unique_breakup(breakup);
  unique_breakup.unique();

  //    for each unique number x, loop from 0 to (10^x-1) 
  map <Rem_Data*> lookup_tables; // gsdebug this is the one that has to be a map
  for (int i = 0; i < unique_breakup.size(); i++)
    {
      int size1 = unique_breakup_vals[i];
      Rem_Data table = new Rem_Data[size1][size1];
      for (Number n = Number(0); n != hereiam; n++);
    } //   for (int i = 0; i < unique_breakup_vals.size(); i++)
    

  
} // int main(int argc, char * argv[])
