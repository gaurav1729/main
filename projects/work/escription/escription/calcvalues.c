#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI (3.14159265358979)
#define N_OUT (100000)
#define N_DIM (50)

#define ARB1 (1.0) /* arbitrary values for initialization */
#define ARB2 (2.0)
#define ARB3 (3.0)
#define ARB4 (4.0)
main ()
{
  unsigned int i_out, i_dim, offset;
  double temp, log_const, exponent, diff;
  printf("here at 0\n"); fflush(stdout);
  double s[N_OUT];
  double c[N_OUT];

  double x[N_DIM];
  printf("here at 0\n"); fflush(stdout);
  double    mu[N_OUT * N_DIM];
  double sigma[N_OUT * N_DIM];
  /*  There are two options here: allocating off the stack or multiple
      mallocs.  It's not clear a priori which one is faster.
      
      If we had chosen multiple mallocs, we would have declared
      double * mu[N_OUT];

      for (i_out = 0; i_out < N_OUT; i_out++)
      {
         mu[i_out]    = (double *) malloc(N_DIM * sizeof(double));
         if (mu[i_out] == NULL)
            free previously allocated data and return non-zero value
      }

      And anywhere in the rest of the code, wherever p_mu is initialized, it is
      p_mu = mu[i_out];
      instead of 
      p_mu = mu + offset;

      Also, at the end, we would have:
      for (i_out = 0; i_out < N_OUT; i_out++)
      {
         free( sigma[i_out] );
         free( mu[i_out] );
      } 


  */



  double* p_mu;
  double* p_sigma;

  log_const = -2*log(2*PI);

  printf("here at 1\n"); fflush(stdout);

  /* ____________One-time initialization of constants */
  for (i_dim = 0; i_dim < N_DIM; i_dim++)
    x[i_dim] = ARB1;

  for (i_out = 0; i_out < N_OUT; i_out++)
    {
      c[i_out]     = log(ARB2); /* log of 'c' coefficients rather than coeffs themeselves.*/

      offset = i_out * N_DIM;
      p_mu    = mu + offset;
      p_sigma = sigma + offset;
      for (i_dim = 0; i_dim < N_DIM; i_dim++)
        {
          p_mu[i_dim]    = ARB3; /* Any compiler should turn this into pointer arithmetic! */
          p_sigma[i_dim] = ARB4;
        }
    }
  printf("here at 2\n"); fflush(stdout);

  /* ______________Data processing loop_*/

  for (i_out = 0; i_out < N_OUT; i_out++)
    {
      offset = i_out * N_DIM;
      p_mu     = mu + offset;
      p_sigma  = sigma + offset;
      exponent = 0;
      for (i_dim = 0; i_dim < N_DIM; i_dim++)
        {
          diff = x[i_dim] - p_mu[i_dim];
          exponent += diff * diff * p_sigma[i_dim];
        }
      exponent *= 0.5;
      exponent += (c[i_out] + log_const);

      s[i_out] = exp(exponent);
    }
  printf("here at 3\n"); fflush(stdout);

  return 0;
}
