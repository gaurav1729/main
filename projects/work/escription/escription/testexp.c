#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI (3.14159265358979)
#define N_OUT (100000)
#define N_DIM (50)

#define ARB1 (1.0) /* arbitrary values for initialization */
#define ARB2 (2.0)
#define ARB3 (3.0)
#define ARB4 (4.0)
main ()
{
  unsigned int i_out, i_dim, offset;
  double temp, log_const, exponent, diff;

  double s[N_OUT];
  double c[N_OUT];

  double x[N_DIM];
  
  double*    mu[N_OUT];
  double* sigma[N_OUT];

  /*  There are two options here: allocating off the stack or using multiple
      mallocs (i.e., from the heap).  It's not clear a priori which one is
      faster. In this code, we allocate from the heap.
      
      If we had chosen off the stack, we would have declared mu as:
      double mu[N_OUT * N_DIM];

      And anywhere in the rest of the code, wherever p_mu is
      initialized, it would have been:
      p_mu = mu + i_out * N_DIM;
      instead of 
      p_mu = mu[i_out];

  */



  double* p_mu;
  double* p_sigma;

  log_const = -2*log(2*PI);

  /* ____________One-time initialization of constants */
  for (i_dim = 0; i_dim < N_DIM; i_dim++)
    x[i_dim] = ARB1;

  for (i_out = 0; i_out < N_OUT; i_out++)
    {
      c[i_out]     = log(ARB2); /* log of 'c' coefficients rather than coeffs themeselves.*/

      /* allocate memory, with appropriate safeguards */
      mu[i_out]    = (double *) malloc(N_DIM * sizeof(double));
      if (mu[i_out]    == NULL)/* out of memory -- return error value */
        {
          for (int i = 0; i < i_out; i++)
            {
              free mu[i];
              free sigma[i];
            }
          return 1; 
        }
      sigma[i_out] = (double *) malloc(N_DIM * sizeof(double));
      if (sigma[i_out] == NULL)
        {
          for (int i = 0; i < i_out; i++)
            {
              free mu[i];
              free sigma[i];
            }
          return 1; 
        }

      /* initialization of arrays */
      for (i_dim = 0; i_dim < N_DIM; i_dim++)
        {
          mu[i_out][i_dim]    = ARB3;
          sigma[i_out][i_dim] = ARB4;
        }
    }

  /* ______________Data processing loop_*/

  for (i_out = 0; i_out < N_OUT; i_out++)
    {
      p_mu     = mu[i_out];
      p_sigma  = sigma[i_out];
      exponent = 0;
      for (i_dim = 0; i_dim < N_DIM; i_dim++)
        {
          diff = x[i_dim] - p_mu[i_dim];
          exponent += diff * diff * p_sigma[i_dim];
        }
      exponent *= 0.5;
      exponent += (c[i_out] + log_const);

      s[i_out] = exp(exponent);
    }

  /* ______________Free allocated memory */
  for (i_out = 0; i_out < N_OUT; i_out++)
    {
      free( sigma[i_out] );
      free( mu[i_out] );
    } 


  return 0;
}
