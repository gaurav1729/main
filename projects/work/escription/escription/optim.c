#include <stdio.h>
#define V1
#define N 20000
#define M 50 
#define pi 3.141
main()
{
int i, j;
int q; 
double s[N]; 
double c[N];
double x[M];
double mu[N*M]; 
double sigma[N*M]; 
double temp; 
double tempsum; 

// Arbitrary intialization 
for(j=0; j<M;j++) {
	*(x+j) = j;  // arbit 
	}// j 
for(i=0; i<N; i++) {
	*(c+i)= i; // arbit 
	for(j=0; j<M;j++) {
		*(mu+j+i*M)=i*j; 	// arbit
		*(sigma+j+i*M) = i*j;	// arbit
		}// j 
	}//i
// -------------------------------------
#ifdef V1
// Version 1
for(i=0; i<N; i++) {
	*(s+i) = 1/sqrt(2*pi); 
	*(s+i) /= c[i]; 
	tempsum = 0;
	for(j=0; j<M; j++) {
		temp = *(x+j) - *(mu+j+i*M); 
		temp /= *(sigma+j+i*M); 
		temp *= temp; 
		tempsum += temp; 
		} // j
	tempsum *= -0.5; 
	tempsum = exp(tempsum); 
	*(s+i) *= tempsum; 
	} // i 
#endif
// -------------------------------------
} // main
