% $Id: standardized_test_environment.tex 6181 2009-05-22 23:19:22Z goodell $
% Copyright (c) 2009 PhaseCapital LP

\documentclass[10pt]{article}

\pdfpagewidth 8.5in
\pdfpageheight 11in

\begin{document}



\vspace*{.75in}
\begin{center}
\bf \large The Standardized Test Environment
\end{center}

\vspace{0.35in}

\begin{center} % YOUR NAMES HERE
Gaurav Shah\\
\vspace{0.25in}
PhaseCapital LP\\200 Clarendon Street, 25th Floor\\Boston MA 02216
\end{center}
\vspace{0.35in}

\pagebreak

%%%%%%%
\section{Preliminaries}

The purpose of this document is to describe the current Standardized Test
Environment used for the pre-release testing of various algorithms, and for
optimization of various free parameters.

%%%%%%%
\section{Description}

The heart of the backtest environment is the file backtest.py, located under the
SVN root directory \$Root/util/backtest.py.  The code is launched from the
command line.  The parameters used are passed to it either as command line
arguments, or are read from the parameter file lib/params.py.  The main command
line arguments to be passed to it include the universe, the signalling
algorithm, the PMR algorithm, and the time scale; but there are many possible
additional arguments allowed.

As an output, the backtest environment creates a pdf file that summarizes the
most import results of the run.  We believe that these results include the
information we need to judge the efficacy of the algorithms, and in particular
all the information we need to decide whether to release the algorithms to
production.  The most important of these results include

\begin{itemize}
\item the returns
\item the Sharpe and Sortino ratios 
\item the average turnover per rebalance
\item the transaction cost sensitivity of the returns
\end{itemize}


%%%%%%%
\section{How It Is Currently Utilized}

The backtest environment is the utility tool for most of signaling and PMR
research.  It is currently the most important test that any new algorithm in
either of those fields is required to pass before production release.

As such, it is used by nearly everyone involved in research and development.
The largest barrier to further use is that some of the research infrastructure
is in Mathematica or C; at present, if we want to utilize this part of the
infrastructure, we must forego using backtest.  This is slowly being overcome,
by migrating more code to Python, and by developing our abilities to use
Mathematica and C code from Python.


%%%%%%%
\section{What Happens During Backtesting}

Backtesting strives to simulate as closely as possible the performance of the
given signalling and PMR algorithms in the marketplace.  It does this by reading
the historical price data over the relevant time interval and then feeding this
price data in a causal manner to the combined signaling and PMR code.

Here is a rough explanation of the steps involved, with explanation of the more
important ones.

\begin{enumerate}

\item \textbf{Setup:} The first step is to read and parse the simulation parameters.
  Note that the files that are read at this step are identical to the ones to be
  read during actual production; this helps ensure fidelity.  

\item \textbf{The Universe:} The universe of appropriate tickers is assembled.  This
  step is identical to live trading.

\item \textbf{Signaling:} For each ticker in the universe, for each signaling
  period, the signaling algorithm is run.  The input to this algorithm is the
  price time series for this ticker, truncated before the signaling time.  Note
  that the prices used are VWAP prices.  This step is also identical to the
  corresponding step in live trading.

\item \textbf{PMR:} The PMR algorithm is run on the universe of tickers at this
  stage, to find the desired portfolio composition \emph{at the last VWAP
    price}.  It will be assumed that this portfolio composition will be achieved
  precisely, at this exact price.  This differs from live trading.  In live
  trading, the prices at which we buy or sell depends on higher frequency
  movement of the market, and the amount we wish to buy or sell depends on both
  these higher frequency movements, and the imperfect execution that has
  occurred.


\item \textbf{Scoring:} Scoring in backtesting differs from that in live trading
  in that corporate actions are not taken into account.

\end{enumerate}


%%%%%%%
\section{Reliability}

As backtesting is our main criterion for validating algorithms prior to live
trading, it is critical that backtesting itself be reliable.  This has been
ensured  in several ways.

\begin{enumerate}

\item The code has undergone rigorous quality testing. In particular, care has
  been taken to ensure that the data that is used is causal -- that is, while we
  are running in backtest for a particular date, no data from future dates is
  supplied to the calculations.

\item The scoring has been independently verified by comparison with the results
  created by our accounting team.

\item The results of the code have been compared with previous
  \textbf{independent} programs that were designed to do the same job.

\item The code is mature and has been used extensively without known problems.

\end{enumerate}


%%%%%%%
\section{Possible improvements}

After discussions with the users of backtesting, some areas of possible
improvement were identified.

\begin{itemize}

\item The single largest obstacle to use of the backtest infrastructure was lack
  of documentation.  The command line arguments are fairly cryptic and almost no
  one had a good understanding of the majority of possible arguments and flags.

\item As described in a previous section, trade execution is \emph{not}
  simulated.  This means that

  \begin{itemize}
    \item the effects of imperfect execution on the PMR algorithms, and the 
      effect of this change on the final results, cannot be studied.
    \item the effects of market volatility or systematic market movements on the 
      results cannot be studied
    \item the actual trade execution algorithms themselves cannot be compared or
      tested prior to production release.
  \end{itemize}

  There is no doubt that there is a need, in the long term, for a testing
  platform that can simulate trade execution.  This can be done in either of two
  ways:

  \begin{itemize}
  \item simulate price movements using a random generator
  \item store level two data for a certain period of time and run execution tests 
    with that data.
  \end{itemize}


\end{itemize}



%%%%%%%
\section{Conclusion}

When we make decisions on deployment of signaling and PMR algorithms, the
backtest infrastructure plays a crucial role.  This framework is robust and has
been vetted.  There is a need to make it easier to use, and to extend it to
realistic trading scenarios.


\end{document}

