% $Id: research_current_pre_release_process.tex 6181 2009-05-22 23:19:22Z goodell $
% Copyright (c) 2009 PhaseCapital LP

\documentclass[10pt]{article}

\pdfpagewidth 8.5in
\pdfpageheight 11in

\begin{document}



\vspace*{.75in}
\begin{center}
\bf \large The Research to Pre-Release Process
\end{center}

\vspace{0.35in}

\begin{center} % YOUR NAMES HERE
Gaurav Shah\\
\vspace{0.25in}
PhaseCapital LP\\200 Clarendon Street, 25th Floor\\Boston MA 02216
\end{center}
\vspace{0.35in}

\pagebreak

%%%%%%%
\section{Preliminaries}

The topic of this document is the pre-release process for PhaseCapital
signalling algorithms.  In this document, we will first discuss the current
state of pre-release process requirements and the current state of adherence to
these requirements.  Next, we will discuss the pros and cons of the current
process, and finally suggest possible improvements.


%%%%%%%
\section{The current signalling pre-release process}

This can roughly be divided into three parts: the hypothesis sheet, the research
stage (including backtesting), and the pre-release document.  The final decision
whether to release the signalling algorithm or not is decided on the basis of
this final pre-release document.

\subsection{Hypothesis Sheets}

The first stage in the current signalling pre-release process is the hypothesis
sheet.  This is the preliminary document; it is produced to help the involved
people decide whether the project is to continue or not.  In order to help in
this decision making, there are some aspects that must be addressed in
the hypothesis sheet at this stage:

\begin{description}
\item[Business Case:]  How will this research help PhaseCapital advance its business interests?
\item[Description:]    A detailed description of the proposal, including any relevant mathematics.
\item[Proposed Tests:] It is important at this stage to describe which tests will be used to decide
the success or failure of the research proposal.  \emph{This includes possible null tests which may be
used for comparison.}
\item[Resource Requirements:] People and computational time required for the project.
\item[Project Plan:] A summary of the steps planned at this preliminary stage.
\end{description}
% living or not

\subsection{Research}

The research stage consists of all calculations and coding required to test the
signalling hypothesis described in the hypothesis sheet.  In particular, it
often involves extensive use of the backtest facility, which will be described
in greater detail in standardized\_test\_environment.tex.  The information
garnered from the backtests will be used in the next step.

Management and monitoring of this stage is accomplished in three ways:
\begin{description}

\item[Weekly meetings:] There are regular signaling meetings to review and
  discuss current projects.

\item[Spreadsheets:] Each individual working on research projects (with some
  exceptions!)  is expected to maintain a spreadsheet in the
  User\_Shared\_Directory, under Projects Plans.  For each project, this
  spreadsheet lists the name, descriptions, investigators, and stage of
  progress.  Each individual is expected to update this spreadsheet on a weekly
  basis, or more often as needed.

\item[Hypothesis Sheets:] The researchers are expected to regularly update their
  hypothesis sheets to reflect new investigations.

\end{description}

\subsection{Pre-release}

We have implemented the pre-release stage for only two signalling algorithms
so far: Wavelet Trend Reversion (WTR) and Mean-Reversion Direct Kelly (MRDK).
The pre-release document is a more comprehensive review of the algorithm, and is
the final stage before the algorithm is traded live.  It consists of an
Executive Summary, and relevant information by individuals with knowledge of
different facets of the algorithm and its use: development, PMR, trading,
operations, and engineering.  Each of these individuals must describe how the
algorithm interacts with their area of expertise, and agree to go live.

Here is a brief description of some of the various sections:

\begin{description} % pre-release

\item[Research:] This section is largely a summary of the hypothesis sheet,
  including the conclusions from backtesting.

\item[PMR:] The portfolio manager's responsibilities are to oversee the effects
  of trading the algorithm on the portfolio as a whole.  This includes aspects
  such as the portfolio generation techniques to be employed with a given
  algorithm, and studies of volatility, sector concentration and correlation
  risk, risk premium (Sharpe and Sortino ratios) and liquidity.  These numbers
  for these studies are typically achieved through backtest results.  Here are
  some of the issues discussed in this section:


  \begin{enumerate}

  \item \emph{Constraints:} This section is a review of the constraints that are
    imposed on the portfolio externally.  This includes maximum and minimum
    leverage, and constraints on the net exposure.

  \item \emph{Returns:} The average annual returns is of course one of the most
    important inputs to deciding the utility of the algorithm.
    
  \item \emph{Value at Risk:} 


  \item \emph{Risk from correlations:} The correlation risk is divided into two
    parts:

    \begin{enumerate}
      
      \item \emph{Correlations of tickers in the portfolio with each other:}
        Having multiple tickers that are strongly correlated with each other
        decreases the diversity of the portfolio and increases the risk.  We
        estimate this risk in our portfolios by first calculating the
        correlation matrix of the portfolio, and then calculating the inner
        product of the portfolio weights on this correlation matrix.  This 
        is the standard way of estimating portfolio risk.  Note that under
        this algorithm, there is  no importance sampling of the returns.


      \item \emph{Correlation with the market:} The correlation with the market
        -- also known as beta -- is treated separately.  This is calculated by
        regressing each of the tickers against the S\&P 500, and finding the dot
        product of the vector of regression coefficients and the vector of 
        the portfolio weights.


    \end{enumerate}

  \end{enumerate}


\item[Trading Analysis:] This includes discussion of the following aspects of
  the algorithm:

  \begin{description} % Trading Analysis

    \item[Liquidity and Capacity:] This is a study the turnover per rebalance,
      and compare that to the volumes traded in the same time period in the
      universe we are trading.

    \item[Execution:] This is an analysis of how our trading algorithms interact
      with the signalling algorithm.  In particular, it looks at the fraction of
      the transactions in which we provide liquidity, versus the fraction of the
      transactions in which we take liquidity, and how this split affects the profits.

    \item[Constraints:] This section discusses the constraints on trading; in particular, 
      the short locate list and the ``do not trade'' list, and how they affect the
      results of trading the signalling algorithm.
      

  \end{description} % Trading Analysis

\subsection{Final decision}

%based on pre-release docu. there are no hard cutoffs for most of these quantities.  consensus.

\end{description} % pre-release

%%%%%%%
\section{Adherence to the hypothesis sheet process}

\begin{description} % adherence

  \item[How often are hypothesis sheets used?] All researchers interviewed said
    that they were scrupulous in writing hypothesis sheets for all research
    projects.

  \item[How often are hypothesis sheets updated?] This was more problematic.
    Hypothesis sheets tend to get updated every month or so at the fastest,
    which is not really fast enough to keep up with the pace of the research;
    some people did not update them at all once they were created.

  \item[How many hypothesis sheets do the researchers maintain?] Researchers
    typically maintained between two and five hypothesis sheets at a time.  Of
    these, not more than two were typically active; the others may have been
    complete or dormant.

  \item[Where are they kept?] Most of the hypothesis sheets are in latex format
    and have been checked into the SVN repository.  However, some of the older
    hypothesis sheets, in particular those that are in Microsoft Word format, are
    stored on the individual's local computer, and are therefore not available 
    to all individuals.  Newer hypothesis sheets have been consistently checked
    in to SVN, however.

  \item[How useful are old hypothesis sheets?] There was a consensus that old
    hypothesis sheets \emph{could} be useful, but are not actually referred to on any
    consistent basis.

\end{description} %adherence

%%%%%%%
\section{Pros and Cons of the current process}

\subsection{Pros}

\begin{itemize}

\item The fixed format of the current hypothesis sheets enforces rigor in their
  preparation.

\item As LaTeX documents, they are easy to put into the SVN repository; 

\item The spreadsheets take very little time to fill out and print, making them 
  convenient for 


\end{itemize}

\subsection{Cons}


\subsubsection{Hypothesis Sheet Cons}

\begin{itemize}

\item The hypothesis sheet is not a living document: it is not easy to quickly
  update or modify latex documents.  The spreadsheets are not set up to record
  daily progress, either.

\item There is a disconnect between the entries in the hypothesis sheet and the
  work.  For example, it is not easy for another researcher to proceed from the
  hypothesis sheet to find the code being referred to.

\item It is not easy to search for a particular project; the location of the
  hypothesis sheet is not easily deducible from the name of the project.

\item The hypothesis sheet includes useful information to help the group decide
  on the worth of a project, and initial ideas on what the steps are.  However,
  it does not usually contain information about the result of the research after
  the project is completed.  There is currently no required \emph{final}
  document for a project.  This means that when we try to commercialize a
  project, there is no single reference point to look at for the \emph{results}
  of the research; this is also a problem when we do not commercialize the
  project, but wish to do later research that builds on the results of the
  previous one.

\end{itemize}

\subsubsection{Spreadsheet Tracking Cons}

\begin{itemize}

\item The spreadsheets require multiple copies of each entry: in the
  supervisor's spreadsheet, and in separate tabs on each individual's
  spreadsheet.  This is likely to create mistakes in the long run.

\item It is inconvenient to create long historical lists of projects as they
  grow unwieldy.  This leads to maintaining multiple copies, one per week.

\item There is presently no official format for entering regular updates to
  track the work as it happens.

\item Searching isn't supported.  This is especially difficult since there is
  typically a separate spreadsheet per week.  Searching would \emph{ideally}
  support selecting

  \begin{itemize}
  \item only open or closed projects.
  \item projects corresponding to a particular group.
  \item projects corresponding to a particular manager or researcher.
  \item projects with a particular keyword
  \item any combination of the above!
  \end{itemize}



\end{itemize}

%%%%%%%
\section{Possible Modifications}

While I feel the hypothesis sheet is a good start to planning a project, I
believe that there is currently no method for documenting the project going
forward.  This leads to a situation where previous results cannot be easily
referenced to help current progress.

Any method to rectify this should include a way to monitor the project as it is
in progress, and a way to document the end of the project.


To avoid duplication, this method should be unified with the spreadsheets that
are used by the managers and researchers for similar purposes.

In addition, it should have searching facilities as described in the previous
section, and a hierarchical breakdown of the project into tasks and subtasks.
Each project, task or subtask should contain information about

\begin{itemize}

\item relevant keywords
\item manager/researcher names
\item whether the project is open, closed or hibernating
\item which group the project belongs to (such as research/pmr/ops)
\item open, target and close dates
\item location of all relevant documents
\item location of all relevant code

\end{itemize}

\subsection{How Should This Be Implemented?}

There are several choices:

\begin{itemize}

\item As a spreadsheet (OpenOffice.org or Excel).  In particular, this can
  include either macros or Basic add-ins.

\item Writing our own GUI, in Python or Java.

\item By modifying Flyspray

\item An open source project management.  Examples include Project.net,
  project-open, Redmine, Mantis Bug Tracker and Collabtive.

\end{itemize}

The only way of getting everything we need is by writing our own, but this could
be prohibitive in terms of resources.  The spreadsheet may be the easiest way of
getting most of what we need.

%%%%%%%
\section{Conclusions}

The current process is effective for codifying the steps required for a project
at a broader level, but not as effective at following research at a finer level,
or for referencing previously completed research.

The next step is to create a spreadsheet template for monitoring project
progress, and a document template to be used at the conclusion of the research.

%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}


