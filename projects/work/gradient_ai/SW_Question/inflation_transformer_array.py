#!/usr/bin/env python3

# This assumes the data has only annual tags on it, and does the naive scaling
# by the inflation factor.  It uses numpy arrays to speed things up.


##:::: Setup
from sklearn.base import TransformerMixin, BaseEstimator
import csv
import pandas as pd
import numpy  as np

##:::: Transformer Class
class Inflation_Transformer(BaseEstimator, TransformerMixin):
    def __init__(self, f_csv='./yearly_med_index.csv'):
        '''Read csv file into dictionary'''
        with open(f_csv) as h_csv:
            r_csv = csv.reader(h_csv)
            header = next(r_csv)
            csv_vals = list(r_csv)
            self.infl_dict = dict([(int(x), float(y)) for x,y in csv_vals])

    def fit(self, X, y=None):
        return self
        
    def transform(self, X, y=None):
        '''X is array-like, n_samples x 2 (year, dollar_val).
        The output is an array of n_samples long, adjusted for
        inflation to dollar values from the first year for which we
        have inflation data.
        '''
        first_year = min(self.infl_dict.keys())
        # infls is an array of inflationary factors, corresponding to each year
        # in the original data.
        infls = np.array([self.infl_dict[y] for y,v in X  ])
        # scale by the first year
        infls /= self.infl_dict[first_year]
        return X[:,1]/infls

    
##:::: Usage

if __name__ == '__main__':
    ## Create fake data
    import random, matplotlib, pdb
    n_samples  = 200
    years_list = list(range(2009, 2020)) * 20
    years_list = years_list[:n_samples]
    rvals      = [random.random() for i in range(n_samples)]
    X          = np.array(list(zip(years_list, rvals)))

    ## Instantiate the transformer and apply it
    inf_tr      = Inflation_Transformer()
    new_data    = inf_tr.transform(X)
    new_data_ft = inf_tr.fit_transform(X)
