/* NOTE: For convenience in this problem, I've put everything into
   a single file.  Ordinarily, the code would be in spearman.c and the
   declarations would be in spearman.h.
 */

/*
Calculates the Spearman rho coefficient.
Not really optimized for efficiency.
If the vectors are different lengths, truncates the longer
one, rather than extending the shorter one by repeating it.
Does not deal correctly with the case of ties.
*/

//:::::::: setup

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

const double MAX_VAL    =   20.0;   // used only in random number generation
const double EPS        =    0.001; // for floating point good-enoughs
const    int VEC_LENGTH =    6;     // used only in the wrapper

//::: declarations
double Spearman_Rho(double *vec1,  int nv1,
		    double *vec2,  int nv2);
int Find_Rank(double in, double *vec,  int vec_size);
int Doublecomp(double *elem1, double *elem2);
int Clean_up(double *vec1, double *vec2, int n);


//:::::::::::::
//:: Just the wrapper used to call the function Spearman_Rho
int main(int argc, char* argv[])
{

  double vec1[] = {0.31, 0.53, 0.83,  NAN, 0.22, 0.18};
  double vec2[] = {0.63, 0.91, 0.25, 0.28, 0.27, 0.04};

  int i;
  
  double rho;

  /* vec1 = (double *)malloc(sizeof(double) * VEC_LENGTH); */
  /* vec2 = (double *)malloc(sizeof(double) * VEC_LENGTH); */
  

  rho = Spearman_Rho(vec1, VEC_LENGTH,
		     vec2, VEC_LENGTH);

  printf("vec1 :\n");
  for (i=0; i<VEC_LENGTH; i++)
    printf(" %7.3f ", vec1[i]);

  printf("\n");

  printf("vec2 :\n");
  for (i=0; i<VEC_LENGTH; i++)
    printf(" %7.3f ", vec2[i]);

  printf("\n::::::::\n");
  printf("Rho = %5.3f\n", rho);

  // tidying up
  /* free(vec1); */
  /* free(vec2); */
  /* vec1 = NULL; */
  /* vec2 = NULL; */
} /* int main(int argc, char* argv[]) */

//::::::: functions we will need

int Doublecomp(double *elem1, double *elem2)
{
  if (*elem1 < *elem2)
    return -1;
  return *elem1 > *elem2;
}

//::::::::::::::::

// get rid of NANs, shift everything over if you find any
int Clean_Up(double *vec1, double *vec2, int n)
{
  int count, i, j;
  count = 0;
  for (i=0; i<n; i++)
  {
    if (!isnan(vec1[i]) &&  !isnan(vec2[i]))
    {
      count++;
      continue;
    }
    
    // if it is a NaN, shift everything left
    for (j=i; j < n-1; j++)
    {
      vec1[j] = vec1[j+1];
      vec2[j] = vec2[j+1];
    }
    n--;
    i--;

  } // i for loop
  return(count);
} /* void Clean_Up(double *vec, int &n) */

//::::::::::::::::

double Spearman_Rho(double *vec1,  int nv1,
		   double *vec2,  int nv2)
{
  double    *nvec1,    *nvec2;
  double *ordered1, *ordered2;
  double   *ranks1,   *ranks2;
  double       rho,     mean1,  mean2;
  double    sum_xy,    sum_xx, sum_yy;

  int nv, i;


  // nv is the min of nv1 and nv2
  nv = nv2;
  if (nv1 < nv2)
    nv = nv1;

  
  //we're going to be destructive, so copy vec1 and vec2
  nvec1 = (double *) malloc(sizeof(double) * nv1);
  nvec2 = (double *) malloc(sizeof(double) * nv2);
  for (i=0; i< nv1; i++)
    nvec1[i] = vec1[i];
  for (i=0; i< nv2; i++)
    nvec2[i] = vec2[i];


  // clean up bad values, find length of resulting vector
  nv = Clean_Up(nvec1, nvec2, nv);

  //::: create ordered versions of vec1 and vec2 for efficient
  //::: lookup (not necessary, but speeds things up a little)
  ordered1 = (double *) malloc(sizeof(double) *nv);
  ordered2 = (double *) malloc(sizeof(double) *nv);
  
  for (i=0; i<nv; i++)
  {
    ordered1[i] = nvec1[i];
    ordered2[i] = nvec2[i];
  } // we could just do memcpy, too

  // qsort is the canned multi-purpose in-place orderer
  qsort(ordered1, nv, sizeof(double), Doublecomp);
  qsort(ordered2, nv, sizeof(double), Doublecomp);


  //::: create vectors of ranks
  ranks1 = (double *) malloc(sizeof(double) * nv);
  ranks2 = (double *) malloc(sizeof(double) * nv);

  for (i=0; i<nv; i++)
  {
    ranks1[i] = Find_Rank(nvec1[i], ordered1, nv);
    ranks2[i] = Find_Rank(nvec2[i], ordered2, nv);
  }

  //:::: the actual correlation between ranks1 and ranks2
  mean1 = (nv + 1)/2.0; // since sum 1 to n = n(n+1)/2
  mean2 = mean1;

  sum_xy = 0.0;
  sum_xx = 0.0;
  sum_yy = 0.0;

  for (i=0; i<nv; i++)
  {
    double d1 = ranks1[i] - mean1;
    double d2 = ranks2[i] - mean2;
    
    sum_xy += d1*d2;
    sum_xx += d1*d1;
    sum_yy += d2*d2;
    
  }

  rho = sum_xy/sqrt(sum_xx * sum_yy);

  //::: housecleaning
  free(ordered1); free(ordered2);
  free(  ranks1); free(  ranks2);

  ordered1 = NULL; ordered2 = NULL;
  ranks1   = NULL; ranks2   = NULL;

  return(rho);
} //Spearman_Rho

//::::::::::::::

// vec is assumed to be already ordered
int Find_Rank(double in, double *vec,  int vec_size)
{
  int i;

  for (i=0; i<vec_size; i++)
  {
    // This breaks down if there are multiple
    // equal numbers (ties).  Easy to fix by adding a
    // static variable to keep track of numbers
    // we've already looked at.
    if (vec[i] == in)
      return(i+1); 
  }

  // If we've reached this point, we've had a floating
  // point number snafu.  So we use EPS.

  for (i=0; i<vec_size; i++)
  {
    if (abs(vec[i]-in) < EPS)
      return(i+1);
  }

  // In an industrial strength version, we should be able to 
  // deal with the contingency that nothing has matched yet.
  // However, we've written the calling function in such a way that
  // that will not happen in practice.
} // Find_Rank

//::::::::::::::
