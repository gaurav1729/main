/* NOTE: For convenience in this problem, I've put everything into
   a single file.  Ordinarily, there code would be in spearman.c and the
   declarations would be in spearman.h.
 */

/*
Calculates the Spearman rho coefficient.
Assumes that numbers equal to NA are not available.
*/

//:::::::: setup

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

const float NA         = -999.0; // anything equal to this is NA
const float MAX_VAL    =   20.0; // used only in random number generation
const float EPS        =    0.001; // for floating point good-enoughs
const   int VEC_LENGTH =    5;

//::::::: declarations
float Spearman_Rho(float *vec1,  int nv1,
		   float *vec2,  int nv2);

int Find_Rank(float in, float *vec,  int vec_size);

int Floatcomp(float *elem1, float *elem2);

//:::::::::::::
//:::::::::::::
int main(int argc, char* argv[])
{

  float vec1[] = {0.31, 0.53, 0.83, 0.22, 0.18};
  float vec2[] = {0.63, 0.91, 0.25, 0.27, 0.04};

  int i;
  
  float rho;


  rho = Spearman_Rho(vec1, VEC_LENGTH,
		     vec2, VEC_LENGTH);

  printf("vec1 :\n");
  for (i=0; i<VEC_LENGTH; i++)
    printf(" %7.3f ", vec1[i]);

  printf("\n");

  printf("vec2 :\n");
  for (i=0; i<VEC_LENGTH; i++)
    printf(" %7.3f ", vec2[i]);

  printf("\n::::::::\n");
  printf("Rho = %5.3f\n", rho);

} /* int main(int argc, char* argv[]) */

//::::::: functions we will need

int Floatcomp(float *elem1, float *elem2)
{
  if (*elem1 < *elem2)
    return -1;
  return *elem1 > *elem2;
}

//::::::::::::::::

float Spearman_Rho(float *vec1,  int nv1,
		   float *vec2,  int nv2)
{
  float *ordered1, *ordered2;
  float   *ranks1,   *ranks2;
  float       rho,     mean1,  mean2;
  float    sum_xy,    sum_xx, sum_yy;

  int nv, i;

  // nv is the min of nv1 and nv2
  nv = nv2;
  if (nv1 < nv2)
    nv = nv1;

  //::: create ordered versions of vec1 and vec2 for efficient
  //::: lookup
  ordered1 = (float *) malloc(sizeof(float) *nv);
  ordered2 = (float *) malloc(sizeof(float) *nv);
  
  for (i=0; i<nv; i++)
  {
    ordered1[i] = vec1[i];
    ordered2[i] = vec2[i];
  } // we could just do memcpy, too

  // qsort is the canned multi-purpose in-place orderer
  qsort(ordered1, nv, sizeof(float), Floatcomp);
  qsort(ordered2, nv, sizeof(float), Floatcomp);


  //::: create vectors of ranks
  ranks1 = (float *) malloc(sizeof(float) * nv);
  ranks2 = (float *) malloc(sizeof(float) * nv);

  for (i=0; i<nv; i++)
  {
    ranks1[i] = Find_Rank(vec1[i], ordered1, nv);
    ranks2[i] = Find_Rank(vec2[i], ordered2, nv);
  }

  //:::: the actual correlation between ranks1 and ranks2
  mean1 = (nv + 1)/2.0; // since sum 1 to n = n(n+1)/2
  mean2 = mean1;

  sum_xy = 0.0;
  sum_xx = 0.0;
  sum_yy = 0.0;

  for (i=0; i<nv; i++)
  {
    float d1 = ranks1[i] - mean1;
    float d2 = ranks2[i] - mean2;
    
    sum_xy += d1*d2;
    sum_xx += d1*d1;
    sum_yy += d2*d2;
    
  }

  rho = sum_xy/sqrt(sum_xx * sum_yy);

  //::: housecleaning
  free(ordered1); free(ordered2);
  free(  ranks1); free(  ranks2);

  ordered1 = NULL; ordered2 = NULL;
  ranks1   = NULL; ranks2   = NULL;

  return(rho);
} //Spearman_Rho

//::::::::::::::

// vec is assumed to be already ordered
int Find_Rank(float in, float *vec,  int vec_size)
{
  int i;

  for (i=0; i<vec_size; i++)
  {
    // This breaks down if there are multiple
    // equal numbers (ties).  Easy to fix by adding a
    // static variable to keep track of numbers
    // we've already looked at.
    if (vec[i] == in)
      return(i+1); 
  }

  // If we've reached this point, we've had a floating
  // point number snafu.  So we use EPS.

  for (i=0; i<vec_size; i++)
  {
    if (abs(vec[i]-in) < EPS)
      return(i+1);
  }
} // Find_Rank

//::::::::::::::
