##:: NOTE: The canonical answer to this question is probably found
##:: in the mclust package, using Expectation-Maximization methods.  I'm
##:: trying to create an original answer.


##:: Problem 1
##
## (a) Calculation of covariance with missing data:
##   separate into good rows and bad rows
##   set the missing values in bad rows to zero
##   duplicate the good rows nreps times
##   join up again
##   find covariance of the resulting matrix
##
## (b) Imputation of missing values:
##   Divide the inverse covariance matrix into four blocks,
##    based on missing and present values.
##   Find the value of the mean of the resulting conditional
##    Gaussian distribution, fill up the missing values with this.


##::::::::::::::::::::::::::::::::
##:: Parameters that we can tweak

## how much relative importance to give to full data.
maxreps <- 5 # must be an integer, at least two.


##::::::::::::::::::::::::::::::::
##:: Initial setup

source('rcode.R') ## creates R, de-meaned return matrix
library(zoo)      ## time series class
library(Matrix)
endl <- '\n'

##::::::::::::::::::::::::::::::::
##:: Functions we will use

##:: calculate covariance of a matrix with missing values
##:: by giving extra importance to the full rows
calc.na.cov <- function(R, nreps)
{
  bad.row.inds <- which(is.na(rowSums(R)))

  R.bad  <- R[ bad.row.inds , ] ## no value judgments are implied
  R.good <- R[-bad.row.inds , ] ## by the names

  R.bad[is.na(R.bad)] <- 0
  
  R.aug <- R.bad
  for (i in 1:nreps)
    R.aug <- rbind(R.good, R.aug)

  return(cov(R.aug))
} ## calc.na.cov <- function(R)

##:::::::::::::::::::::::::

##:: Given a covariance matrix and a row with missing
##:: values, find missing values.
##::
##:: Assumes mean of zero.
calc.row.imputed <- function(inv.V, old.row)
{
  ##:: Regularizing them with the Bayesian
  ##:: prior of zero -- just a hint of a kludge
  regularizer <- 2
  
  na.inds <- which(is.na(old.row))
  inv.V.xx <- inv.V[  na.inds,  na.inds]
  inv.V.xy <- inv.V[  na.inds, -na.inds]
  inv.V.yy <- inv.V[ -na.inds, -na.inds]

  blank.vals <- -solve(inv.V.xx) %*% inv.V.xy %*% old.row[-na.inds]
  return(blank.vals/regularizer)
} ## calc.row.imputed <- function(V, old.row)

##:::::::::::::::::::::::::

##:: Calls calc.row.imputed row by row
##:: and fills in the blanks in R
impute.na.mat <- function(R, V)
{
  inv.V <- solve(V)
  for (i.row in 1:nrow(R))
  {
    old.row <- R[i.row , ]
    if (! is.na(sum(old.row)))
      next
    blank.vals <- calc.row.imputed(inv.V, old.row)
    R[i.row , which(is.na(old.row))] <- blank.vals
  }   ## for (i.row in 1:nrow(R))
  return(R)
} ## impute.na.mat <- function(R, V)

##:::::::::::::::::::::::::

simpleImputeNA <- function(R, V) { # The naive way of imputing NAs doesn't use V at all.
   R[is.na(R)] <- 0
   R
}

testImputeFUN <- function(R, V, FUN) { # Calc bias and err stats for our impute NA function, FUN
   R <- simpleImputeNA(R,V)

   bias <- err <- NULL
   bias.simple <- err.simple <- NULL   
   for (i in 1:50) {
       idx <- sample(1:length(R), length(R) %/% 10)
       RNA <- R; RNA[idx] <- NA
       R0 <- FUN(RNA, V)
       R.simple <- simpleImputeNA(RNA,V)
       
       bias[i] <- mean(R[idx]-R0[idx])
       err[i]  <- mean((R[idx]-R0[idx])^2)

       bias.simple[i] <- mean(R[idx]-R.simple[idx])
       err.simple[i]  <- mean((R[idx]-R.simple[idx])^2)
   }
   list(bias=mean(bias), err=mean(err),
        bias.simple=mean(bias.simple),
        err.simple=mean(err.simple)
        )
}



##::::::::::::::::::::::::::::::::
##:: Body of code

for (nreps in 2:maxreps)
{
  cat('nreps = ', nreps, endl)
  V <- calc.na.cov(R,nreps)
  R.imputed <- impute.na.mat(R,V)
  out.stats <- testImputeFUN(R,V,impute.na.mat)
  print(out.stats)
}
