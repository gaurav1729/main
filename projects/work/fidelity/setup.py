## Fidelity coding test
## 2014-07-25
## Gaurav Shah
## gaurav1729@gmail.com


# create appropriate error classes
# unit testing (create subroutines for this purpose)
# test for N being an integer
##::::::: some functions we shall need

def Is_Alphabet(c):
    """checks to see if c is an English alphabet"""
    ordc = ord(c)
    is_lower = ord('a') <= ordc and ordc <= ord('z')
    is_upper = ord('A') <= ordc and ordc <= ord('Z')
    return is_lower or is_upper

assert Is_Alphabet('c')
assert Is_Alphabet('D')
assert not Is_Alphabet(',')

def my_len(in_str):
    """utility function to find the length of a string"""
    i = 0
    for c in in_str:
        i += 1
    return i

assert my_len("hello") == 5
assert my_len("") == 0


def shift_char(c, n):
    """Shifts a character 'c' by 'n', where 'n' is an integer.
    Does appropriate wrapping if the letter 'z'/'Z' is passed during shift.
    Does NOT error out for absolute values of 'n' larger than 26; instead,
    it takes the remainder divided by 26"""

    assert Is_Alphabet(c)
    
    n = n % 26 # n is now between 0 and 25
    
    assert 0 <= n and n <= 25

    ordc = ord(c)
    if ordc <= ord('z'):
        lower = ord('a')
        upper = ord('z')
    else :
        lower = ord('A')
        upper = ord('Z')

    ord_out = ordc + n
    if ord_out > upper:
        ord_out = lower + ord_out - upper
    return chr(ord_out)

assert shift_char('a',   0) == 'a'
assert shift_char('a',   2) == 'c'
assert shift_char('c',  -2) == 'a'
assert shift_char('y',   2) == 'a'
assert shift_char('a',  26) == 'a'
assert shift_char('b', -26) == 'b'
assert shift_char('y',  54) == 'a'


##:::::::: custom Exception classes

class Non_Alphabet(Exception):
    pass

class Bad_Length(Exception):
    pass

class Not_Int(Exception):
    pass

##:::::::: the main routine

def main():

    ##::::: setup: input and error checking
    
    S     = raw_input("Please enter the string S: ")
    C     = raw_input("Please enter the string C: ")
    N_str = raw_input("Please enter a natural number: ")

    S_len = my_len(S)
    
    if S_len % 3 != 0:
        raise Bad_Length("Length of S was " + str(S_len)
                         + " but must be a multiple of 3")

    if not all([Is_Alphabet(c) for c in S]):
        raise Non_Alphabet("S contains non-alphabetic characters")

    if not all([Is_Alphabet(c) for c in C]):
        raise Non_Alphabet("C contains non-alphabetic characters")

    try:
        N = int(N_str)
    except ValueError, ve:
        raise Not_Int(N_str + " is not a valid int")

    ##::::: algorithm

##:::::::::: only run this if called from the console, not when we import
##:: the module

if __name__ == "__main__":
    main()
