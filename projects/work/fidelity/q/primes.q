// primes.q
// Find all primes from 1 to N differing by a given amount
//
//
// As Q/KDB practice, this has been written to use tables and q-sql rather than
// other possible container-based techniques.

primes:{[N]
 
 // setup
 maxFactor: floor sqrt N;
 v:2+til N-1; // skip 1 because 1 is not a prime
 tN:([k:v]n:v); // keyed table
 
 // A helper function used only inside primes
 sieve:{[t; thisFactor; N]
  
  // QUESTION: Isn't there a better notation than t[thisFactor;`n]?
  if [t[thisFactor; `n] = 0;
   :t];
  
  // QUESTION: perhaps the -[;1] notation for subtracting one is too obscure?
  // QUESTION: is there a functional form that can do this better, such as ?[a;b;c;d]?
  allMults:thisFactor * 2 + til -[;1] floor N%thisFactor;
  update n:0 from t where `boolean$(allMults?k) <> count allMults};
 // end of sieve
 
 // repeat the recursive function maxFactor times.  QUESTION: any way
 // to vectorize this?
 thisFactor : 2;
 do [maxFactor-1; tN : sieve[tN; thisFactor; N]; thisFactor +:1];
 tPrimes: select from tN where n > 0}

// now try to find all pairs of primes that differ by 'diff'
// The self join functionality is pretty confusing to me (how do you
 // deal with columns that have the same names?) so unfortunately
// I am creating a new table and re-naming the columns.

numMatches:{[tPrimes; diff]
 show "nm1";
 show "diff";
 show diff;
 show "tPrimes";
 show tPrimes;
 tpp: `k xkey `p`k xcol () xkey tPrimes + diff;
 show "nm2";
 tMatches: tPrimes ij tpp;
 show "nm3";
 :count tMatches}
// end of primes

calcMatches: {[N]
 tPrimes:primes N;
 allDiffs: 2 * til floor (sqrt N%2); // no particular reason for this max
 show "1";
 / numMatchesProj: numMatches[tPrimes;]
 / show "2";
 / show numMatchesProj[4];
 / show "3";
 tCounts:([diffs:allDiffs] nums: til count allDiffs);
 show "4";
 tCounts: update nums:numMatches[tPrimes;diffs] from tCounts;
 :tCounts;
 }
