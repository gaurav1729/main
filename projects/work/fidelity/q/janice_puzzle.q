// Solve c0 b0 a0 + c1 b1 a1 = cs bs as where the letters are in 0-9
//  and there are no repeats.

/
Steps:

Note that if you look at the columns of numbers, they can:
 (i)   add up, 
 (ii)  add up and carry 1 over
 (iii) add up if you carried 1 over.
 (iv)  add up if you carried 1 over and also carry 1 over

There are three possibilities:
 a) All three columns belong to category (i)
 b) One from (i), (ii), (iii)
 c) One from (ii), (iii), (iv)

This method looks at the second possibility (b):

1) Create list of numbers v1: 0 to 9, v2: 1 to 9
2) Create tPairs: all ordered pairs (x,y) from v2, where x < y
3) Create three tables: 
  a) tDirect, (x,y,z) where x,y,z in v2, unequal, x < y, x + y = z
  b) tCarry,  (x,y,z) where x,y in v2, z in v1, unequal, x < y, x + y = z + 10
  c) tCarried,(x,y,z) where x,y in v1, z in v2, unequal, x < y, x + y = z - 1
  d) tBoth,   (x,y,z) where x,y,z in v1, unequal, x < y, x + y = z + 9
4) t01 is a join of tDirect and tCarry where Disjoint returns true
5) Join t01 and tCarried with the condition that Disjoint returns true
\




//::::: Start the algorithm

/ 1) Create list of numbers v1: 0 to 9, v2: 1 to 9

v09:1 + til 9;
v10:til 10;

t09:([] v:v09);
t10:([] v:v10);

/ 2) Create tPairs: all ordered pairs (x,y) from v2, where x < y

v1009: v10 cross v09;
t1009:([]a0:v1009[;0];a1:v1009[;1]);
t1009:select from t1009 where a0<a1

v0909: v09 cross v09
t0909:select from ([]a0:v0909[;0]; a1:v0909[;1]) where a0 < a1


/ 3) Create three tables: 
/   a) tDirect, (x,y,z) where x,y,z in v2, unequal, x < y, x + y = z
/   b) tCarry,  (x,y,z) where x,y in v2, z in v1, unequal, x < y, x + y = z + 10
/   c) tCarried,(x,y,z) where x,y in v1, z in v2, unequal, x < y, x + y = z - 1
/   d) tBoth,   (x,y,z) where x,y,z in v2, unequal, x < y, x + y = z + 9

tDirect:  select a0, a1, as: a0 + a1
  from t0909 where (t0909.a0 + t0909.a1) < 10; // 16
tCarry:   select b0:a0, b1:a1, bs: (a0 + a1 - 10)
  from t0909 where (t0909.a0 + t0909.a1) >  9; // 20
// We assume numbers cannot start with '0'.  If not, use t1009 in the next statement.
tCarried: select c0:a0, c1:a1, cs: (a0 + a1 +  1)
  from t0909 where (t0909.a0 + t0909.a1) <  9; // 20
tBoth:    select d0:a0, d1:a1, ds: (a0 + a1 - 9) from t0909
  where ((t0909.a0 + t0909.a1) > 9) and (t0909.a0 + t0909.a1-9) <> t0909.a0; // 12




/ 4) tba is a join of tDirect and tCarry where Disjoint returns true

tba:tDirect cross tCarry;
tbat:update countba:(count each distinct each tba.a0,' tba.a1,'tba.as,'tba.b0,'tba.b1,'tba.bs)
  from tba;
tba:select from tbat where tbat.countba=6; // 86
delete countba from `tba;


/ 5) Join t01 and tCarried with the condition that Disjoint returns true

tcba: tba cross tCarried;
tcbat: update countcba:(count each distinct each tcba.a0,'tcba.a1,'tcba.as,'tcba.b0,'tcba.b1,'tcba.bs,'tcba.c0,'tcba.c1,'tcba.cs) from tcba;
tcba:select from tcbat where tcbat.countcba=9;
// put it in order so we can read the numbers off of it
tcba: select c0,b0,a0,c1,b1,a1,cs,bs,as from tcba;

st:{[c,b,a]string each a+10*b+10*c};
tEqs:select  (st[cs;bs;as] ,' "=" ,/: st[c1;b1;a1] ,' "+" ,/: st[c0;b0;a0] ) from tcba;
show tEqs;