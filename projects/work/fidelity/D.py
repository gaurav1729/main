#!/usr/bin/env python

## Fidelity coding test
## 2014-07-25
## Gaurav Shah
## gaurav1729@gmail.com

# to run it, $> python B.py

##:::::::: custom Exception class for later

class Bad_Input(Exception):
    pass


##::::::: some functions we shall need

def Is_Alphabet(c):
    """c is a character.  checks to see if c is an English alphabet"""
    try:
        ordc = ord(c)
    except TypeError, te: # if ord() does not work on it, it's not an alphabet
        return False
    is_lower = ord('a') <= ordc and ordc <= ord('z')
    is_upper = ord('A') <= ordc and ordc <= ord('Z')
    return is_lower or is_upper

assert     Is_Alphabet('c')
assert     Is_Alphabet('D')
assert not Is_Alphabet(',')
assert not Is_Alphabet('^')
assert not Is_Alphabet("hello")
assert not Is_Alphabet(90)

##::::

def str2arr(in_string):
    """ the question requests strings be stored as arrays"""
    return [c for c in in_string]

##::::

def my_len(in_vec):
    """utility function to find the length of an array instead of len()"""
    assert type(in_vec) is type(["a"])
    i = 0
    for c in in_vec:
        i += 1
    return i

assert my_len(["1", "2"]) == 2
assert my_len([]) == 0


##::::

def veccat(v1, v2):
    """concatenate two arrays.  equivalent of pythonic v1 + v2"""
    out = (my_len(v1) + my_len(v2)) * [0]
    for i in range(my_len(v1)):
        out[i] = v1[i]
    for i in range(my_len(v2)):
        out[my_len(v1) + i] = v2[i]
    return out

assert veccat([0,1], [2,3]) == [0,1,2,3]
assert veccat(   [], [0,1]) == [0,1]


##::::

def shift_char(c, n):
    """Shifts a character 'c' by 'n', where 'n' is an integer.
    Does appropriate wrapping if the letter 'z'/'Z' is passed during shift.
    Does NOT error out for absolute values of 'n' larger than 26; instead,
    it takes the remainder divided by 26"""

    assert Is_Alphabet(c)
    assert type(n) is type(0)
    n = n % 26 # n is now between 0 and 25
    
    assert 0 <= n and n <= 25

    ordc = ord(c)
    if ord('a') <= ordc and ordc <= ord('z'):
        lower = ord('a')
        upper = ord('z')
    elif ord('A') <= ordc and ordc <= ord('Z'):
        lower = ord('A')
        upper = ord('Z')
    else :
        raise Bad_Input(c + " is not in the English alphabet")

    ord_out = ordc + n
    if ord_out > upper:
        ord_out = lower + ord_out - upper - 1
    return chr(ord_out)

assert shift_char('a',   0) == 'a'
assert shift_char('a',   2) == 'c'
assert shift_char('a',  -1) == 'z'
assert shift_char('c',  -2) == 'a'
assert shift_char('y',   2) == 'a'
assert shift_char('a',  26) == 'a'
assert shift_char('b', -26) == 'b'
assert shift_char('y',  54) == 'a'
assert shift_char('Y',   2) == 'A'
assert shift_char('A',  -1) == 'Z'

##:::::::: 

def main():

    ##::::: setup: input and error handling

    ##::: first, the string S
    S = raw_input("Please enter the string S: ")
    #:: the conditions of the problem request that the strings be
    #:: stored as arrays:
    S = [c for c in S]
    S_len = my_len(S)
    if S_len % 3 != 0:
        raise Bad_Input("Length of S was " + str(S_len)
                         + " but must be a multiple of 3")
    if not all([Is_Alphabet(c) for c in S]):
        raise Bad_Input("S contains non-alphabetic characters")
    if S_len == 0:
        raise Bad_Input("Please provide a non-zero length string!")


    ##:::: the string C
    C = raw_input("Please enter the string C: ")
    C = [c for c in C]
    C_len = my_len(C)
    if not all([Is_Alphabet(c) for c in C]):
        raise Bad_Input("C contains non-alphabetic characters")
    if C_len == 0:
        raise Bad_Input("Please provide a non-zero length string!")


    ##:::: Integer N
    N_str = raw_input("Please enter a natural number: ")
    try:
        N = int(N_str)
    except ValueError, ve:
        raise Bad_Input(N_str + " is not a valid int")

    string_manipulate(S, C, N)
    return


##::::::::::

def S2_ready_func(S2, C_shift, C, C_len):
    """First element of tuple returned is whether C_shift is found at the start
    of S2 (with possible other letters interspersed.  If False, empty lists are
    returned for the next two elements.

    Second element is the part of S2 that just barely contains C_shift.

    The third element is the rest of S2.

    The fourth element is the first part of S2 with C_shift replaced by C.
    """
    S2_len = my_len(S2)

    if S2[0] != C_shift[0]:
        return (False, [], [], [])

    S2_emended = []

    ics = 0
    for is2 in range(0, S2_len):
        if C_shift[ics] == S2[is2]:
            S2_emended = veccat(S2_emended, [C[ics]])
            ics += 1
            if ics == C_len:
                return (True, S2[:is2+1], S2[is2+1:], S2_emended)
        else:
            S2_emended = veccat(S2_emended, [S2[is2]])

    # actually, in this case, there is no point iterating further in S2,
    # since there is no chance of a match of C_shift:
    return (False, [], [], []) 

    

##::::::::::

def recursive(S1, S2, C, N, C_shift, S_len, C_len):

    if my_len(S1) < C_len or my_len(S2) < C_len:
        return (S1, S2)

    S1_ready = all([S1[i] == C[i]       for i in range(C_len)])
    S2_ready = all([S2[i] == C_shift[i] for i in range(C_len)])
    S2_ready, S2_left, S2_right, S2_new = S2_ready_func(
        S2, C_shift, C, C_len)


    if S1_ready and S2_ready:
        segs = recursive(S1[C_len:], S2_right, C, N, C_shift, S_len, C_len)
        return (veccat(C_shift, segs[0]), veccat(S2_new, segs[1]))

    if S1_ready and not S2_ready:
        segs = recursive(S1, S2[1:], C, N, C_shift, S_len, C_len)
        return (segs[0], veccat([S2[0]], segs[1]))

    if not S1_ready and S2_ready:
        segs = recursive(S1[1:], S2, C, N, C_shift, S_len, C_len)
        return (veccat([S1[0]], segs[0]), segs[1])

    if not S1_ready and not S2_ready:
        segs = recursive(S1[1:], S2[1:], C, N, C_shift, S_len, C_len)
        return (veccat([S1[0]], segs[0]), veccat([S2[0]], segs[1]))
    assert False # we should never reach here


##::::::::::

def string_manipulate(S, C, N, do_print = True):
    C_len = my_len(C)
    S_len = my_len(S)

    assert S_len % 3 == 0

    ##::: copy the first third of S into one array, and the rest into another
    S1 = [0] * (S_len/3)
    S2 = [0] * (2*S_len/3)

    for i in range(S_len/3):
        S1[i] = S[i]

    for i in range(S_len/3, S_len):
        S2[i - S_len/3]   = S[i]

    ##::::: shift
    C_shift = [shift_char(c, N) for c in C]

    str_segs = recursive(S1, S2, C, N, C_shift, S_len, C_len)
    out = ''.join(str_segs[0] + str_segs[1])

    if do_print:
        print "S     : " + ''.join(S)
        print "C     : " + ''.join(C)
        print "N     : " + str(N)
        print "Answer: " + out
    return out

##:: end of main()
s1 = "ABCXXABCXXBXXCXDXXBCD"
s2 = "ABC"
s3 = "BCDXXABCXXAXXBXCXXBCD"
assert string_manipulate(str2arr(s1), str2arr(s2), 1, False) == s3
assert string_manipulate(str2arr("ABC"), str2arr("A"), 1, False) == "BAC"

s1 = "ABCXXABCXXBCDXXBCD"
s2 = "ABC"
s3 = "BCDXXAABXXBCCXXBCD"
assert string_manipulate(str2arr(s1), str2arr(s2), 1, False) == s3

s1 = "ABXXABXXBXCXXXXXXXXXXXBC"
s2 = "AB"
s3 = "BCXXBCXXAXBXXXXXXXXXXXAB"
assert string_manipulate(str2arr(s1), str2arr(s2), 1, False) == s3

##:::::::::: 

##:: only run this if called from the console, not when we load the module

if __name__ == "__main__":
    main()
