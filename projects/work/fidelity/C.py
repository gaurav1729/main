#!/usr/bin/env python

## Fidelity coding test
## 2014-07-25
## Gaurav Shah
## gaurav1729@gmail.com

# to run it, $> python C.py


# NOTE: The question was not completely clear on how "greedy" this algorithm is
# intended to be.  For example, if C is "AB", C_shift is "BC" and the string S
# is "ABXBXBXCX", should the result be "BCXAXBXBX" or "BCXBXAXBX"?  This code
# assumes the first is correct.  In other words, do we choose the longest string
# C_shift, or the smallest?


##:::::::: custom Exception class for later

class Bad_Input(Exception):
    pass


##::::::: some functions we shall need

def Is_Alphabet(c):
    """c is a character.  checks to see if c is an English alphabet"""
    try:
        ordc = ord(c)
    except TypeError, te: # if ord() does not work on it, it's not an alphabet
        return False
    is_lower = ord('a') <= ordc and ordc <= ord('z')
    is_upper = ord('A') <= ordc and ordc <= ord('Z')
    return is_lower or is_upper

assert     Is_Alphabet('c')
assert     Is_Alphabet('D')
assert not Is_Alphabet(',')
assert not Is_Alphabet('^')
assert not Is_Alphabet("hello")
assert not Is_Alphabet(90)

##::::

def str2arr(in_string):
    return [c for c in in_string]

##::::

def my_len(in_vec):
    """utility function to find the length of an array"""
    assert type(in_vec) is type(["a"])
    i = 0
    for c in in_vec:
        i += 1
    return i

assert my_len(["1", "2"]) == 2
assert my_len([]) == 0

##::::

def shift_char(c, n):
    """Shifts a character 'c' by 'n', where 'n' is an integer.
    Does appropriate wrapping if the letter 'z'/'Z' is passed during shift.
    Does NOT error out for absolute values of 'n' larger than 26; instead,
    it takes the remainder divided by 26"""

    assert Is_Alphabet(c)
    assert type(n) is type(0)
    n = n % 26 # n is now between 0 and 25
    
    assert 0 <= n and n <= 25

    ordc = ord(c)
    if ord('a') <= ordc and ordc <= ord('z'):
        lower = ord('a')
        upper = ord('z')
    elif ord('A') <= ordc and ordc <= ord('Z'):
        lower = ord('A')
        upper = ord('Z')
    else :
        raise Bad_Input(c + " is not in the English alphabet")

    ord_out = ordc + n
    if ord_out > upper:
        ord_out = lower + ord_out - upper - 1
    return chr(ord_out)

assert shift_char('a',   0) == 'a'
assert shift_char('a',   2) == 'c'
assert shift_char('a',  -1) == 'z'
assert shift_char('c',  -2) == 'a'
assert shift_char('y',   2) == 'a'
assert shift_char('a',  26) == 'a'
assert shift_char('b', -26) == 'b'
assert shift_char('y',  54) == 'a'
assert shift_char('Y',   2) == 'A'
assert shift_char('A',  -1) == 'Z'

##:::::::: state class that helps us do the analysis

class StateClass(object):
    def __init__(self, S, C, C_shift):
        self.str = S[:]      # the string we change for output
        self.ref = "012345678901234567890" # purely for debugging
        self.S   = S[:]      # a copy of the original string S
        self.C   = C[:]
        self.CS  = C_shift[:]
        self.S_len = my_len(S)
        self.C_len = my_len(C)

        self.i1 = -1       # index to look into the first third of S
        self.i2 = self.S_len/3 -1 # index to look into the second part of S

        self.n1 = 0       # number of characters of C that have been matched so far
        self.n2 = 0       # number of characters of C_shift that have been matched

        self.r1 = 0       # index of which character to replace in first third
        self.r2 = []      # indexes of which character to replace in second part
        self.ir2 = 0      # index into r2 array
        self.nrep = 0     # number of characters which have been replaced

        self.all_states = ['SEEKING_C_MATCH',
                           'MID_C_MATCH',
                           'SEEKING_CS_MATCH',
                           'MID_CS_MATCH',
                           'REPLACING_C_STRING',
                           'REPLACING_CS_STRING',
                           'END_OF_STRING',
                           ]
        self.state = 'SEEKING_C_MATCH'
        return # not necessary, but makes it easier to read!
    # end of __init__
    ##::::::::::::::
    def print_obj(self):
        """to print the entire object"""
        for name, obj in self.__dict__.iteritems():
            print '%-25s --> %r' %(name, obj)

    ##::::::::::::::
    def __repr__(self):
        """printing just some of the more pertinent aspects of the state"""
        s = [
            "string   : " + ''.join(self.str),
            "S        : " + ''.join(self.S),
            "ref      : " + self.ref,
            "C        : " + ''.join(self.C),
            "CS       : " + ''.join(self.CS),
            "i1    : " + str(self.i1),
            "i2    : " + str(self.i2),
            "n1    : " + str(self.n1),
            "n2    : " + str(self.n2),
            "r1    : " + str(self.r1),
            "r2    : " + str(self.r2),
            "ir2   : " + str(self.ir2),
            "nrep  : " + str(self.nrep),
            "state : " + self.state,
            ]
        return "\n".join(s)
        
    ##:: end of __repr__

    ##::::::::::::::
    def next_step(self):
        """proceed to the next step, based on the state we're already in"""
        assert self.state != 'END_OF_STRING'
        assert self.state in self.all_states
        if self.state == 'SEEKING_C_MATCH':
            self.i1 += 1 # look at the next character to find a match

        if self.state == 'MID_C_MATCH':
            self.i1 += 1 # examine the next character to see if it is a match

        if self.state == 'SEEKING_CS_MATCH': # searching for a copy of C_shift
            self.i2 += 1 

        if self.state == 'MID_CS_MATCH':
            self.i2 += 1

        if self.state == 'REPLACING_C_STRING':
            self.r1 += 1 # move to the next character to replace
        
        if self.state == 'REPLACING_CS_STRING':
            self.ir2 += 1

        self.refresh()
        return
    ## end of next_step


    ##::::::::::::::
    def refresh(self):
        """take action based on the step we've just taken"""
        if self.i1 == self.S_len/3 or self.i2 == self.S_len:
            self.state = 'END_OF_STRING'
            return
        ##:: end of END_OF_STRING

        if self.state == 'SEEKING_C_MATCH':
            if self.S[self.i1] == self.C[0]:
                self.n1 = 1
                self.r1 = self.i1
                # if we're done with the C string, move onto C_shift:
                if self.C_len == 1: 
                    self.state = 'SEEKING_CS_MATCH'
                    self.n1 = 0
                    self.r1 -= 1
                else :
                    self.state = 'MID_C_MATCH'
                return
            return
        ##:: end of NO_S_MATCH

        if self.state == 'MID_C_MATCH':
            assert self.n1 != self.C_len

            if self.S[self.i1] != self.C[self.n1]:
                self.n1 = 0
                self.r1 = 0
                self.state = 'SEEKING_C_MATCH'
                return
            assert self.S[self.i1] == self.C[self.n1]

            if self.S[self.i1] == self.C[self.n1]:
                self.n1 += 1
                if self.n1 == self.C_len:
                    self.state = 'SEEKING_CS_MATCH'
                    self.n1 = 0
                    self.r1 -= 1
                    return
                return
            assert False # we should never hit this line
        ##:: end of MID_S_MATCH

        if self.state == 'SEEKING_CS_MATCH':
            if self.S[self.i2] == self.CS[0]:
                self.r2.append( self.i2 )
                self.n2 = 1
                if self.C_len == 1:
                    self.state = 'REPLACING_C_STRING'
                    self.n2 = 0
                else:
                    self.state = 'MID_CS_MATCH'
                return
            return
        ##:: end of SEEKING_CS_MATCH

        if self.state == 'MID_CS_MATCH':
            # if self.S[self.i2] != self.CS[self.n2]:
            #     self.state = 'SEEKING_CS_MATCH'
            #     self.r2 = 0
            #     self.n2 = 0
            #     return

            if self.S[self.i2] == self.CS[self.n2]:
                self.n2 += 1
                self.r2.append(self.i2)
                if self.n2 == self.C_len:
                    self.state = 'REPLACING_C_STRING'
                    self.n2 = 0
                    return
                return
        ##:: end of SEEKING_CS_MATCH

        if self.state == 'REPLACING_C_STRING':

            self.str[self.r1] = self.CS[self.nrep]
            self.nrep += 1
            if self.nrep == self.C_len:
                self.state = 'REPLACING_CS_STRING'
                self.nrep = 0
                self.r1 = 0
                return
            return
        ##:: end of REPLACING_C_STRING


        if self.state == 'REPLACING_CS_STRING':
            self.str[self.r2[self.ir2-1]] = self.C[self.nrep]
            self.nrep += 1
            if self.nrep == self.C_len:
                self.state = 'SEEKING_C_MATCH'
                self.nrep = 0
                self.r2   = []
                self.ir2  = 0
                return
            return
        ##:: end of REPLACING_CS_STRING
    ##:: end of refresh
##:: end of state class

##:::::::: the main routine

def main():

    ##::::: setup: input and error handling

    ##::: first, the string S
    S = raw_input("Please enter the string S: ")
    #:: the conditions of the problem request that the strings be
    #:: stored as arrays:
    S = [c for c in S]
    S_len = my_len(S)
    if S_len % 3 != 0:
        raise Bad_Input("Length of S was " + str(S_len)
                         + " but must be a multiple of 3")
    if not all([Is_Alphabet(c) for c in S]):
        raise Bad_Input("S contains non-alphabetic characters")
    if S_len == 0:
        raise Bad_Input("Please provide a non-zero length string!")


    ##:::: the string C
    C = raw_input("Please enter the string C: ")
    C = [c for c in C]
    C_len = my_len(C)
    if not all([Is_Alphabet(c) for c in C]):
        raise Bad_Input("C contains non-alphabetic characters")
    if C_len == 0:
        raise Bad_Input("Please provide a non-zero length string!")


    ##:::: Integer N
    N_str = raw_input("Please enter a natural number: ")
    try:
        N = int(N_str)
    except ValueError, ve:
        raise Bad_Input(N_str + " is not a valid int")

    string_manipulate(S, C, N)
    return

def string_manipulate(S, C, N, do_print = True):

    ##::::: shift
    C_shift = [shift_char(c, N) for c in C]

    state = StateClass(S, C, C_shift)
    while(True):
        state.next_step()
        # print(repr(state))
        # print("")

        if state.state == 'END_OF_STRING':
            break

    if do_print:
        print "S     : " + ''.join(S)
        print "C     : " + ''.join(C)
        print "N     : " + str(N)
        print "Answer: " + ''.join(state.str)
    return ''.join(state.str)
##:: end of main()
s1 = "ABCXXABCXXBXXCXDXXBCD"
s2 = "ABC"
s3 = "BCDXXABCXXAXXBXCXXBCD"
assert string_manipulate(str2arr(s1), str2arr(s2), 1, False) == s3
assert string_manipulate(str2arr("ABC"), str2arr("A"), 1, False) == "BAC"

s1 = "ABCXXABCXXBCDXXBCD"
s2 = "ABC"
s3 = "BCDXXAABXXBCCXXBCD"
assert string_manipulate(str2arr(s1), str2arr(s2), 1, False) == s3

s1 = "ABXXABXXBXCXXXXXXXXXXXBC"
s2 = "AB"
s3 = "BCXXBCXXAXBXXXXXXXXXXXAB"
assert string_manipulate(str2arr(s1), str2arr(s2), 1, False) == s3
##:::::::::: 

##:: only run this if called from the console, not when we load the module

if __name__ == "__main__":
    main()
