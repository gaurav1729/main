setuptools>=18.5
decorator
pickleshare
simplegeneric>0.8
traitlets
pexpect

[nbformat]
nbformat

[all]
nbformat
ipykernel
pygments
testpath
notebook
nbconvert
ipyparallel
qtconsole
Sphinx>=1.3
requests
nose>=0.10.1
ipywidgets
mock

[kernel]
ipykernel

[doc]
Sphinx>=1.3

[:sys_platform == "darwin"]
appnope

[terminal:sys_platform == "win32"]
pyreadline>=2

[terminal]


[test:python_version == "2.7"]
mock

[:sys_platform == "darwin" and platform_python_implementation == "CPython"]
gnureadline

[notebook]
notebook
ipywidgets

[nbconvert]
nbconvert

[qtconsole]
qtconsole

[test]
nose>=0.10.1
requests
testpath
pygments
mock

[:sys_platform != "win32"]
pexpect

[parallel]
ipyparallel