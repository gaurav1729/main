#!/usr/bin/env python

## This should hopefully bring up Python 2.7!

##:::::::::::::::::::::::::::::

"""Kayak conversion rate analysis
Gaurav Shah
2016-04-13
"""



##:::::::::::::::::::::::::::::
##:: Setup
import os
import sys

import pandas     as pd
import datetime   as dt
import numpy      as np
import scipy      as sp

import matplotlib.pyplot as plt

##:::::::::::::::::::::::::::::
##:: Here are the variables the user can set:

env = {'fvisit'     : os.getcwd() + '/data_files-8/visits.csv', # visits file path
       'fconv'      : os.getcwd() + '/data_files-8/conversions.csv', #conversions file
       'doplotting' : True, # plot when we run the code?
       'min_monthly_entries' : 10 # minimum entries in a month to be included
       }



##:::::::::::::::::::::::::::::
##:: Some classes we will use

class Data_Block(object):
    """This handles data ingestion, and the data access methods."""
    
    import datetime
    import sqlite3
    import csv

    value_str = '' 
    tablename = ''
    conn = sqlite3.connect("kayak_experiment.db")

    ##:::::Class methods
    def __init__(self, filename):
        '''filename is either visits.csv or conversions.csv'''

        ## Is the visits file or the conversions file?
        if "visits" in filename:
            self.value_str = "user_visits"
        elif "conversions" in filename:
            self.value_str = "conversions"
        else:
            raise Exception("Wrong data file name {0}".format(filename))

        
        with self.conn:
            cur  = self.conn.cursor()
            ## create a table
            self.tablename = "kayak_" + self.value_str
            ## WARNING: this appears to be vulnerable to the SQL injection
            ## attack.  Sqlite3 provide '?' notation in the queries that should
            ## prevent this, but it gave errors when I tried to use it in this
            ## particular context.
            exec_str = ("CREATE TABLE {0} (datestamp DATE, country_code TEXT," + \
                        " marketing_channel TEXT, {1} INTEGER)").format(
                self.tablename, self.value_str)
            
            try:
                cur.execute(exec_str)
            except self.sqlite3.OperationalError, e:
                pass # table already exists


            ## parse each line of the file
            with open(filename, "rb") as filehandle:
                dreader = self.csv.DictReader(filehandle)
                data_list = [(line['datestamp'],
                              line['country_code'],
                              line['marketing_channel'],
                              line[self.value_str]) for line in dreader]

            ## insert the csv file into the database
            cur.executemany(("INSERT INTO {0} (datestamp, country_code, " + \
                             "marketing_channel, {1})"                    + \
                             " VALUES (?, ?, ?, ?);"
                             ).format(self.tablename, self.value_str),
                            data_list)
                            
        return

    ##::::
    def get_all_countries(self):
        '''list of all the entries in the country_code field'''
        with self.conn:
            cur = self.conn.cursor()
            exec_str = "select distinct country_code from {0}".format(self.tablename)
            return [c[0] for c in list(cur.execute(exec_str))]

    ##::::
    def get_all_channels(self):
        '''list of all marketing_channels'''
        with self.conn:
            cur = self.conn.cursor()
            exec_str = "select distinct marketing_channel from {0}".format(self.tablename)
            return [c[0] for c in list(cur.execute(exec_str))]
            
    ##::::
    def get_data(self, country_code = None, marketing_channel = None):
        '''Access data by either country code or marketing channel or both. For
        maximum convenience, get it in the form of a pandas time series.'''
        with self.conn:
            cur = self.conn.cursor()
            
            if country_code is not None and marketing_channel is not None:
                exec_str = ("select datestamp, {0} from {1} " + \
                           "where country_code=? and marketing_channel=?"
                            ).format(self.value_str, self.tablename) 
                out = list(cur.execute(exec_str,(country_code,
                                                 marketing_channel)))
                out = [ (dt.datetime( *[int(d) for d in x[0].split('-')]), x[1])
                        for x in out]
                dates, vals = zip(*out)                
                return pd.Series(vals, index = dates)

            if country_code is not None and marketing_channel is None:
                return {channel : self.get_data(country_code, channel)
                        for channel in self.get_all_channels() }
            
            if country_code is None and marketing_channel is  not None:
                return {cc : self.get_data(cc, marketing_channel)
                        for cc in self.get_all_countries() }

            if country_code is None and marketing_channel is None:
                return {cc : self.get_data(cc)
                        for cc in self.get_all_countries() }


    ##::::  
    def __del__(self): # Could be cleanup().  People get religious about this!
        with self.conn:
            cur = self.conn.cursor()
            exec_str = "drop table {0}".format(self.tablename)
            cur.execute(exec_str)
            return
                
##:::::::::::::::::::::::::::::
##:: Some functions we will use later

def ts_groom(ts_in, min_monthly):
    '''Takes a daily time series.  Resamples it to monthly, and
    restricts it to months that have a minimum number of days reporting.'''
    ts_out = ts_in.resample('M', kind = 'period').mean().dropna()
    in_count = pd.Series(1, index = ts_in.index).resample('M',
                                                          kind = 'period'
                                                          ).sum().dropna()
    return ts_out[in_count > min_monthly]
    
##:::
def vector_dot(v1, v2):
    '''dot product of two vectors'''
    assert len(v1) == len(v2)
    return sum([a1 * a2 for (a1, a2) in zip(v1,v2)])

##:::
def combine_channels(data_dict, min_monthly):
    '''Takes a dictionary whose key is the type of channel, and whose value is a
    time series showing with the raw visits or conversions data for that channel,
    for a particular country.

    Returns the monthly visits or conversions, with appropriate weighting for
    each channel.  This weighting is determined by finding the scaling factor to
    minimize the mean square difference between each channel and a given
    reference channel, over the interval where both time series are supported.

    '''
    
    # The longest channel is chose to be the reference channel, and we aggregate
    # it monthly.  Also remove any months with insufficient coverage.

    ts_groomed = { k : ts_groom(ts, min_monthly)
                   for k, ts in data_dict.items()}
    ts_groomed_keys = sorted(ts_groomed.keys(),
                             key=(lambda x: len(ts_groomed[x])),
                             reverse=True)
    ts0 = ts_groomed[ts_groomed_keys[0]] 

    ts0_index = ts0.index
    ts0_weight = pd.Series(1, ts0_index) # the weighting for the ref ts is 1

    # the coefficient for the reference channel is defined to be 1
    channel_coefs = [1] + [0]*(len(data_dict.keys())-1) 

    ts_sum    = ts0 # initializing the ts for the running sum
    ts_weight = ts0_weight # the ts for the running weights per day

    for i, channel in enumerate(ts_groomed_keys[1:]):
        i = i + 1 # because we're starting with the second channel
        ts_channel = ts_groom(data_dict[channel], min_monthly)
        ## Algorithm:
        ## 1. Find the dates that form the common support for these two series
        ## 2. Find the optimal coefficient to bring the two series closest together.
        ## 3. Multipy the second time series by that coefficient and add it to rolling sum
        ## 4. Create a weighting time series to keep track of how the weighting
        ##    of each time series that reports on a given date
        ## 5. Divide the sum ts by the count ts.

        ## 1. intersection of the two time series:
        common_months  = ts0_index.intersection(ts_channel.index)
        ts0_common     = ts0[common_months]
        channel_common = ts_channel[common_months]

        ## 2. Optimal coefficient:
        try:
            coef = vector_dot(ts0_common, channel_common)/vector_dot(channel_common,
                                                                     channel_common)
        except ZeroDivisionError, e:
            continue
        channel_coefs[i] = coef
    
        ## 3. Find the rolling sum:
        ts_sum = ts_sum.add( ts_channel, fill_value = 0)

        ## 4. Keep track of the weight of each reporting ts
        ts_weight = ts_weight.add(pd.Series(1.0/coef, index = ts_channel.index),
                                fill_value = 0)

    assert all(ts_weight != 0)
    return  ts_sum/ts_weight # the monthly average, with appropriate weighting

##:::::::::::::::::::::::::::::
##:: Setup

dv = Data_Block(env['fvisit']) # visits data
dc = Data_Block(env['fconv'])  # conversions data
                


##:::::::::::::::::::::::::::::
##:: Body




##:::::::::::
## 1.  Do you see any interesting trends in the data?  For example, are there
## any seasonality trends in user visits?  Do these seasonality trends differ between
## countries?  Can you think of any potential explanations for any of the patterns/
## trends/differences you saw?

## Let's look at one particular time series: say, Display Ads for Belgium

# visits:
vbeda = dv.get_data('be', 'Display Ads')
cbeda = dc.get_data('be', 'Display Ads')

# Let's plot this:

if  env['doplotting']:
    vbeda.plot(title='Belgium Display Ad visits',
               grid = True)
    plt.show()

## There are clear indications that this depends on the day of the week.  So let's
## aggregate by day of week and look at that:

vbeda_wd = vbeda.groupby(lambda x: x.weekday).mean()
if  env['doplotting']:
    plt.plot(vbeda_wd)
    plt.xticks(range(7), ("Monday","Tuesday","Wednesday",
                           "Thursday","Friday","Saturday","Sunday"))
    plt.title('Mean Belgium Display Ad visits by weekday')
    plt.show()

# Another way of showing this is looking at the auto-correlation.  There is a
# clear peak at 7.
vbedacorr = np.correlate(vbeda, vbeda, mode = 'full')
if  env['doplotting']:
    corrplot = plt.plot(vbedacorr[ vbedacorr.size/2 : vbedacorr.size/2 + 10])
    plt.title('Auto-correlation of Display Ad visits vs. offset')
    plt.show()

## To examine the seasonal dependence, we have to remove the dependence on the
## day of the week.  One way to do this to aggregate by month, as each month has
## approximately the same number of days for each day of the week.  Since
## different months have different numbers of days, and may have different
## amounts of data coverage, the mean is the best way of studying this.

vbeda_monthly = vbeda.resample('M', kind = 'period').mean()
if  env['doplotting']:
    vbeda_monthly.plot(title='Belgium Display Ad visits, aggregated monthly',
                       grid=True)
    plt.show()

## To study this seasonal dependence by country, it's more robust (and more
## informative) if we aggregate over all channels.  This raises some issues:
## What if all channels do not report data over all dates?  If we choose to do
## calculations only when *all* of the channels or countries have data, we throw
## away a lot of precious data.  And this scales very poorly as the number of
## countries or channels increases.  However, some channels have vastly larger
## numbers of visits than others, so we have to be careful combining them.

## Since variability over each day is large, it makes sense to aggregate the
## data first, on a monthly scale, say, to remove dependence on day of the week.

## Here is one way to combine the various channels for each country: Choose a
## scaling factor for each channel to make them commensurate, then take the
## average of whatever channels are reporting for each day.  An optimal solution
## would be to minimize the sums of the square distance of each point from the
## average for that date.  It's a little bit involved, computationally, so we're
## not going to go with that one for this coding project.

## Here is a heuristic solution that we can actually implement.  Choose one
## channel, and then scale each of the others to minimize the mean square
## difference with that channel.  This gives scaling factors for each of the
## other channels.  The visits data for the other channels can be multiplied by
## these scaling factors, and the resuting time series' can then be averaged to
## find the monthly variability of the visits or conversions.

## Note that this will lead to a solution that depends on the choice of the
## initial channel.  There are ways to minimize this dependence.  The most
## obvious method would be to choose each channel in turn as the reference
## channel and solve, and then average the answers.

## Note that a month that has only a few days of data is not going to be
## statistically significant, and should be excluded.

monthly_ave_visits = dict()

styles = [ ('-','o'),
           ('--','*'),
           ('-.','h'),
           (':','+'),
           ('-','d'),
           ('--','1'),
           ('-.','.'),
           ('-','s')]
#plt.figure()
for country in dv.get_all_countries():
    ts = combine_channels(dv.get_data(country),
                          env['min_monthly_entries'])
    ts = ts/ts.mean() # normalize to have an average of 1
    monthly_ave_visits[country] = ts
    ls, marker = styles.pop()
    try:
        if env['doplotting']:
            ts.plot(label = country, linestyle = ls, marker = marker)
    except Exception, e:
        pass
if env['doplotting']:
    plt.legend(loc="upper center", ncol = 4,
               bbox_to_anchor = (0.5, 1.0))
    plt.title('Monthly average visits per country')
    plt.grid()
    plt.show()

## As we can see, there are significant differences in the seasonality trends
## between many of the countries.  In particular, the warmer countries such as
## France, Greece and Portugal experience a large rise in visits in the late
## summer months of July and August.  It is possible that this is internal
## travel: it seems unlikely that the visitors to these countries would be
## looking up ways to get home at this late stage in the summer.  There is also
## a clear uptick in most countries in February -- it seems plausible that this
## is when people start planning their summer vacations and doing last minute
## searches to escape the winter.  This would imply that spring break vacations
## are planned at shorter notice than summer vacations, which is not unlikely.

##:::::::::::
## 2. Which channels have the best and worst conversion rates?  What reasons can
## you think of to explain the differences?  Do you see any major conversion
## differences for the same channel in different countries?

visits = {channel: dict()
          for channel in dv.get_all_channels()}

conversions = {channel: dict()
               for channel in dv.get_all_channels()}

summary = {channel: dict()
           for channel in dv.get_all_channels()}

for channel in dv.get_all_channels():
    for country in dv.get_all_countries():

        tsv = dv.get_data(country, channel)
        # bad data alert!  null strings found.
        tsc = dc.get_data(country, channel).replace('', np.NaN).dropna()
        common_dates = tsv.index.intersection(tsc.index)
        tsv = tsv[common_dates]
        tsc = tsc[common_dates]

        
        visits[channel][country]      = tsv.sum()
        conversions[channel][country] = tsc.sum()

    tot_visits      = sum([ visits[channel][country]
                            for country in dv.get_all_countries()])
    tot_conversions = sum([ conversions[channel][country]
                            for country in dv.get_all_countries()])
    ## This has a limitation: different countries have different mixes
    ## of the different channels, while all of them are being added up
    ## here.
    try:
        summary[channel] = float(tot_conversions)/tot_visits
    except ZeroDivisionError, e:
        summary[channel] = 0
    if env['doplotting']:
        plt.plot([ float(conversions[channel][country])/visits[channel][country]
                   if visits[channel][country] != 0 else NaN
                   for country in dv.get_all_countries()],
                 label = channel)




    
if env['doplotting']:
    plt.xticks(range(len(dv.get_all_countries())),dv.get_all_countries())
    plt.title('conversion rate per country for channel ' + channel)
    plt.legend(bbox_to_anchor = (0.5,1))
    plt.show()

## There are clear discrepancies between conversion rates for specific channels
## between different countries.  For example, Travel Search Sites are much more
## successful in Norway and Finland, while Display Ads are successful in France.

## plot summary of conversion rates
    plt.plot([summary[channel]
              for channel in dv.get_all_channels()])
    plt.xticks(range(len(summary)), dv.get_all_channels(),
               rotation = -15)
    plt.title('Conversion rates for all channels')
    plt.grid()
    plt.show()

## As we can see, the search engine results and emails have the highest
## conversion rates.  This makes sense: both of these require cooperation by the
## customers -- that is, they have to consciously choose to enlist in the
## emails, or search for a travel site.  This filters the customers into the
## more motivated buyers.



