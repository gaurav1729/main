


##:::::::::::
## 1.  Do you see any interesting trends in the data?  For example, are there
## any seasonality trends in user visits?  Do these seasonality trends differ between
## countries?  Can you think of any potential explanations for any of the patterns/
## trends/differences you saw?

## Let's look at one particular time series: say, Display Ads for Belgium


## There are clear indications that this depends on the day of the week.  So let's
## aggregate by day of week and look at that:


# Another way of showing this is looking at the auto-correlation.  There is a
# clear peak at 7.

## To examine the seasonal dependence, we have to remove the dependence on the
## day of the week.  One way to do this to aggregate by month, as each month has
## approximately the same number of days for each day of the week.  Since
## different months have different numbers of days, and may have different
## amounts of data coverage, the mean is the best way of studying this.


## To study this seasonal dependence by country, it's more robust (and more
## informative) if we aggregate over all channels.  This raises some issues:
## What if all channels do not report data over all dates?  If we choose to do
## calculations only when *all* of the channels or countries have data, we throw
## away a lot of precious data.  And this scales very poorly as the number of
## countries or channels increases.  However, some channels have vastly larger
## numbers of visits than others, so we have to be careful combining them.

## Since variability over each day is large, it makes sense to aggregate the
## data first, on a monthly scale, say, to remove dependence on day of the week.

## Here is one way to combine the various channels for each country: Choose a
## scaling factor for each channel to make them commensurate, then take the
## average of whatever channels are reporting for each day.  An optimal solution
## would be to minimize the sums of the square distance of each point from the
## average for that date.  It's a little bit involved, computationally, so we're
## not going to go with that one for this coding project.

## Here is a heuristic solution that we can actually implement.  Choose one
## channel, and then scale each of the others to minimize the mean square
## difference with that channel.  This gives scaling factors for each of the
## other channels.  The visits data for the other channels can be multiplied by
## these scaling factors, and the resuting time series' can then be averaged to
## find the monthly variability of the visits or conversions.

## Note that this will lead to a solution that depends on the choice of the
## initial channel.  There are ways to minimize this dependence.  The most
## obvious method would be to choose each channel in turn as the reference
## channel and solve, and then average the answers.

## Note that a month that has only a few days of data is not going to be
## statistically significant, and should be excluded.


## As we can see, there are significant differences in the seasonality trends
## between many of the countries.  In particular, the warmer countries such as
## France, Greece and Portugal experience a large rise in visits in the late
## summer months of July and August.  It is possible that this is internal
## travel: it seems unlikely that the visitors to these countries would be
## looking up ways to get home at this late stage in the summer.  There is also
## a clear uptick in most countries in February -- it seems plausible that this
## is when people start planning their summer vacations and doing last minute
## searches to escape the winter.  This would imply that spring break vacations
## are planned at shorter notice than summer vacations, which is not unlikely.

##:::::::::::
## 2. Which channels have the best and worst conversion rates?  What reasons can
## you think of to explain the differences?  Do you see any major conversion
## differences for the same channel in different countries?


## There are clear discrepancies between conversion rates for specific channels
## between different countries.  For example, Travel Search Sites are much more
## successful in Norway and Finland, while Display Ads are successful in France.

## As we can see, the search engine results and emails have the highest
## conversion rates.  This makes sense: both of these require cooperation by the
## customers -- that is, they have to consciously choose to enlist in the
## emails, or search for a travel site.  This filters the customers into the
## more motivated buyers.



#import os; import sys; sys.path.append('.'); from solution import *
