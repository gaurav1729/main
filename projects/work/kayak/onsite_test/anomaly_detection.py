#!/usr/bin/env python

## This was written in Python 2.7, not 3.x!XS

'''
Kayak onsite coding tets.

Gaurav Shah
'''

## First cut: we're looking for anomalous values in revenue/visit.
##
## What are the different possible types of anomalies?  That could include:
##
## (a) values that are NaN or Inf
## (b) missing values
## (c) values that are too far outside the expected range.
## (d) repeated values
##
## What's the expected range?  That can depend on the seasonality or day of the
## week.

## NOTE: we will be focusing on values that are too far out of the range!  For
## the time being, we will ASSUME that all values have been fixed if they are
## Nan, Inf, missing, or repeated.


##::::: User controls:
working_dir =  '/home/gaurav/Dropbox/gaurav/svnco/svn_repository/work/kayak/onsite_test'
data_file   = 'interview_data_set.csv'
do_plot     = True

rolling_window = 15
min_window     = 10
num_stddevs    =  2

##::::: Setup

import pandas as pd
import csv
import os
import matplotlib.pyplot as plt
import datetime as dt

os.chdir('')
df = pd.read_csv(data_file)
# keys: account_name, date, device_type, visits, cost, revenue


##::::: Analysis

account_names = df.account_name.unique()

## Let's do it for just one first.  Automating it for other accounts will be
## easier later.

## First, just revenue:
df0 = df[df.account_name  == account_names[0]]
df0 = df0[df0.device_type == 'c']

vals  = df0.revenue
title = 'Revenue'

dates = [dt.datetime.strptime(d, '%Y-%m-%d') for d in df0.date]
ts = pd.Series(vals.values, index =dates)
ts.plot()
plt.grid()
plt.title(title)
plt.show()


## Next, revenue per visit:

df0 = df[df.account_name  == account_names[0]]
df0 = df0[df0.device_type == 'c']

vals  = df0.revenue/df0.visits
title = 'Revenue per visit'

dates = [dt.datetime.strptime(d, '%Y-%m-%d') for d in df0.date]
ts = pd.Series(vals.values, index =dates)
ts.plot()
plt.grid()
plt.title(title)
plt.show()

## This shows a strong variability.  Let's just look at approximately one month.

df0 = df[df.account_name  == account_names[0]]
df0 = df0[df0.device_type == 'c']
df0 = df0[50:80]

vals  = df0.revenue/df0.visits
title = 'Revenue per visit for one month'

dates = [dt.datetime.strptime(d, '%Y-%m-%d') for d in df0.date]
ts = pd.Series(vals.values, index =dates)
ts.plot()
plt.grid()
plt.title(title)
plt.show()

## This plot showed that the variability was *not* weekly.

## First cut at finding the anomalous values: Find the running variance over a
## window, flag values that are too many sigmas outside that range.

##  WARNING:
## this will be less effective at finding values that are too small.  So a
## better method might be to take a logarithm of the value first, and then do
## the calculations.

rolling_std = pd.rolling_std(ts, 15, min_periods = 10)
rolling_ave = pd.rolling_mean(ts, 15, min_periods = 10)

ts_min = rolling_ave - num_stddevs * rolling_std
ts_max = rolling_ave + num_stddevs * rolling_std

ts_min.plot()
ts_max.plot()
ts.plot()
plt.show()

ts_bad = ts[ (ts > ts_max) | (ts < ts_min) ]

## Now we can tie it together for all of the time series:
bad_vals = dict()
for account_name in account_names:
    
