#!/usr/bin/env python

## This should hopefully bring up Python 2.7!

##:::::::::::::::::::::::::::::
## Notes for Gaurav.

## Number of lines in conversions and visits is different.  Check for duplicate
## data, and for contradictory data, and missing data.  Remember data can be
## missing both ways.  Check for different channels and see if there are any
## with too few values, or too many; same for country, or date.

## Check for outliers.  You can do this by date, or by country, or by channel.

## Plot data, perhaps aggregated by month.

## Detrend the data

## Need to understand: how will we be calling the data?  The data structure
## seems to depend on that.  If we are looking at time series, perhaps the order
## should be country --> channel --> date [value].  Create a class to put the
## data into?

## When we make predictions, compare the values of the regression coefficients
## for different parameters.  We may like to improve robustsness by increasing
## sample size by aggregating.

## For the last one (5): kalman filter?  

## Don't forget unit tests and error classes

##:::::::::::::::::::::::::::::

"""Conversion rate analysis
Gaurav Shah
2016-04-13
"""

##:::::::::::::::::::::::::::::
##:: Setup
import os
import sys

import pandas as pd
import matplotlib as plt
import datetime as dt

#from collections import defaultdict



##:::::::::::::::::::::::::::::
##:: Here are the variables the user can set:

env = {'fvisit' : os.getcwd() + '/data_files-8/visits.csv',
       'fconv'  : os.getcwd() + '/data_files-8/conversions.csv'
       }


##:::::::::::::::::::::::::::::
##:: Some classes we will use

class Data_Block(object):
    """This handles data ingestion, and the data access methods."""
    
    
    import datetime
    import sqlite3
    import csv


    value_str = {} # gsdebug: give underscore names?
    tablename = {}
    conn = sqlite3.connect("kayak_experiment.db")

    ##:::::Class methods
    def __init__(self, fvisit, fconv):
        '''filename is either visits.csv or conversions.csv'''

        self.value_str[fvisit] = "user_visits"
        self.value_str[fconv]  = "conversions"

        self.tablename[fvisit] = "kayak_" + self.value_str[fvisit]
        self.tablename[fconv]  = "kayak_" + self.value_str[fconv]


        
        with self.conn:
            cur  = self.conn.cursor()
            ## create a table
            self.tablename = "kayak_" + self.value_str
            exec_str = ("CREATE TABLE {0} (datestamp DATE, country_code TEXT," + \
                        " marketing_channel TEXT, {1} INTEGER)").format(
                self.tablename, self.value_str)
            
            try:
                cur.execute(exec_str)
            except self.sqlite3.OperationalError, e:
                pass # table already exists


            ## parse each line of the file
            with open(filename, "rb") as filehandle:
                dreader = self.csv.DictReader(filehandle)
                self.data_list = [(line['datestamp'],
                                   line['country_code'],
                                   line['marketing_channel'],
                                   line[self.value_str]) for line in dreader]

            ## insert the csv file into the database
            cur.executemany(("INSERT INTO {0} (datestamp, country_code, " + \
                             "marketing_channel, {1})"                    + \
                             " VALUES (?, ?, ?, ?);"
                             ).format(self.tablename, self.value_str),
                            self.data_list)
                            
        return

    ##::::
    def get_all_countries(self):
        '''list of all the entries in the country_code field'''
        with self.conn:
            cur = self.conn.cursor()
            ## in the general case, this would be a union of the country
            ## codes from both tables
            exec_str = "select distinct country_code from {0}".format(
                self.tablename[fvisit])
            return [c[0] for c in list(cur.execute(exec_str))]

    ##::::
    def get_all_channels(self):
        '''list of all marketing_channels'''
        with self.conn:
            cur = self.conn.cursor()
            ## in the general case, this would be a union of the marketing
            ## channels from both tables
            exec_str = "select distinct marketing_channel from {0}".format(
                self.tablename[fvisit])
            return [c[0] for c in list(cur.execute(exec_str))]
            
    ##::::
    def get_data(self, country_code = None, marketing_channel = None):
        '''Access data by either country code or marketing channel or both, in
        the form of a (dictionary of) pandas time series.  This may be a little
        slower, but gets the data in a form that's easier to use.'''
        with self.conn:
            cur = self.conn.cursor()
            
            if country_code is not None and marketing_channel is not None:
                # exec_str = ("select datestamp, user_visits from {0} " + \
                #            "where country_code=? and marketing_channel=?"
                #             ).format(self.tablename)
                exec_str = '''
select vis.datestamp, vis.user_visits, con.conversions
from ? as vis
  outer join ? as con
    on vis.datestamp = con.datestamp
where country_code=? and market_channel=?
'''
                out = list(cur.execute(exec_str,(self.tablenames[fvisit],
                                                 self.tablenames[fconv],
                                                 country_code,
                                                 marketing_channel)))
                out = [ (dt.date( *[int(d) for d in x[0].split('-')]), x[1])
                        for x in out]
                dates, vals = zip(*out)                
                return pd.Series(vals, index=dates)

            if country_code is not None and marketing_channel is None:
                return {channel : self.get_data(country_code, channel)
                        for channel in self.get_all_channels() }
            
            if country_code is None and marketing_channel is  not None:
                return {cc : self.get_data(cc, marketing_channel)
                        for cc in self.get_all_countries() }

            if country_code is None and marketing_channel is None:
                return {cc : self.get_data(cc)
                        for cc in self.get_all_countries() }


    ##::::  
    def __del__(self): # Could be cleanup().  People get religious about this!
        with self.conn:
            cur = self.conn.cursor()
            exec_str = "drop table {0}".format(self.tablename)
            cur.execute(exec_str)
            return
                


##:::::::::::::::::::::::::::::
##:: Some functions we will use

##:::::::::::::::::::::::::::::
##:: Setup

##:::::::::::::::::::::::::::::
##:: Body


##gsdebug temp
import os
import sys
sys.path.append('.')
from solution import *
fv = os.getcwd() + '/data_files-8/visits.csv'
fc = os.getcwd() + '/data_files-8/conversions.csv'
dv = Data_Block(fv)
beda = dv.get_data('be', 'Display Ads')
be = dv.get_data('be')
da = dv.get_data(marketing_channel = 'Display Ads')
#bedaplot = beda.plot()
#matplotlib.pyplot.show()
