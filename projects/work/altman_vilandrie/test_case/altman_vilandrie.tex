\documentclass{article}
\usepackage{pdfpages}
\usepackage{verbatim}
\begin{document}

\author{Gaurav Shah}
\title{Altman Vilandrie test case answer}


\maketitle


% \centerline{\sc \large A Simple Sample \LaTeX\ File}
% \vspace{.5pc}
% \centerline{\sc Stupid Stuff I Wish Someone Had Told Me Four Years Ago}
% \centerline{\it (Read the .tex file along with this or it won't 
%             make much sense)}
% \vspace{2pc}


\section{Initial investigation}

All code is found in the file altman\_vilandrie\_investigation.R.

\subsection{Setup}



We start the investigation with the setup code.  Nothing too interesting here, but it's worth pointing out that we explicitly cast the structural data as factors.



\subsection{Plotting sales}
The first thing to do is take a look at the sales to see if
anything sticks out.

\includepdf[pages={1}]{sales.pdf}

The plot shows that there are no obvious patterns, and no obvious outliers
that need to get cleaned up.


\subsection{Seasonal dependence}
The next question is whether or not the sales have a weekly (seasonal)
dependence, so we look at the spectrum.

\begin{verbatim}
sales.fft <- fft(sales)
plot(abs(sales.fft))
\end{verbatim}

\includepdf[pages={1}]{sales_fft.pdf}

This plot shows that the spectrum is pretty flat, with no significant peak at
52 weeks.  To really establish a seasonal dependence, we would probably need
more than two years of data anyways (unless it were a very strong dependence,
which is ruled out by a visual perusal of the sales).

This means that we do not have to include the week number (that is, the row index) as one of the elements of the modeling.


\subsection{Principal Component Analysis}

If the columns are linearly dependent, that will affect whether they all need to be part of the investigation or not.  Let's take a look at the principal components, to see if the data is full rank or not.  

\begin{verbatim}
obs.media.pr <- prcomp(observations.media)
print(summary(obs.media.pr))
plot(obs.media.pr)
\end{verbatim}

which gives

\includepdf[pages={1}]{obs_pr.pdf}


There's no sharp dropoff in the principal components that will tell us to
stop at a certain number.  That is, the dimensionality of the data seems
pretty full.  

\section{Linear regression}


We have 30 independent variables and only 102 observations.  This does not give us the luxury of investigating interaction or non-linear terms at this stage, because we'd end up with an under-determined system.  We \emph{may} be able to do so later, after we get rid of the irrelevant covariates.

\subsection{Regression against all independent variables}

As a first cut, we do a regression against all the numeric and factor variables in the data set.

\begin{verbatim}
sales.lm <- lm(Sales ~ ., data=observations)
print(summary(sales.lm))
\end{verbatim}

\verbatiminput{sales_lm.txt}

The adjusted R-squared is 0.8137.  The results confirm that the independent
variables have a wide range of strength in prediction of the sales.  


\subsection{Choosing a more robust set}
One systematic way of winnowing them is the "step" function in R.  This uses the Akaike Information Criterion in choosing the optimal model.


\begin{verbatim}
sales.step <- step(sales.lm, direction="both") # note that "backward" obtains
                                               # the same answer

print(summary(sales.step))
\end{verbatim}

which gives us:

\verbatiminput{sales_step.txt}

This has a adjusted R-squared of 0.8246.  An interesting point is that it
shows negative coefficients for two of the media.  I'm leaving them in the
model, but we should also consider the possibility that this is an artifact
and not a genuine result.


This has brought us down to 14 different media and 7 different occurrences.  This number still seems high, especially if we want to look at non-linear or interaction terms.

\subsection{Manually choosing the regressors}
As I don't know how accurate the ``step'' algorithm is, I'm going to try to tweak it manually to see if I can do better, by choosing only a subset of the variables.  Consider this:

\begin{verbatim}
sales.red.lm <- lm(Sales ~ Media.4.Impressions + Media.9.Impressions +
  Media.11.Impressions + Media.14.Impressions + Structural.1.Occurrence +
  Structural.2.Occurrence + Structural.5.Occurrence + Structural.7.Occurrence +
  Structural.10.Occurrence, data=observations)

print(summary(sales.red.lm))
\end{verbatim}

which gives us

\verbatiminput{sales_red_lm.txt}


This actually has a much \emph{worse} adjusted R-squared. However, as it has a
smaller number of terms, it gives us the opportunity to use it as a starting
point to introduce interaction and non-linear effects without leading to too
much curve-fitting.  


\subsection{Interactions between media and occurrences}
This is a model that contains interactions between the
occurrences and the media impressions:

\begin{verbatim}
sales.red.xn.lm <- lm(Sales ~ (
  Media.4.Impressions + Media.9.Impressions +
  Media.11.Impressions + Media.14.Impressions)
  * (Structural.1.Occurrence + Structural.2.Occurrence + Structural.5.Occurrence
     + Structural.7.Occurrence + Structural.10.Occurrence),
  data=observations)

print(summary(sales.red.xn.lm))
\end{verbatim}

which gives

\verbatiminput{sales_red_xn.txt}

Not at all successful!  Adjusted R-squared decreases further to 0.7131.  So
that's no good.  


\subsection{Non-linear terms}
Instead, let's try non-linear terms.  The motivation for this is that
there could be saturation when we see too much advertising for the same product;
or, alternatively,  that when we see more advertising it registers in the viewers' minds better.


\begin{verbatim}
sales.red.nonl.lm <- lm(Sales ~ Media.4.Impressions + I(Media.4.Impressions^2)
                   + Media.9.Impressions + I(Media.9.Impressions^2)
                   + Media.11.Impressions + I(Media.11.Impressions^2)
                   + Media.14.Impressions + I(Media.14.Impressions^2)
                   + Structural.1.Occurrence +
                   Structural.2.Occurrence +
                   Structural.5.Occurrence +
                   Structural.7.Occurrence +
                   Structural.10.Occurrence, data=observations)

print(sales.red.nonl.lm)
\end{verbatim}
which gives:

\verbatiminput{sales_red_nonl.txt}



This is not that successful either.  The adjusted R-squared is the same as
without the non-linear terms.  There is a strange aspect to this: the
significance of the media impressions (linear and non-linear) has decreased
compared to the structural occurrences.  I'm not sure why this happened.


\subsection{Interaction between different media}
We can investigate interaction terms between different media, although I
don't have a lot of hope for that approach:


\begin{verbatim}
sales.red.media.xn.lm <- lm(formula = Sales ~ (Media.1.Impressions + Media.4.Impressions + 
    +Media.8.Impressions + Media.9.Impressions + Media.11.Impressions + 
    Media.12.Impressions + Media.14.Impressions + Media.15.Impressions + 
    Media.16.Impressions + Media.18.Impressions + Media.19.Impressions + 
    Media.20.Impressions)*(Media.1.Impressions + Media.4.Impressions + 
    +Media.8.Impressions + Media.9.Impressions + Media.11.Impressions + 
    Media.12.Impressions + Media.14.Impressions + Media.15.Impressions + 
    Media.16.Impressions + Media.18.Impressions + Media.19.Impressions + 
    Media.20.Impressions) + Structural.1.Occurrence + Structural.2.Occurrence + 
    Structural.3.Occurrence + Structural.5.Occurrence + Structural.6.Occurrence + 
    Structural.7.Occurrence + Structural.10.Occurrence, data = observations)
print(summary(sales.red.media.xn.lm))
\end{verbatim}

which gives

\verbatiminput{sales_red_media_xn.txt}

This does not improve the adjusted R-squared at all, either.

\subsection{Using the ``log'' ansatz}

If we assume that there is saturation in the sales response to the media
exposure, it is possible that the log function may be a good fit:

\begin{verbatim}
sales.red.log.lm <- lm(Sales ~ log(Media.4.Impressions) + log(Media.9.Impressions) +
  log(Media.11.Impressions) + log(Media.14.Impressions) + Structural.1.Occurrence +
  Structural.2.Occurrence + Structural.5.Occurrence + Structural.7.Occurrence +
  Structural.10.Occurrence, data=observations)

print(summary(sales.red.log.lm))
\end{verbatim}

which gives

\verbatiminput{sales_red_log.txt}

This is not productive either: Adjusted R-squared goes further down to 0.6606


\subsection{Summary}
To sum up, the best model that we have so far is the one that we obtained
using "step".  It seems a little surprising that it retains so many of the
media and occurrences; I would have expected fewer in a robust model.

\begin{verbatim}
lm(formula = Sales ~ Media.1.Impressions + Media.4.Impressions + 
     Media.6.Impressions + Media.7.Impressions + Media.8.Impressions + 
     Media.9.Impressions + Media.11.Impressions + Media.12.Impressions + 
     Media.14.Impressions + Media.15.Impressions + Media.16.Impressions + 
     Media.18.Impressions + Media.19.Impressions + Media.20.Impressions + 
     Structural.1.Occurrence + Structural.2.Occurrence + Structural.3.Occurrence + 
     Structural.5.Occurrence + Structural.6.Occurrence + Structural.7.Occurrence + 
     Structural.10.Occurrence, data = observations)
\end{verbatim}


There are further investigations that can be done.  For example,
\begin{itemize}
\item We can spend more time searching for a better subset of regressors.
\item We can investigate for non-linear terms to extend this model; there's good 
reason to believe they exist.
\item It is reasonable to try to regress changes in sales rather than the sales themselves.
\item And we should look for regression of the time-delayed sales against the
advertising -- if this product is not an impulse buy, there may be a delay
from when the advertising happened to when the sales was affected.  Another
way of doing this would be to regress sales vs the moving average of the
media exposure.
\end{itemize}


Also, as a check of the previous results, here is a quick and dirty investigation: look at the individual regressions against each medium.  This is NOT as good as a multi-linear regression!


\begin{verbatim}
for (i in media.cols) {
  mylm <- lm(Sales ~ observations[[i]], data=observations);
  print(paste("****i is ", i));
  print(summary(mylm)) }
\end{verbatim}

This gives only 4, 9, 14 as major contributors, which actually broadly confirms the previous results.




\section{Relative efficiency of the media tactics}

In other words, calculation of sales per unit cost.
For this part, we will use the model that was obtained by "step".

The sales per unit cost are the sales per unit exposure, times the total exposure, divided by the total cost, which gives
\verbatiminput{sales_efficiency.txt}




\section{Optimal advertising budget}

Since we have a linear model, the optimal budget is to spend it
all on the media that provides the best sales efficiency.  So the winner in this simplified model is Medium 15!

Note that if we had non-linear terms, we would be able to optimize it in a more
sophisticated way.

Here's another aspect that we have not incorporated into the optimization that can be incorporated if we have more time: the uncertainty in the value of the coefficient.  Higher uncertainties should translate to lower investment in that medium.


\end{document}