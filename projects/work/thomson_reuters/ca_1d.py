#!/usr/bin/python

"""One dimensional Cellular Automaton

Written by Gaurav Shah

Usage: $ ./ca_1d.py ncols nrows r0 r1 r2 r3

ncols: Number of columns, must be one or more
nrows: Number of rows, must be zero or more
ri:    Four numbers that must be zero or one; desired output if the sum of the
       cell plus neighbors is 'i'.

NOTE: I love the patterns on the suggested rule, 0 1 0 0!

"""
import random
import sys

##::::::::::: classes we shall use

# custom exception class for parameter checking
class Input_Exception(BaseException):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


##::::::::::: functions we shall use

## Test the command line arguments and convert to appropriate type
def Parse_Inputs(args):

    ## check the number of inputs
    if len(args) != 6:
        raise Input_Exception(
            "ERROR: The number of command line arguments should be 6")

    ## check that they are integers (and convert)
    try:
        numerical_params = [int(x) for x in args]
    except:
        raise Input_Exception(
            "ERROR: Input wasn't an integer")

    ncols = numerical_params[0]
    nrows = numerical_params[1]
    rule  = numerical_params[2:]

    ## bounds checking
    if ncols < 1:
        raise Input_Exception(
            "ERROR: Number of columns should be positive")
    if nrows < 0:
        raise Input_Exception(
            "ERROR: Number of rows should be non-negative")

    if ( any( [r != 0 and r != 1 for r in rule] ) ):
        raise Input_Exception(
            "ERROR: Rule parameters should be 0 or 1")

    return(ncols, nrows, rule)
## end of Parse_Inputs

## uses the rule to generate the next line
def Next_Line(prev_line, rule, ncols):
    if ncols == 1:
        return [rule[prev_line[0]]]

    first_elem = rule[prev_line[0]      + prev_line[1]]
    last_elem  = rule[prev_line[ncols-2] + prev_line[ncols-1]]
    mid_elems  = [rule[prev_line[i-1] + prev_line[i] + prev_line[i+1]]
                  for i in xrange(1, ncols-1)]
    return [first_elem] + mid_elems + [last_elem]
## end of Next_Line

## pretty printing
def Print_Line(in_line):
    strs = [str(x) for x in in_line]
    str_rep = ''.join(strs)
    print str_rep
## end of Print_Line



##::::::::::: body of code
def main(args):

    ## Setup
    try:
        ncols, nrows, rule = Parse_Inputs(args)
    except Input_Exception as e:
        print e.value
        print "Usage: $ ./ca_1d.py ncols nrows r0 r1 r2 r3"
        exit(1)

    ## The first line is created at random
    ca_line = [random.randint(0,1) for i in xrange(ncols)]
    Print_Line(ca_line)

    ## Iterate
    for i in xrange(nrows):
        ca_line = Next_Line(ca_line, rule, ncols)
        Print_Line(ca_line)
## end of main


if __name__ == '__main__':
    main(sys.argv[1:])
