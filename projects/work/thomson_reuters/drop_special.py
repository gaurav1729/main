import re


## hfile_name = sys.argv[1]
hfile_name = 'test.txt' # holdings file
hfile_h = open(hfile_name)

fields = hfile_name.split('.')
ofile_name = fields[0] + '_purified.' + fields[1] # output file
ofile_h = open(ofile_name, 'w')

for line in hfile_h:
    new_line = re.sub(r'[^a-zA-Z0-9\s]', "", line)
    print("old:" + line)
    print("new:" + new_line)
    print("\n\n")
    ofile_h.write(new_line)

ofile_h.close()
hfile_h.close()


