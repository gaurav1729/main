username = "Akash"

operator_probs = {
    '+': 0.5,
    '-': 0.5,
    '*': 0.0,
    '/': 0.0,
    }

max_int = 12

results_file = 'arith_results.pkl'

results_persistence = 0.5
nflickers = 4

RANDOM   = 'random'
PRIORITY = 'priority'

next_prob_delay_seconds = 1

program_order = [
    RANDOM,
    PRIORITY,
    PRIORITY,
    RANDOM,
    RANDOM,
    PRIORITY,
    RANDOM,
    RANDOM,
    PRIORITY,
    RANDOM,
    RANDOM,
    PRIORITY]

def program_length():
    return len(program_order)
