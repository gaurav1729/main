import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;


import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.event.*;
import javax.swing.BorderFactory.*;
import javax.swing.border.Border.*;
import javax.swing.border.TitledBorder.*;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.io.InputStream;

import java.applet.*;

public class test
{
    public static void main(String[] args)
    {
        Window win = new Window();
    }
}


class ImageLabel extends JLabel {
   Image image;
   ImageObserver imageObserver; 

   // constructor with filename		
   ImageLabel(String filename) {
	  ImageIcon icon = new ImageIcon(filename);
	  image = icon.getImage();
	  imageObserver = icon.getImageObserver();
   }

   // constructor with icon
   ImageLabel(ImageIcon icon) {
	  image = icon.getImage();
	  imageObserver = icon.getImageObserver();
   }

   // overload setIcon method
   void setIcon(ImageIcon icon) {
	  image = icon.getImage();
	  imageObserver = icon.getImageObserver();
   }

   // overload paint()
   public void paint( Graphics g ) {
	   super.paint( g );
	   g.drawImage(image,  0 , 0 , getWidth() , getHeight() , imageObserver);
   }
}


class Window
    extends JFrame
            //implements KeyListener
{


    /** Returns an ImageIcon, or null if the path was invalid. */
    protected ImageIcon createImageIcon(String path,
                                        String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }


    public Window()
    {
        super("Akash Arithmetic");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        Container ca = getContentPane();
        ca.setBackground(Color.green);

        ImageLabel il1 = new ImageLabel("8M1Zp.jpg");
        ca.add(il1);

        setContentPane(ca);
    }     // public Window()
}     // implements ActionListener