import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;


import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.event.*;
import javax.swing.BorderFactory.*;
import javax.swing.border.Border.*;
import javax.swing.border.TitledBorder.*;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.io.InputStream;

import java.applet.*;

public class arithmetic
{
    public static void main(String[] args)
    {
        Window win = new Window();
    }
}


class ImageLabel extends JLabel {
    Image image;
    ImageObserver imageObserver; 

    // constructor with filename		
    ImageLabel(String filename) {
        ImageIcon icon = new ImageIcon(filename);
        image = icon.getImage();
        imageObserver = icon.getImageObserver();
    }

    // constructor with icon
    ImageLabel(ImageIcon icon) {
        image = icon.getImage();
        imageObserver = icon.getImageObserver();
    }

    // overload setIcon method
    void setIcon(ImageIcon icon) {
        image = icon.getImage();
        imageObserver = icon.getImageObserver();
    }

    // overload paint()
    public void paint( Graphics g ) {
        super.paint( g );
        g.drawImage(image,  0 , 0 , getWidth() , getHeight() , imageObserver);
    }
}

class Tushar2 {
    public void scaleImage() {
        try {
            ImageIcon ii = new ImageIcon("8M1Zp.jpg");
            BufferedImage bi = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = (Graphics2D)bi.createGraphics();
            g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING,
                                                     RenderingHints.VALUE_RENDER_QUALITY));
            boolean b = g2d.drawImage(ii.getImage(), 0, 0, 50, 50, null);
            System.out.println(b);
            ImageIO.write(bi, "jpg", new File("/tmp/apple50.jpg"));
        }
        catch (Exception e)
            {
                e.printStackTrace();
            }
    }

}


class Window
    extends JFrame
            //implements KeyListener
{


    /** Returns an ImageIcon, or null if the path was invalid. */
    protected ImageIcon createImageIcon(String path,
                                        String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }


    public Window()
    {
        super("Akash Arithmetic");
        setSize(500,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        Container ca = getContentPane();
        ca.setBackground(Color.green);

        FlowLayout flm = new FlowLayout();
        ca.setLayout(flm);

        JPanel panel_math = new JPanel();
        panel_math.setBackground(Color.white);

        JPanel panel_pic = new JPanel();
        panel_pic.setBackground(Color.black);


        JLabel textLabel = new JLabel("5 + 3 = ");
        panel_math.add(textLabel);

        JTextField textField = new JTextField("", 10);
        panel_math.add(textField);


        ImageLabel il1 = new ImageLabel("8M1Zp.jpg");
        panel_pic.add(il1);


        // ca.add(panel_math);
        ca.add(panel_pic);

        setContentPane(ca);
    }     // public Window()
}     // implements ActionListener