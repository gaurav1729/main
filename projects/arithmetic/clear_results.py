#!/usr/bin/env python
import pickle, os, sys

run_dir   = os.path.join('..', '..', '..', '..', '..', '..',
                          'Not_Dropbox', 'arithmetic')
results_dir = os.path.join(run_dir, 'results')

sys.path.append(run_dir)
import config_arithmetic as config

results_file = os.path.join(results_dir, config.results_file)
new_results_file = os.path.join(results_dir, config.results_file + '.old')
print("removing " + results_file)
print(str(os.path.isfile(results_file)))
os.remove(results_file)
