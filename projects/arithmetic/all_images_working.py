import random
import time
import glob
import os
has_Image = True
try:
    import Image
except Exception, e:
    has_Image = False

random.seed(time.time())

class An_Image(object):
    def __init__(self, name_, imagetype_, nframes_ = 3):
        self.name      = name_
        self.imagetype = imagetype_

        if self.imagetype == 'jpg' :
            self.nframes = 30
        else:
            self.nframes = nframes_

all_images = [
    An_Image('test.gif',          'gif', 4),
    An_Image('test2.gif',         'gif', 5),
    An_Image('test3.gif',         'gif', 5),
    An_Image('test4.gif',         'gif', 5),
    An_Image('test5.gif',         'gif', 5),
    An_Image('test6.gif',         'gif', 5),
    An_Image('test7.gif',         'gif', 5),
    An_Image('test8.gif',         'gif', 5),
    An_Image('test9.gif',         'gif', 5),

    # An_Image('bluewhale.gif',     'gif', 6),
    # An_Image('test.jpg',          'jpg'),
    # An_Image('test2.jpg',         'jpg'),
]

def random_image(dirname):
    '''get a list of all files (only gif? gif + image?) in the
    directory.  choose one randomly.  return An_Image object.'''
    dirname = os.path.abspath(dirname)
    gif_list = glob.glob(dirname + os.sep + "*.gif")
    rand_gif = random.choice(gif_list)
    num_frames = Num_Frames(rand_gif)
    return An_Image(rand_gif, 'gif', num_frames)



def Num_Frames(gif_name):
    if not has_Image:
        return -1
    im = Image.open(gif_name)
    try:
        while 1:
            im.seek(im.tell()+1)
    except EOFError:
        return im.tell()
    
