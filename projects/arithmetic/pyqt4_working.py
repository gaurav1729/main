#! /usr/bin/env python

## create a window
## create a window for the jpeg/gif (the movie screen)
## create a panel for the questions
## create a panel for the answer
## create a panel for comments and instructions
## create a button for "enter" if required (need to find a way
##   to do this without switching from keyboard to mouse!)
## perhaps have a set of buttons from 0-9 instead of keyboard?
##
## have a dict of operations to probability of asking that operation
## set a maximum number to ask from, max number of seconds (perhaps dynamic, so it
##  keeps getting faster the more you ask the same question)
## have a multi-level dict: operation, tuple of operands --> #wrong, right, slow
## have a way to go from the score to the probability of asking a given question
## 
## display the question on the panel
## start timer
## wait for answer
## if time taken is less than max, display the gif. [do we have a map
##   that tells us how long to play each gif?]
##
## if we're displaying jpegs too, have a code that goes through the jpegs and 
## resizes them appropriately (or do it on the fly)
##
## display the stats for the session
## load the pickle file for previous stats
## on exit, merge the stats for this session with the previous stats and save
##   the pickle file

# refactor -- only config.py should be in Not_Dropbox
#   -- create Weighted_Choice for each operator during init

# better keeping track of what is done -- date, number of questions,
#   score


##:::::::: some setup 

import threading
import sys
import os
import pickle
import copy
import random
import time
from collections import deque

# not sure why we have to do this; we have an __init__.py in '.'
sys.path.append(os.getcwd())
# utils_dir = '/home/gaurav/Dropbox/gaurav/svnco/svn_repository/projects/pylib'
# sys.path.append(utils_dir)
try:
    from utils import Weighted_Choice
except:
    utils_dir = '/home/gaurav/Dropbox/gaurav/svnco/svn_repository/projects/pylib'
    sys.path.append(utils_dir)
    from utils import Weighted_Choice
import all_images_working as all_images
try:
    import config_arithmetic as config
except:
    import config_arithmetic_default as config

# too lazy to keep track of QtCore or QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui  import *

##::::::::: some helper classes and functions

class Answer(object):
    """wrong is larger because we want the prob weighting to increase for wrong
    answers.  it's helpful to start halfway between right and wrong"""
    RIGHT_TIMELY = 1
    RIGHT_LATE   = 5
    WRONG        = 9

class Question(object):
    def __init__(self, operator, operand1, operand2):
        self.operator = operator
        self.operand1 = operand1
        self.operand2 = operand2

def create_results_struct_one_op(op):
    if op in ('-', '/'):
        pairs = [((x, y), Answer.RIGHT_TIMELY)
                 for x in range(1, config.max_int + 1)
                 for y in range(1, x + 1)]
    if op in ('+', '*'):
        pairs = [((x,y), Answer.RIGHT_TIMELY)
                 for x in range(1, config.max_int + 1)
                 for y in range(1, config.max_int + 1)]
    return dict(pairs)

def create_results_struct():
    ops = config.operator_probs.keys()
    return dict([(op, create_results_struct_one_op(op))
                  for op in ops])

def create_prob_weights():
    ops = config.operator_probs.keys()
    return dict([(op, create_prob_weights_one_op(op))
                  for op in ops])
    
def create_prob_weights_one_op(op):
    if op in ('+', '*', '/'):
        pairs = [((x,y), min(x,y))
                 for x in range(1, config.max_int + 1)
                 for y in range(1, x + 1)]
    if op == '-':
        pairs = [((x,y), min(x, y, x - y))
                 for x in range(1, config.max_int + 1)
                 for y in range(1, x + 1)]
    return dict(pairs)

##::::::::::: the main gui class
class MoviePlayer(QWidget):

    def __init__(self, image_dir, results_dir, parent=None):
        ##:::::::::: pre-gui
        # directory to find gif files
        self.image_dir = image_dir
        # directory to store the results pickle
        self.results_dir = results_dir

        random.seed(time.time())
        ## initialize the random choice for the operator.
        self.rand_ops = Weighted_Choice(config.operator_probs)

        ## read the results so far, initialize data structures
        try:
            with open(os.path.join(results_dir, config.results_file)) as results_h:
                self.results = pickle.read(results_h)
                print("reading pickle file")
        except:
            self.results = create_results_struct()
            print("creating results struct")

        self.prob_weights = create_prob_weights()

        # list to store any tougher questions
        self.tougher_qs = []

        ##:::::::::: start gui: initialize buttons, windows and panels
        # as we type in the answer number keys, they are stored here:
        self.answer_dq = deque()

        QWidget.__init__(self, parent) # this is the window (parent is None)
        self.setGeometry(200, 200, 400, 400)
        self.setWindowTitle(config.username + "'s Math Problems!")
        main_layout = QGridLayout()

        ##::: the movie part
        # set up the movie screen on a label
        self.movie_screen = QLabel()
        # expand and center the label
        self.movie_screen.setSizePolicy(QSizePolicy.Expanding,
                                        QSizePolicy.Expanding)
        self.movie_screen.setAlignment(Qt.AlignCenter)

        ##::: the question panel
        self.qpanel = QLabel("the question will be here")
        self.qpanel_default_style =             \
            'background: lightblue;'            \
            + 'border-style: inset;'            \
            + 'border-width: 3px;'              \
            + 'border-color: black'
        self.qpanel.setStyleSheet(self.qpanel_default_style)
        qfont = QFont()
        qfont.setBold(True)
        self.qpanel.setFont(qfont)

        ##::: the answer panel
        self.apanel = QLabel("the answer panel")
        self.apanel_default_style = 'background: red;' \
            + 'border-style: inset;'                   \
            + 'border-width: 3px;'                     \
            + 'border-color: black'

        self.apanel.setStyleSheet(self.apanel_default_style)
        afont = QFont()
        afont.setBold(True)
        self.apanel.setFont(afont)

        ##::: messages from the app:
        self.mpanel = QLabel("messages will be displayed here")
        self.mpanel_default_style = \
            'border-style: inset;'  \
            + 'border-width: 3px;'  \
            + 'border-color: black'
        self.mpanel.setStyleSheet(self.mpanel_default_style)
        mfont = QFont()
        mfont.setBold(True)
        self.mpanel.setFont(mfont)


        ##::: the answer buttons
        btn0 = QPushButton("0")
        btn1 = QPushButton("1")
        btn2 = QPushButton("2")
        btn3 = QPushButton("3")
        btn4 = QPushButton("4")
        btn5 = QPushButton("5")
        btn6 = QPushButton("6")
        btn7 = QPushButton("7")
        btn8 = QPushButton("8")
        btn9 = QPushButton("9")
        btne = QPushButton("enter")
        btns = QPushButton("start")
        btnd = QPushButton("I think I'm done.")


        ##::::::::::: add them to the layout
        main_layout.addWidget(self.movie_screen, 0, 0, 15,9)
        main_layout.addWidget(self.qpanel, 21,0,5,2)
        main_layout.addWidget(self.apanel, 21,5,5,2)
        main_layout.addWidget(self.mpanel, 21,10,5,2)

        main_layout.addWidget(btne, 38,6, 3, 3)
        
        main_layout.addWidget(btn1, 34, 6)
        main_layout.addWidget(btn2, 34, 7)
        main_layout.addWidget(btn3, 34, 8)
        main_layout.addWidget(btn4, 35, 6)
        main_layout.addWidget(btn5, 35, 7)
        main_layout.addWidget(btn6, 35, 8)
        main_layout.addWidget(btn7, 36, 6)
        main_layout.addWidget(btn8, 36, 7)
        main_layout.addWidget(btn9, 36, 8)
        main_layout.addWidget(btn0, 37, 7)

        main_layout.addWidget(btns, 38, 2)
        main_layout.addWidget(btnd, 38, 4)
        # set layout:
        self.setLayout(main_layout)

        ##:::::::::::: connect them with the correct behaviour
        self.connect(btn0, SIGNAL("clicked()"), self.exec0)
        self.connect(btn1, SIGNAL("clicked()"), self.exec1)
        self.connect(btn2, SIGNAL("clicked()"), self.exec2)
        self.connect(btn3, SIGNAL("clicked()"), self.exec3)
        self.connect(btn4, SIGNAL("clicked()"), self.exec4)
        self.connect(btn5, SIGNAL("clicked()"), self.exec5)
        self.connect(btn6, SIGNAL("clicked()"), self.exec6)
        self.connect(btn7, SIGNAL("clicked()"), self.exec7)
        self.connect(btn8, SIGNAL("clicked()"), self.exec8)
        self.connect(btn9, SIGNAL("clicked()"), self.exec9)
        self.connect(btne, SIGNAL("clicked()"), self.exece)
        self.connect(btns, SIGNAL("clicked()"), self.execs)
        self.connect(btnd, SIGNAL("clicked()"), self.execd)

        ##::::::::::: end of __init__

    def keyPressEvent(self, e):
        print "in keypress event"

        if e.key() == Qt.Key_0:
            self.exec0()
        if e.key() == Qt.Key_1:
            self.exec1()
        if e.key() == Qt.Key_2:
            self.exec2()
        if e.key() == Qt.Key_3:
            self.exec3()
        if e.key() == Qt.Key_4:
            self.exec4()
        if e.key() == Qt.Key_5:
            self.exec5()
        if e.key() == Qt.Key_6:
            self.exec6()
        if e.key() == Qt.Key_7:
            self.exec7()
        if e.key() == Qt.Key_8:
            self.exec8()
        if e.key() == Qt.Key_9:
            self.exec9()
        if e.key() in (Qt.Key_Enter, Qt.Key_Return):
            self.exece()

        ##::::::::::: end of keyPressEvent

    ##::::::::::  all the callbacks

    ##::: first, just the number buttons
    def exec0(self):
        self.answer_dq.appendleft(0)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec1(self):
        self.answer_dq.appendleft(1)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec2(self):
        self.answer_dq.appendleft(2)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec3(self):
        self.answer_dq.appendleft(3)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec4(self):
        self.answer_dq.appendleft(4)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec5(self):
        self.answer_dq.appendleft(5)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec6(self):
        self.answer_dq.appendleft(6)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec7(self):
        self.answer_dq.appendleft(7)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec8(self):
        self.answer_dq.appendleft(8)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()

    def exec9(self):
        self.answer_dq.appendleft(9)
        self.answer_entered = self.answer()
        self.apanel.setText(str(self.answer_entered))
        if self.answer_entered == self.correct_answer:
            self.exece()


    ##::: non-number buttons
    def execs(self):
        """pressed the start button: this button is to be used only at the very
        beginning to create the first question; all other questions should come
        up automatically"""
        self.new_cycle()

    def execd(self):
        """pressed the done button: stop the animation; merge the new results
        struct with the old one; save pickle"""
        self.movie.stop()
        self.closeEvent()

    def exece(self):
        """pressed enter """
        curr_score = self.results[self.operator][(self.operand1, self.operand2)]
        if self.correct_answer != self.answer_entered:
            # if the answer is wrong, append it to tougher_qs TWICE
            self.tougher_qs.append((self.operator, self.operand1, self.operand2))
            self.tougher_qs.append((self.operator, self.operand1, self.operand2))

            self.mpanel.setText(
                "WRONG ANSWER, should have been "+ str(self.correct_answer))
            self.mpanel.setStyleSheet('background: red')
            print("wrong answer")
            time.sleep(1)
            new_score = self.calc_new_score(curr_score, Answer.WRONG)
            self.results[self.operator][(self.operand1, self.operand2)] = new_score
            threading.Timer(config.next_prob_delay_seconds,
                            self.new_cycle).start()
            return

        ##:::: the answer was correct:
        self.mpanel.setStyleSheet('background: blue')
        time.sleep(1)

        t_delta = time.time() - self.t_start
        self.mpanel.setText("CORRECT! time taken: " + str(round(t_delta, 1)))
        if t_delta> self.t_target:
            print("too slow")
            # too slow, append it to answer_qs ONCE
            self.tougher_qs.append((self.operator, self.operand1, self.operand2))
            new_score = self.calc_new_score(curr_score, Answer.RIGHT_LATE)
            self.results[self.operator][(self.operand1, self.operand2)] = new_score
            # i have no idea why we need to use a timer here rather than simply
            # self.new_cycle
            t = threading.Timer(config.next_prob_delay_seconds, self.new_cycle)
            t.start()
            return

        ##:::: the answer was correct and in time:
        try:
            self.tougher_qs.remove((
                    self.operator, self.operand1, self.operand2))
        except Exception, e:
            pass

        new_score = self.calc_new_score(curr_score, Answer.RIGHT_TIMELY)
        self.results[self.operator][(self.operand1, self.operand2)] = new_score
        this_image = all_images.random_image(self.image_dir)
        print("on time: " + this_image.name)
        self.movie = QMovie(this_image.name, QByteArray(), self)
        self.movie.setCacheMode(QMovie.CacheAll)
        self.movie.setSpeed(100)
        self.movie_screen.setMovie(self.movie)
        self.movie.start()


        duration = config.default_duration
        if this_image.nframes > 0:
            duration = this_image.nframes/config.frames_per_sec


        t = threading.Timer(duration, self.new_cycle)
        t.start()


    ##:::::::: non-callback methods of the class
    def closeEvent(self, event = None):
        '''gets called on x-ing it out and with the "i'm done for 
        now" button'''
        try:
            rf = os.path.join(self.results_dir, config.results_file)
            with open(rf, 'wb') as results_h:
                pickle.dump(self.results, results_h)
                print("saving pickle file")
        except Exception, e:
            print("could not save")
            print(e)
        print "closing"

    def calc_new_score(self, old_score, latest_val):
        r = config.results_persistence
        return old_score*r + latest_val*(1-r)

    def answer(self):
        "this is the answer *submitted* as an int"
        return sum([v*10**i for (i,v) in enumerate(self.answer_dq)])

    def flash_panel(self, panelref, prev_style, t, n, strDisp):
        for i in range(n):
            time.sleep(t)
            panelref.setText("")
            time.sleep(t)
            panelref.setText(strDisp)
            
            # this leads to warnings regarding pixmaps
            # panelref.setStyleSheet(
            #     'background: white;'
            #     + 'border-style: inset;'
            #     + 'border-width: 3px;'
            #     + 'border-color: black'
            #     )
            # print("finished setting")
            # time.sleep(t)
            # print("starting setting2")
            # # leads to segmentation faults
            # # palette = QPalette()
            # # palette.setColor(QPalette.Background, QColor("green"))
            # # panelref.setPalette(palette)
            
            # panelref.setStyleSheet(
            #     'background: black;'
            #     + 'border-style: inset;'
            #     + 'border-width: 3px;'
            #     + 'border-color: black'
            #     )
            # print("finished setting2")
        ## end of for loop

    def new_cycle(self):
        try:
            self.movie.stop()
        except AttributeError, e:
            pass

        self.flash_panel(self.qpanel, self.qpanel_default_style, 0.3, 5, 'NEW PROBLEM')
        time.sleep(1)
        self.answer_dq = deque([])
        self.apanel.setText("")
        self.new_question()
        self.t_start = time.time()
        print("\n\nstarting new cycle")

    def new_question(self):
        """get the problem (and solution) and display it"""
        n_tougher = len(self.tougher_qs)
        rand_int = random.randint(0, n_tougher)
        if rand_int == 0 :
            operation_str, operand1, operand2, t_target = self.get_question()
        else:
            operation_str, operand1, operand2 = random.choice(self.tougher_qs)
            t_target = self.calc_t_target(
                self.results[operation_str][(operand1, operand2)])
        self.operator = operation_str
        self.operand1 = operand1
        self.operand2 = operand2
        self.t_target = t_target
        
        if operation_str == '*':
            self.correct_answer = operand1 * operand2
        if operation_str == '+':
            self.correct_answer = operand1 + operand2
        if operation_str == '-':
            self.correct_answer = operand1 - operand2
        if operation_str == '/':
            self.correct_answer = operand1 / operand2

        q_string = str(operand1) + " " + operation_str + str(operand2)
        self.qpanel.setText(q_string)

    def get_question(self):
        """choose an operator with the probability structure defined in config
        choose operand1, 2 from the keys of the result struct for that op
        choose the target time intelligently"""

        operator = self.rand_ops.choose()
        rand_operands = Weighted_Choice(self.prob_weights[operator])
        operand1, operand2 = rand_operands.choose()

        t_target = self.calc_t_target(self.results[operator][(operand1, operand2)])
        return operator, operand1, operand2, t_target

    def calc_t_target(self, score):
        lim0 = (Answer.RIGHT_TIMELY + Answer.RIGHT_LATE)/2.0
        lim1 = (Answer.WRONG + Answer.RIGHT_LATE)/2.0
        if score < lim0:
            return 5
        if lim0 <= score and score <= lim1:
            return 7
        return 9

    ##::::::::::  done with the class
##:::::::: main stuff

def main(image_dir, results_dir):
    print("%%%%%%%  "+str(sys.argv))
    app = QApplication(sys.argv)
    player = MoviePlayer(image_dir, results_dir)
    player.show()
    sys.exit(app.exec_())


##:::  start the gui and run the main loop
if __name__ == "__main__":
    main('.', 'results')
