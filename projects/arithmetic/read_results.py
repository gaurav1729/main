#!/usr/bin/env python
import pickle, os, sys

def main(sysargs):
    try:
        last_ind = int(sysargs[1])
    except IndexError, e:
        last_ind = 0

    run_dir   = os.path.join('..', '..', '..', '..', '..', '..',
                              'Not_Dropbox', 'arithmetic')
    results_dir = os.path.join(run_dir, 'results')

    sys.path.append(run_dir)
    import config_arithmetic as config

    results_file = os.path.join(results_dir, config.results_file)
    with open(results_file) as results_h:
        temp = pickle.load(results_h)
        for op in temp.keys():
            print("\n\n\n" + op)
            count = 1
            for operands, score in reversed(
                sorted(
                    list(temp[op].iteritems()),
                    key = lambda x : x[1])):
                print('{:<4}'.format(str(count))
                      + '{:<10}'.format(str(operands))
                      + '{:<5}'.format(str(score)))
                count += 1
                if count == (last_ind + 1):
                    break
    ##::::::::::: end of main

if __name__ == "__main__":
    print str(sys.argv)
    main(sys.argv)
