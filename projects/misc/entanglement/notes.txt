Entangelment is a bit of a red herring -- do we really need the second particle to have the spooky action at a distance?

For example, if you have a single particle wave function, we can do two simultaneous measurements to see what it's positions is, at two different places, separated by timelike separations.  You will never get both of them give positive results -- so the "spooky action at a distance" is in the wave-function collapse, and you don't need a second particle for it!

The wave function in the entangled case is |up down> + |down up>, and this is collapsed by measurement to one of these two.  The fact that there are two particles in this bizarre behaviour is irrelevant.


Read up on Bell's Inequality.

Is there any sort of equivalent for this, for other bases?  Such as momentum measurements rather than position measurements.  