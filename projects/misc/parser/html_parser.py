#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import urllib2
import os
import codecs
import sys
import string

# outdir  = "/home/gaurav/tmp/code/cpp/transfer/usr/m/parser"
# urlfile = '/home/gaurav/tmp/code/cpp/transfer/usr/m/parser/url_list.txt'

outdir = '/media/gaurav/ExtraDrive1/tobe/code/cpp/transfer/usr/m/parser'
urlfile = outdir + "/url_list.txt"

def main():
    multiparser()


def multiparser():
    with  open(urlfile) as h_urlfile:
        urls = h_urlfile.readlines()
        urls = [url.strip() for url in urls]
        print len(urls)
        for url in urls:
            print url
            try:
                parser(url) # parser(url)
            except ValueError, e: ## empty line, usually
                pass

def parser(url):
    # read file, parse html
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    # find author, title; create output file
    title = soup.find_all('title')[0].get_text().replace(' ', '_').replace("Inc", "").replace("?","").replace(":","").replace(".","").replace("'", "").replace('"', '').replace("!","").replace("$","").replace("&","").replace("*","")
    try:
        author = soup.find_all('h2')[0].find_all('strong')[0].get_text().replace(' ', '_')
    except IndexError, ie:
        try:
            author = soup.find_all('meta')[1].get('content').split(',')[1].strip()
        except Exception, e:
            author = 'misc'
    str_outfile = os.path.join(outdir, author, title + '.html')
    try:
        h_outfile = open(str_outfile, 'w')
    except IOError, e:
        os.makedirs(os.path.dirname(str_outfile))
        h_outfile = open(str_outfile, 'w')

    # print initial stuff
    init_string = '''<!DOCTYPE html>
<html>
<body>
<br/>
'''
    h_outfile.write('<a href="' + url + '">link</a><br/>')
    h_outfile.write(init_string)

    # print text
    text = soup.find_all('body')[0].get_text()
    str_text = ''.join(s for s in text if s in string.printable)
    str_text = str_text.replace("\n", "<br/>")
    h_outfile.write(str_text)

    # print other pages
    for i in range(2, 30):
        print "i = " + str(i)
        try:
            next_url     = url + '&&page=' + str(i)
            next_content = urllib2.urlopen(next_url).read()
            next_soup    = BeautifulSoup(next_content)
            next_text    = next_soup.find_all('body')[0].get_text()
            next_str_text = ''.join(s for s in next_text if s in string.printable)
            next_str_text = next_str_text.replace("\n", "<br/>")
            h_outfile.write(next_str_text)
            if 'VOTING MODULE' in next_str_text or 'Please Rate' in next_str_text:
                print "breaking on voting, i = " + str(i)
                break
        except urllib2.URLError, e:
            print "broken on i = " + str(i)
            break


    # print stuff at end
    fin_string = '''</body></html>
'''
    h_outfile.write(fin_string)
    h_outfile.close()

    return soup

if __name__ == "__main__":
    main()


# <!DOCTYPE html>
# <html>
# <body>

# <h1>My First Heading</h1>

# <p>My first paragraph.</p>

# </body>
# </html>


    # print(soup.get_text())
    # print soup.prettify()
    # # print title
    # for link in soup.find_all('a'):
    #     print(link.get('href'))
    # soup.find_all('p')
    # soup.find_all('h1') # header
    # soup.find_all('strong') # has author, especially inside h2
    # soup.find_all('body')
    # soup.find_all('body')[0].get_text()
