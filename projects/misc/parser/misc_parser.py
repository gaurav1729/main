### solution 1 -- http://stackoverflow.com/questions/1936466/beautifulsoup-grab-visible-webpage-text
html = urllib.urlopen('http://www.nytimes.com/2009/12/21/us/21storm.html').read()
soup = BeautifulSoup.BeautifulSoup(html)
texts = soup.findAll(text=True)

def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', str(element)):
        return False
    return True

visible_texts = filter(visible, texts)


### solution 2 http://stackoverflow.com/questions/10061544/reading-some-content-from-a-web-page-read-in-python
soup.find_all('b')
# This code finds all the tags whose names start with the letter "b",
# in this case, the 'body' tag and the 'b' tag:
import re
for tag in soup.find_all(re.compile("^b")):
    print(tag.name)

#This code finds all the tags whose names contain the letter "t":
for tag in soup.find_all(re.compile("t")):
    print(tag.name)
    
# The value True matches everything it can.
# This code finds all the tags in the document, but none of the text strings:
for tag in soup.find_all(True):
    print(tag.name)

# The find_all method is one of the most common methods in BeautifulSoup. 

# It looks through a tag's descendants and retrieves all descendants that match
# your filters. 
soup.find_all("title")
soup.find_all("p", "title")
soup.find_all("a")
soup.find_all(id="link2")

#
from bs4 import BeautifulSoup
import urllib2

url = "http://www.pythonforbeginners.com"
content = urllib2.urlopen(url).read()
soup = BeautifulSoup(content)

print soup.prettify()
print title
#

for link in soup.find_all('a'):
    print(link.get('href'))
#
print(soup.get_text())
#
#As a last example, let's grab all the links from Reddit

from bs4 import BeautifulSoup
import urllib2

redditFile = urllib2.urlopen("http://www.reddit.com")
redditHtml = redditFile.read()
redditFile.close()

soup = BeautifulSoup(redditHtml)
redditAll = soup.find_all("a")
for links in soup.find_all('a'):
    print (links.get('href'))
#

url = "http://www.i.literotica.com/stories/showstory.php?id=576538"
