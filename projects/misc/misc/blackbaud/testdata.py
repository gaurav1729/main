#!/usr/bin/env python
filename = "datawrong.txt"

state01 = "ARCDB01"
state02 = "ARCDB02"

checksum = dict()

state   = "None"
with open(filename) as file_h:
    file_lines = file_h.readlines()
    for (i, fl) in enumerate(file_lines):
        fl = fl.split()[0]
        if fl == state01:
            state = state01
            continue
        if fl == state02:
            state = state02
            continue
        if state == state01:
            checksum[state01] = fl
            continue
        if state == state02:
            checksum[state02] = fl #unnecessary but aesthetically satisfying
            if checksum[state01] == checksum[state02]:
                print "it's fine in line " + str(i+1)
            else:
                print "\nERROR in line " + str(i+1)
                print "ARCDB01 = " + checksum[state01]
                print "ARCDB02 = " + checksum[state02]
                print "\n"
            state = "None"
        # end of if state == state02
    # end of for loop
# end of with statement            
