#include <iostream>
#include <exception>
using namespace std;

int main(int argc, char* argv[])
{
  int mynum;

  try
    {
      cout << "enter a number: ";
      cin >> mynum;
      if (mynum == 0)
	{
	  throw ("my error");
	}       // if (mynum == 0)
    }
  catch (const char* ex)
    {
      cout << "not a number?" << endl;
      cout << "*** " << ex << endl;
      return 1;
    }
  cout << "mynum is " << mynum << endl;
  return 0;
} // int main(int argc, char* argv[])
