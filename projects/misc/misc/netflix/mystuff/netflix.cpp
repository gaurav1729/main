/*

This code attempts to solve the netflix prize.

Here's the algo:

1) Read the data
2) Do *linear* restricted SVD as a starting point.
3) Save it!  If it already exists, we don't have to do the previous step.
4) Do non-linear work to improve the algo.

*/

#include <iostream>
#include <assert.h>
#include <time.h>

using namespace std;
