/*

This code tries to minimize the MSE of the predictions of the movie ratings for each individual.

Algorithm:
Read the data or read the latest SVD or latest qil values, depending on the flag.
Find the SVD
  Optionally, print out into a file.
Converge to the right value based on a function f.
  Optionally, print out into a file..

*/
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <dirent.h>


using namespace std;

// struct of parameters
struct
{
  string datadir;
  
} p; // struct p

//:::::::::::::::::::::::::
// classes used in the code
//:::::::::::::::::::::::::
struct Flags
{
  int start_point;
} flags; //   struct Flags


struct Rating_Pair
{
  int viewer;
  int rating;
}; // struct Rating_Pair

class Movie_Ratings
{
  int movie;
  vector<Rating_Pair> data;
}; // class Movie_Ratings


//:::::::::::::::::::::::::
// function declarations
//:::::::::::::::::::::::::
int Parse_Args(int argc, char * argv[], Flags & flags);
int Read_Data(string datadir, list<Movie_Ratings> & ratings);

//:::::::::::::::::::::::::
// main
//:::::::::::::::::::::::::
int main(int argc, char * argv[])
{
  Flags flags;
  Parse_Args(argc, argv, flags);
  p.datadir = "/home/gaurav/projects/netflix/download/";

  list<Movie_Ratings> ratings;
  Read_Data(p.datadir, ratings);


} // int main(int argc, char * argv[])


//:::::::::::::::::::::::::
// functions
//:::::::::::::::::::::::::

int Parse_Args(int argc, char * argv[], Flags & flags)
{
  flags.start_point = 0;
} // int Parse_Args(int argc, char * argv[], Flags flags)

int Read_Data(string datadir, list<Movie_Ratings> & ratings)
{
  DIR * pdir;
  struct dirent *pent;

  pdir = opendir(p.datadir.c_str());
  while(pent = readdir(pdir))
    {
      cout << "::";
      cout << pent->d_name << endl;
    } //   while(pent = readdir(pdir))
} // int Read_Data(string datadir, Movie_Ratings& ratings)
