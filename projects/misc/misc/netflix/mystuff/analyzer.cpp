/*

This code tries to minimize the MSE of the predictions of the movie ratings for each individual.

Algorithm:
Read the data or read the latest SVD or latest qil values, depending on the flag.
Find the SVD
  Optionally, print out into a file.
Converge to the right value based on a function f.
  Optionally, print out into a file..

*/

#define TEST

#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <dirent.h>
#define  MYLIB /home/gaurav/lib/cpp

#include "/home/gaurav/lib/cpp/tokenize.h"

using namespace std;



//:::::::::::::::::::::::::
// classes used in the code
//:::::::::::::::::::::::::

// struct of parameters
struct Params
{
  string datadir;
  unsigned int svd_size;
  unsigned long int nviewers;
  unsigned int nmovies;
}; // struct Params

struct Flags
{
  int start_point; // start the convergence at a given stage
} flags; //   struct Flags


// gsdebug -- this should probably be combined into one int instead -- and make
// it a class rather than a struct
class Rating_Pair
{
public:
  unsigned long int viewer;
  unsigned char rating;
  unsigned long date; // should do this as epoch (days from 1900)
}; // struct Rating_Pair

class Movie_Ratings
{
  unsigned int movie;
  vector<Rating_Pair> data;
}; // class Movie_Ratings


//:::::::::::::::::::::::::
// function declarations
//:::::::::::::::::::::::::
int Parse_Args(int argc, char * argv[], Flags & flags);
int Read_Data(string datadir, list<Movie_Ratings> & ratings);
int Initialize_Params(Params & p)
{
#ifdef TEST
  p.datadir    = "/home/gaurav/projects/netflix/download/test_set";
#else
  p.datadir    = "/home/gaurav/projects/netflix/download/training_set";
#endif
  p.svd_size   =     50; // size of reduced svd
  p.nviewers   = 480189;
  p.nmovies    =  17770;
} // int Initialize_Params(Params & p)


//:::::::::::::::::::::::::
// main
//:::::::::::::::::::::::::
int main(int argc, char * argv[])
{
  Flags flags;
  Parse_Args(argc, argv, flags);
  Initialize_Params(p);


  list<Movie_Ratings> ratings;
  map<const long int, long int> viewer_serials;
  list<long int> viewer_ids;
  Read_Data(p.datadir, ratings, viewer_serials, viewer_ids);


} // int main(int argc, char * argv[])


//:::::::::::::::::::::::::
// functions
//:::::::::::::::::::::::::

int Parse_Args(int argc, char * argv[], Flags & flags)
{
  flags.start_point = 0;
} // int Parse_Args(int argc, char * argv[], Flags flags)

int Read_Data(string datadir, list<Movie_Ratings> & ratings, map<const long int, long int> viewer_serials, list<long int> viewer_ids)
{
  DIR * pdir;
  struct dirent *pent;

  //:::: get directory listing
  pdir = opendir(p.datadir.c_str());
  while(pent = readdir(pdir))
    {
      cout << "::";
      cout << pent->d_name << endl;

      // read the file, neglecting '.' and '..'
      // also, create a lookup table from customer id to customer serial num
      if (string(pent->d_name) == "." || string(pent->d_name) == "..")
        continue;
      ofstream movie_file;
      string trash;
      string line;
      movie_file.open(pent->d_name, ios::in);
      movie_file.getline(trash, 100); // discard the first line, which is the movie number
      while ( ! movie_file.eof() )
        {
          movie_file.getline(line, 100);        
          // split the line into its fields based on commas: usernumber, rating, date
          vector<string> fields =  tokenize(',', line);
          ratings.push_back();
          ratings.end()->viewer = fields[0]; // gsdebug: perhaps this should be serial number instead
          ratings.end()->rating = fields[1];
          ratings.end()->date   = days_from_start(fields[2]);
        } //       while ( ! file.eof() )

    } //   while(pent = readdir(pdir))
  closedir(pdir);
  


} // int Read_Data(string datadir, Movie_Ratings& ratings)
