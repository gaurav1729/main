import javax.swing.*;
import java.awt.*;

class SwingWindow extends JFrame
{
    public SwingWindow()
    {
        super("Hello From Java Swing");
        setSize(300, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        Container contentArea = getContentPane();
        contentArea.setBackground(Color.white);
    }
}

public class HelloSwing
{
    public static void main(String[] args)
    {
        SwingWindow Hello = new SwingWindow();
    }
}
