#!/usr/bin/env python
'''Run this script to find an activity to do!
Usage: ./activities.py to run it interactively
'''
## can be called randomly or to list all possibilities

## activities dict:
## level one:   indoors, summer, winter
## level two:   short, day, multi-day
## level three: e.g. Akash or Family

activities_dict= {'indoors':
                  {'short':
                   {'Akash': ['a coding project',
                              'modding of games',
                              'write a story',
                              'draw some cartoons',
                              'write a script and make a movie',
                              {'math':['read a math book','solve math problems']},
                              {'robotics':['hexxy', 'create a new program for ev3', 'arduino']},
                              'decide on a new topic and learn about it',
                              'learn photography',
                              'photograph splashes',
                              'science kits',
                              'read neal de grasse tyson',
                              'find a new book and read it',
                              'write a poem',
                              ], ## Akash
                    'Family':[{'board game': ['pandemic',
                                              'settlers of catan',
                                              'chinese checkers',
                                              'fluxx',
                                              'spy alley',
                                              'the resistance',
                                              'hanabi',]},
                              'read night',
                              'roller blading at a rink',
                              'watch a documentary',
                              'go swimming at the ymca',
                              ], ## family
                    }, ## short
                   'day': ## indoors
                   {'Family':[{'art museum': ['Peabody Essex Museum',
                                              'Isabella Stewart Museum',
                                              ],
                               },
                              'Mystic Aquarium',
                              'New England Aquarium',
                              'Museum of Science',
                              ],
                    }, ## day
                  }, ## indoors
                  'summer outdoors':
                  {'short':
                   {'Akash': ['cycling',
                              'roller blading',
                              'walk in the woods',
                              'fly a drone',
                              'kayaking',
                              ], ## Akash
                    'Family':['cycling',
                              'roller blading',
                              'walk in the woods in one of the trails we know',
                              'frisbee',
                              'play catch',
                              'fly drone',
                              'kayaking',
                              'explore new trail in Sharon or nearby',
                              'nearby mass audubons',
                              'sleep in a tent',
                              'Massapoag beach',
                              ], ## Family
                    }, ## short
                   'day':
                   {'Family': [{'day hike':['blue hills',
                                            'new trail in Sharon',
                                            'great meadows national wildlife refuge',
                                            'monadnock',
                                            'wachusetts',
                                            ],
                                }, ## day hike 
                                {'beach': ['Plum Island',
                                           'Cape Cod',]},
                               {'cycling day trip': ['Cape Cod Trail',
                                                     'Minuteman Bike Path',
                                                     'Quabbin Reservoir',
                                                     'find a new trail',
                                                     ]
                                }, ## cycling day trip
                               {'kayaking': ['Neponset',
                                             'some of the rivers on the South Shore',
                                             ],
                                }, ## canoeing
                               {'day trip': ['Cape Cod',
                                             'Block Island',
                                             'Cape Ann',
                                             ],
                                },
                               ],
                    }, ## day
                   'multi-day':
                   {'Family': ['Western Massachusetts',
                               'New York City',
                               'Montreal',
                               'Quebec City',
                               'Cape Cod',
                               'Monadnock',
                               'Greylock',
                               'Block Island',
                               'Nantucket',
                               "Martha's Vineyard",
                               {"Go overnight camping":[
                                 "Plymouth",
                                 "New Hampshire",
                                 "Somewhere else!"
                               ]}
                               ],
                    }, ## multi-day
                   }, ## summer outdoors
                  'winter outdoors':
                  {'day':{'Family':['cross-country skiing',
                                    'down-hill skiing',
                                    ],
                          'Akash': ['sledding',
                                    ],
                          }, ## day
                   'multi-day':{'Family':['cross-country skiing',
                                          'down-hill skiing',
                                          ]
                                }, ## multi-day
                   'short': {'Family': ['sledding',
                                       'snow-shoeing',
                                       ],
                             }, ## short
                   } ## winter outdoors
                  } ## activities dict


##:::::::::::: no editing below this line!
import curses, traceback, time, random

class Attributes:
  listable = False;
  name     = ""
  def __init__(self, inval):
    typ = type(inval)
    if typ == dict:
      self.nkeys   = len(inval.keys())
      if self.nkeys > 1:
        self.listable = True
        self.name = ""
      else:
        self.listable = False
        self.name = inval.keys()[0]
    ## end typ == dict
    elif typ == list:
      ## we assume that any dict inside a list has only one key
      self.listable = True
      self.name = ''
    ## end typ == list
    elif typ == str:
      self.listable = False
      self.name = inval
    ## end if
## end class

class UserQuit(Exception):
  '''user asked to quit'''
  pass

class BadChoice(Exception):
  '''user supplied a bad choice'''
  pass

def main(stdscr, activities_dict):
  '''The structure of the initial dictionary is: 
  type of activity
  duration of activity
  target of activity
  The value of these keys is a list.  The values of the list
  are either strings, or a dictionary with a SINGLE key.
  '''

  ## first level is the duration of the activity
  types        = activities_dict.keys()
  hist    = list()
  type_sublist = [Attributes(x).name for x in activities_dict]
  type_top_str = '''Let's find something to do!
  Please choose what type of activity you're interested in.'''
  type_bot_str = '''Type the number corresponding to the activity, then press "Enter".
  Please enter the number here : 
  '''
  ccol = len("  Please enter the number here : ")
  crow   = Curses_Display(stdscr, type_sublist, type_top_str, type_bot_str, hist)
  type_input = stdscr.getstr(crow, ccol)
  if type_input == 'q':
    raise UserQuit
  type_sel   = types[int(type_input)-1]
  dur_dict = activities_dict[type_sel]
  hist.append(Attributes(type_sel).name)
  stdscr.addstr(crow+1, 0, "You chose: " + Attributes(type_sel).name)
  stdscr.refresh()
  time.sleep(1)
  
  ## the next level is the duration of the activity
  durs        = dur_dict.keys()
  dur_sublist = [Attributes(x).name for x in dur_dict]
  dur_top_str = '''Now choose what duration of activity you're interested in.
'''
  dur_bot_str = '''Type the number corresponding to the duration, then press "Enter".
  Please enter the number here : 
  '''
  ccol = len("  Please enter the number here : ")
  crow   = Curses_Display(stdscr, dur_sublist, dur_top_str, dur_bot_str, hist)
  dur_input = stdscr.getstr(crow, ccol)
  if dur_input == 'q':
    raise UserQuit
  dur_sel   = durs[int(dur_input)-1]
  hist.append(Attributes(dur_sel).name)
  who_dict = dur_dict[dur_sel]
  stdscr.addstr(crow+1, 0, "You chose: " + Attributes(dur_sel).name)
  stdscr.refresh()
  time.sleep(1)

  ## the next level is who is doing the activity
  whos        = who_dict.keys()
  who_sublist = [Attributes(x).name for x in who_dict]
  who_top_str = '''Now choose whom this activity is for.
'''
  who_bot_str = '''Type the number corresponding to the choice, then press "Enter".
  Please enter the number here : 
  '''
  ccol = len("  Please enter the number here : ")
  crow   = Curses_Display(stdscr, who_sublist, who_top_str, who_bot_str, hist)
  who_input = stdscr.getstr(crow, ccol)
  if who_input == 'q':
    raise UserQuit
  who_sel   = whos[int(who_input)-1]
  hist.append(Attributes(who_sel).name)
  acts = who_dict[who_sel]
  stdscr.addstr(crow+1, 0, "You chose: " + Attributes(who_sel).name)
  stdscr.refresh()
  time.sleep(1)

  ## Now ask if you want a list to choose from, or a random choice.
  stdscr.clear()
  y = 0
  stdscr.addstr(y, 0, "So far, you have chosen: ")
  for i, choice in enumerate(hist):
    stdscr.addstr(choice + ', ', curses.color_pair(i+3))
  y += 2
  stdscr.addstr(y, 0, '''If you want to see a list of activities and choose one, please type 'l'.
If you want me to choose a random activity for you, please type 'r'.
Please type your choice here: ''')
  random_str = stdscr.getstr(y+2, len("Please type your choice here: "))
  stdscr.refresh()
  stdscr.addstr(y+4, 0, "You chose: " + random_str)
  stdscr.refresh()
  time.sleep(2)
  
  ## the next level is a list of activities (not a dict).  The elements of the
  ## list can be strings or dictionaries
  ## acts        = act_dict.keys()
  if random_str not in ("q", "l", "r"):
    raise BadChoice
  act_sublist = [Attributes(x).name for x in acts] ## strings or dicts!
  if random_str == "l":
    ## first, offer a top-level menu of choices
    act_top_str = '''Please choose which activity you want to do.
    '''
    act_bot_str = '''Type the number corresponding to the choice, then press "Enter".
Please enter the number here : 
    '''
    ccol = len("  Please enter the number here : ")
    crow   = Curses_Display(stdscr, act_sublist, act_top_str, act_bot_str, hist)
    act_input = stdscr.getstr(crow, ccol)
    if act_input == 'q':
      raise UserQuit
    act_sel   = acts[int(act_input)-1]
    hist.append(Attributes(act_sel).name)
    stdscr.addstr(crow+1, 0, "You chose: " + Attributes(act_sel).name)
    stdscr.refresh()
    time.sleep(1)

    ## if the choice is a string, we're all set.  if the choice is a dictionary,
    ##  (which is assumed to have ONLY ONE key),  offer a choice of details.
    if type(act_sel) == dict:
      details_sublist = act_sel[Attributes(act_sel).name]
      details_top_str = '''Please choose from this list of details for this activity.'''
      details_bot_str = '''Type the number corresponding to the choice, then press "Enter".
Please enter the number here : 
    '''
      ccol = len("  Please enter the number here : ")
      crow   = Curses_Display(stdscr, details_sublist, details_top_str,
                              details_bot_str, hist)
      if act_input == 'q':
        raise UserQuit
      details_input = stdscr.getstr(crow, ccol)
      details_sel = details_sublist[int(details_input)-1]
      hist.append(details_sel)
      stdscr.addstr(crow+1, 0, "You chose: " + details_sel)
      stdscr.refresh()
      time.sleep(1)
  ## end of if random_str == "l"
  elif random_str == "r":
    act_sel = random.choice(acts)
    act_sel_name = Attributes(act_sel).name
    hist.append(act_sel_name)
    if type(act_sel) == dict:
      details_sublist = act_sel[Attributes(act_sel).name]
      details_sel = random.choice(details_sublist)
      hist.append(details_sel)
  ## end elif random_str == "r"
  act_sel_name = Attributes(act_sel).name
  try:
    act_sel_name += (": " + details_sel)
  except:
    pass
  stdscr.clear()
  final_str = "Great, " + hist[2] + "!  Your " + hist[1] +" "+hist[0]+" selection is "
  stdscr.addstr(0,0,final_str)
  stdscr.addstr(0,len(final_str) + 1, act_sel_name + ".\n", 
                curses.color_pair(3) | curses.A_BOLD| curses.A_REVERSE)
  stdscr.addstr(2,0, "Now get off your butt and have fun!", 
                curses.color_pair(2) | curses.A_BOLD | curses.A_REVERSE)
  stdscr.addstr(4,0, "Press q to exit: ")
  qin = stdscr.getstr(4, len("Press q to exit: "))
  if qin == "q":
    return
# end main

def Curses_Display(stdscr, sublist, top_str, bot_str, hist):
  '''Clear the screen.  Print the top string.
  Create list with the choice number followed by each choice.
  Print the bottom string, asking for input.
  Returns the cursor row.
  '''
  y = 0
  x = 0
  stdscr.clear()
  if len(hist) > 0:
    stdscr.addstr(y, x, "So far, you have chosen: ")
    for i, choice in enumerate(hist):
      stdscr.addstr(choice + ', ', curses.color_pair(i+3))
      y += 2
  stdscr.addstr(y,0, top_str, curses.color_pair(1)| curses.A_BOLD)
  y += 3
  nlist = len(sublist)
  choices = [str(ct) + ' : ' + str(val)
             for ct, val in enumerate(sublist,1)]
  for i, choice in enumerate(choices):
    stdscr.addstr(y,0,choice, curses.A_REVERSE)
    y += 1
    
  stdscr.addstr(y + 1, 0, bot_str, curses.color_pair(0))
  y +=1
  return y+1
# end Curses_Display




if __name__=='__main__':
  try:
      # Initialize curses
      stdscr=curses.initscr()
      curses.start_color()
      curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
      curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_BLACK)
      curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
      curses.init_pair(4, curses.COLOR_CYAN, curses.COLOR_BLACK)
      curses.init_pair(5, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
      curses.init_pair(6, curses.COLOR_YELLOW, curses.COLOR_BLACK)
      curses.curs_set(2)
      stdscr.border(5)

      main(stdscr, activities_dict)     # Enter the main loop
  except UserQuit, e:
    pass
  finally:
      # In event of error, restore terminal to sane state.
      curses.endwin()
