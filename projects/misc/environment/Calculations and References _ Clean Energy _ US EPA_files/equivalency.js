/**
 * File last updated 4/16/2014
 * 
 *
 * Notes: kilowatthours = National non-baseload /1000 * 0.00045359237
 *
 * 4/16/2014: Changed electricity constant from 0.00068927 to 0.000689551, changed wind turbines constant from 3,631 to 3,633.
 *
 * 9/17/2013: Changed conversion factor for home electricity from 6.68 to 7.268 metric tons of CO2. Changed home energy use from 19.43 to 20.02 metric tons of CO2.
 * 
 * 04/24/2013:
 *  Changed the conversion factor for natural gas to be more precise, from 0.005 metric tons of CO2 per therm to 0.005306
 * 
 * 10/4/2012:
 *  Changed electricity use (kilowatt-hours) from 0.0006895556567977 to 0.00070555 metric tons CO2 per kWh
 *  Changed passenger vehicles per year from 5.1 to 4.8 metric tons CO2e per vehicle per year
 *  Changed home electricity use from 8.02 to 6.68 metric tons CO2 per home
 *  Changed home energy use from 11.55 to 19.43 metric tons CO2 per home per year
 *  Changed acres of U.S. forests storing carbon for one year from 4.69 to
 *   1.22 metric ton CO2 sequestered annually by one acre of average U.S. forest
 *  Changed acres of U.S. forests preserved from conversion to cropland from 100.94 to
 *   129.51 metric tons CO2 per acre per year
 *  Changed railcars of coal burned from 183.65 to 232.74 metric tons CO2 per railcar
 *  Changed tons of waste recycled instead of landfilled from 2.87 to
 *   2.67 metric tons CO2e per ton of waste recycled instead of landfilled
 *  Changed coal-fired power plant emissions for one year from 4,220,543 to 3,533,098 metric tons CO2 per power plant
 * 
 * 10/31/2011:
 *  Household energy use from 11.53 to 11.55 
 *
 * 5/25/2011:
 *  Household energy use from 11.77 to 11.53
 *  Household electricity use for one year (# of households) from 8.24 to 8.02
 * 
 * 5/18/2011:
 *  Changed electricity use (kilowatt-hours) from 0.0006895102975607 to 0.0006895556567977 metric tons CO2 per kWh
 *
 * 4/14/2011:
 *  Changed electricity use (kilowatt-hours) from 7.18 x 10-4 to 6.8951 x 10-4 metric tons CO2 per kWh
 *
 * 2/21/2011:
 *  Changed electricity use (kilowatt-hours) from 7.18 x 10-4 to 6.91 x 10-4 metric tons CO2 per kWh
 *  Changed passenger vehicles per year from 5.23 to 5.1 metric tons CO2e per vehicle per year
 *  Changed gallons of gasoline consumed from 8.89 x 10^-3 to 8.92 x 10^-3 metric tons CO2 per gallon of gasoline
 *  Changed tanker trucks filled with gasoline from 74.89 to 75.82 metric tons Co2 per tanker truck
 *  Changed home energy use from 11.75 to 11.77 metric tons CO2 per home
 *  Changed acres of forest carbon preserved from avoided deforestation from 105.38 to 100.94 metric tons CO2 per acre converted
 *  Changed railcars of coal burned from 191.5 to 183.65 metric tons CO2 per railcar
 *  Changed tons of waste recycled instead of landfilled from 2.97 to
 *   2.87 metric tons CO2e per ton of waste recycled instead of landfilled
 *  Changed coal-fired power plant emissions for one year from 3,850,479 to 4,220,543 metric tons CO2 per power plant
 *
 * 3/07/2010:
 *  Changed Propane cylinders used for home barbeques from 0.054 to 0.024 
 *  Changed Household electricity use from 7.7 to 8.24
 *  Changed Household energy use from 11.21 to 11.75
 *  
 * 11/05/2009:
 *  Passenger vehicles per year: 5.23 (was 5.46)
 *  Gallons of gasoline consumed: 8.89 x 10-3 (was 8.81)
 *  Tanker trucks filled with gasoline: 74.89 (was 74.88)
 *  Home electricity use: 7.70 (was 7.21)
 *  Home energy use: 11.21 (was 10.99)
 *  Acres of pine or fir forests storing carbon for one year: 4.69 (was 4.4)
 *  Acres of forest preserved from deforestation: 105.38 (was 143.37)
 *  Propane cylinders used for home barbeques: 0.054 (was 0.024)
 *  Tons of waste recycled instead of landfilled: 2.97 (was 2.90)
 *  Coal-fired power plant emissions for one year: 3,850,479 (was 4,643,734)
 *  
 * 2/12/2009:
 *  Values are from the formulae, please click the question mark besides each item on the html page to find it
 *  Household electricity use for one year (# of households): change from 7.55 to 7.21
 *  Household energy use: change from 11.33 to 10.99
 *  
 */

var EquivCalc = {};

// Factors for converting co2,ch4,n2o,hfc,pfc,sf6,and carb to Carbon Dioxide Equivalent
EquivCalc.conv_factors = new Hash({
						  'co2' : 1,
						  'ch4' : 25,
						  'n2o' : 298,
						  'hfc' : 14800,
						  'pfc' : 7390,
						  'sf6' : 22800,
						  'carb' : 44/12});

EquivCalc.starting_points_factors = { "gasoline" : 0.00889,
				"kilowatthours" : 0.000689551,
				"therms" : 0.005302,
				"vehicles" : 4.75 };

/*
	equivalency factors; 
	i.e. 	4.62 metric ton of Carbon Dioxide Equivalent = 1 car off the road for one year
*/
EquivCalc.equiv_factors = [
	4.75,		// Passenger cars not driven for one year
	10.96,		// Household energy use
	0.008887,	// Gallons of gasoline
	0.43,		// Barrels of oil
	75.54,		// Tanker trucks filled with gasoline
	7.270,		// Household electricity use for one year (# of households)
	0.039,		// Number of tree seedlings grown for 10 years
	1.22,		// Acres of U.S. forests storing carbon for 1 year
	129.51,		// Acres of U.S. forests preserved from conversion to cropland
	0.024,		// Propane cylinders used for home barbeques
	186.50,		// Coal burned/railcars of coal
	2.79,		// Tons of waste recycled instead of landfilled 
	3808651.00,	// coal fired power plant for one year
	0.000931,	// Pounds of coal burned
	0.000420,	// Miles driven by the average passenger vehicle per year
	19.51,		// Number of garbage trucks of waste recycled instead of landfilled
	3633,		// Number of wind turbines installed
	0.03823 	// Number of incandescent bulbs switched to compact fluorescent bulbs
];

EquivCalc.multiples = [0.9071847,0.00045359237,1,0.001]; //multiples for converting into metric tons
// The order is: Tons, Pounds, Metric Tons, Kilograms
// 1 ton [metric] = 1.1023113 ton [short]
// 1 ton [short] = 907.18474 kilogram
// 1 ton [metric] = 2,204.6226218 pound
// 1 ton [short] = 2,000 pound
EquivCalc.finalResultUnits = 2; // the default units for displaying the result is in Metric Tons

EquivCalc.setConvFactors = function(e)
{
	var targetSelect = Event.element(e);
	var chemName = targetSelect.name.substring(0, 3);
	var chemValue = $F($('conversion_table')[chemName]);
	this.conv_factors.set(chemName, parseFloat($F(targetSelect)));
	if (chemValue != "" && $F($('conversion_table')['y0']) != "") this.calculateTotal();
}

EquivCalc.calculateTotal = function(e)
{   //alert(this.finalResultUnits)
	if(e) Event.stop(e);
	var total = 0; // variable holding the total greenhouse gas reduction
	var error_flag = "No GHG values have been entered"; // variable holding the appropriate error message
	this.conv_factors.each(function(pair) {
		var name = pair.key;
		var value = EquivCalc.stripCommas($F($('conversion_table')[name]));
		if (value != "" && !isNaN(value)) {
			value = value * pair.value; // convert to Carbon Dioxide Equivalent
			value = parseFloat(EquivCalc.convertToTons(name, value)); // convert to Metric Tons
			total += value;
			error_flag = "";
		} else if (value != "" && isNaN(value)) {
			$('conversion_table')[name].focus();
			$('conversion_table')[name].select();
			error_flag = "<span class=\"calculator-warning\">Please enter a valid numerical value.</span>";
			return;
		}
	});
	
	if (error_flag == "") {
		this.clearErrs();
		$('output').show();
		$('conversion_table')['y0'].value = this.commaSplit(this.roundIt(total / this.multiples[this.finalResultUnits]));
		$('conversion_table').resultUnits[this.finalResultUnits].selected = true;
		window.location.href = "#results";
		for (i = 1; i <= this.equiv_factors.length; i++){
			var fieldName = "y" + i;
			$('conversion_table')[fieldName].value = this.commaSplit(this.roundIt(total / this.equiv_factors[i - 1]));
		}
	}
	else {
		this.convErr(error_flag);
	}
	
}

EquivCalc.setResultUnits = function()
{   //alert("set result units")
	this.finalResultUnits = parseFloat($F($('conversion_table')['resultUnits']));
	//alert($F($('starting_points')['equivalency']))
	if ($F($('conversion_table')['y0']) != "")  //this.calculateTotalV2();
	{
	  	if  ($F($('starting_points')['equivalency']) != "") {
	   		this.calculateTotalV2();
		}
	   
	    if  ($F($('starting_points')['equivalency']) == "") {
	   		this.calculateTotal();
		}	   
	}
}
		
EquivCalc.convertToTons = function(name, value)
{
	var factor = $F($('conversion_table')[name + "units"]);
	return (value * this.multiples[factor]);
}

EquivCalc.roundIt = function(number)
{
// this function rounds according to the following rules:
// - If the figure is above 100 it rounds to a whole number.
// - If the figure is between 1 and 100, it rounds to one decimal place.
// - If the figure is between 0.001 and 1, it rounds to three decimal places.
// - If the figure is less than 0.001, it rounds to four decimal places.

	var absolute = Math.abs(number);
	if (isNaN(number) || number == 0)
	{
		return number;
	}
	else
	{
		// If the result is above 100 let's just use a whole number. 
		if (absolute > 100)
		{
			number = Math.round(number);
		}
		// If it's lower than 100 but greater than 1, let's show it out to one decimal place (e.g., 10.1).
		else if ((absolute < 100) && (absolute > 1))
		{
			number = Math.round(number * 10) / 10;
		}
		// If it's below zero, let's show it out to two decimal places for numbers up to 1/100 (e.g., 0.95, or 0.09)
		// (Note: at the request of the client, changed this part of the rounding rule to round to 3 decimal places)
		else if ((absolute < 1) && (absolute > 0.001))
		{
			//number = Math.round(number * 100) / 100;
			number = Math.round(number * 1000) / 1000;
		}
		//  and out to four decimal places for numbers smaller than that (e.g., 0.0097, 0.0030).
		else
		{
			number = Math.round(number * 10000) / 10000;
		}
		return number;
	}
}

EquivCalc.stripCommas = function(str)
{
	var commaFilter = /,/g;
	return str.replace(commaFilter, "");
}

EquivCalc.commaSplit = function(srcNumber)
{
	var txtNumber = new String(srcNumber);
	
	if (isNaN(srcNumber) || txtNumber == "") {
		return srcNumber;
	}
	else {
		var rxSplit = new RegExp('([0-9])([0-9][0-9][0-9][,.])');
		var arrNumber = txtNumber.split('.');
		arrNumber[0] += '.';
		do {
			arrNumber[0] = arrNumber[0].replace(rxSplit, '$1,$2');
		} while (rxSplit.test(arrNumber[0]));
		
		if (arrNumber.length > 1) {
			return arrNumber.join('');
		}
		else {
			return arrNumber[0].split('.')[0];
		}
    }
}

EquivCalc.handleStartingPoints = function(e)
{
	Event.stop(e);
	var typeChoice = $('starting_points')['equiv_type']
	var eType = $F(typeChoice);
	if(eType != "")
	{
		var userValue = 0;
		userValue = this.stripCommas($F($('starting_points')['equivalency']));
		if (userValue == "" || isNaN(userValue))
		{
			var typeString = typeChoice.options[typeChoice.selectedIndex].innerHTML;
			this.startErr("<span class=\"calculator-warning\">Please enter a valid numerical value.</span> " + typeString + ".");
			return false;
		}
		this.clearErrs();
		
		var CO2equiv = userValue * EquivCalc.starting_points_factors[eType];
	
		var form = $('conversion_table');
		form.reset();
		form.co2.value = CO2equiv; // set the value for CO2
		form.co2units[2].selected = true; // select metric tons
		this.calculateTotal(); // run the calculator
		form.co2.value = this.commaSplit(this.roundIt(CO2equiv)); // round the value for CO2
	}
}


EquivCalc.handleStartingPoints2 = function(e)
{
	var typeChoice = $('starting_points')['equiv_type']
	var eType = $F(typeChoice);
	if(eType != "")
	{
		var userValue = 0;
		userValue = this.stripCommas($F($('starting_points')['equivalency']));
		if (userValue == "" || isNaN(userValue))
		{
			var typeString = typeChoice.options[typeChoice.selectedIndex].innerHTML;
			this.startErr("<span class=\"calculator-warning\">Please enter a valid numerical value.</span> " + typeString + ".");
			return false;
		}
		this.clearErrs();
		
		var CO2equiv = userValue * EquivCalc.starting_points_factors[eType];
	
		var form = $('conversion_table');
		form.reset();
		form.co2.value = CO2equiv; // set the value for CO2
		form.co2units[2].selected = true; // select metric tons
		
		this.calculateTotal(); // run the calculator
		form.co2.value = this.commaSplit(this.roundIt(CO2equiv)); // round the value for CO2
	}
}

EquivCalc.handleCalcsAndRefs = function(e)
{
	var eType = $F($('starting_points')['equiv_type']);
	if(eType != "")
	{
		this.href = "refs.html#" + eType;
	}
	else
	{
		Event.stop(e);
	}
}

EquivCalc.startErr = function(errorMessage)
{
	$('startError').update(errorMessage);
}

EquivCalc.convErr = function(errorMessage)
{
	$('convError').update(errorMessage);
}

EquivCalc.clearErrs = function()
{
	$('convError').update("");
	$('startError').update("");
}

document.observe("dom:loaded", function() {
	$$('#starting_points a')[0].observe('click', EquivCalc.handleCalcsAndRefs);
	$('starting_points').observe('submit', EquivCalc.handleStartingPoints.bindAsEventListener(EquivCalc));
	$('conversion_table').observe('submit', EquivCalc.calculateTotal.bindAsEventListener(EquivCalc));
	$('conversion_table').observe('reset', EquivCalc.clearErrs);
	$($('conversion_table')['resultUnits']).observe('change', EquivCalc.setResultUnits.bindAsEventListener(EquivCalc));
	$($('conversion_table')['hfcoptions']).observe('change', EquivCalc.setConvFactors.bindAsEventListener(EquivCalc));
	$($('conversion_table')['pfcoptions']).observe('change', EquivCalc.setConvFactors.bindAsEventListener(EquivCalc));
});


EquivCalc.calculateTotalV2 = function(e)
{
		this.handleStartingPoints2(); // run the calculator	
}