(function ($) {
    'use strict';
    var LEFT_DIRECTION = 'left';
    var RIGHT_DIRECTION = 'right';

    var CarouselFlex = function(config) {
        this.initCarousel = function (config) {
            var carouselThis = this,
                breakPointsConfig = [],
                defaultBreakPoints = [
                    {
                        slidesVisible: 4,
                        slidesSwipe: 4,
                        minScreenWidth: 1200
                    },
                    {
                        slidesVisible: 2,
                        slidesSwipe: 2,
                        minScreenWidth: 768
                    }
                ],
                breakPoint;

            this.$carousel = config.carousel;
            this.$prevButton = this.$carousel.find('.carousel-control.left');
            this.$nextButton = this.$carousel.find('.carousel-control.right');
            this.$pagination = this.$carousel.find('.carousel-pagination');
            this.$slideContainer = this.$carousel.find('.carousel-slide-content');
            this.slidePosition = 1; // index of first item on the current slide

            if(config.breakPoints && config.breakPoints.length) {

                for(var i = 0; i < config.breakPoints.length; i++) {
                    breakPoint = {};
                    breakPoint.slidesVisible = parseInt(config.breakPoints[i].slidesVisible);
                    breakPoint.slidesSwipe = parseInt(config.breakPoints[i].slidesSwipe);
                    breakPoint.minScreenWidth = parseInt(config.breakPoints[i].minScreenWidth);

                    if(breakPoint.slidesVisible && breakPoint.slidesSwipe && breakPoint.minScreenWidth) {
                        breakPointsConfig.push(breakPoint);
                    } else {
                        breakPointsConfig = [];
                        break;
                    }
                }

                breakPointsConfig.sort(function(a, b) {
                    return  b.minScreenWidth - a.minScreenWidth;
                });
            }

            this.breakPoints = breakPointsConfig.length ? breakPointsConfig : defaultBreakPoints;
            this.minApplyingScreenWidth = this.breakPoints[this.breakPoints.length - 1].minScreenWidth;
            this.setSlidesConfig();

            this.$prevButton.on('click', function() {
                carouselThis.doSlidesTransition(LEFT_DIRECTION);
                config.clickPrevButtonHandler(carouselThis);
            });

            this.$nextButton.on('click', function() {
                carouselThis.doSlidesTransition(RIGHT_DIRECTION);
                config.clickNextButtonHandler(carouselThis);
            });

            $(window).on('resize', function() {
                if($(window).width() < carouselThis.minApplyingScreenWidth) {
                    carouselThis.$slideContainer.css('transform', 'translateX(0)');
                } else {
                    carouselThis.setSlidesConfig();
                }
            });
        };

        this.setSlidesConfig = function () {
            var $slides = this.$carousel.find('li.carousel-slide-item'),
                $firstSlide = $($slides[0]),
                windowWidth = $(window).width(),
                breakPoint,
                slideMinWidth;

            for(var i = 0; i < this.breakPoints.length; i++) {
                if(windowWidth >= this.breakPoints[i].minScreenWidth) {
                    breakPoint = this.breakPoints[i];
                    this.slidesVisible = breakPoint.slidesVisible;
                    this.slidesSwipe = breakPoint.slidesSwipe;
                    this.minScreenWidth = breakPoint.minScreenWidth;
                    break;
                }
            }

            this.slidesNumber = $slides.length;
            slideMinWidth = 'calc(' + 100 / this.slidesVisible + '% - ' + ($firstSlide.outerWidth(true) - $firstSlide.width()) + 'px)';
            $slides.css('min-width', slideMinWidth);
            this.doSlidesTransition();
        };

        this.hasPrevSlide = function() {
            return (this.slidePosition > 1);
        };

        this.hasNextSlide = function() {
            return ((this.slidesNumber - this.slidePosition - this.slidesSwipe) >= 0);
        };

        this.setButtonsClasses = function () {
            if (this.hasNextSlide()) {
                this.$nextButton.addClass('active');
            } else {
                this.$nextButton.removeClass('active');
            }

            if (this.hasPrevSlide()) {
                this.$prevButton.addClass('active');
            } else {
                this.$prevButton.removeClass('active');
            }
        };

        this.modifyPagination = function() {
            var text = '',
                next,
                from,
                to;

            if(this.slidesNumber) {
                from = this.slidePosition;
                next = this.slidePosition + this.slidesVisible - 1;
                to = (next < this.slidesNumber) ? next : this.slidesNumber;
                text = (from === to) ? (from + ' of ' + this.slidesNumber) : (from + ' - ' + to + ' of ' + this.slidesNumber);
                this.$pagination.text(text);
            }
        };

        this.doSlidesTransition = function(direction) {
            var nextPosition;

            if(direction === LEFT_DIRECTION) {
                nextPosition = this.slidePosition - this.slidesSwipe;
                this.slidePosition = (nextPosition > 1) ? nextPosition : 1;
            } else if(direction === RIGHT_DIRECTION) {
                this.slidePosition = this.slidePosition + this.slidesSwipe;
            }

            this.$slideContainer.css('transform', 'translateX(1px) translateX(-' + ((this.slidePosition - 1) / this.slidesVisible) * 100 +'%)');
            this.modifyPagination();
            this.setButtonsClasses();
        };

        this.initCarousel(config);
    };

    $.fn.carouselFlex = function(config) {
        var defaultConfig = {
            breakPoints: [],
            clickPrevButtonHandler: function(){},
            clickNextButtonHandler: function(){}
        };

        return this.each(function() {
            var settings = $.extend({carousel : $(this)}, defaultConfig, config),
                carouselFlex = new CarouselFlex(settings);
        });
    };
}(jQuery));
