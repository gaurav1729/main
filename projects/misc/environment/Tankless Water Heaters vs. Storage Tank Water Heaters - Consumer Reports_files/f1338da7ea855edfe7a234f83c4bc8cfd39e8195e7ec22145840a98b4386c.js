window.optimizely.push({type:"load",data:{changes:[{"dependencies": [], "type": "custom_code", "id": "18072F76-0D11-4970-8783-A7FF2A3918FC", "value": function($){(function optimizely() {
  if(document.getElementsByClassName("preview-bar-container").length>0){
    document.getElementsByClassName("preview-bar-container")[0].getElementsByClassName("anonymous")[0].getElementsByClassName("preview-bar-title")[0].innerHTML="Our ratings are backed by science and powered by consumers";
    document.getElementsByClassName("preview-bar-container")[0].getElementsByClassName("anonymous")[0].getElementsByClassName("preview-bar-text")[0].innerHTML="We’re independent because we don’t accept any advertising or free samples. Get access to the ratings in this article, and the rest of our 100% unbiased ratings and reviews.";
  }
})();
}}]}});