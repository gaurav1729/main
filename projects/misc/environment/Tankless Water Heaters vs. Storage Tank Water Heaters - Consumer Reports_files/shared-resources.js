/*
 ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ============
 Adobe Visitor API for JavaScript version: 2.0.0
 Copyright 1996-2015 Adobe, Inc. All Rights Reserved
 More info available at http://www.omniture.com
*/
function Visitor(s,w){function y(a){function c(a,d,b){b=b?b+="|":b;return b+(a+"="+encodeURIComponent(d))}for(var b="",e=0,f=a.length;e<f;e++){var g=a[e],h=g[0],g=g[1];g!=i&&g!==u&&(b=c(h,g,b))}return function(a){var d=m.Ba(),a=a?a+="|":a;return a+("TS="+d)}(b)}if(!s)throw"Visitor requires Adobe Marketing Cloud Org ID";var a=this;a.version="2.0.0";var n=window,l=n.Visitor;l.version=a.version;n.s_c_in||(n.s_c_il=[],n.s_c_in=0);a._c="Visitor";a._il=n.s_c_il;a._in=n.s_c_in;a._il[a._in]=a;n.s_c_in++;
    a.la={Ja:[]};var v=n.document,i=l.Kb;i||(i=null);var E=l.Lb;E||(E=void 0);var j=l.Ta;j||(j=!0);var k=l.Qa;k||(k=!1);a.ia=function(a){var c=0,b,e;if(a)for(b=0;b<a.length;b++)e=a.charCodeAt(b),c=(c<<5)-c+e,c&=c;return c};a.t=function(a,c){var b="0123456789",e="",f="",g,h,i=8,k=10,l=10;c===o&&(x.isClientSideMarketingCloudVisitorID=j);if(1==a){b+="ABCDEF";for(g=0;16>g;g++)h=Math.floor(Math.random()*i),e+=b.substring(h,h+1),h=Math.floor(Math.random()*i),f+=b.substring(h,h+1),i=16;return e+"-"+f}for(g=
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           0;19>g;g++)h=Math.floor(Math.random()*k),e+=b.substring(h,h+1),0==g&&9==h?k=3:(1==g||2==g)&&10!=k&&2>h?k=10:2<g&&(k=10),h=Math.floor(Math.random()*l),f+=b.substring(h,h+1),0==g&&9==h?l=3:(1==g||2==g)&&10!=l&&2>h?l=10:2<g&&(l=10);return e+f};a.Wa=function(){var a;!a&&n.location&&(a=n.location.hostname);if(a)if(/^[0-9.]+$/.test(a))a="";else{var c=a.split("."),b=c.length-1,e=b-1;1<b&&2>=c[b].length&&(2==c[b-1].length||0>",ac,ad,ae,af,ag,ai,al,am,an,ao,aq,ar,as,at,au,aw,ax,az,ba,bb,be,bf,bg,bh,bi,bj,bm,bo,br,bs,bt,bv,bw,by,bz,ca,cc,cd,cf,cg,ch,ci,cl,cm,cn,co,cr,cu,cv,cw,cx,cz,de,dj,dk,dm,do,dz,ec,ee,eg,es,et,eu,fi,fm,fo,fr,ga,gb,gd,ge,gf,gg,gh,gi,gl,gm,gn,gp,gq,gr,gs,gt,gw,gy,hk,hm,hn,hr,ht,hu,id,ie,im,in,io,iq,ir,is,it,je,jo,jp,kg,ki,km,kn,kp,kr,ky,kz,la,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md,me,mg,mh,mk,ml,mn,mo,mp,mq,mr,ms,mt,mu,mv,mw,mx,my,na,nc,ne,nf,ng,nl,no,nr,nu,nz,om,pa,pe,pf,ph,pk,pl,pm,pn,pr,ps,pt,pw,py,qa,re,ro,rs,ru,rw,sa,sb,sc,sd,se,sg,sh,si,sj,sk,sl,sm,sn,so,sr,st,su,sv,sx,sy,sz,tc,td,tf,tg,th,tj,tk,tl,tm,tn,to,tp,tr,tt,tv,tw,tz,ua,ug,uk,us,uy,uz,va,vc,ve,vg,vi,vn,vu,wf,ws,yt,".indexOf(","+
        c[b]+","))&&e--;if(0<e)for(a="";b>=e;)a=c[b]+(a?".":"")+a,b--}return a};a.cookieRead=function(a){var a=encodeURIComponent(a),c=(";"+v.cookie).split(" ").join(";"),b=c.indexOf(";"+a+"="),e=0>b?b:c.indexOf(";",b+1);return 0>b?"":decodeURIComponent(c.substring(b+2+a.length,0>e?c.length:e))};a.cookieWrite=function(d,c,b){var e=a.cookieLifetime,f,c=""+c,e=e?(""+e).toUpperCase():"";b&&"SESSION"!=e&&"NONE"!=e?(f=""!=c?parseInt(e?e:0,10):-60)?(b=new Date,b.setTime(b.getTime()+1E3*f)):1==b&&(b=new Date,f=
        b.getYear(),b.setYear(f+2+(1900>f?1900:0))):b=0;return d&&"NONE"!=e?(v.cookie=encodeURIComponent(d)+"="+encodeURIComponent(c)+"; path=/;"+(b?" expires="+b.toGMTString()+";":"")+(a.cookieDomain?" domain="+a.cookieDomain+";":""),a.cookieRead(d)==c):0};a.h=i;a.z=function(a,c){try{"function"==typeof a?a.apply(n,c):a[1].apply(a[0],c)}catch(b){}};a.L=function(d,c){c&&(a.h==i&&(a.h={}),a.h[d]==E&&(a.h[d]=[]),a.h[d].push(c))};a.s=function(d,c){if(a.h!=i){var b=a.h[d];if(b)for(;0<b.length;)a.z(b.shift(),c)}};
    a.w=function(a,c,b,e){b=encodeURIComponent(c)+"="+encodeURIComponent(b);c=m.Bb(a);a=m.sb(a);if(-1===a.indexOf("?"))return a+"?"+b+c;var f=a.split("?"),a=f[0]+"?",e=m.fb(f[1],b,e);return a+e+c};a.Va=function(a,c){var b=RegExp("[\\?&#]"+c+"=([^&#]*)").exec(a);if(b&&b.length)return decodeURIComponent(b[1])};a.bb=function(){var d=i,c=n.location.href;try{var b=a.Va(c,p.aa);if(b)for(var d={},e=b.split("|"),c=0,f=e.length;c<f;c++){var g=e[c].split("=");d[g[0]]=decodeURIComponent(g[1])}return d}catch(h){}};
    a.ea=function(){var d=a.bb();if(d&&d.TS&&!(Math.floor((m.Ba()-d.TS)/60)>p.Oa||d[I]!==s)){var c=d[o],b=a.setMarketingCloudVisitorID;c&&c.match(p.v)&&b(c);a.j(t,-1);d=d[r];c=a.setAnalyticsVisitorID;d&&d.match(p.v)&&c(d)}};a.ab=function(d){function c(d){m.Ea(d)&&a.setCustomerIDs(d)}function b(d){d=d||{};a._supplementalDataIDCurrent=d.supplementalDataIDCurrent||"";a._supplementalDataIDCurrentConsumed=d.supplementalDataIDCurrentConsumed||{};a._supplementalDataIDLast=d.supplementalDataIDLast||"";a._supplementalDataIDLastConsumed=
        d.supplementalDataIDLastConsumed||{}}if(d)try{if(d=m.Ea(d)?d:m.Cb(d),d[a.marketingCloudOrgID]){var e=d[a.marketingCloudOrgID];c(e.customerIDs);b(e.sdid)}}catch(f){throw Error("`serverState` has an invalid format.");}};a.l=i;a.Ya=function(d,c,b,e){c=a.w(c,"d_fieldgroup",d,1);e.url=a.w(e.url,"d_fieldgroup",d,1);e.m=a.w(e.m,"d_fieldgroup",d,1);x.d[d]=j;e===Object(e)&&e.m&&"XMLHttpRequest"===a.na.F.G?a.na.ob(e,b,d):a.useCORSOnly||a.Za(d,c,b)};a.Za=function(d,c,b){var e=0,f=0,g;if(c&&v){for(g=0;!e&&2>
    g;){try{e=(e=v.getElementsByTagName(0<g?"HEAD":"head"))&&0<e.length?e[0]:0}catch(h){e=0}g++}if(!e)try{v.body&&(e=v.body)}catch(k){e=0}if(e)for(g=0;!f&&2>g;){try{f=v.createElement(0<g?"SCRIPT":"script")}catch(l){f=0}g++}}!c||!e||!f?b&&b():(f.type="text/javascript",f.src=c,e.firstChild?e.insertBefore(f,e.firstChild):e.appendChild(f),e=a.loadTimeout,q.d[d]={requestStart:q.p(),url:c,va:e,ta:q.Aa(),ua:0},b&&(a.l==i&&(a.l={}),a.l[d]=setTimeout(function(){b(j)},e)),a.la.Ja.push(c))};a.Ua=function(d){a.l!=
    i&&a.l[d]&&(clearTimeout(a.l[d]),a.l[d]=0)};a.ja=k;a.ka=k;a.isAllowed=function(){if(!a.ja&&(a.ja=j,a.cookieRead(a.cookieName)||a.cookieWrite(a.cookieName,"T",1)))a.ka=j;return a.ka};a.b=i;a.c=i;var F=l.bc;F||(F="MC");var o=l.ic;o||(o="MCMID");var I=l.fc;I||(I="MCORGID");var G=l.cc;G||(G="MCCIDH");var L=l.gc;L||(L="MCSYNCS");var J=l.hc;J||(J="MCSYNCSOP");var K=l.dc;K||(K="MCIDTS");var B=l.ec;B||(B="MCOPTOUT");var D=l.$b;D||(D="A");var r=l.Xb;r||(r="MCAID");var C=l.ac;C||(C="AAM");var A=l.Zb;A||(A=
        "MCAAMLH");var t=l.Yb;t||(t="MCAAMB");var u=l.jc;u||(u="NONE");a.M=0;a.ha=function(){if(!a.M){var d=a.version;a.audienceManagerServer&&(d+="|"+a.audienceManagerServer);a.audienceManagerServerSecure&&(d+="|"+a.audienceManagerServerSecure);a.M=a.ia(d)}return a.M};a.ma=k;a.f=function(){if(!a.ma){a.ma=j;var d=a.ha(),c=k,b=a.cookieRead(a.cookieName),e,f,g,h,l=new Date;a.b==i&&(a.b={});if(b&&"T"!=b){b=b.split("|");b[0].match(/^[\-0-9]+$/)&&(parseInt(b[0],10)!=d&&(c=j),b.shift());1==b.length%2&&b.pop();
        for(d=0;d<b.length;d+=2)if(e=b[d].split("-"),f=e[0],g=b[d+1],1<e.length?(h=parseInt(e[1],10),e=0<e[1].indexOf("s")):(h=0,e=k),c&&(f==G&&(g=""),0<h&&(h=l.getTime()/1E3-60)),f&&g&&(a.e(f,g,1),0<h&&(a.b["expire"+f]=h+(e?"s":""),l.getTime()>=1E3*h||e&&!a.cookieRead(a.sessionCookieName))))a.c||(a.c={}),a.c[f]=j}if(!a.a(r)&&m.o()&&(b=a.cookieRead("s_vi")))b=b.split("|"),1<b.length&&0<=b[0].indexOf("v1")&&(g=b[1],d=g.indexOf("["),0<=d&&(g=g.substring(0,d)),g&&g.match(p.v)&&a.e(r,g))}};a._appendVersionTo=
        function(d){var c="vVersion|"+a.version,b=Boolean(d)?a._getCookieVersion(d):null;b?m.gb(b,a.version)&&(d=d.replace(p.da,c)):d+=(d?"|":"")+c;return d};a.eb=function(){var d=a.ha(),c,b;for(c in a.b)!Object.prototype[c]&&a.b[c]&&"expire"!=c.substring(0,6)&&(b=a.b[c],d+=(d?"|":"")+c+(a.b["expire"+c]?"-"+a.b["expire"+c]:"")+"|"+b);d=a._appendVersionTo(d);a.cookieWrite(a.cookieName,d,1)};a.a=function(d,c){return a.b!=i&&(c||!a.c||!a.c[d])?a.b[d]:i};a.e=function(d,c,b){a.b==i&&(a.b={});a.b[d]=c;b||a.eb()};
    a.Xa=function(d,c){var b=a.a(d,c);return b?b.split("*"):i};a.cb=function(d,c,b){a.e(d,c?c.join("*"):"",b)};a.Rb=function(d,c){var b=a.Xa(d,c);if(b){var e={},f;for(f=0;f<b.length;f+=2)e[b[f]]=b[f+1];return e}return i};a.Tb=function(d,c,b){var e=i,f;if(c)for(f in e=[],c)Object.prototype[f]||(e.push(f),e.push(c[f]));a.cb(d,e,b)};a.j=function(d,c,b){var e=new Date;e.setTime(e.getTime()+1E3*c);a.b==i&&(a.b={});a.b["expire"+d]=Math.floor(e.getTime()/1E3)+(b?"s":"");0>c?(a.c||(a.c={}),a.c[d]=j):a.c&&(a.c[d]=
        k);b&&(a.cookieRead(a.sessionCookieName)||a.cookieWrite(a.sessionCookieName,"1"))};a.ga=function(a){if(a&&("object"==typeof a&&(a=a.d_mid?a.d_mid:a.visitorID?a.visitorID:a.id?a.id:a.uuid?a.uuid:""+a),a&&(a=a.toUpperCase(),"NOTARGET"==a&&(a=u)),!a||a!=u&&!a.match(p.v)))a="";return a};a.k=function(d,c){a.Ua(d);a.i!=i&&(a.i[d]=k);q.d[d]&&(q.d[d].Ib=q.p(),q.J(d));x.d[d]&&x.La(d,k);if(d==F){x.isClientSideMarketingCloudVisitorID!==j&&(x.isClientSideMarketingCloudVisitorID=k);var b=a.a(o);if(!b||a.overwriteCrossDomainMCIDAndAID){b=
        "object"==typeof c&&c.mid?c.mid:a.ga(c);if(!b){if(a.D){a.getAnalyticsVisitorID(i,k,j);return}b=a.t(0,o)}a.e(o,b)}if(!b||b==u)b="";"object"==typeof c&&((c.d_region||c.dcs_region||c.d_blob||c.blob)&&a.k(C,c),a.D&&c.mid&&a.k(D,{id:c.id}));a.s(o,[b])}if(d==C&&"object"==typeof c){b=604800;c.id_sync_ttl!=E&&c.id_sync_ttl&&(b=parseInt(c.id_sync_ttl,10));var e=a.a(A);e||((e=c.d_region)||(e=c.dcs_region),e&&(a.j(A,b),a.e(A,e)));e||(e="");a.s(A,[e]);e=a.a(t);if(c.d_blob||c.blob)(e=c.d_blob)||(e=c.blob),a.j(t,
        b),a.e(t,e);e||(e="");a.s(t,[e]);!c.error_msg&&a.C&&a.e(G,a.C)}if(d==D){b=a.a(r);if(!b||a.overwriteCrossDomainMCIDAndAID)(b=a.ga(c))?b!==u&&a.j(t,-1):b=u,a.e(r,b);if(!b||b==u)b="";a.s(r,[b])}a.idSyncDisableSyncs?z.Ca=j:(z.Ca=k,b={},b.ibs=c.ibs,b.subdomain=c.subdomain,z.Db(b));if(c===Object(c)){var f;a.isAllowed()&&(f=a.a(B));f||(f=u,c.d_optout&&c.d_optout instanceof Array&&(f=c.d_optout.join(",")),b=parseInt(c.d_ottl,10),isNaN(b)&&(b=7200),a.j(B,b,j),a.e(B,f));a.s(B,[f])}};a.i=i;a.u=function(d,c,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         b,e,f){var g="",h,k=m.ub(d);if(a.isAllowed())if(a.f(),g=a.a(d,M[d]===j),(!g||a.c&&a.c[d])&&(!a.disableThirdPartyCalls||k)){if(d==o||d==B?h=F:d==A||d==t?h=C:d==r&&(h=D),h){if(c&&(a.i==i||!a.i[h]))a.i==i&&(a.i={}),a.i[h]=j,a.Ya(h,c,function(b){a.a(d)||(q.d[h]&&(q.d[h].timeout=q.p(),q.d[h].tb=!!b,q.J(h)),b&&x.La(h,j),b="",d==o?b=a.t(0,o):h==C&&(b={error_msg:"timeout"}),a.k(h,b))},f);a.L(d,b);if(g)return g;c||a.k(h,{id:u});return""}}else g||(d===o?(a.L(d,b),g=a.t(0,o),a.setMarketingCloudVisitorID(g)):
        d===r?(a.L(d,b),g="",a.setAnalyticsVisitorID(g)):(g="",e=j));if((d==o||d==r)&&g==u)g="",e=j;b&&e&&a.z(b,[g]);return g};a._setMarketingCloudFields=function(d){a.f();a.k(F,d)};a.setMarketingCloudVisitorID=function(d){a._setMarketingCloudFields(d)};a.D=k;a.getMarketingCloudVisitorID=function(d,c){if(a.isAllowed()){a.marketingCloudServer&&0>a.marketingCloudServer.indexOf(".demdex.net")&&(a.D=j);var b=a.B("_setMarketingCloudFields");return a.u(o,b.url,d,c,b)}return""};a.$a=function(){a.getAudienceManagerBlob()};
    l.AuthState={UNKNOWN:0,AUTHENTICATED:1,LOGGED_OUT:2};a.A={};a.fa=k;a.C="";a.setCustomerIDs=function(d){if(a.isAllowed()&&d){a.f();var c,b;for(c in d)if(!Object.prototype[c]&&(b=d[c]))if("object"==typeof b){var e={};b.id&&(e.id=b.id);b.authState!=E&&(e.authState=b.authState);a.A[c]=e}else a.A[c]={id:b};var d=a.getCustomerIDs(),e=a.a(G),f="";e||(e=0);for(c in d)Object.prototype[c]||(b=d[c],f+=(f?"|":"")+c+"|"+(b.id?b.id:"")+(b.authState?b.authState:""));a.C=a.ia(f);a.C!=e&&(a.fa=j,a.$a())}};a.getCustomerIDs=
        function(){a.f();var d={},c,b;for(c in a.A)Object.prototype[c]||(b=a.A[c],d[c]||(d[c]={}),b.id&&(d[c].id=b.id),d[c].authState=b.authState!=E?b.authState:l.AuthState.UNKNOWN);return d};a._setAnalyticsFields=function(d){a.f();a.k(D,d)};a.setAnalyticsVisitorID=function(d){a._setAnalyticsFields(d)};a.getAnalyticsVisitorID=function(d,c,b){if(!m.o()&&!b)return a.z(d,[""]),"";if(a.isAllowed()){var e="";b||(e=a.getMarketingCloudVisitorID(function(){a.getAnalyticsVisitorID(d,j)}));if(e||b){var f=b?a.marketingCloudServer:
        a.trackingServer,g="";a.loadSSL&&(b?a.marketingCloudServerSecure&&(f=a.marketingCloudServerSecure):a.trackingServerSecure&&(f=a.trackingServerSecure));var h={};if(f){var f="http"+(a.loadSSL?"s":"")+"://"+f+"/id",e="d_visid_ver="+a.version+"&mcorgid="+encodeURIComponent(a.marketingCloudOrgID)+(e?"&mid="+encodeURIComponent(e):"")+(a.idSyncDisable3rdPartySyncing?"&d_coppa=true":""),i=["s_c_il",a._in,"_set"+(b?"MarketingCloud":"Analytics")+"Fields"],g=f+"?"+e+"&callback=s_c_il%5B"+a._in+"%5D._set"+(b?
        "MarketingCloud":"Analytics")+"Fields";h.m=f+"?"+e;h.qa=i}h.url=g;return a.u(b?o:r,g,d,c,h)}}return""};a._setAudienceManagerFields=function(d){a.f();a.k(C,d)};a.B=function(d){var c=a.audienceManagerServer,b="",e=a.a(o),f=a.a(t,j),g=a.a(r),g=g&&g!=u?"&d_cid_ic=AVID%01"+encodeURIComponent(g):"";a.loadSSL&&a.audienceManagerServerSecure&&(c=a.audienceManagerServerSecure);if(c){var b=a.getCustomerIDs(),h,i;if(b)for(h in b)Object.prototype[h]||(i=b[h],g+="&d_cid_ic="+encodeURIComponent(h)+"%01"+encodeURIComponent(i.id?
        i.id:"")+(i.authState?"%01"+i.authState:""));d||(d="_setAudienceManagerFields");c="http"+(a.loadSSL?"s":"")+"://"+c+"/id";e="d_visid_ver="+a.version+"&d_rtbd=json&d_ver=2"+(!e&&a.D?"&d_verify=1":"")+"&d_orgid="+encodeURIComponent(a.marketingCloudOrgID)+"&d_nsid="+(a.idSyncContainerID||0)+(e?"&d_mid="+encodeURIComponent(e):"")+(a.idSyncDisable3rdPartySyncing?"&d_coppa=true":"")+(f?"&d_blob="+encodeURIComponent(f):"")+g;f=["s_c_il",a._in,d];b=c+"?"+e+"&d_cb=s_c_il%5B"+a._in+"%5D."+d;return{url:b,m:c+
            "?"+e,qa:f}}return{url:b}};a.getAudienceManagerLocationHint=function(d,c){if(a.isAllowed()&&a.getMarketingCloudVisitorID(function(){a.getAudienceManagerLocationHint(d,j)})){var b=a.a(r);!b&&m.o()&&(b=a.getAnalyticsVisitorID(function(){a.getAudienceManagerLocationHint(d,j)}));if(b||!m.o())return b=a.B(),a.u(A,b.url,d,c,b)}return""};a.getLocationHint=a.getAudienceManagerLocationHint;a.getAudienceManagerBlob=function(d,c){if(a.isAllowed()&&a.getMarketingCloudVisitorID(function(){a.getAudienceManagerBlob(d,
        j)})){var b=a.a(r);!b&&m.o()&&(b=a.getAnalyticsVisitorID(function(){a.getAudienceManagerBlob(d,j)}));if(b||!m.o()){var b=a.B(),e=b.url;a.fa&&a.j(t,-1);return a.u(t,e,d,c,b)}}return""};a._supplementalDataIDCurrent="";a._supplementalDataIDCurrentConsumed={};a._supplementalDataIDLast="";a._supplementalDataIDLastConsumed={};a.getSupplementalDataID=function(d,c){!a._supplementalDataIDCurrent&&!c&&(a._supplementalDataIDCurrent=a.t(1));var b=a._supplementalDataIDCurrent;a._supplementalDataIDLast&&!a._supplementalDataIDLastConsumed[d]?
        (b=a._supplementalDataIDLast,a._supplementalDataIDLastConsumed[d]=j):b&&(a._supplementalDataIDCurrentConsumed[d]&&(a._supplementalDataIDLast=a._supplementalDataIDCurrent,a._supplementalDataIDLastConsumed=a._supplementalDataIDCurrentConsumed,a._supplementalDataIDCurrent=b=!c?a.t(1):"",a._supplementalDataIDCurrentConsumed={}),b&&(a._supplementalDataIDCurrentConsumed[d]=j));return b};l.OptOut={GLOBAL:"global"};a.getOptOut=function(d,c){if(a.isAllowed()){var b=a.B("_setMarketingCloudFields");return a.u(B,
        b.url,d,c,b)}return""};a.isOptedOut=function(d,c,b){return a.isAllowed()?(c||(c=l.OptOut.GLOBAL),(b=a.getOptOut(function(b){a.z(d,[b==l.OptOut.GLOBAL||0<=b.indexOf(c)])},b))?b==l.OptOut.GLOBAL||0<=b.indexOf(c):i):k};a.appendVisitorIDsTo=function(d){var c=p.aa,b=y([[o,a.a(o)],[r,a.a(r)],[I,a.marketingCloudOrgID]]);try{return a.w(d,c,b)}catch(e){return d}};var p={r:!!n.postMessage,Pa:1,ca:864E5,aa:"adobe_mc",v:/^[0-9a-fA-F\-]+$/,Oa:5,Ra:/^\d+$/,da:/vVersion\|((\d+\.)?(\d+\.)?(\*|\d+))(?=$|\|)/};a.Mb=
        p;a.pa={postMessage:function(a,c,b){var e=1;c&&(p.r?b.postMessage(a,c.replace(/([^:]+:\/\/[^\/]+).*/,"$1")):c&&(b.location=c.replace(/#.*$/,"")+"#"+ +new Date+e++ +"&"+a))},W:function(a,c){var b;try{if(p.r)if(a&&(b=function(b){if("string"===typeof c&&b.origin!==c||"[object Function]"===Object.prototype.toString.call(c)&&!1===c(b.origin))return!1;a(b)}),window.addEventListener)window[a?"addEventListener":"removeEventListener"]("message",b,!1);else window[a?"attachEvent":"detachEvent"]("onmessage",
            b)}catch(e){}}};var m={N:function(){if(v.addEventListener)return function(a,c,b){a.addEventListener(c,function(a){"function"===typeof b&&b(a)},k)};if(v.attachEvent)return function(a,c,b){a.attachEvent("on"+c,function(a){"function"===typeof b&&b(a)})}}(),map:function(a,c){if(Array.prototype.map)return a.map(c);if(void 0===a||a===i)throw new TypeError;var b=Object(a),e=b.length>>>0;if("function"!==typeof c)throw new TypeError;for(var f=Array(e),g=0;g<e;g++)g in b&&(f[g]=c.call(c,b[g],g,b));return f},
        xa:function(a,c){return this.map(a,function(a){return encodeURIComponent(a)}).join(c)},Bb:function(a){var c=a.indexOf("#");return 0<c?a.substr(c):""},sb:function(a){var c=a.indexOf("#");return 0<c?a.substr(0,c):a},fb:function(a,c,b){a=a.split("&");b=b!=i?b:a.length;a.splice(b,0,c);return a.join("&")},ub:function(d,c,b){if(d!==r)return k;c||(c=a.trackingServer);b||(b=a.trackingServerSecure);d=a.loadSSL?b:c;return"string"===typeof d&&d.length?0>d.indexOf("2o7.net")&&0>d.indexOf("omtrdc.net"):k},Ea:function(a){return Boolean(a&&
            a===Object(a))},vb:function(d,c){return 0>a._compareVersions(d,c)},gb:function(d,c){return 0!==a._compareVersions(d,c)},Hb:function(a){document.cookie=encodeURIComponent(a)+"=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;"},o:function(){return!!a.trackingServer||!!a.trackingServerSecure},Cb:function(a,c){function b(a,d){var e,i,j=a[d];if(j&&"object"===typeof j)for(e in j)Object.prototype.hasOwnProperty.call(j,e)&&(i=b(j,e),void 0!==i?j[e]=i:delete j[e]);return c.call(a,d,j)}if("object"===typeof JSON&&
            "function"===typeof JSON.parse)return JSON.parse(a,c);var e;e=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;a=""+a;e.lastIndex=0;e.test(a)&&(a=a.replace(e,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return e=eval("("+
            a+")"),"function"===typeof c?b({"":e},""):e;throw new SyntaxError("JSON.parse");},Ba:function(){return Math.round((new Date).getTime()/1E3)}};a.Sb=m;a.na={F:function(){var a="none",c=j;"undefined"!==typeof XMLHttpRequest&&XMLHttpRequest===Object(XMLHttpRequest)&&("withCredentials"in new XMLHttpRequest?a="XMLHttpRequest":"undefined"!==typeof XDomainRequest&&XDomainRequest===Object(XDomainRequest)&&(c=k),0<Object.prototype.toString.call(window.Jb).indexOf("Constructor")&&(c=k));return{G:a,Vb:c}}(),
        pb:function(){return"none"===this.F.G?i:new window[this.F.G]},ob:function(d,c,b){var e=this;c&&(d.T=c);try{var f=this.pb();f.open("get",d.m+"&ts="+(new Date).getTime(),j);"XMLHttpRequest"===this.F.G&&(f.withCredentials=j,f.timeout=a.loadTimeout,f.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),f.onreadystatechange=function(){if(4===this.readyState&&200===this.status)a:{var a;try{if(a=JSON.parse(this.responseText),a!==Object(a)){e.n(d,i,"Response is not JSON");break a}}catch(b){e.n(d,
            b,"Error parsing response as JSON");break a}try{for(var c=d.qa,f=window,g=0;g<c.length;g++)f=f[c[g]];f(a)}catch(j){e.n(d,j,"Error forming callback function")}}});f.onerror=function(a){e.n(d,a,"onerror")};f.ontimeout=function(a){e.n(d,a,"ontimeout")};f.send();q.d[b]={requestStart:q.p(),url:d.m,va:f.timeout,ta:q.Aa(),ua:1};a.la.Ja.push(d.m)}catch(g){this.n(d,g,"try-catch")}},n:function(d,c,b){a.CORSErrors.push({Wb:d,error:c,description:b});d.T&&("ontimeout"===b?d.T(j):d.T(k))}};var z={Sa:3E4,ba:649,
        Na:k,id:i,V:[],R:i,za:function(a){if("string"===typeof a)return a=a.split("/"),a[0]+"//"+a[2]},g:i,url:i,qb:function(){var d="http://fast.",c="?d_nsid="+a.idSyncContainerID+"#"+encodeURIComponent(v.location.href);this.g||(this.g="nosubdomainreturned");a.loadSSL&&(d=a.idSyncSSLUseAkamai?"https://fast.":"https://");d=d+this.g+".demdex.net/dest5.html"+c;this.R=this.za(d);this.id="destination_publishing_iframe_"+this.g+"_"+a.idSyncContainerID;return d},jb:function(){var d="?d_nsid="+a.idSyncContainerID+
            "#"+encodeURIComponent(v.location.href);"string"===typeof a.K&&a.K.length&&(this.id="destination_publishing_iframe_"+(new Date).getTime()+"_"+a.idSyncContainerID,this.R=this.za(a.K),this.url=a.K+d)},Ca:i,wa:k,Y:k,H:i,kc:i,Ab:i,lc:i,X:k,I:[],yb:[],zb:[],Fa:p.r?15:100,S:[],wb:[],ra:j,Ia:k,Ha:function(){return!a.idSyncDisable3rdPartySyncing&&(this.wa||a.Ob)&&this.g&&"nosubdomainreturned"!==this.g&&this.url&&!this.Y},P:function(){function a(){e=document.createElement("iframe");e.sandbox="allow-scripts allow-same-origin";
            e.title="Adobe ID Syncing iFrame";e.id=b.id;e.style.cssText="display: none; width: 0; height: 0;";e.src=b.url;b.Ab=j;c();document.body.appendChild(e)}function c(){m.N(e,"load",function(){e.className="aamIframeLoaded";b.H=j;b.q()})}this.Y=j;var b=this,e=document.getElementById(this.id);e?"IFRAME"!==e.nodeName?(this.id+="_2",a()):"aamIframeLoaded"!==e.className?c():(this.H=j,this.Da=e,this.q()):a();this.Da=e},q:function(d){var c=this;d===Object(d)&&(this.S.push(d),this.Eb(d));if((this.Ia||!p.r||this.H)&&
            this.S.length)this.J(this.S.shift()),this.q();!a.idSyncDisableSyncs&&this.H&&this.I.length&&!this.X&&(this.Na||(this.Na=j,setTimeout(function(){c.Fa=p.r?15:150},this.Sa)),this.X=j,this.Ka())},Eb:function(a){var c,b,e;if((c=a.ibs)&&c instanceof Array&&(b=c.length))for(a=0;a<b;a++)e=c[a],e.syncOnPage&&this.sa(e,"","syncOnPage")},J:function(a){var c=encodeURIComponent,b,e,f,g,h;if((b=a.ibs)&&b instanceof Array&&(e=b.length))for(f=0;f<e;f++)g=b[f],h=[c("ibs"),c(g.id||""),c(g.tag||""),m.xa(g.url||[],","),
            c(g.ttl||""),"","",g.fireURLSync?"true":"false"],g.syncOnPage||(this.ra?this.O(h.join("|")):g.fireURLSync&&this.sa(g,h.join("|")));this.wb.push(a)},sa:function(d,c,b){var e=(b="syncOnPage"===b?j:k)?J:L;a.f();var f=a.a(e),g=k,h=k,i=Math.ceil((new Date).getTime()/p.ca);f?(f=f.split("*"),h=this.Fb(f,d.id,i),g=h.mb,h=h.nb,(!g||!h)&&this.ya(b,d,c,f,e,i)):(f=[],this.ya(b,d,c,f,e,i))},Fb:function(a,c,b){var e=k,f=k,g,h,i;for(h=0;h<a.length;h++)g=a[h],i=parseInt(g.split("-")[1],10),g.match("^"+c+"-")?(e=
            j,b<i?f=j:(a.splice(h,1),h--)):b>=i&&(a.splice(h,1),h--);return{mb:e,nb:f}},xb:function(a){if(a.join("*").length>this.ba)for(a.sort(function(a,b){return parseInt(a.split("-")[1],10)-parseInt(b.split("-")[1],10)});a.join("*").length>this.ba;)a.shift()},ya:function(d,c,b,e,f,g){var h=this;if(d){if("img"===c.tag){var d=c.url,b=a.loadSSL?"https:":"http:",j,k,l;for(e=0,j=d.length;e<j;e++){k=d[e];l=/^\/\//.test(k);var n=new Image;m.N(n,"load",function(b,d,c,e){return function(){h.V[b]=i;a.f();var g=a.a(f),
            j=[];if(g){var g=g.split("*"),k,l,m;for(k=0,l=g.length;k<l;k++)m=g[k],m.match("^"+d.id+"-")||j.push(m)}h.Ma(j,d,c,e)}}(this.V.length,c,f,g));n.src=(l?b:"")+k;this.V.push(n)}}}else this.O(b),this.Ma(e,c,f,g)},O:function(d){var c=encodeURIComponent;this.I.push((a.Pb?c("---destpub-debug---"):c("---destpub---"))+d)},Ma:function(d,c,b,e){d.push(c.id+"-"+(e+Math.ceil(c.ttl/60/24)));this.xb(d);a.e(b,d.join("*"))},Ka:function(){var d=this,c;this.I.length?(c=this.I.shift(),a.pa.postMessage(c,this.url,this.Da.contentWindow),
            this.yb.push(c),setTimeout(function(){d.Ka()},this.Fa)):this.X=k},W:function(a){var c=/^---destpub-to-parent---/;"string"===typeof a&&c.test(a)&&(c=a.replace(c,"").split("|"),"canSetThirdPartyCookies"===c[0]&&(this.ra="true"===c[1]?j:k,this.Ia=j,this.q()),this.zb.push(a))},Db:function(d){if(this.url===i||d.subdomain&&"nosubdomainreturned"===this.g)this.g="string"===typeof a.oa&&a.oa.length?a.oa:d.subdomain||"",this.url=this.qb();d.ibs instanceof Array&&d.ibs.length&&(this.wa=j);this.Ha()&&(a.idSyncAttachIframeOnWindowLoad?
            (l.$||"complete"===v.readyState||"loaded"===v.readyState)&&this.P():this.hb());"function"===typeof a.idSyncIDCallResult?a.idSyncIDCallResult(d):this.q(d);"function"===typeof a.idSyncAfterIDCallResult&&a.idSyncAfterIDCallResult(d)},ib:function(d,c){return a.Qb||!d||c-d>p.Pa},hb:function(){function a(){c.Y||(document.body?c.P():setTimeout(a,30))}var c=this;a()}};a.Nb=z;a.timeoutMetricsLog=[];var q={lb:window.performance&&window.performance.timing?1:0,Ga:window.performance&&window.performance.timing?
            window.performance.timing:i,Z:i,Q:i,d:{},U:[],send:function(d){if(a.takeTimeoutMetrics&&d===Object(d)){var c=[],b=encodeURIComponent,e;for(e in d)d.hasOwnProperty(e)&&c.push(b(e)+"="+b(d[e]));d="http"+(a.loadSSL?"s":"")+"://dpm.demdex.net/event?d_visid_ver="+a.version+"&d_visid_stg_timeout="+a.loadTimeout+"&"+c.join("&")+"&d_orgid="+b(a.marketingCloudOrgID)+"&d_timingapi="+this.lb+"&d_winload="+this.rb()+"&d_ld="+this.p();(new Image).src=d;a.timeoutMetricsLog.push(d)}},rb:function(){this.Q===i&&(this.Q=
            this.Ga?this.Z-this.Ga.navigationStart:this.Z-l.kb);return this.Q},p:function(){return(new Date).getTime()},J:function(a){var c=this.d[a],b={};b.d_visid_stg_timeout_captured=c.va;b.d_visid_cors=c.ua;b.d_fieldgroup=a;b.d_settimeout_overriden=c.ta;c.timeout?c.tb?(b.d_visid_timedout=1,b.d_visid_timeout=c.timeout-c.requestStart,b.d_visid_response=-1):(b.d_visid_timedout="n/a",b.d_visid_timeout="n/a",b.d_visid_response="n/a"):(b.d_visid_timedout=0,b.d_visid_timeout=-1,b.d_visid_response=c.Ib-c.requestStart);
            b.d_visid_url=c.url;l.$?this.send(b):this.U.push(b);delete this.d[a]},Gb:function(){for(var a=0,c=this.U.length;a<c;a++)this.send(this.U[a])},Aa:function(){return"function"===typeof setTimeout.toString?-1<setTimeout.toString().indexOf("[native code]")?0:1:-1}};a.Ub=q;var x={isClientSideMarketingCloudVisitorID:i,MCIDCallTimedOut:i,AnalyticsIDCallTimedOut:i,AAMIDCallTimedOut:i,d:{},La:function(a,c){switch(a){case F:c===k?this.MCIDCallTimedOut!==j&&(this.MCIDCallTimedOut=k):this.MCIDCallTimedOut=c;break;
            case D:c===k?this.AnalyticsIDCallTimedOut!==j&&(this.AnalyticsIDCallTimedOut=k):this.AnalyticsIDCallTimedOut=c;break;case C:c===k?this.AAMIDCallTimedOut!==j&&(this.AAMIDCallTimedOut=k):this.AAMIDCallTimedOut=c}}};a.isClientSideMarketingCloudVisitorID=function(){return x.isClientSideMarketingCloudVisitorID};a.MCIDCallTimedOut=function(){return x.MCIDCallTimedOut};a.AnalyticsIDCallTimedOut=function(){return x.AnalyticsIDCallTimedOut};a.AAMIDCallTimedOut=function(){return x.AAMIDCallTimedOut};a.idSyncGetOnPageSyncInfo=
        function(){a.f();return a.a(J)};a.idSyncByURL=function(d){var c,b=d||{};c=b.minutesToLive;var e="";a.idSyncDisableSyncs&&(e=e?e:"Error: id syncs have been disabled");if("string"!==typeof b.dpid||!b.dpid.length)e=e?e:"Error: config.dpid is empty";if("string"!==typeof b.url||!b.url.length)e=e?e:"Error: config.url is empty";if("undefined"===typeof c)c=20160;else if(c=parseInt(c,10),isNaN(c)||0>=c)e=e?e:"Error: config.minutesToLive needs to be a positive number";c={error:e,mc:c};if(c.error)return c.error;
        var e=d.url,f=encodeURIComponent,b=z,g,e=e.replace(/^https:/,"").replace(/^http:/,"");g=m.xa(["",d.dpid,d.dpuuid||""],",");d=["ibs",f(d.dpid),"img",f(e),c.ttl,"",g];b.O(d.join("|"));b.q();return"Successfully queued"};a.idSyncByDataSource=function(d){if(d!==Object(d)||"string"!==typeof d.dpuuid||!d.dpuuid.length)return"Error: config or config.dpuuid is empty";d.url="//dpm.demdex.net/ibs:dpid="+d.dpid+"&dpuuid="+d.dpuuid;return a.idSyncByURL(d)};a._compareVersions=function(a,c){if(a===c)return 0;var b=
        a.toString().split("."),e=c.toString().split("."),f;a:{f=b.concat(e);for(var g=0,h=f.length;g<h;g++)if(!p.Ra.test(f[g])){f=k;break a}f=j}if(!f)return NaN;for(;b.length<e.length;)b.push("0");for(;e.length<b.length;)e.push("0");a:{for(f=0;f<b.length;f++){g=parseInt(b[f],10);h=parseInt(e[f],10);if(g>h){b=1;break a}if(h>g){b=-1;break a}}b=0}return b};a._getCookieVersion=function(d){d=d||a.cookieRead(a.cookieName);return(d=p.da.exec(d))&&1<d.length?d[1]:null};a._resetAmcvCookie=function(d){var c=a._getCookieVersion();
        (!c||m.vb(c,d))&&m.Hb(a.cookieName)};0>s.indexOf("@")&&(s+="@AdobeOrg");a.marketingCloudOrgID=s;a.cookieName="AMCV_"+s;a.sessionCookieName="AMCVS_"+s;a.cookieDomain=a.Wa();a.cookieDomain==n.location.hostname&&(a.cookieDomain="");a.loadSSL=0<=n.location.protocol.toLowerCase().indexOf("https");a.loadTimeout=3E4;a.CORSErrors=[];a.marketingCloudServer=a.audienceManagerServer="dpm.demdex.net";var M={};M[A]=j;M[t]=j;if(w&&"object"==typeof w){for(var H in w)!Object.prototype[H]&&(a[H]=w[H]);a.idSyncContainerID=
        a.idSyncContainerID||0;a.resetBeforeVersion&&a._resetAmcvCookie(a.resetBeforeVersion);a.ea();a.f();H=a.a(K);var N=Math.ceil((new Date).getTime()/p.ca);!a.idSyncDisableSyncs&&z.ib(H,N)&&(a.j(t,-1),a.e(K,N));a.getMarketingCloudVisitorID();a.getAudienceManagerLocationHint();a.getAudienceManagerBlob();a.ab(a.serverState)}else a.ea();if(!a.idSyncDisableSyncs){z.jb();m.N(window,"load",function(){l.$=j;q.Z=q.p();q.Gb();var a=z;a.Ha()&&a.P()});try{a.pa.W(function(a){z.W(a.data)},z.R)}catch(O){}}}
Visitor.getInstance=function(s,w){var y,a=window.s_c_il,n;0>s.indexOf("@")&&(s+="@AdobeOrg");if(a)for(n=0;n<a.length;n++)if((y=a[n])&&"Visitor"==y._c&&y.marketingCloudOrgID==s)return y;return new Visitor(s,w)};(function(){function s(){w.$=y}var w=window.Visitor,y=w.Ta,a=w.Qa;y||(y=!0);a||(a=!1);window.addEventListener?window.addEventListener("load",s):window.attachEvent&&window.attachEvent("onload",s);w.kb=(new Date).getTime()})();
//  Visitor.getInstance after the code library
var visitor = Visitor.getInstance("926602BE533057650A490D4C@AdobeOrg", {
    trackingServer: "metrics.consumerreports.org",
    trackingServerSecure: "smetrics.consumerreports.org",
    marketingCloudServer: "metrics.consumerreports.org", // CNAME support
    marketingCloudServerSecure: "smetrics.consumerreports.org"
});
// start Omniture
if(typeof(s_account) === "undefined"){
    s_account = "cuglobal";
    if(/(file:\/\/\/|localhost|\.crinfra\.net|\.consumer\.org)/.test(window.location.href)){s_account = "cuglobaldev";}
    else if(/(stage2\.consumerreports\.org|stage\.consumerreports\.org)/.test(window.location.href)){s_account = "cuglobalqa";}
}
var s=s_gi(s_account);
/******** VISITOR ID SERVICE CONFIG - REQUIRES VisitorAPI.js ********/
s.visitor=Visitor.getInstance("926602BE533057650A490D4C@AdobeOrg");
/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here. */
s_getLoadTime();
s.trackDownloadLinks=true;
s.trackExternalLinks=true;
s.trackInlineStats=true;
s.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
s.linkInternalFilters="javascript:,consumerreports.org,consumerreportshealth.org,consumer.org,crinfra.net,jobs-consumers.icims.com,shopsmartmag.org";
s.linkLeaveQueryString=false;
s.linkTrackVars="None";
s.linkTrackEvents="None";
s.charSet = "utf-8";
s.currencyCode="USD";
/* enable video tracking */
s.enableVideoTracking = true;
/* initialize plugins */
s.usePlugins=true;
function s_doPlugins(s) {
    /* campaigns */
    if (!s.campaign) {s.campaign = s.Util.getQueryParam("EXTKEY");}
    s.campaign = s.getValOnce(s.campaign,"s_extkey",0);
    if (s.campaign && s.Util.getQueryParam('ref')) {s.referrer = s.Util.getQueryParam('ref');}
    if (!s.eVar19) {s.eVar19 = s.Util.getQueryParam('mkwid');}
    if (!s.eVar20) {s.eVar20 = s.Util.getQueryParam('pcrid');}
    /* Campaign Stacking */
    s.eVar14 = s.crossVisitParticipation(s.campaign, 's_ev14', '30', '5', '>', '', 0);
    if (!s.eVar1) {s.eVar1 = s.Util.getQueryParam("INTKEY");}
    /* standard variables */
    s.events = s.apl(s.events,"event1",",","1");
    s.server = window.location.hostname;
    s.prop13 = s.eVar13 = s.getNewRepeat(90);
    s.prop29 = document.location.href.split("?")[0];
    s.eVar73 = "D=c29";
    s.eVar35 = "D=g"; // full URL
    /* time parting */
    s.prop25 = s.eVar11 = s.getTimeParting('h', '-5'); // Hour
    s.prop26 = s.getTimeParting('d', '-5'); // Day
    s.eVar12 = "D=c26";
    s.prop43 = s.getPreviousValue(s.pageName, 's_ppn'); // prev page name
    s.prop44 = s.getPercentPageViewed(); // page % viewed
    if (s.Util.cookieRead("userInfo") !== "") s.prop47 = new RegExp("ID=([^&]*)|$").exec(s.Util.cookieRead("userInfo"))[1]; //ecq id
    if (s.Util.cookieRead("userInfo") !== "") s.prop15 = new RegExp("erightsID=([^&]*)|$").exec(s.Util.cookieRead("userInfo"))[1]; //erights id
    if (!s.prop43 || s.prop43 == 'no value') {s.prop44 = '';}
    /* previous template type */
    if(s.linkType!=0 && s.linkType!='o') {s.eVar84 = s.getPreviousValue(s.prop72, 's_ptmpl','event1');}
    /* visit frequency */
    s.eVar81 = s.getDaysSinceLastVisit('s_dslast');
    s.eVar82 = s.getVisitNum('m');
    s.prop50 = "cro|JS-2.0.0|VISID-2.0.0|2018.08.08";
    (typeof(Visitor) != "undefined") ? s.prop73 = "visid" : s.prop73 = "visid missing";
    (typeof(visitor) != "undefined") ? s.prop70 =s.eVar77 = "D=mid" : s.prop70 = s.eVar77 = "mcid unknown";
    s.prop75 = s.eVar75 = s_getLoadTime(); // page load time
    s.events = s.apl(s.events,"event45="+s_getLoadTime(),",",1);
    /* copies of props */
    if(s.channel && !s.eVar15) {s.eVar15 = s.channel;}
    if(s.pageName) {s.eVar21 = "D=pageName";}
    if(s.prop1) {s.eVar40 = s.prop1;}
    if(s.prop2) {s.eVar41 = s.prop2;}
    if(s.prop3) {s.eVar42 = s.prop3;}
    if(s.prop4) {s.eVar43 = "D=c4";}
    if(s.prop5) {s.eVar44 = "D=c5";}
    if(s.prop9 && !s.eVar16) {s.eVar16 = s.prop9;}
    if(s.prop15 && !s.eVar8) {s.eVar8 = s.prop15;}
    if(s.prop16 && !s.eVar9) {s.eVar9 = s.prop16;}
    if(s.prop17) {s.eVar10 = "D=c17"}
    if(s.prop35 && !s.list1) {s.list1 = "D=c35";}// taxonomy tags
    if(s.prop35) {s.eVar33 = "D=c35";}
    if(s.prop38) {s.eVar38 = "D=c38";}
    if(s.prop39) {s.eVar39 = "D=c39";}
    if(s.prop40) {s.eVar30 = "D=c40";} // publish date
    if(s.prop41) {s.eVar29 = "D=c41";} // author
    if(s.prop47 && !s.eVar83) {s.eVar83 = s.prop47;}
    if(s.prop48 && !s.eVar48) {s.eVar48 = s.prop48;}
    if(s.prop64) {s.eVar64 = "D=c64";}
    if(s.prop72 && !s.eVar72) {s.eVar72 = s.prop72;}
    if(s.prop73) {s.eVar78 = "D=c73";}
    /* internal search*/
    if (s.prop67) { s.prop67 = s.eVar67 = s.prop67.toLowerCase(); }
    s.tempSearchToken = s.Util.getQueryParam('searchToken');
    if (s.tempSearchToken) {
        s.eVar67 = s.prop67 = s.tempSearchToken.toLowerCase() + '-redirect';
    }
    if (s.prop10) {
        s.prop10 = s.eVar2 = s.prop10.toLowerCase();
        var t_search = s.getValOnce(s.eVar2, 'ev2', 0);
        if (t_search) {s.events = s.apl(s.events, "event2", ",", 2);}
        s.eVar22 = "+1";
    }
    var s_temp1 = s.getPreviousValue(s.pageName, 'gpv_tmp1', ''); //search origination page
    if (s.events && s.events.indexOf('event2') != -1) {s.prop11 = s_temp1;}
    /* website version */
    if ((typeof(jQuery)!="undefined") && !s.prop58){
        s.prop58=s.eVar58="Mobile";
        if (jQuery(window).width() > 1199) { s.prop58=s.eVar58="Desktop";}
        else if(jQuery(window).width() > 767) { s.prop58=s.eVar58="Tablet";}
    }
    /* Date Stamp / Time Stamp */
    s.prop19=s_getDateStamp();
    if(s.prop19){s.eVar24 = "D=c19";}
    s.prop60=s_getTimeStamp();
    if(s.prop60){s.eVar60 = "D=c60";}
    /* Capture Purchase ID into eVar */
    if (s.purchaseID) {s.eVar17 = s.purchaseID;}
    /* Product View (custom) Event */
    if (s.events && s.events.indexOf('prodView') != -1) {
        s.events = s.apl(s.events, "event15", ",", 2);
        s.eVar23 = "+1"; //counter
    }
    /* Device Name and Resolution */
    if (navigator.userAgent.indexOf('iPhone') > -1) { s.eVar25 = screen.width + "x" + screen.height;}
    /* Device Orientation */
    var getOrientation = function() {
        if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) ||
            navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
            return window.innerWidth > window.innerHeight ? 'Landscape' : 'Portrait';
        } else {return null;}
    };
    s.eVar4 = s.prop42 = getOrientation();
    window.onresize = function() {s.eVar4 = s.prop42 = getOrientation(); };
    // Optimizely Adobe Analytics - Optimizely X integration
    if (window.optimizely && typeof window.optimizely.get === 'function' && window.optimizely.get("custom/adobeIntegrator")) { window.optimizely.get("custom/adobeIntegrator").assignCampaigns(s);}
}
s.doPlugins=s_doPlugins;
/*********Media Module Calls**************/
s.loadModule("Media");
s.Media.autoTrack = false;
s.Media.trackVars = "None";
s.Media.trackEvents = "None";
if (s.enableVideoTracking) {
    s.Media.autoTrack = false;
    s.Media.trackWhilePlaying = true;
    s.Media.trackVars = "events,eVar51,eVar52,eVar53,eVar54,eVar55,prop51,prop52,prop53,prop54";
    s.Media.trackEvents = "event51,event52,event53,event54,event55,event56,event57";
    s.Media.trackMilestones = "25,50,75";
    s.Media.segmentByMilestones = true;
    s.Media.trackUsingContextData = true;
    s.Media.contextDataMapping = {
        "a.media.name": "eVar51,prop51",
        "a.media.segment": "eVar52",
        "a.media.timePlayed": "event52",
        "a.media.view": "event51",
        "a.media.segmentView": "event53",
        "a.media.complete": "event54",
        "a.media.milestones": {
            25: "event55",
            50: "event56",
            75: "event57"
        }
    };
}
;
/********* Adds Additional Variables to Omniture Media Calls **************/
s.Media.monitor = function (s, media) {
    function sendRequest() {
        s.Media.track(media.name);
    }
    if (media.event == "OPEN") {
        s.events = '';
        window.omnVidPlaying = true;
        s.prop52 = location.href;
        s.prop53 = s.eVar53 = s.pageName;
        s.prop54 = s.eVar54 = s.eVar16;
        sendRequest();
    }
    if (media.event == "MILESTONE") {
        window.omnVidPlaying = true;
        s.prop52 = location.href;
        s.eVar53 = s.pageName;
        s.eVar54 = s.eVar16;
        sendRequest();
    }
    if (media.event == "CLOSE" && window.omnVidPlaying) {
        window.omnVidPlaying = false;
        s.prop52 = location.href;
        s.eVar53 = s.pageName;
        s.eVar54 = s.eVar16;
        sendRequest();
    }
};
// Optimizely Adobe Analytics - Legacy Integration
window.optimizely = window.optimizely || [];
window.optimizely.push("activateSiteCatalyst");
/* WARNING: Changing any of the below variables will cause drastic
changes to how your visitor data is collected.  Changes should only be
made when instructed to do so by your account manager.*/
s.trackingServer="metrics.consumerreports.org";
s.trackingServerSecure="smetrics.consumerreports.org";
s.visitorNamespace = "consumersunion";
/************************** PLUGINS SECTION *************************/
/* TimeStamp: YYYY:MM:DD:HH:MM:SS */
function s_getTimeStamp() {
    var now = new Date(),
        now_m = now.getMonth() + 1,
        now_d = now.getDate(),
        now_h = now.getHours(),
        now_min = now.getMinutes(),
        now_s = now.getSeconds();
    if (new String(now_m).length == 1) {now_m = "0" + now_m;}
    if (new String(now_d).length == 1) {now_d = "0" + now_d;}
    if (new String(now_h).length == 1) {now_h = "0" + now_h;}
    if (new String(now_s).length == 1) {now_s = "0" + now_s;}
    if (new String(now_min).length == 1) {now_min = "0" + now_min;}
    var fullTs = now.getFullYear() + ":" + now_m + ":" + now_d + ":" + now_h + ":" + now_min + ":" + now_s;
    return fullTs;
}
/* DateStamp: YYYY:M:D */
function s_getDateStamp() {
    var now = new Date(),
        now_m = now.getMonth() + 1,
        now_d = now.getDate();
    var fullDs = now.getFullYear() + ":" + now_m + ":" + now_d;
    return fullDs;
}
function s_getLoadTime(){
    if(!window.s_loadT){
        var b=new Date().getTime(),o=window.performance?performance.timing:0,a=o?o.requestStart:window.inHeadTS||0;
        s_loadT=a?Math.round((b-a)/100):''
    }
    return s_loadT;
}
/* Plugin: crossVisitParticipation v1.7 - stacks values from specified variable in cookie and returns value */
s.crossVisitParticipation = new Function("v", "cn", "ex", "ct", "dl", "ev", "dv", "" +
    "var s=this,ce;if(typeof(dv)==='undefined')dv=0;if(s.events&&ev){var" +
    " ay=s.split(ev,',');var ea=s.split(s.events,',');for(var u=0;u<ay.l" +
    "ength;u++){for(var x=0;x<ea.length;x++){if(ay[u]==ea[x]){ce=1;}}}}i" +
    "f(!v||v==''){if(ce){s.c_w(cn,'');return'';}else return'';}v=escape(" +
    "v);var arry=new Array(),a=new Array(),c=s.c_r(cn),g=0,h=new Array()" +
    ";if(c&&c!=''){arry=s.split(c,'],[');for(q=0;q<arry.length;q++){z=ar" +
    "ry[q];z=s.repl(z,'[','');z=s.repl(z,']','');z=s.repl(z,\"'\",'');arry" +
    "[q]=s.split(z,',')}}var e=new Date();e.setFullYear(e.getFullYear()+" +
    "5);if(dv==0&&arry.length>0&&arry[arry.length-1][0]==v)arry[arry.len" +
    "gth-1]=[v,new Date().getTime()];else arry[arry.length]=[v,new Date(" +
    ").getTime()];var start=arry.length-ct<0?0:arry.length-ct;var td=new" +
    " Date();for(var x=start;x<arry.length;x++){var diff=Math.round((td." +
    "getTime()-arry[x][1])/86400000);if(diff<ex){h[g]=unescape(arry[x][0" +
    "]);a[g]=[arry[x][0],arry[x][1]];g++;}}var data=s.join(a,{delim:','," +
    "front:'[',back:']',wrap:\"'\"});s.c_w(cn,data,e);var r=s.join(h,{deli" +
    "m:dl});if(ce)s.c_w(cn,'');return r;");
/* Plugin Utility: Replace v1.0 */
s.repl = new Function("x", "o", "n", "" +
    "var i=x.indexOf(o),l=n.length;while(x&&i>=0){x=x.substring(0,i)+n+x." +
    "substring(i+o.length);i=x.indexOf(o,i+l)}return x");
/* Plugin: getNewRepeat 1.2 - Returns whether user is new or repeat */
s.getNewRepeat = new Function("d", "cn", "" +
    "var s=this,e=new Date(),cval,sval,ct=e.getTime();d=d?d:30;cn=cn?cn:" +
    "'s_nr';e.setTime(ct+d*24*60*60*1000);cval=s.c_r(cn);if(cval.length=" +
    "=0){s.c_w(cn,ct+'-New',e);return'New';}sval=s.split(cval,'-');if(ct" +
    "-sval[0]<30*60*1000&&sval[1]=='New'){s.c_w(cn,ct+'-New',e);return'N" +
    "ew';}else{s.c_w(cn,ct+'-Repeat',e);return'Repeat';}");
/* Plugin: getTimeParting 2.0 */
s.getTimeParting = new Function("t", "z", "y", "l", "" +
    "var s=this,d,A,U,X,Z,W,B,C,D,Y;d=new Date();A=d.getFullYear();Y=U=S" +
    "tring(A);if(s.dstStart&&s.dstEnd){B=s.dstStart;C=s.dstEnd}else{;U=U" +
    ".substring(2,4);X='090801|101407|111306|121104|131003|140902|150801" +
    "|161306|171205|181104|191003';X=s.split(X,'|');for(W=0;W<=10;W++){Z" +
    "=X[W].substring(0,2);if(U==Z){B=X[W].substring(2,4);C=X[W].substrin" +
    "g(4,6)}}if(!B||!C){B='08';C='01'}B='03/'+B+'/'+A;C='11/'+C+'/'+A;}D" +
    "=new Date('1/1/2000');if(D.getDay()!=6||D.getMonth()!=0){return'Dat" +
    "a Not Available'}else{z=z?z:'0';z=parseFloat(z);B=new Date(B);C=new" +
    " Date(C);W=new Date();if(W>B&&W<C&&l!='0'){z=z+1}W=W.getTime()+(W.g" +
    "etTimezoneOffset()*60000);W=new Date(W+(3600000*z));X=['Sunday','Mo" +
    "nday','Tuesday','Wednesday','Thursday','Friday','Saturday'];B=W.get" +
    "Hours();C=W.getMinutes();D=W.getDay();Z=X[D];U='AM';A='Weekday';X='" +
    "00';if(C>30){X='30'}if(B>=12){U='PM';B=B-12};if(B==0){B=12};if(D==6" +
    "||D==0){A='Weekend'}W=B+':'+X+U;if(y&&y!=Y){return'Data Not Availab" +
    "le'}else{if(t){if(t=='h'){return W}if(t=='d'){return Z}if(t=='w'){r" +
    "eturn A}}else{return Z+', '+W}}}");
/* Utility Function: split v1.5 - split a string (JS 1.0 compatible) */
s.split = new Function("l", "d", "" +
    "var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x" +
    "++]=l.substring(0,i);l=l.substring(i+d.length);}return a");
/* Plugin: getValOnce 0.2 - get a value once per session or number of days */
s.getValOnce = new Function("v", "c", "e", "" +
    "var s=this,k=s.c_r(c),a=new Date;e=e?e:0;if(v){a.setTime(a.getTime(" +
    ")+e*86400000);s.c_w(c,v,e?a:0);}return v==k?'':v");
/* Plugin Utility: apl v1.1 */
s.apl = new Function("L", "v", "d", "u", "" +
    "var s=this,m=0;if(!L)L='';if(u){var i,n,a=s.split(L,d);for(i=0;i<a." +
    "length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCas" +
    "e()));}}if(!m)L=L?L+d+v:v;return L");
/* s.join: 1.0 - Joins an array into a string */
s.join = new Function("v", "p", "" +
    "var s = this;var f,b,d,w;if(p){f=p.front?p.front:'';b=p.back?p.back" +
    ":'';d=p.delim?p.delim:'';w=p.wrap?p.wrap:'';}var str='';for(var x=0" +
    ";x<v.length;x++){if(typeof(v[x])=='object' )str+=s.join( v[x],p);el" +
    "se str+=w+v[x]+w;if(x<v.length-1)str+=d;}return f+str+b;");
/* Plugin: getPreviousValue_v1.0 - return previous value of designated variable  */
s.getPreviousValue = new Function("v", "c", "el", "" +
    "var s=this,t=new Date,i,j,r='';t.setTime(t.getTime()+1800000);if(el" +
    "){if(s.events){i=s.split(el,',');j=s.split(s.events,',');for(x in i" +
    "){for(y in j){if(i[x]==j[y]){if(s.c_r(c)) r=s.c_r(c);v?s.c_w(c,v,t)" +
    ":s.c_w(c,'no value',t);return r}}}}}else{if(s.c_r(c)) r=s.c_r(c);v?" +
    "s.c_w(c,v,t):s.c_w(c,'no value',t);return r}");
/* Plugin: getPercentPageViewed v1.71 */
s.getPercentPageViewed = new Function("n", "" +
    "var s=this,W=window,EL=W.addEventListener,AE=W.attachEvent,E=['load" +
    "','unload','scroll','resize','zoom','keyup','mouseup','touchend','o" +
    "rientationchange','pan'];W.s_Obj=s;s_PPVid=(n=='-'?s.pageName:n)||s" +
    ".pageName||location.href;if(!W.s_PPVevent){s.s_PPVg=function(n,r){v" +
    "ar k='s_ppv',p=k+'l',c=s.c_r(n||r?k:p),a=c.indexOf(',')>-1?c.split(" +
    "',',10):[''],l=a.length,i;a[0]=unescape(a[0]);r=r||(n&&n!=a[0])||0;" +
    "a.length=10;if(typeof a[0]!='string')a[0]='';for(i=1;i<10;i++)a[i]=" +
    "!r&&i<l?parseInt(a[i])||0:0;if(l<10||typeof a[9]!='string')a[9]='';" +
    "if(r){s.c_w(p,c);s.c_w(k,'?')}return a};W.s_PPVevent=function(e){va" +
    "r W=window,D=document,B=D.body,E=D.documentElement,S=window.screen|" +
    "|0,Ho='offsetHeight',Hs='scrollHeight',Ts='scrollTop',Wc='clientWid" +
    "th',Hc='clientHeight',C=100,M=Math,J='object',N='number',s=W.s_Obj|" +
    "|W.s||0;e=e&&typeof e==J?e.type||'':'';if(!e.indexOf('on'))e=e.subs" +
    "tring(2);s_PPVi=W.s_PPVi||0;if(W.s_PPVt&&!e){clearTimeout(s_PPVt);s" +
    "_PPVt=0;if(s_PPVi<2)s_PPVi++}if(typeof s==J){var h=M.max(B[Hs]||E[H" +
    "s],B[Ho]||E[Ho],B[Hc]||E[Hc]),X=W.innerWidth||E[Wc]||B[Wc]||0,Y=W.i" +
    "nnerHeight||E[Hc]||B[Hc]||0,x=S?S.width:0,y=S?S.height:0,r=M.round(" +
    "C*(W.devicePixelRatio||1))/C,b=(D.pageYOffset||E[Ts]||B[Ts]||0)+Y,p" +
    "=h>0&&b>0?M.round(C*b/h):0,O=W.orientation,o=!isNaN(O)?M.abs(o)%180" +
    ":Y>X?0:90,L=e=='load'||s_PPVi<1,a=s.s_PPVg(s_PPVid,L),V=function(i," +
    "v,f,n){i=parseInt(typeof a==J&&a.length>i?a[i]:'0')||0;v=typeof v!=" +
    "N?i:v;v=f||v>i?v:i;return n?v:v>C?C:v<0?0:v};if(new RegExp('(iPod|i" +
    "Pad|iPhone)').exec(navigator.userAgent||'')&&o){o=x;x=y;y=o}o=o?'P'" +
    ":'L';a[9]=L?'':a[9].substring(0,1);s.c_w('s_ppv',escape(W.s_PPVid)+" +
    "','+V(1,p,L)+','+(L||!V(2)?p:V(2))+','+V(3,b,L,1)+','+X+','+Y+','+x" +
    "+','+y+','+r+','+a[9]+(a[9]==o?'':o))}if(!W.s_PPVt&&e!='unload')W.s" +
    "_PPVt=setTimeout(W.s_PPVevent,333)};for(var f=W.s_PPVevent,i=0;i<E." +
    "length;i++)if(EL)EL(E[i],f,false);else if(AE)AE('on'+E[i],f);f()};v" +
    "ar a=s.s_PPVg();return!n||n=='-'?a[1]:a");
/* Plugin: getVisitNum - version 3.0 */
s.getVisitNum=new Function("tp","c","c2",""
    +"var s=this,e=new Date,cval,cvisit,ct=e.getTime(),d;if(!tp){tp='m';}"
    +"if(tp=='m'||tp=='w'||tp=='d'){eo=s.endof(tp),y=eo.getTime();e.setTi"
    +"me(y);}else {d=tp*86400000;e.setTime(ct+d);}if(!c){c='s_vnum';}if(!"
    +"c2){c2='s_invisit';}cval=s.c_r(c);if(cval){var i=cval.indexOf('&vn="
    +"'),str=cval.substring(i+4,cval.length),k;}cvisit=s.c_r(c2);if(cvisi"
    +"t){if(str){e.setTime(ct+1800000);s.c_w(c2,'true',e);return str;}els"
    +"e {return 'unknown visit number';}}else {if(str){str++;k=cval.substri"
    +"ng(0,i);e.setTime(k);s.c_w(c,k+'&vn='+str,e);e.setTime(ct+1800000);"
    +"s.c_w(c2,'true',e);return str;}else {s.c_w(c,e.getTime()+'&vn=1',e)"
    +";e.setTime(ct+1800000);s.c_w(c2,'true',e);return 1;}}");
s.dimo=new Function("m","y",""
    +"var d=new Date(y,m+1,0);return d.getDate();");
s.endof=new Function("x",""
    +"var t=new Date;t.setHours(0);t.setMinutes(0);t.setSeconds(0);if(x=="
    +"'m'){d=s.dimo(t.getMonth(),t.getFullYear())-t.getDate()+1;}else if("
    +"x=='w'){d=7-t.getDay();}else {d=1;}t.setDate(t.getDate()+d);return "
    +"t;");
/* Plugin: Days since last Visit 1.1 - capture time from last visit */
s.getDaysSinceLastVisit=new Function("c",""
    +"var s=this,e=new Date(),es=new Date(),cval,cval_s,cval_ss,ct=e.getT"
    +"ime(),day=24*60*60*1000,f1,f2,f3,f4,f5;e.setTime(ct+3*365*day);es.s"
    +"etTime(ct+30*60*1000);f0='Cookies Not Supported';f1='First Visit';f"
    +"2='More than 30 days';f3='More than 7 days';f4='Less than 7 days';f"
    +"5='Less than 1 day';cval=s.c_r(c);if(cval.length==0){s.c_w(c,ct,e);"
    +"s.c_w(c+'_s',f1,es);}else{var d=ct-cval;if(d>30*60*1000){if(d>30*da"
    +"y){s.c_w(c,ct,e);s.c_w(c+'_s',f2,es);}else if(d<30*day+1 && d>7*day"
    +"){s.c_w(c,ct,e);s.c_w(c+'_s',f3,es);}else if(d<7*day+1 && d>day){s."
    +"c_w(c,ct,e);s.c_w(c+'_s',f4,es);}else if(d<day+1){s.c_w(c,ct,e);s.c"
    +"_w(c+'_s',f5,es);}}else{s.c_w(c,ct,e);cval_ss=s.c_r(c+'_s');s.c_w(c"
    +"+'_s',cval_ss,es);}}cval_s=s.c_r(c+'_s');if(cval_s.length==0) retur"
    +"n f0;else if(cval_s!=f1&&cval_s!=f2&&cval_s!=f3&&cval_s!=f4&&cval_s"
    +"!=f5) return '';else return cval_s;");
/****************************** MODULES *****************************/
/* Media Module */
function AppMeasurement_Module_Media(q){var b=this;b.s=q;q=window;q.s_c_in||(q.s_c_il=[],q.s_c_in=0);b._il=q.s_c_il;b._in=q.s_c_in;b._il[b._in]=b;q.s_c_in++;b._c="s_m";b.list=[];b.open=function(d,c,e,k){var f={},a=new Date,l="",g;c||(c=-1);if(d&&e){b.list||(b.list={});b.list[d]&&b.close(d);k&&k.id&&(l=k.id);if(l)for(g in b.list)!Object.prototype[g]&&b.list[g]&&b.list[g].R==l&&b.close(b.list[g].name);f.name=d;f.length=c;f.offset=0;f.e=0;f.playerName=b.playerName?b.playerName:e;f.R=l;f.C=0;f.a=0;f.timestamp=
    Math.floor(a.getTime()/1E3);f.k=0;f.u=f.timestamp;f.c=-1;f.n="";f.g=-1;f.D=0;f.I={};f.G=0;f.m=0;f.f="";f.B=0;f.L=0;f.A=0;f.F=0;f.l=!1;f.v="";f.J="";f.K=0;f.r=!1;f.H="";f.complete=0;f.Q=0;f.p=0;f.q=0;b.list[d]=f}};b.openAd=function(d,c,e,k,f,a,l,g){var h={};b.open(d,c,e,g);if(h=b.list[d])h.l=!0,h.v=k,h.J=f,h.K=a,h.H=l};b.M=function(d){var c=b.list[d];b.list[d]=0;c&&c.monitor&&clearTimeout(c.monitor.interval)};b.close=function(d){b.i(d,0,-1)};b.play=function(d,c,e,k){var f=b.i(d,1,c,e,k);f&&!f.monitor&&
(f.monitor={},f.monitor.update=function(){1==f.k&&b.i(f.name,3,-1);f.monitor.interval=setTimeout(f.monitor.update,1E3)},f.monitor.update())};b.click=function(d,c){b.i(d,7,c)};b.complete=function(d,c){b.i(d,5,c)};b.stop=function(d,c){b.i(d,2,c)};b.track=function(d){b.i(d,4,-1)};b.P=function(d,c){var e="a.media.",k=d.linkTrackVars,f=d.linkTrackEvents,a="m_i",l,g=d.contextData,h;c.l&&(e+="ad.",c.v&&(g["a.media.name"]=c.v,g[e+"pod"]=c.J,g[e+"podPosition"]=c.K),c.G||(g[e+"CPM"]=c.H));c.r&&(g[e+"clicked"]=
    !0,c.r=!1);g["a.contentType"]="video"+(c.l?"Ad":"");g["a.media.channel"]=b.channel;g[e+"name"]=c.name;g[e+"playerName"]=c.playerName;0<c.length&&(g[e+"length"]=c.length);g[e+"timePlayed"]=Math.floor(c.a);0<Math.floor(c.a)&&(g[e+"timePlayed"]=Math.floor(c.a));c.G||(g[e+"view"]=!0,a="m_s",b.Heartbeat&&b.Heartbeat.enabled&&(a=c.l?b.__primetime?"mspa_s":"msa_s":b.__primetime?"msp_s":"ms_s"),c.G=1);c.f&&(g[e+"segmentNum"]=c.m,g[e+"segment"]=c.f,0<c.B&&(g[e+"segmentLength"]=c.B),c.A&&0<c.a&&(g[e+"segmentView"]=
    !0));!c.Q&&c.complete&&(g[e+"complete"]=!0,c.S=1);0<c.p&&(g[e+"milestone"]=c.p);0<c.q&&(g[e+"offsetMilestone"]=c.q);if(k)for(h in g)Object.prototype[h]||(k+=",contextData."+h);l=g["a.contentType"];d.pe=a;d.pev3=l;var q,s;if(b.contextDataMapping)for(h in d.events2||(d.events2=""),k&&(k+=",events"),b.contextDataMapping)if(!Object.prototype[h]){a=h.length>e.length&&h.substring(0,e.length)==e?h.substring(e.length):"";l=b.contextDataMapping[h];if("string"==typeof l)for(q=l.split(","),s=0;s<q.length;s++)l=
    q[s],"a.contentType"==h?(k&&(k+=","+l),d[l]=g[h]):"view"==a||"segmentView"==a||"clicked"==a||"complete"==a||"timePlayed"==a||"CPM"==a?(f&&(f+=","+l),"timePlayed"==a||"CPM"==a?g[h]&&(d.events2+=(d.events2?",":"")+l+"="+g[h]):g[h]&&(d.events2+=(d.events2?",":"")+l)):"segment"==a&&g[h+"Num"]?(k&&(k+=","+l),d[l]=g[h+"Num"]+":"+g[h]):(k&&(k+=","+l),d[l]=g[h]);else if("milestones"==a||"offsetMilestones"==a)h=h.substring(0,h.length-1),g[h]&&b.contextDataMapping[h+"s"][g[h]]&&(f&&(f+=","+b.contextDataMapping[h+
"s"][g[h]]),d.events2+=(d.events2?",":"")+b.contextDataMapping[h+"s"][g[h]]);g[h]&&(g[h]=0);"segment"==a&&g[h+"Num"]&&(g[h+"Num"]=0)}d.linkTrackVars=k;d.linkTrackEvents=f};b.i=function(d,c,e,k,f){var a={},l=(new Date).getTime()/1E3,g,h,q=b.trackVars,s=b.trackEvents,t=b.trackSeconds,u=b.trackMilestones,v=b.trackOffsetMilestones,w=b.segmentByMilestones,x=b.segmentByOffsetMilestones,p,n,r=1,m={},y;b.channel||(b.channel=b.s.w.location.hostname);if(a=d&&b.list&&b.list[d]?b.list[d]:0)if(a.l&&(t=b.adTrackSeconds,
    u=b.adTrackMilestones,v=b.adTrackOffsetMilestones,w=b.adSegmentByMilestones,x=b.adSegmentByOffsetMilestones),0>e&&(e=1==a.k&&0<a.u?l-a.u+a.c:a.c),0<a.length&&(e=e<a.length?e:a.length),0>e&&(e=0),a.offset=e,0<a.length&&(a.e=a.offset/a.length*100,a.e=100<a.e?100:a.e),0>a.c&&(a.c=e),y=a.D,m.name=d,m.ad=a.l,m.length=a.length,m.openTime=new Date,m.openTime.setTime(1E3*a.timestamp),m.offset=a.offset,m.percent=a.e,m.playerName=a.playerName,m.mediaEvent=0>a.g?"OPEN":1==c?"PLAY":2==c?"STOP":3==c?"MONITOR":
    4==c?"TRACK":5==c?"COMPLETE":7==c?"CLICK":"CLOSE",2<c||c!=a.k&&(2!=c||1==a.k)){f||(k=a.m,f=a.f);if(c){1==c&&(a.c=e);if((3>=c||5<=c)&&0<=a.g&&(r=!1,q=s="None",a.g!=e)){h=a.g;h>e&&(h=a.c,h>e&&(h=e));p=u?u.split(","):0;if(0<a.length&&p&&e>=h)for(n=0;n<p.length;n++)(g=p[n]?parseFloat(""+p[n]):0)&&h/a.length*100<g&&a.e>=g&&(r=!0,n=p.length,m.mediaEvent="MILESTONE",a.p=m.milestone=g);if((p=v?v.split(","):0)&&e>=h)for(n=0;n<p.length;n++)(g=p[n]?parseFloat(""+p[n]):0)&&h<g&&e>=g&&(r=!0,n=p.length,m.mediaEvent=
    "OFFSET_MILESTONE",a.q=m.offsetMilestone=g)}if(a.L||!f){if(w&&u&&0<a.length){if(p=u.split(","))for(p.push("100"),n=h=0;n<p.length;n++)if(g=p[n]?parseFloat(""+p[n]):0)a.e<g&&(k=n+1,f="M:"+h+"-"+g,n=p.length),h=g}else if(x&&v&&(p=v.split(",")))for(p.push(""+(0<a.length?a.length:"E")),n=h=0;n<p.length;n++)if((g=p[n]?parseFloat(""+p[n]):0)||"E"==p[n]){if(e<g||"E"==p[n])k=n+1,f="O:"+h+"-"+g,n=p.length;h=g}f&&(a.L=!0)}(f||a.f)&&f!=a.f&&(a.F=!0,a.f||(a.m=k,a.f=f),0<=a.g&&(r=!0));(2<=c||100<=a.e)&&a.c<e&&
(a.C+=e-a.c,a.a+=e-a.c);if(2>=c||3==c&&!a.k)a.n+=(1==c||3==c?"S":"E")+Math.floor(e),a.k=3==c?1:c;!r&&0<=a.g&&3>=c&&(t=t?t:0)&&a.a>=t&&(r=!0,m.mediaEvent="SECONDS");a.u=l;a.c=e}if(!c||3>=c&&100<=a.e)2!=a.k&&(a.n+="E"+Math.floor(e)),c=0,q=s="None",m.mediaEvent="CLOSE";7==c&&(r=m.clicked=a.r=!0);if(5==c||b.completeByCloseOffset&&(!c||100<=a.e)&&0<a.length&&e>=a.length-b.completeCloseOffsetThreshold)r=m.complete=a.complete=!0;l=m.mediaEvent;"MILESTONE"==l?l+="_"+m.milestone:"OFFSET_MILESTONE"==l&&(l+=
    "_"+m.offsetMilestone);a.I[l]?m.eventFirstTime=!1:(m.eventFirstTime=!0,a.I[l]=1);m.event=m.mediaEvent;m.timePlayed=a.C;m.segmentNum=a.m;m.segment=a.f;m.segmentLength=a.B;b.monitor&&4!=c&&b.monitor(b.s,m);b.Heartbeat&&b.Heartbeat.enabled&&0<=a.g&&(r=!1);0==c&&b.M(d);r&&a.D==y&&(d={contextData:{}},d.linkTrackVars=q,d.linkTrackEvents=s,d.linkTrackVars||(d.linkTrackVars=""),d.linkTrackEvents||(d.linkTrackEvents=""),b.P(d,a),d.linkTrackVars||(d["!linkTrackVars"]=1),d.linkTrackEvents||(d["!linkTrackEvents"]=
    1),b.s.track(d),a.F?(a.m=k,a.f=f,a.A=!0,a.F=!1):0<a.a&&(a.A=!1),a.n="",a.p=a.q=0,a.a-=Math.floor(a.a),a.g=e,a.D++)}return a};b.O=function(d,c,e,k,f){var a=0;if(d&&(!b.autoTrackMediaLengthRequired||c&&0<c)){if(b.list&&b.list[d])a=1;else if(1==e||3==e)b.open(d,c,"HTML5 Video",f),a=1;a&&b.i(d,e,k,-1,0)}};b.attach=function(d){var c,e,k;d&&d.tagName&&"VIDEO"==d.tagName.toUpperCase()&&(b.o||(b.o=function(c,a,d){var e,h;b.autoTrack&&(e=c.currentSrc,(h=c.duration)||(h=-1),0>d&&(d=c.currentTime),b.O(e,h,a,
    d,c))}),c=function(){b.o(d,1,-1)},e=function(){b.o(d,1,-1)},b.j(d,"play",c),b.j(d,"pause",e),b.j(d,"seeking",e),b.j(d,"seeked",c),b.j(d,"ended",function(){b.o(d,0,-1)}),b.j(d,"timeupdate",c),k=function(){d.paused||d.ended||d.seeking||b.o(d,3,-1);setTimeout(k,1E3)},k())};b.j=function(b,c,e){b.attachEvent?b.attachEvent("on"+c,e):b.addEventListener&&b.addEventListener(c,e,!1)};void 0==b.completeByCloseOffset&&(b.completeByCloseOffset=1);void 0==b.completeCloseOffsetThreshold&&(b.completeCloseOffsetThreshold=
    1);b.Heartbeat={};b.N=function(){var d,c;if(b.autoTrack&&(d=b.s.d.getElementsByTagName("VIDEO")))for(c=0;c<d.length;c++)b.attach(d[c])};b.j(q,"load",b.N)}
/* ActivityMap Module */
function AppMeasurement_Module_ActivityMap(f){function g(a,d){var b,c,n;if(a&&d&&(b=e.c[d]||(e.c[d]=d.split(","))))for(n=0;n<b.length&&(c=b[n++]);)if(-1<a.indexOf(c))return null;p=1;return a}function q(a,d,b,c,e){var g,h;if(a.dataset&&(h=a.dataset[d]))g=h;else if(a.getAttribute)if(h=a.getAttribute("data-"+b))g=h;else if(h=a.getAttribute(b))g=h;if(!g&&f.useForcedLinkTracking&&e&&(g="",d=a.onclick?""+a.onclick:"")){b=d.indexOf(c);var l,k;if(0<=b){for(b+=10;b<d.length&&0<="= \t\r\n".indexOf(d.charAt(b));)b++;
    if(b<d.length){h=b;for(l=k=0;h<d.length&&(";"!=d.charAt(h)||l);)l?d.charAt(h)!=l||k?k="\\"==d.charAt(h)?!k:0:l=0:(l=d.charAt(h),'"'!=l&&"'"!=l&&(l=0)),h++;if(d=d.substring(b,h))a.e=new Function("s","var e;try{s.w."+c+"="+d+"}catch(e){}"),a.e(f)}}}return g||e&&f.w[c]}function r(a,d,b){var c;return(c=e[d](a,b))&&(p?(p=0,c):g(k(c),e[d+"Exclusions"]))}function s(a,d,b){var c;if(a&&!(1===(c=a.nodeType)&&(c=a.nodeName)&&(c=c.toUpperCase())&&t[c])&&(1===a.nodeType&&(c=a.nodeValue)&&(d[d.length]=c),b.a||
b.t||b.s||!a.getAttribute||((c=a.getAttribute("alt"))?b.a=c:(c=a.getAttribute("title"))?b.t=c:"IMG"==(""+a.nodeName).toUpperCase()&&(c=a.getAttribute("src")||a.src)&&(b.s=c)),(c=a.childNodes)&&c.length))for(a=0;a<c.length;a++)s(c[a],d,b)}function k(a){if(null==a||void 0==a)return a;try{return a.replace(RegExp("^[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]+","mg"),"").replace(RegExp("[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]+$",
    "mg"),"").replace(RegExp("[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]{1,}","mg")," ").substring(0,254)}catch(d){}}var e=this;e.s=f;var m=window;m.s_c_in||(m.s_c_il=[],m.s_c_in=0);e._il=m.s_c_il;e._in=m.s_c_in;e._il[e._in]=e;m.s_c_in++;e._c="s_m";e.c={};var p=0,t={SCRIPT:1,STYLE:1,LINK:1,CANVAS:1};e._g=function(){var a,d,b,c=f.contextData,e=f.linkObject;(a=f.pageName||f.pageURL)&&(d=r(e,"link",f.linkName))&&(b=r(e,"region"))&&(c["a.activitymap.page"]=a.substring(0,
    255),c["a.activitymap.link"]=128<d.length?d.substring(0,128):d,c["a.activitymap.region"]=127<b.length?b.substring(0,127):b,c["a.activitymap.pageIDType"]=f.pageName?1:0)};e.link=function(a,d){var b;if(d)b=g(k(d),e.linkExclusions);else if((b=a)&&!(b=q(a,"sObjectId","s-object-id","s_objectID",1))){var c,f;(f=g(k(a.innerText||a.textContent),e.linkExclusions))||(s(a,c=[],b={a:void 0,t:void 0,s:void 0}),(f=g(k(c.join(""))))||(f=g(k(b.a?b.a:b.t?b.t:b.s?b.s:void 0)))||!(c=(c=a.tagName)&&c.toUpperCase?c.toUpperCase():
    "")||("INPUT"==c||"SUBMIT"==c&&a.value?f=g(k(a.value)):"IMAGE"==c&&a.src&&(f=g(k(a.src)))));b=f}return b};e.region=function(a){for(var d,b=e.regionIDAttribute||"id";a&&(a=a.parentNode);){if(d=q(a,b,b,b))return d;if("BODY"==a.nodeName)return"BODY"}}}
/* End ActivityMap Module */
/*
 ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ===============
AppMeasurement for JavaScript version: 2.0.0
Copyright 1996-2016 Adobe, Inc. All Rights Reserved
More info available at http://www.adobe.com/marketing-cloud.html
*/
function AppMeasurement(){var a=this;a.version="2.0.0";var h=window;h.s_c_in||(h.s_c_il=[],h.s_c_in=0);a._il=h.s_c_il;a._in=h.s_c_in;a._il[a._in]=a;h.s_c_in++;a._c="s_c";var n=h.AppMeasurement.Ob;n||(n=null);var p=h,l,r;try{for(l=p.parent,r=p.location;l&&l.location&&r&&""+l.location!=""+r&&p.location&&""+l.location!=""+p.location&&l.location.host==r.host;)p=l,l=p.parent}catch(s){}a.P=function(a){try{console.log(a)}catch(b){}};a.La=function(a){return""+parseInt(a)==""+a};a.replace=function(a,b,d){return!a||
0>a.indexOf(b)?a:a.split(b).join(d)};a.escape=function(c){var b,d;if(!c)return c;c=encodeURIComponent(c);for(b=0;7>b;b++)d="+~!*()'".substring(b,b+1),0<=c.indexOf(d)&&(c=a.replace(c,d,"%"+d.charCodeAt(0).toString(16).toUpperCase()));return c};a.unescape=function(c){if(!c)return c;c=0<=c.indexOf("+")?a.replace(c,"+"," "):c;try{return decodeURIComponent(c)}catch(b){}return unescape(c)};a.vb=function(){var c=h.location.hostname,b=a.fpCookieDomainPeriods,d;b||(b=a.cookieDomainPeriods);if(c&&!a.cookieDomain&&
    !/^[0-9.]+$/.test(c)&&(b=b?parseInt(b):2,b=2<b?b:2,d=c.lastIndexOf("."),0<=d)){for(;0<=d&&1<b;)d=c.lastIndexOf(".",d-1),b--;a.cookieDomain=0<d?c.substring(d):c}return a.cookieDomain};a.c_r=a.cookieRead=function(c){c=a.escape(c);var b=" "+a.d.cookie,d=b.indexOf(" "+c+"="),f=0>d?d:b.indexOf(";",d);c=0>d?"":a.unescape(b.substring(d+2+c.length,0>f?b.length:f));return"[[B]]"!=c?c:""};a.c_w=a.cookieWrite=function(c,b,d){var f=a.vb(),e=a.cookieLifetime,g;b=""+b;e=e?(""+e).toUpperCase():"";d&&"SESSION"!=
e&&"NONE"!=e&&((g=""!=b?parseInt(e?e:0):-60)?(d=new Date,d.setTime(d.getTime()+1E3*g)):1==d&&(d=new Date,g=d.getYear(),d.setYear(g+5+(1900>g?1900:0))));return c&&"NONE"!=e?(a.d.cookie=a.escape(c)+"="+a.escape(""!=b?b:"[[B]]")+"; path=/;"+(d&&"SESSION"!=e?" expires="+d.toGMTString()+";":"")+(f?" domain="+f+";":""),a.cookieRead(c)==b):0};a.K=[];a.ia=function(c,b,d){if(a.Ea)return 0;a.maxDelay||(a.maxDelay=250);var f=0,e=(new Date).getTime()+a.maxDelay,g=a.d.visibilityState,k=["webkitvisibilitychange",
    "visibilitychange"];g||(g=a.d.webkitVisibilityState);if(g&&"prerender"==g){if(!a.ja)for(a.ja=1,d=0;d<k.length;d++)a.d.addEventListener(k[d],function(){var c=a.d.visibilityState;c||(c=a.d.webkitVisibilityState);"visible"==c&&(a.ja=0,a.delayReady())});f=1;e=0}else d||a.p("_d")&&(f=1);f&&(a.K.push({m:c,a:b,t:e}),a.ja||setTimeout(a.delayReady,a.maxDelay));return f};a.delayReady=function(){var c=(new Date).getTime(),b=0,d;for(a.p("_d")?b=1:a.xa();0<a.K.length;){d=a.K.shift();if(b&&!d.t&&d.t>c){a.K.unshift(d);
    setTimeout(a.delayReady,parseInt(a.maxDelay/2));break}a.Ea=1;a[d.m].apply(a,d.a);a.Ea=0}};a.setAccount=a.sa=function(c){var b,d;if(!a.ia("setAccount",arguments))if(a.account=c,a.allAccounts)for(b=a.allAccounts.concat(c.split(",")),a.allAccounts=[],b.sort(),d=0;d<b.length;d++)0!=d&&b[d-1]==b[d]||a.allAccounts.push(b[d]);else a.allAccounts=c.split(",")};a.foreachVar=function(c,b){var d,f,e,g,k="";e=f="";if(a.lightProfileID)d=a.O,(k=a.lightTrackVars)&&(k=","+k+","+a.na.join(",")+",");else{d=a.g;if(a.pe||
    a.linkType)k=a.linkTrackVars,f=a.linkTrackEvents,a.pe&&(e=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[e]&&(k=a[e].Mb,f=a[e].Lb));k&&(k=","+k+","+a.G.join(",")+",");f&&k&&(k+=",events,")}b&&(b=","+b+",");for(f=0;f<d.length;f++)e=d[f],(g=a[e])&&(!k||0<=k.indexOf(","+e+","))&&(!b||0<=b.indexOf(","+e+","))&&c(e,g)};a.r=function(c,b,d,f,e){var g="",k,m,h,t,l=0;"contextData"==c&&(c="c");if(b){for(k in b)if(!(Object.prototype[k]||e&&k.substring(0,e.length)!=e)&&b[k]&&(!d||0<=d.indexOf(","+(f?f+
    ".":"")+k+","))){h=!1;if(l)for(m=0;m<l.length;m++)k.substring(0,l[m].length)==l[m]&&(h=!0);if(!h&&(""==g&&(g+="&"+c+"."),m=b[k],e&&(k=k.substring(e.length)),0<k.length))if(h=k.indexOf("."),0<h)m=k.substring(0,h),h=(e?e:"")+m+".",l||(l=[]),l.push(h),g+=a.r(m,b,d,f,h);else if("boolean"==typeof m&&(m=m?"true":"false"),m){if("retrieveLightData"==f&&0>e.indexOf(".contextData."))switch(h=k.substring(0,4),t=k.substring(4),k){case "transactionID":k="xact";break;case "channel":k="ch";break;case "campaign":k=
    "v0";break;default:a.La(t)&&("prop"==h?k="c"+t:"eVar"==h?k="v"+t:"list"==h?k="l"+t:"hier"==h&&(k="h"+t,m=m.substring(0,255)))}g+="&"+a.escape(k)+"="+a.escape(m)}}""!=g&&(g+="&."+c)}return g};a.usePostbacks=0;a.yb=function(){var c="",b,d,f,e,g,k,m,h,l="",p="",q=e="";if(a.lightProfileID)b=a.O,(l=a.lightTrackVars)&&(l=","+l+","+a.na.join(",")+",");else{b=a.g;if(a.pe||a.linkType)l=a.linkTrackVars,p=a.linkTrackEvents,a.pe&&(e=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[e]&&(l=a[e].Mb,p=a[e].Lb));
    l&&(l=","+l+","+a.G.join(",")+",");p&&(p=","+p+",",l&&(l+=",events,"));a.events2&&(q+=(""!=q?",":"")+a.events2)}if(a.visitor&&a.visitor.getCustomerIDs){e=n;if(g=a.visitor.getCustomerIDs())for(d in g)Object.prototype[d]||(f=g[d],"object"==typeof f&&(e||(e={}),f.id&&(e[d+".id"]=f.id),f.authState&&(e[d+".as"]=f.authState)));e&&(c+=a.r("cid",e))}a.AudienceManagement&&a.AudienceManagement.isReady()&&(c+=a.r("d",a.AudienceManagement.getEventCallConfigParams()));for(d=0;d<b.length;d++){e=b[d];g=a[e];f=e.substring(0,
    4);k=e.substring(4);!g&&"events"==e&&q&&(g=q,q="");if(g&&(!l||0<=l.indexOf(","+e+","))){switch(e){case "supplementalDataID":e="sdid";break;case "timestamp":e="ts";break;case "dynamicVariablePrefix":e="D";break;case "visitorID":e="vid";break;case "marketingCloudVisitorID":e="mid";break;case "analyticsVisitorID":e="aid";break;case "audienceManagerLocationHint":e="aamlh";break;case "audienceManagerBlob":e="aamb";break;case "authState":e="as";break;case "pageURL":e="g";255<g.length&&(a.pageURLRest=g.substring(255),
    g=g.substring(0,255));break;case "pageURLRest":e="-g";break;case "referrer":e="r";break;case "vmk":case "visitorMigrationKey":e="vmt";break;case "visitorMigrationServer":e="vmf";a.ssl&&a.visitorMigrationServerSecure&&(g="");break;case "visitorMigrationServerSecure":e="vmf";!a.ssl&&a.visitorMigrationServer&&(g="");break;case "charSet":e="ce";break;case "visitorNamespace":e="ns";break;case "cookieDomainPeriods":e="cdp";break;case "cookieLifetime":e="cl";break;case "variableProvider":e="vvp";break;case "currencyCode":e=
    "cc";break;case "channel":e="ch";break;case "transactionID":e="xact";break;case "campaign":e="v0";break;case "latitude":e="lat";break;case "longitude":e="lon";break;case "resolution":e="s";break;case "colorDepth":e="c";break;case "javascriptVersion":e="j";break;case "javaEnabled":e="v";break;case "cookiesEnabled":e="k";break;case "browserWidth":e="bw";break;case "browserHeight":e="bh";break;case "connectionType":e="ct";break;case "homepage":e="hp";break;case "events":q&&(g+=(""!=g?",":"")+q);if(p)for(k=
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  g.split(","),g="",f=0;f<k.length;f++)m=k[f],h=m.indexOf("="),0<=h&&(m=m.substring(0,h)),h=m.indexOf(":"),0<=h&&(m=m.substring(0,h)),0<=p.indexOf(","+m+",")&&(g+=(g?",":"")+k[f]);break;case "events2":g="";break;case "contextData":c+=a.r("c",a[e],l,e);g="";break;case "lightProfileID":e="mtp";break;case "lightStoreForSeconds":e="mtss";a.lightProfileID||(g="");break;case "lightIncrementBy":e="mti";a.lightProfileID||(g="");break;case "retrieveLightProfiles":e="mtsr";break;case "deleteLightProfiles":e=
    "mtsd";break;case "retrieveLightData":a.retrieveLightProfiles&&(c+=a.r("mts",a[e],l,e));g="";break;default:a.La(k)&&("prop"==f?e="c"+k:"eVar"==f?e="v"+k:"list"==f?e="l"+k:"hier"==f&&(e="h"+k,g=g.substring(0,255)))}g&&(c+="&"+e+"="+("pev"!=e.substring(0,3)?a.escape(g):g))}"pev3"==e&&a.e&&(c+=a.e)}return c};a.D=function(a){var b=a.tagName;if("undefined"!=""+a.Rb||"undefined"!=""+a.Hb&&"HTML"!=(""+a.Hb).toUpperCase())return"";b=b&&b.toUpperCase?b.toUpperCase():"";"SHAPE"==b&&(b="");b&&(("INPUT"==b||
    "BUTTON"==b)&&a.type&&a.type.toUpperCase?b=a.type.toUpperCase():!b&&a.href&&(b="A"));return b};a.Ha=function(a){var b=h.location,d=a.href?a.href:"",f,e,g;f=d.indexOf(":");e=d.indexOf("?");g=d.indexOf("/");d&&(0>f||0<=e&&f>e||0<=g&&f>g)&&(e=a.protocol&&1<a.protocol.length?a.protocol:b.protocol?b.protocol:"",f=b.pathname.lastIndexOf("/"),d=(e?e+"//":"")+(a.host?a.host:b.host?b.host:"")+("/"!=d.substring(0,1)?b.pathname.substring(0,0>f?0:f)+"/":"")+d);return d};a.L=function(c){var b=a.D(c),d,f,e="",
    g=0;return b&&(d=c.protocol,f=c.onclick,!c.href||"A"!=b&&"AREA"!=b||f&&d&&!(0>d.toLowerCase().indexOf("javascript"))?f?(e=a.replace(a.replace(a.replace(a.replace(""+f,"\r",""),"\n",""),"\t","")," ",""),g=2):"INPUT"==b||"SUBMIT"==b?(c.value?e=c.value:c.innerText?e=c.innerText:c.textContent&&(e=c.textContent),g=3):"IMAGE"==b&&c.src&&(e=c.src):e=a.Ha(c),e)?{id:e.substring(0,100),type:g}:0};a.Pb=function(c){for(var b=a.D(c),d=a.L(c);c&&!d&&"BODY"!=b;)if(c=c.parentElement?c.parentElement:c.parentNode)b=
    a.D(c),d=a.L(c);d&&"BODY"!=b||(c=0);c&&(b=c.onclick?""+c.onclick:"",0<=b.indexOf(".tl(")||0<=b.indexOf(".trackLink("))&&(c=0);return c};a.Gb=function(){var c,b,d=a.linkObject,f=a.linkType,e=a.linkURL,g,k;a.oa=1;d||(a.oa=0,d=a.clickObject);if(d){c=a.D(d);for(b=a.L(d);d&&!b&&"BODY"!=c;)if(d=d.parentElement?d.parentElement:d.parentNode)c=a.D(d),b=a.L(d);b&&"BODY"!=c||(d=0);if(d&&!a.linkObject){var m=d.onclick?""+d.onclick:"";if(0<=m.indexOf(".tl(")||0<=m.indexOf(".trackLink("))d=0}}else a.oa=1;!e&&d&&
(e=a.Ha(d));e&&!a.linkLeaveQueryString&&(g=e.indexOf("?"),0<=g&&(e=e.substring(0,g)));if(!f&&e){var l=0,p=0,n;if(a.trackDownloadLinks&&a.linkDownloadFileTypes)for(m=e.toLowerCase(),g=m.indexOf("?"),k=m.indexOf("#"),0<=g?0<=k&&k<g&&(g=k):g=k,0<=g&&(m=m.substring(0,g)),g=a.linkDownloadFileTypes.toLowerCase().split(","),k=0;k<g.length;k++)(n=g[k])&&m.substring(m.length-(n.length+1))=="."+n&&(f="d");if(a.trackExternalLinks&&!f&&(m=e.toLowerCase(),a.Ka(m)&&(a.linkInternalFilters||(a.linkInternalFilters=
    h.location.hostname),g=0,a.linkExternalFilters?(g=a.linkExternalFilters.toLowerCase().split(","),l=1):a.linkInternalFilters&&(g=a.linkInternalFilters.toLowerCase().split(",")),g))){for(k=0;k<g.length;k++)n=g[k],0<=m.indexOf(n)&&(p=1);p?l&&(f="e"):l||(f="e")}}a.linkObject=d;a.linkURL=e;a.linkType=f;if(a.trackClickMap||a.trackInlineStats)a.e="",d&&(f=a.pageName,e=1,d=d.sourceIndex,f||(f=a.pageURL,e=0),h.s_objectID&&(b.id=h.s_objectID,d=b.type=1),f&&b&&b.id&&c&&(a.e="&pid="+a.escape(f.substring(0,255))+
    (e?"&pidt="+e:"")+"&oid="+a.escape(b.id.substring(0,100))+(b.type?"&oidt="+b.type:"")+"&ot="+c+(d?"&oi="+d:"")))};a.zb=function(){var c=a.oa,b=a.linkType,d=a.linkURL,f=a.linkName;b&&(d||f)&&(b=b.toLowerCase(),"d"!=b&&"e"!=b&&(b="o"),a.pe="lnk_"+b,a.pev1=d?a.escape(d):"",a.pev2=f?a.escape(f):"",c=1);a.abort&&(c=0);if(a.trackClickMap||a.trackInlineStats||a.ActivityMap){var b={},d=0,e=a.cookieRead("s_sq"),g=e?e.split("&"):0,k,m,h,e=0;if(g)for(k=0;k<g.length;k++)m=g[k].split("="),f=a.unescape(m[0]).split(","),
    m=a.unescape(m[1]),b[m]=f;f=a.account.split(",");k={};for(h in a.contextData)h&&!Object.prototype[h]&&"a.activitymap."==h.substring(0,14)&&(k[h]=a.contextData[h],a.contextData[h]="");a.e=a.r("c",k)+(a.e?a.e:"");if(c||a.e){c&&!a.e&&(e=1);for(m in b)if(!Object.prototype[m])for(h=0;h<f.length;h++)for(e&&(g=b[m].join(","),g==a.account&&(a.e+=("&"!=m.charAt(0)?"&":"")+m,b[m]=[],d=1)),k=0;k<b[m].length;k++)g=b[m][k],g==f[h]&&(e&&(a.e+="&u="+a.escape(g)+("&"!=m.charAt(0)?"&":"")+m+"&u=0"),b[m].splice(k,
    1),d=1);c||(d=1);if(d){e="";k=2;!c&&a.e&&(e=a.escape(f.join(","))+"="+a.escape(a.e),k=1);for(m in b)!Object.prototype[m]&&0<k&&0<b[m].length&&(e+=(e?"&":"")+a.escape(b[m].join(","))+"="+a.escape(m),k--);a.cookieWrite("s_sq",e)}}}return c};a.Ab=function(){if(!a.Kb){var c=new Date,b=p.location,d,f,e=f=d="",g="",k="",h="1.2",l=a.cookieWrite("s_cc","true",0)?"Y":"N",n="",q="";if(c.setUTCDate&&(h="1.3",(0).toPrecision&&(h="1.5",c=[],c.forEach))){h="1.6";f=0;d={};try{f=new Iterator(d),f.next&&(h="1.7",
c.reduce&&(h="1.8",h.trim&&(h="1.8.1",Date.parse&&(h="1.8.2",Object.create&&(h="1.8.5")))))}catch(r){}}d=screen.width+"x"+screen.height;e=navigator.javaEnabled()?"Y":"N";f=screen.pixelDepth?screen.pixelDepth:screen.colorDepth;g=a.w.innerWidth?a.w.innerWidth:a.d.documentElement.offsetWidth;k=a.w.innerHeight?a.w.innerHeight:a.d.documentElement.offsetHeight;try{a.b.addBehavior("#default#homePage"),n=a.b.Qb(b)?"Y":"N"}catch(s){}try{a.b.addBehavior("#default#clientCaps"),q=a.b.connectionType}catch(u){}a.resolution=
    d;a.colorDepth=f;a.javascriptVersion=h;a.javaEnabled=e;a.cookiesEnabled=l;a.browserWidth=g;a.browserHeight=k;a.connectionType=q;a.homepage=n;a.Kb=1}};a.Q={};a.loadModule=function(c,b){var d=a.Q[c];if(!d){d=h["AppMeasurement_Module_"+c]?new h["AppMeasurement_Module_"+c](a):{};a.Q[c]=a[c]=d;d.cb=function(){return d.hb};d.ib=function(b){if(d.hb=b)a[c+"_onLoad"]=b,a.ia(c+"_onLoad",[a,d],1)||b(a,d)};try{Object.defineProperty?Object.defineProperty(d,"onLoad",{get:d.cb,set:d.ib}):d._olc=1}catch(f){d._olc=
    1}}b&&(a[c+"_onLoad"]=b,a.ia(c+"_onLoad",[a,d],1)||b(a,d))};a.p=function(c){var b,d;for(b in a.Q)if(!Object.prototype[b]&&(d=a.Q[b])&&(d._olc&&d.onLoad&&(d._olc=0,d.onLoad(a,d)),d[c]&&d[c]()))return 1;return 0};a.Cb=function(){var c=Math.floor(1E13*Math.random()),b=a.visitorSampling,d=a.visitorSamplingGroup,d="s_vsn_"+(a.visitorNamespace?a.visitorNamespace:a.account)+(d?"_"+d:""),f=a.cookieRead(d);if(b){b*=100;f&&(f=parseInt(f));if(!f){if(!a.cookieWrite(d,c))return 0;f=c}if(f%1E4>b)return 0}return 1};
    a.R=function(c,b){var d,f,e,g,k,h;for(d=0;2>d;d++)for(f=0<d?a.Aa:a.g,e=0;e<f.length;e++)if(g=f[e],(k=c[g])||c["!"+g]){if(!b&&("contextData"==g||"retrieveLightData"==g)&&a[g])for(h in a[g])k[h]||(k[h]=a[g][h]);a[g]=k}};a.Ua=function(c,b){var d,f,e,g;for(d=0;2>d;d++)for(f=0<d?a.Aa:a.g,e=0;e<f.length;e++)g=f[e],c[g]=a[g],b||c[g]||(c["!"+g]=1)};a.ub=function(a){var b,d,f,e,g,k=0,h,l="",n="";if(a&&255<a.length&&(b=""+a,d=b.indexOf("?"),0<d&&(h=b.substring(d+1),b=b.substring(0,d),e=b.toLowerCase(),f=0,
        "http://"==e.substring(0,7)?f+=7:"https://"==e.substring(0,8)&&(f+=8),d=e.indexOf("/",f),0<d&&(e=e.substring(f,d),g=b.substring(d),b=b.substring(0,d),0<=e.indexOf("google")?k=",q,ie,start,search_key,word,kw,cd,":0<=e.indexOf("yahoo.co")&&(k=",p,ei,"),k&&h)))){if((a=h.split("&"))&&1<a.length){for(f=0;f<a.length;f++)e=a[f],d=e.indexOf("="),0<d&&0<=k.indexOf(","+e.substring(0,d)+",")?l+=(l?"&":"")+e:n+=(n?"&":"")+e;l&&n?h=l+"&"+n:n=""}d=253-(h.length-n.length)-b.length;a=b+(0<d?g.substring(0,d):"")+
        "?"+h}return a};a.$a=function(c){var b=a.d.visibilityState,d=["webkitvisibilitychange","visibilitychange"];b||(b=a.d.webkitVisibilityState);if(b&&"prerender"==b){if(c)for(b=0;b<d.length;b++)a.d.addEventListener(d[b],function(){var b=a.d.visibilityState;b||(b=a.d.webkitVisibilityState);"visible"==b&&c()});return!1}return!0};a.ea=!1;a.I=!1;a.kb=function(){a.I=!0;a.j()};a.ca=!1;a.V=!1;a.gb=function(c){a.marketingCloudVisitorID=c;a.V=!0;a.j()};a.fa=!1;a.W=!1;a.lb=function(c){a.visitorOptedOut=c;a.W=!0;
        a.j()};a.Z=!1;a.S=!1;a.Wa=function(c){a.analyticsVisitorID=c;a.S=!0;a.j()};a.ba=!1;a.U=!1;a.Ya=function(c){a.audienceManagerLocationHint=c;a.U=!0;a.j()};a.aa=!1;a.T=!1;a.Xa=function(c){a.audienceManagerBlob=c;a.T=!0;a.j()};a.Za=function(c){a.maxDelay||(a.maxDelay=250);return a.p("_d")?(c&&setTimeout(function(){c()},a.maxDelay),!1):!0};a.da=!1;a.H=!1;a.xa=function(){a.H=!0;a.j()};a.isReadyToTrack=function(){var c=!0,b=a.visitor,d,f,e;a.ea||a.I||(a.$a(a.kb)?a.I=!0:a.ea=!0);if(a.ea&&!a.I)return!1;b&&
    b.isAllowed()&&(a.ca||a.marketingCloudVisitorID||!b.getMarketingCloudVisitorID||(a.ca=!0,a.marketingCloudVisitorID=b.getMarketingCloudVisitorID([a,a.gb]),a.marketingCloudVisitorID&&(a.V=!0)),a.fa||a.visitorOptedOut||!b.isOptedOut||(a.fa=!0,a.visitorOptedOut=b.isOptedOut([a,a.lb]),a.visitorOptedOut!=n&&(a.W=!0)),a.Z||a.analyticsVisitorID||!b.getAnalyticsVisitorID||(a.Z=!0,a.analyticsVisitorID=b.getAnalyticsVisitorID([a,a.Wa]),a.analyticsVisitorID&&(a.S=!0)),a.ba||a.audienceManagerLocationHint||!b.getAudienceManagerLocationHint||
    (a.ba=!0,a.audienceManagerLocationHint=b.getAudienceManagerLocationHint([a,a.Ya]),a.audienceManagerLocationHint&&(a.U=!0)),a.aa||a.audienceManagerBlob||!b.getAudienceManagerBlob||(a.aa=!0,a.audienceManagerBlob=b.getAudienceManagerBlob([a,a.Xa]),a.audienceManagerBlob&&(a.T=!0)),c=a.ca&&!a.V&&!a.marketingCloudVisitorID,b=a.Z&&!a.S&&!a.analyticsVisitorID,d=a.ba&&!a.U&&!a.audienceManagerLocationHint,f=a.aa&&!a.T&&!a.audienceManagerBlob,e=a.fa&&!a.W,c=c||b||d||f||e?!1:!0);a.da||a.H||(a.Za(a.xa)?a.H=!0:
        a.da=!0);a.da&&!a.H&&(c=!1);return c};a.o=n;a.u=0;a.callbackWhenReadyToTrack=function(c,b,d){var f;f={};f.pb=c;f.ob=b;f.mb=d;a.o==n&&(a.o=[]);a.o.push(f);0==a.u&&(a.u=setInterval(a.j,100))};a.j=function(){var c;if(a.isReadyToTrack()&&(a.jb(),a.o!=n))for(;0<a.o.length;)c=a.o.shift(),c.ob.apply(c.pb,c.mb)};a.jb=function(){a.u&&(clearInterval(a.u),a.u=0)};a.eb=function(c){var b,d,f=n,e=n;if(!a.isReadyToTrack()){b=[];if(c!=n)for(d in f={},c)f[d]=c[d];e={};a.Ua(e,!0);b.push(f);b.push(e);a.callbackWhenReadyToTrack(a,
        a.track,b);return!0}return!1};a.wb=function(){var c=a.cookieRead("s_fid"),b="",d="",f;f=8;var e=4;if(!c||0>c.indexOf("-")){for(c=0;16>c;c++)f=Math.floor(Math.random()*f),b+="0123456789ABCDEF".substring(f,f+1),f=Math.floor(Math.random()*e),d+="0123456789ABCDEF".substring(f,f+1),f=e=16;c=b+"-"+d}a.cookieWrite("s_fid",c,1)||(c=0);return c};a.t=a.track=function(c,b){var d,f=new Date,e="s"+Math.floor(f.getTime()/108E5)%10+Math.floor(1E13*Math.random()),g=f.getYear(),g="t="+a.escape(f.getDate()+"/"+f.getMonth()+
        "/"+(1900>g?g+1900:g)+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds()+" "+f.getDay()+" "+f.getTimezoneOffset());a.visitor&&a.visitor.getAuthState&&(a.authState=a.visitor.getAuthState());a.p("_s");a.eb(c)||(b&&a.R(b),c&&(d={},a.Ua(d,0),a.R(c)),a.Cb()&&!a.visitorOptedOut&&(a.analyticsVisitorID||a.marketingCloudVisitorID||(a.fid=a.wb()),a.Gb(),a.usePlugins&&a.doPlugins&&a.doPlugins(a),a.account&&(a.abort||(a.trackOffline&&!a.timestamp&&(a.timestamp=Math.floor(f.getTime()/1E3)),f=h.location,a.pageURL||
    (a.pageURL=f.href?f.href:f),a.referrer||a.Va||(a.referrer=p.document.referrer),a.Va=1,a.referrer=a.ub(a.referrer),a.p("_g")),a.zb()&&!a.abort&&(a.visitor&&!a.supplementalDataID&&a.visitor.getSupplementalDataID&&(a.supplementalDataID=a.visitor.getSupplementalDataID("AppMeasurement:"+a._in,a.expectSupplementalData?!1:!0)),a.Ab(),g+=a.yb(),a.Fb(e,g),a.p("_t"),a.referrer=""))),c&&a.R(d,1));a.abort=a.supplementalDataID=a.timestamp=a.pageURLRest=a.linkObject=a.clickObject=a.linkURL=a.linkName=a.linkType=
        h.s_objectID=a.pe=a.pev1=a.pev2=a.pev3=a.e=a.lightProfileID=0};a.za=[];a.registerPreTrackCallback=function(c){for(var b=[],d=1;d<arguments.length;d++)b.push(arguments[d]);"function"==typeof c?a.za.push([c,b]):a.debugTracking&&a.P("DEBUG: Non function type passed to registerPreTrackCallback")};a.bb=function(c){a.wa(a.za,c)};a.ya=[];a.registerPostTrackCallback=function(c){for(var b=[],d=1;d<arguments.length;d++)b.push(arguments[d]);"function"==typeof c?a.ya.push([c,b]):a.debugTracking&&a.P("DEBUG: Non function type passed to registerPostTrackCallback")};
    a.ab=function(c){a.wa(a.ya,c)};a.wa=function(c,b){if("object"==typeof c)for(var d=0;d<c.length;d++){var f=c[d][0],e=c[d][1];e.unshift(b);if("function"==typeof f)try{f.apply(null,e)}catch(g){a.debugTracking&&a.P(g.message)}}};a.tl=a.trackLink=function(c,b,d,f,e){a.linkObject=c;a.linkType=b;a.linkName=d;e&&(a.l=c,a.A=e);return a.track(f)};a.trackLight=function(c,b,d,f){a.lightProfileID=c;a.lightStoreForSeconds=b;a.lightIncrementBy=d;return a.track(f)};a.clearVars=function(){var c,b;for(c=0;c<a.g.length;c++)if(b=
        a.g[c],"prop"==b.substring(0,4)||"eVar"==b.substring(0,4)||"hier"==b.substring(0,4)||"list"==b.substring(0,4)||"channel"==b||"events"==b||"eventList"==b||"products"==b||"productList"==b||"purchaseID"==b||"transactionID"==b||"state"==b||"zip"==b||"campaign"==b)a[b]=void 0};a.tagContainerMarker="";a.Fb=function(c,b){var d,f=a.trackingServer;d="";var e=a.dc,g="sc.",h=a.visitorNamespace;f?a.trackingServerSecure&&a.ssl&&(f=a.trackingServerSecure):(h||(h=a.account,f=h.indexOf(","),0<=f&&(h=h.substring(0,
        f)),h=h.replace(/[^A-Za-z0-9]/g,"")),d||(d="2o7.net"),e=e?(""+e).toLowerCase():"d1","2o7.net"==d&&("d1"==e?e="112":"d2"==e&&(e="122"),g=""),f=h+"."+e+"."+g+d);d=a.ssl?"https://":"http://";e=a.AudienceManagement&&a.AudienceManagement.isReady()||0!=a.usePostbacks;d+=f+"/b/ss/"+a.account+"/"+(a.mobile?"5.":"")+(e?"10":"1")+"/JS-"+a.version+(a.Jb?"T":"")+(a.tagContainerMarker?"-"+a.tagContainerMarker:"")+"/"+c+"?AQB=1&ndh=1&pf=1&"+(e?"callback=s_c_il["+a._in+"].doPostbacks&et=1&":"")+b+"&AQE=1";a.bb(d);
        a.sb(d);a.ka()};a.Ta=/{(%?)(.*?)(%?)}/;a.Nb=RegExp(a.Ta.source,"g");a.tb=function(c){if("object"==typeof c.dests)for(var b=0;b<c.dests.length;++b){var d=c.dests[b];if("string"==typeof d.c&&"aa."==d.id.substr(0,3))for(var f=d.c.match(a.Nb),e=0;e<f.length;++e){var g=f[e],h=g.match(a.Ta),l="";"%"==h[1]&&"timezone_offset"==h[2]?l=(new Date).getTimezoneOffset():"%"==h[1]&&"timestampz"==h[2]&&(l=a.xb());d.c=d.c.replace(g,a.escape(l))}}};a.xb=function(){var c=new Date,b=new Date(6E4*Math.abs(c.getTimezoneOffset()));
        return a.k(4,c.getFullYear())+"-"+a.k(2,c.getMonth()+1)+"-"+a.k(2,c.getDate())+"T"+a.k(2,c.getHours())+":"+a.k(2,c.getMinutes())+":"+a.k(2,c.getSeconds())+(0<c.getTimezoneOffset()?"-":"+")+a.k(2,b.getUTCHours())+":"+a.k(2,b.getUTCMinutes())};a.k=function(a,b){return(Array(a+1).join(0)+b).slice(-a)};a.ta={};a.doPostbacks=function(c){if("object"==typeof c)if(a.tb(c),"object"==typeof a.AudienceManagement&&"function"==typeof a.AudienceManagement.isReady&&a.AudienceManagement.isReady()&&"function"==typeof a.AudienceManagement.passData)a.AudienceManagement.passData(c);
    else if("object"==typeof c&&"object"==typeof c.dests)for(var b=0;b<c.dests.length;++b){var d=c.dests[b];"object"==typeof d&&"string"==typeof d.c&&"string"==typeof d.id&&"aa."==d.id.substr(0,3)&&(a.ta[d.id]=new Image,a.ta[d.id].alt="",a.ta[d.id].src=d.c)}};a.sb=function(c){a.i||a.Bb();a.i.push(c);a.ma=a.C();a.Ra()};a.Bb=function(){a.i=a.Db();a.i||(a.i=[])};a.Db=function(){var c,b;if(a.ra()){try{(b=h.localStorage.getItem(a.pa()))&&(c=h.JSON.parse(b))}catch(d){}return c}};a.ra=function(){var c=!0;a.trackOffline&&
    a.offlineFilename&&h.localStorage&&h.JSON||(c=!1);return c};a.Ia=function(){var c=0;a.i&&(c=a.i.length);a.q&&c++;return c};a.ka=function(){if(a.q&&(a.B&&a.B.complete&&a.B.F&&a.B.va(),a.q))return;a.Ja=n;if(a.qa)a.ma>a.N&&a.Pa(a.i),a.ua(500);else{var c=a.nb();if(0<c)a.ua(c);else if(c=a.Fa())a.q=1,a.Eb(c),a.Ib(c)}};a.ua=function(c){a.Ja||(c||(c=0),a.Ja=setTimeout(a.ka,c))};a.nb=function(){var c;if(!a.trackOffline||0>=a.offlineThrottleDelay)return 0;c=a.C()-a.Oa;return a.offlineThrottleDelay<c?0:a.offlineThrottleDelay-
        c};a.Fa=function(){if(0<a.i.length)return a.i.shift()};a.Eb=function(c){if(a.debugTracking){var b="AppMeasurement Debug: "+c;c=c.split("&");var d;for(d=0;d<c.length;d++)b+="\n\t"+a.unescape(c[d]);a.P(b)}};a.fb=function(){return a.marketingCloudVisitorID||a.analyticsVisitorID};a.Y=!1;var q;try{q=JSON.parse('{"x":"y"}')}catch(u){q=null}q&&"y"==q.x?(a.Y=!0,a.X=function(a){return JSON.parse(a)}):h.$&&h.$.parseJSON?(a.X=function(a){return h.$.parseJSON(a)},a.Y=!0):a.X=function(){return null};a.Ib=function(c){var b,
        d,f;a.fb()&&2047<c.length&&("undefined"!=typeof XMLHttpRequest&&(b=new XMLHttpRequest,"withCredentials"in b?d=1:b=0),b||"undefined"==typeof XDomainRequest||(b=new XDomainRequest,d=2),b&&(a.AudienceManagement&&a.AudienceManagement.isReady()||0!=a.usePostbacks)&&(a.Y?b.Ba=!0:b=0));!b&&a.Sa&&(c=c.substring(0,2047));!b&&a.d.createElement&&(0!=a.usePostbacks||a.AudienceManagement&&a.AudienceManagement.isReady())&&(b=a.d.createElement("SCRIPT"))&&"async"in b&&((f=(f=a.d.getElementsByTagName("HEAD"))&&f[0]?
        f[0]:a.d.body)?(b.type="text/javascript",b.setAttribute("async","async"),d=3):b=0);b||(b=new Image,b.alt="",b.abort||"undefined"===typeof h.InstallTrigger||(b.abort=function(){b.src=n}));b.Da=function(){try{b.F&&(clearTimeout(b.F),b.F=0)}catch(a){}};b.onload=b.va=function(){a.ab(c);b.Da();a.rb();a.ga();a.q=0;a.ka();if(b.Ba){b.Ba=!1;try{a.doPostbacks(a.X(b.responseText))}catch(d){}}};b.onabort=b.onerror=b.Ga=function(){b.Da();(a.trackOffline||a.qa)&&a.q&&a.i.unshift(a.qb);a.q=0;a.ma>a.N&&a.Pa(a.i);
        a.ga();a.ua(500)};b.onreadystatechange=function(){4==b.readyState&&(200==b.status?b.va():b.Ga())};a.Oa=a.C();if(1==d||2==d){var e=c.indexOf("?");f=c.substring(0,e);e=c.substring(e+1);e=e.replace(/&callback=[a-zA-Z0-9_.\[\]]+/,"");1==d?(b.open("POST",f,!0),b.send(e)):2==d&&(b.open("POST",f),b.send(e))}else if(b.src=c,3==d){if(a.Ma)try{f.removeChild(a.Ma)}catch(g){}f.firstChild?f.insertBefore(b,f.firstChild):f.appendChild(b);a.Ma=a.B}b.F=setTimeout(function(){b.F&&(b.complete?b.va():(a.trackOffline&&
    b.abort&&b.abort(),b.Ga()))},5E3);a.qb=c;a.B=h["s_i_"+a.replace(a.account,",","_")]=b;if(a.useForcedLinkTracking&&a.J||a.A)a.forcedLinkTrackingTimeout||(a.forcedLinkTrackingTimeout=250),a.ha=setTimeout(a.ga,a.forcedLinkTrackingTimeout)};a.rb=function(){if(a.ra()&&!(a.Na>a.N))try{h.localStorage.removeItem(a.pa()),a.Na=a.C()}catch(c){}};a.Pa=function(c){if(a.ra()){a.Ra();try{h.localStorage.setItem(a.pa(),h.JSON.stringify(c)),a.N=a.C()}catch(b){}}};a.Ra=function(){if(a.trackOffline){if(!a.offlineLimit||
        0>=a.offlineLimit)a.offlineLimit=10;for(;a.i.length>a.offlineLimit;)a.Fa()}};a.forceOffline=function(){a.qa=!0};a.forceOnline=function(){a.qa=!1};a.pa=function(){return a.offlineFilename+"-"+a.visitorNamespace+a.account};a.C=function(){return(new Date).getTime()};a.Ka=function(a){a=a.toLowerCase();return 0!=a.indexOf("#")&&0!=a.indexOf("about:")&&0!=a.indexOf("opera:")&&0!=a.indexOf("javascript:")?!0:!1};a.setTagContainer=function(c){var b,d,f;a.Jb=c;for(b=0;b<a._il.length;b++)if((d=a._il[b])&&"s_l"==
        d._c&&d.tagContainerName==c){a.R(d);if(d.lmq)for(b=0;b<d.lmq.length;b++)f=d.lmq[b],a.loadModule(f.n);if(d.ml)for(f in d.ml)if(a[f])for(b in c=a[f],f=d.ml[f],f)!Object.prototype[b]&&("function"!=typeof f[b]||0>(""+f[b]).indexOf("s_c_il"))&&(c[b]=f[b]);if(d.mmq)for(b=0;b<d.mmq.length;b++)f=d.mmq[b],a[f.m]&&(c=a[f.m],c[f.f]&&"function"==typeof c[f.f]&&(f.a?c[f.f].apply(c,f.a):c[f.f].apply(c)));if(d.tq)for(b=0;b<d.tq.length;b++)a.track(d.tq[b]);d.s=a;break}};a.Util={urlEncode:a.escape,urlDecode:a.unescape,
        cookieRead:a.cookieRead,cookieWrite:a.cookieWrite,getQueryParam:function(c,b,d){var f;b||(b=a.pageURL?a.pageURL:h.location);d||(d="&");return c&&b&&(b=""+b,f=b.indexOf("?"),0<=f&&(b=d+b.substring(f+1)+d,f=b.indexOf(d+c+"="),0<=f&&(b=b.substring(f+d.length+c.length+1),f=b.indexOf(d),0<=f&&(b=b.substring(0,f)),0<b.length)))?a.unescape(b):""}};a.G="supplementalDataID timestamp dynamicVariablePrefix visitorID marketingCloudVisitorID analyticsVisitorID audienceManagerLocationHint authState fid vmk visitorMigrationKey visitorMigrationServer visitorMigrationServerSecure charSet visitorNamespace cookieDomainPeriods fpCookieDomainPeriods cookieLifetime pageName pageURL referrer contextData currencyCode lightProfileID lightStoreForSeconds lightIncrementBy retrieveLightProfiles deleteLightProfiles retrieveLightData".split(" ");
    a.g=a.G.concat("purchaseID variableProvider channel server pageType transactionID campaign state zip events events2 products audienceManagerBlob tnt".split(" "));a.na="timestamp charSet visitorNamespace cookieDomainPeriods cookieLifetime contextData lightProfileID lightStoreForSeconds lightIncrementBy".split(" ");a.O=a.na.slice(0);a.Aa="account allAccounts debugTracking visitor visitorOptedOut trackOffline offlineLimit offlineThrottleDelay offlineFilename usePlugins doPlugins configURL visitorSampling visitorSamplingGroup linkObject clickObject linkURL linkName linkType trackDownloadLinks trackExternalLinks trackClickMap trackInlineStats linkLeaveQueryString linkTrackVars linkTrackEvents linkDownloadFileTypes linkExternalFilters linkInternalFilters useForcedLinkTracking forcedLinkTrackingTimeout trackingServer trackingServerSecure ssl abort mobile dc lightTrackVars maxDelay expectSupplementalData usePostbacks registerPreTrackCallback registerPostTrackCallback AudienceManagement".split(" ");
    for(l=0;250>=l;l++)76>l&&(a.g.push("prop"+l),a.O.push("prop"+l)),a.g.push("eVar"+l),a.O.push("eVar"+l),6>l&&a.g.push("hier"+l),4>l&&a.g.push("list"+l);l="pe pev1 pev2 pev3 latitude longitude resolution colorDepth javascriptVersion javaEnabled cookiesEnabled browserWidth browserHeight connectionType homepage pageURLRest".split(" ");a.g=a.g.concat(l);a.G=a.G.concat(l);a.ssl=0<=h.location.protocol.toLowerCase().indexOf("https");a.charSet="UTF-8";a.contextData={};a.offlineThrottleDelay=0;a.offlineFilename=
        "AppMeasurement.offline";a.Oa=0;a.ma=0;a.N=0;a.Na=0;a.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";a.w=h;a.d=h.document;try{if(a.Sa=!1,navigator){var v=navigator.userAgent;if("Microsoft Internet Explorer"==navigator.appName||0<=v.indexOf("MSIE ")||0<=v.indexOf("Trident/")&&0<=v.indexOf("Windows NT 6"))a.Sa=!0}}catch(w){}a.ga=function(){a.ha&&(h.clearTimeout(a.ha),a.ha=n);a.l&&a.J&&a.l.dispatchEvent(a.J);a.A&&("function"==typeof a.A?a.A():a.l&&a.l.href&&(a.d.location=
        a.l.href));a.l=a.J=a.A=0};a.Qa=function(){a.b=a.d.body;a.b?(a.v=function(c){var b,d,f,e,g;if(!(a.d&&a.d.getElementById("cppXYctnr")||c&&c["s_fe_"+a._in])){if(a.Ca)if(a.useForcedLinkTracking)a.b.removeEventListener("click",a.v,!1);else{a.b.removeEventListener("click",a.v,!0);a.Ca=a.useForcedLinkTracking=0;return}else a.useForcedLinkTracking=0;a.clickObject=c.srcElement?c.srcElement:c.target;try{if(!a.clickObject||a.M&&a.M==a.clickObject||!(a.clickObject.tagName||a.clickObject.parentElement||a.clickObject.parentNode))a.clickObject=
        0;else{var k=a.M=a.clickObject;a.la&&(clearTimeout(a.la),a.la=0);a.la=setTimeout(function(){a.M==k&&(a.M=0)},1E4);f=a.Ia();a.track();if(f<a.Ia()&&a.useForcedLinkTracking&&c.target){for(e=c.target;e&&e!=a.b&&"A"!=e.tagName.toUpperCase()&&"AREA"!=e.tagName.toUpperCase();)e=e.parentNode;if(e&&(g=e.href,a.Ka(g)||(g=0),d=e.target,c.target.dispatchEvent&&g&&(!d||"_self"==d||"_top"==d||"_parent"==d||h.name&&d==h.name))){try{b=a.d.createEvent("MouseEvents")}catch(l){b=new h.MouseEvent}if(b){try{b.initMouseEvent("click",
        c.bubbles,c.cancelable,c.view,c.detail,c.screenX,c.screenY,c.clientX,c.clientY,c.ctrlKey,c.altKey,c.shiftKey,c.metaKey,c.button,c.relatedTarget)}catch(n){b=0}b&&(b["s_fe_"+a._in]=b.s_fe=1,c.stopPropagation(),c.stopImmediatePropagation&&c.stopImmediatePropagation(),c.preventDefault(),a.l=c.target,a.J=b)}}}}}catch(p){a.clickObject=0}}},a.b&&a.b.attachEvent?a.b.attachEvent("onclick",a.v):a.b&&a.b.addEventListener&&(navigator&&(0<=navigator.userAgent.indexOf("WebKit")&&a.d.createEvent||0<=navigator.userAgent.indexOf("Firefox/2")&&
        h.MouseEvent)&&(a.Ca=1,a.useForcedLinkTracking=1,a.b.addEventListener("click",a.v,!0)),a.b.addEventListener("click",a.v,!1))):setTimeout(a.Qa,30)};a.Qa();a.loadModule("ActivityMap")}
function s_gi(a){var h,n=window.s_c_il,p,l,r=a.split(","),s,q,u=0;if(n)for(p=0;!u&&p<n.length;){h=n[p];if("s_c"==h._c&&(h.account||h.oun))if(h.account&&h.account==a)u=1;else for(l=h.account?h.account:h.oun,l=h.allAccounts?h.allAccounts:l.split(","),s=0;s<r.length;s++)for(q=0;q<l.length;q++)r[s]==l[q]&&(u=1);p++}u||(h=new AppMeasurement);h.setAccount?h.setAccount(a):h.sa&&h.sa(a);return h}AppMeasurement.getInstance=s_gi;window.s_objectID||(window.s_objectID=0);
function s_pgicq(){var a=window,h=a.s_giq,n,p,l;if(h)for(n=0;n<h.length;n++)p=h[n],l=s_gi(p.oun),l.setAccount(p.un),l.setTagContainer(p.tagContainerName);a.s_giq=0}s_pgicq();
//end Omniture
//begin OAS Analytics
var d=document;
var OAS_rdl = '';
var OAS_CA = 'N';
if((d.referrer)&&(d.referrer!="[unknown origin]"))
{
    	if(d.referrer.indexOf("?") == -1)
	{
    		OAS_rdl += '&tax23_RefDocLoc='+d.referrer.toString();
    	}
	else
	{
    		var rdl=d.referrer;
                var rdl1=rdl.indexOf("?");
    		var rdl2=rdl.substring(0,rdl1);
    		OAS_rdl += '&tax23_RefDocLoc='+rdl2;
    	}
}

function cookie_check(ifd,ife)
{
    	var s=ife.indexOf(ifd);
    	if(s==-1) return "";
        s+=ifd.length;
    	var e=ife.indexOf(";",s);
     	if(e==-1)
           e=ife.length;
    	return ife.substring(s,e);
}
function write_cookie()
{
    	var d=new Date();
    	var m=d.getTime();
    	document.cookie="OAS_SC1="+m.toString()+";path=/;";
    	var v=cookie_check("OAS_SC1=",document.cookie);
    	if(v!=m.toString())
	   return false;
    	d.setTime(m+3600000);
    	return true;
}

if(write_cookie())OAS_CA="Y";

//end OAS Analytics

// Promise polyfill: https://github.com/taylorhakes/promise-polyfill
!function(e){function n(){}function t(e,n){return function(){e.apply(n,arguments)}}function o(e){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof e)throw new TypeError("not a function");this._state=0,this._handled=!1,this._value=void 0,this._deferreds=[],s(e,this)}function i(e,n){for(;3===e._state;)e=e._value;return 0===e._state?void e._deferreds.push(n):(e._handled=!0,void o._immediateFn(function(){var t=1===e._state?n.onFulfilled:n.onRejected;if(null===t)return void(1===e._state?r:u)(n.promise,e._value);var o;try{o=t(e._value)}catch(i){return void u(n.promise,i)}r(n.promise,o)}))}function r(e,n){try{if(n===e)throw new TypeError("A promise cannot be resolved with itself.");if(n&&("object"==typeof n||"function"==typeof n)){var i=n.then;if(n instanceof o)return e._state=3,e._value=n,void f(e);if("function"==typeof i)return void s(t(i,n),e)}e._state=1,e._value=n,f(e)}catch(r){u(e,r)}}function u(e,n){e._state=2,e._value=n,f(e)}function f(e){2===e._state&&0===e._deferreds.length&&o._immediateFn(function(){e._handled||o._unhandledRejectionFn(e._value)});for(var n=0,t=e._deferreds.length;n<t;n++)i(e,e._deferreds[n]);e._deferreds=null}function c(e,n,t){this.onFulfilled="function"==typeof e?e:null,this.onRejected="function"==typeof n?n:null,this.promise=t}function s(e,n){var t=!1;try{e(function(e){t||(t=!0,r(n,e))},function(e){t||(t=!0,u(n,e))})}catch(o){if(t)return;t=!0,u(n,o)}}var a=setTimeout;o.prototype["catch"]=function(e){return this.then(null,e)},o.prototype.then=function(e,t){var o=new this.constructor(n);return i(this,new c(e,t,o)),o},o.all=function(e){var n=Array.prototype.slice.call(e);return new o(function(e,t){function o(r,u){try{if(u&&("object"==typeof u||"function"==typeof u)){var f=u.then;if("function"==typeof f)return void f.call(u,function(e){o(r,e)},t)}n[r]=u,0===--i&&e(n)}catch(c){t(c)}}if(0===n.length)return e([]);for(var i=n.length,r=0;r<n.length;r++)o(r,n[r])})},o.resolve=function(e){return e&&"object"==typeof e&&e.constructor===o?e:new o(function(n){n(e)})},o.reject=function(e){return new o(function(n,t){t(e)})},o.race=function(e){return new o(function(n,t){for(var o=0,i=e.length;o<i;o++)e[o].then(n,t)})},o._immediateFn="function"==typeof setImmediate&&function(e){setImmediate(e)}||function(e){a(e,0)},o._unhandledRejectionFn=function(e){"undefined"!=typeof console&&console&&console.warn("Possible Unhandled Promise Rejection:",e)},o._setImmediateFn=function(e){o._immediateFn=e},o._setUnhandledRejectionFn=function(e){o._unhandledRejectionFn=e},"undefined"!=typeof module&&module.exports?module.exports=o:e.Promise||(e.Promise=o)}(this);

// Custom event polyfill
;(function customEventPolyfill() {
  if (typeof window.CustomEvent === 'function') return;
  function CustomEvent(event, params) {
    params || (params = { bubbles: false, cancelable: false, detail: undefined });
    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  }
  CustomEvent.prototype = window.Event.prototype;
  window.CustomEvent = CustomEvent;
})();

;(function () {

  function fetch(url, forceBypassCache, withCredentials) {
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if(xhr.status === 200) {
            resolve(JSON.parse(xhr.responseText));
		  } else {
            reject(xhr.status)
          }
        }
      };
	  if(forceBypassCache) {
	  	url = url + '?_=' + Date.now()
	  }
      xhr.open('GET', url);
      if (typeof xhr.withCredentials !== 'undefined') {
        xhr.withCredentials = withCredentials;
      }
      xhr.send();
    });
  }

  function parseCookieValue(s) {
    if (s.indexOf('"') === 0) {
      s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
    }

    try {
      s = decodeURIComponent(s.replace(/\+/g, ' '));
      return s;
    } catch (e) {
    }
  }

  function getCookie(key) {

    var cookies = document.cookie ? document.cookie.split('; ') : [];

    for (var i = 0, l = cookies.length; i < l; i++) {
      var parts = cookies[i].split('=');
      var name = decodeURIComponent(parts.shift());
      var cookie = parts.join('=');

      if (key && key === name) {
        return parseCookieValue(cookie);
      }
    }
  }

  function removeItem(sKey, sPath, sDomain) {
    document.cookie = encodeURIComponent(sKey) +
      '=; expires=Thu, 01 Jan 1970 00:00:00 GMT' +
      (sDomain ? '; domain=' + sDomain : '') +
      (sPath ? '; path=' + sPath : '');
  }

  function clearECCookies(isRedirectToEC) {
    if (location.href.indexOf('consumerreports.org') !== -1) {
      removeItem('userLicenses', '/', '.consumerreports.org');
      removeItem('userInfo', '/', '.consumerreports.org');
      removeItem('userAds', '/', '.consumerreports.org');
      if (isRedirectToEC) {
        window.location = 'https://ec.consumerreports.org/ec/cro/login.htm?loginMethod=auto';
      }
    } else if (location.href.indexOf('consumer.org') !== -1) {
      removeItem('userLicenses', '/', '.consumer.org');
      removeItem('userInfo', '/', '.consumer.org');
      removeItem('userAds', '/', '.consumer.org');
      if (isRedirectToEC) {
        window.location = 'http://ecqa.crinfra.net/ec/cro/login.htm?loginMethod=auto';
      }
    } else if (location.href.indexOf('crinfra.net') !== -1) {
      removeItem('userLicenses', '/', '.crinfra.net');
      removeItem('userInfo', '/', '.crinfra.net');
      removeItem('userAds', '/', '.crinfra.net');
      if (isRedirectToEC) {
        window.location = 'http://ecqa.crinfra.net/ec/cro/login.htm?loginMethod=auto';
      }
    }
  }

  // TODO: check if we need something except CRO and CROAnnual.
  function handleHideShowClasses() {
    // CRUserInfo.hideAndShowClasses("Subscriptions", CRUserInfo.hasSubscriptions);
    // CRUserInfo.hideAndShowClasses("APS", CRUserInfo.hasAPS);
    // CRUserInfo.hideAndShowClasses("APSNew", CRUserInfo.hasAPSNew);
    // CRUserInfo.hideAndShowClasses("APSUsed", CRUserInfo.hasAPSUsed);
    // CRUserInfo.hideAndShowClasses("CBDP", CRUserInfo.hasCBDP);
    // CRUserInfo.hideAndShowClasses("CRMag", CRUserInfo.hasCRMag);
    CRUserInfo.hideAndShowClasses('CRO', CRUserInfo.hasCRO);
    CRUserInfo.hideAndShowClasses("CROAnnual", CRUserInfo.hasCROAnnual);
    // CRUserInfo.hideAndShowClasses("CROMonthly", CRUserInfo.hasCROMonthly);
    // CRUserInfo.hideAndShowClasses("Health", CRUserInfo.hasHealth);
    // CRUserInfo.hideAndShowClasses("MoneyAdviser", CRUserInfo.hasMoneyAdviser);
    // CRUserInfo.hideAndShowClasses("OnHealth", CRUserInfo.hasOnHealth);
    // CRUserInfo.hideAndShowClasses("ShopSmart", CRUserInfo.hasShopSmart);
  }

  function handleUI() {
    handleHideShowClasses();
  }

  function triggerReady(resolve) {
    document.dispatchEvent(new CustomEvent('userInfo_ready'));
    resolve();
  }

  if (window.CRUserInfo === undefined) {
    window.CRUserInfo = {};

    CRUserInfo.itsJson = null;

    /**
     * Dummy anonymous data in case of /bin/userinfo fail
     */
    CRUserInfo.dummyJson = {"products":{"cbdp":false,"apsUsed":false,"apsNew":false,"onHealth":false,"health":false,"croAnnual":false,"croMonthly":false,"cro":false,"employee":false,"crMag":false,"moneyAdviser":false,"shopSmart":false},"name":"","user":"","email":"","tier":"ANONYMOUS","autoLogin":false};

    /**
     * Don't change the order, there is a logic based on it.
     */
    CRUserInfo.TIERS = [
      'ANONYMOUS',
      'EMAIL',
      'PRINT',
      'DIGITAL',
      'BUNDLE',
    ];

    /**
     * Map TIERS to an object. Can be used like this: CRUserInfo.TIER_NAMES.DIGITAL // "DIGITAL"
     * This is an alternative to a less intuitive CRUserInfo.TIER[3]
     */
    CRUserInfo.TIER_NAMES = {};
    CRUserInfo.TIERS.forEach(function (tier) {
        CRUserInfo.TIER_NAMES[tier] = tier;
    });

    /**
     * Makes a promise to work with User profile data once ready
     * @returns {Promise}
     */
    CRUserInfo.ready = function () {
      return CRUserInfo.promise;
    };


    /**
     * This function will return "null" if no alerts exist (or the user is
     * not logged in), or will return an array of String objects if there
     * are alerts to display.  Each String object will contain HTML text
     * and should be output exactly as-is.
     */
    CRUserInfo.getAlerts = function () {
      return CRUserInfo.getData('alerts');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * display name from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's display name (assuming the JSON call has returned already).
     */
    CRUserInfo.getName = function () {
      return CRUserInfo.getData('name');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * username from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's username (assuming the JSON call has returned already)
     * which is going to be all uppercase.
     */
    CRUserInfo.getUser = function () {
      return CRUserInfo.getData('user');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * email from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's email (assuming the JSON call has returned already).
     */
    CRUserInfo.getEmail = function () {
      return CRUserInfo.getData('email');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * first name from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's first name (assuming the JSON call has returned already).
     */
    CRUserInfo.getFirstName = function () {
        return CRUserInfo.getData('firstName');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * last name from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's last name (assuming the JSON call has returned already).
     */
    CRUserInfo.getLastName = function () {
        return CRUserInfo.getData('lastName');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * tier from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's tier (assuming the JSON call has returned already).
     */
    CRUserInfo.getTier = function () {
      return CRUserInfo.getData('tier');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * id from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's id (assuming the JSON call has returned already).
     */
    CRUserInfo.getId = function () {
        return CRUserInfo.getData('id');
    };
    /**
     * This function will return "null" if we cannot obtain the user's
     * erightsId from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's erightsId (assuming the JSON call has returned already).
     */
    CRUserInfo.getErightsId = function () {
        return CRUserInfo.getData('erightsId');
    };

    /**
     * This function will return "null" if we cannot obtain the user's
     * expiredProducts from the userInfo cookie (e.g. if they are not logged
     * in, or in some other problem).  Otherwise, this will return the
     * user's expiredProducts (assuming the JSON call has returned already).
     */
    CRUserInfo.getExpiredProducts = function () {
        return CRUserInfo.getData('expiredProducts');
    };

    /**
     * Convenient function to check if current user has
     * given or higher tier.
     *
     * @param tierToCheck - tier string to check.
     * @return boolean - 'true' if user has given or higher tier, 'false' otherwise
     */
    CRUserInfo.hasAtLeastMembershipTier = function (tierToCheck) {
      return CRUserInfo.TIERS.indexOf(tierToCheck) > -1 &&
          (CRUserInfo.TIERS.indexOf(tierToCheck) <= CRUserInfo.TIERS.indexOf(CRUserInfo.getTier()));
    };


    /**
     * Returns a path to member CTAs for defined tiers. Returns default CTA as a fallback
     *
     * @default '/join'
     * @return String
     */
    CRUserInfo.getMemberCta = function () {
      var defaultMemberCta = '/join';
      var tierToMemberCtaMapping = [defaultMemberCta, defaultMemberCta, defaultMemberCta];
      var usersTier = CRUserInfo.TIERS.indexOf(CRUserInfo.getTier());

      return tierToMemberCtaMapping[usersTier] !== void 0 ? tierToMemberCtaMapping[usersTier] : defaultMemberCta;
    };

    /**
     * Convenient method to check if a user has expired CRO subscription.
     * Returns 'false' if CRUserInfo is not ready or it doesn't have information about 'cro'
     *
     * @return boolean - false is default/fallback
     */
    CRUserInfo.isCroExpired = function () {
        var expiredProducts = CRUserInfo.getData('expiredProducts') || {};
        return !!expiredProducts["cro"];
    };

    /**
     * Checks is service is loaded with data from the backend
     * @returns {boolean}
     */
    CRUserInfo.hasData = function () {
      return CRUserInfo.itsJson != null
        && Object.keys(CRUserInfo.itsJson).length > 0
        && CRUserInfo.itsJson.products.cro !== undefined;
    };


    /**
     * This function will return true or false, depending on whether or not
     * the user has any active subscriptions at all.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasSubscriptions = function () {
      return (CRUserInfo.hasAPSNew() ||
      CRUserInfo.hasAPSUsed() ||
      CRUserInfo.hasCBDP() ||
      CRUserInfo.hasCRMag() ||
      CRUserInfo.hasCRO() ||
      CRUserInfo.hasHealth() ||
      CRUserInfo.hasMoneyAdviser() ||
      CRUserInfo.hasOnHealth() ||
      CRUserInfo.hasShopSmart());
    };

    CRUserInfo.subscriptionsOwned = function () {
        var so = CRUserInfo.hasCRMag() ? 'cr' + '|' : '';
        so = CRUserInfo.hasCROAnnual() ? so + 'cro-annual' + '|' : so;
        so = CRUserInfo.hasCROMonthly() ? so + 'cro-monthly' + '|' : so;
        so = CRUserInfo.hasOnHealth() ? so + 'oh' : so != '' ? so.substring(0, so.length - 1) : so;
        return so;
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasAPSNew = function () {
      return CRUserInfo.getData('products', 'apsNew');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasAPSUsed = function () {
      return CRUserInfo.getData('products', 'apsUsed');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasAPS = function () {
      return CRUserInfo.hasAPSUsed() || CRUserInfo.hasAPSNew();
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasCBDP = function () {
      return CRUserInfo.getData('products', 'cbdp');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasCRMag = function () {
      return CRUserInfo.getData('products', 'crMag');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasCRO = function () {
      var isInsectRepellents = false;
      if (window.location.href.indexOf('category/34772') > 0 || window.location.href.indexOf('/insect-repellent/') > 0) {
        return true;
      }
      if (typeof(DataProvider) !== 'undefined') {
        DataProvider.getProductsByCategoryId('34772', function (items) {
          items.forEach(function (element) {
            if (window.location.href.indexOf('model/' + element.id) > 0) {
              isInsectRepellents = true;
              return;
            }
          });
        }, ['id']).execute();
      }
      return isInsectRepellents ? true : CRUserInfo.getData('products', 'cro');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasCROAnnual = function () {
      return CRUserInfo.getData('products', 'croAnnual');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasCROMonthly = function () {
      return CRUserInfo.getData('products', 'croMonthly');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasHealth = function () {
      return false;
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasMoneyAdviser = function () {
      return CRUserInfo.getData('products', 'moneyAdviser');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasOnHealth = function () {
      return CRUserInfo.getData('products', 'onHealth');
    };

    /**
     * This function will return true or false, depending on whether or not
     * the user has access to this particular product.  Non-logged in users
     * will get "false".  If there is a problem retrieving the data from
     * the server, "false" will be returned.
     */
    CRUserInfo.hasShopSmart = function () {
      return CRUserInfo.getData('products', 'shopSmart');
    };

    /**
     * This function will return true or false, depending on whether or
     * not the user has "auto login" enabled (stored via a separate cookie
     * from user license info, so it is detectable even for logged-out
     * users).  If there is a problem retrieving the data from the server
     * (or if it hasn't gotten the data yet), "false" will be returned.
     */
    CRUserInfo.isAutoLogin = function () {
      var result = CRUserInfo.getData('autoLogin');
      if (result == null) {
        return false;
      } else {
        return result;
      }
    };

    CRUserInfo.isEmployee = function () {
      return CRUserInfo.getData('products', 'employee');
    };

    /**
     * This function should not be called directly by scripts on the page
     * but should only be called by the "getAlerts" and "has*" functions
     * in this JavaScript file.
     *
     * @param inCheck
     * @param inKey
     */
    CRUserInfo.getData = function (inCheck, inKey) {
      if (CRUserInfo.itsJson != null) {
        if (CRUserInfo.itsJson[inCheck]) {
          if (inKey) {
            return CRUserInfo.itsJson[inCheck][inKey];
          } else {
            return CRUserInfo.itsJson[inCheck];
          }
        }
      }
      if (inKey) {
        return false;
      } else {
        return null;
      }
    };

    /**
     * This function should not be called directly by scripts on the page
     * but should only be called by the JSON callback triggered on document
     * ready in this JavaScript file.
     *
     * @param inName
     * @param inFunction
     */
    CRUserInfo.hideAndShowClasses = function (inName, inFunction) {
      function hide(el) {
        el.style.display = 'none';
      }

      function show(el) {
        el.style.display = '';

        var computedDisplay = window.getComputedStyle(el).display;
        if(computedDisplay === 'none') {
          el.style.display = getDefaultDisplay(el);
        }
      }

      /**
       * The only way to get default display value is to create an element and append it to the DOM.
       *
       * @param el - html element
       * @returns default 'display' value for a given element.
       */
      function getDefaultDisplay(el) {
        var tempElement = document.createElement(el.tagName);
        document.body.appendChild(tempElement);
        var defaultDisplay = window.getComputedStyle(tempElement).display;
        document.body.removeChild(tempElement);
        return defaultDisplay;
      }

      function process(name, action) {
		  Array.prototype.slice.apply(document.querySelectorAll(name)).forEach(action);
	  }

      if (inFunction()) {
        process('.no' + inName, hide);
        process('.if' + inName, show);
      } else {
        process('.if' + inName, hide);
        process('.no' + inName, show);
      }
    };

    /**
     * This function should not be called directly by scripts on the page.
     */
    CRUserInfo.isEBSCO = function () {
      var theName = CRUserInfo.getUser();
      if (theName != null) {
        if (theName.indexOf('EBSCO') === 0) {
          return true;
        }
      }
      return false;
    };

    function getUserInfoHost() {
      if (window.globalNavDomain) {
        return window.globalNavDomain;
      }
      if (window.__gnavConfig__ && window.__gnavConfig__.navigationDataHost) {
        return window.__gnavConfig__.navigationDataHost;
      }
      return '';
    }

	  CRUserInfo.refresh = function () {
		  return new Promise(function (resolve) {
			  fetch(getUserInfoHost() + '/bin/userinfo', true, true)
				  .then(function (data) {
					  CRUserInfo.itsJson = data;

					  triggerReady(resolve);
				  })
				  .catch(function () {
					  CRUserInfo.itsJson = CRUserInfo.dummyJson;

					  triggerReady(resolve);
				  });
		  })
	  };

    CRUserInfo.promise = new Promise(function (resolve) {

      /**
       * This function retrieves the user alerts and product info for this
       * user via JSON and then caches the data for all subsequent calls on
       * the page to the "getAlerts" or "has*" functions.
       */
      function fetchData() {
        removeItem('BNES_userInfo', '/', '.consumerreports.org');
        removeItem('BNES_userLicenses', '/', '.consumerreports.org');

        if (getCookie('userLicenses')) {
          var userLicensesTime = parseInt(getCookie('userLicenses').match(/t=(\d+)/)[1]);
          var dayTime = 86400000;
          if ((userLicensesTime + dayTime) < Date.now()) {
            clearECCookies(true);
          }
        }

        CRUserInfo.isEditMode = function () {
          var theWCM = window.CQ && window.CQ.WCM;
          return ((theWCM && theWCM.getMode) && theWCM.getMode() !== 'preview');
        };

        CRUserInfo.ready().then(handleUI);

        if (!CRUserInfo.isEditMode() && !CRUserInfo.isFetching) {

          CRUserInfo.isFetching = true;

			CRUserInfo.refresh()
				.then(function () {
					resolve();
				})
        } else {
          triggerReady(resolve);
        }
      }

      document.addEventListener('DOMContentLoaded', fetchData);
    });
  } else {
    console.warn('CRUserInfo duplicate at', 'etc/designs/cr/shared-resources/scripts/user-info.js');
  }
})();

function readCookie(inName)
{
    var theNameWithEquals = inName + "=";
    var theCookieArray = document.cookie.split(';');
    for (var i = 0; i < theCookieArray.length; i++)
    {
        var c = theCookieArray[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(theNameWithEquals) == 0) return c.substring(theNameWithEquals.length, c.length);
    }
    return "";
}

function isShowAds(){
    return !document.getElementsByTagName("html")[0].classList.contains("ignore-ads");
}

function randomString(inStringLength, inSource)
{
    var theText = "";
    for (var i = 0; i < inStringLength; i++)
    {
        var thePosition = Math.floor(Math.random() * inSource.length);
        theText += inSource.substring(thePosition, thePosition + 1);
    }
    return theText;
}

// This is a pretty horrible hack for JIRA task CROP-675
function x04kill()
{
    var theBugs = jQuery("div .bug");
    if (theBugs)
    {
        if (theBugs.length > 0)
        {
            var theBug = theBugs[0];
            var theImgs = theBug.getElementsByTagName("img");
            if (theImgs)
            {
                if (theImgs.length > 0)
                {
                    var theImg = theImgs[0];
                    var theSrc = theImg.getAttribute("src");
                    if (theSrc.indexOf("cro_cr_plus_full_badge.gif") != -1)
                    {
                        var theAd = document.getElementById("x04");
                        if (theAd)
                        {
                            theAd.parentNode.removeChild(theAd);
                        }
                        var theAdContainers = jQuery("dd .bottompremium");
                        if (theAdContainers)
                        {
                            if (theAdContainers.length > 0)
                            {
                                var theAdContainer = theAdContainers[0];
                                if (theAdContainer)
                                {
                                    theAdContainer.innerHTML = "<div style='background: transparent url(/etc/designs/cro/application-resources/modules/common/images/cro_cr_plus_org_slug.gif) no-repeat scroll right bottom; padding-left: 7px; padding-top: 2px; padding-bottom: 0px; margin-bottom: 2px; margin-right: 3px;'>Get unlimited new and used car price reports.<br/><span style='font-weight: normal;'>Choose car above.</span></div>";
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


function renderHomepageAd(divtag){
    if(divtag != undefined && divtag.length > 0){
        renderAd(divtag[0] , "adhomepagetag");
    }
}

/**
 * Returns the adPath used on the page.
 * This function must be called prior to ads being rendered as the
 * placeholder div gets replaced by the actual ad
 * @returns {string|*}
 */
function findAdPath(){
    var adDivs = jQuery("div .adtag, div .dxTag");
    if(adDivs!=undefined && adDivs.length>0) {
        //expose the adPath as a global variable
        var adPath = adDivs[0].getAttribute("path");
        if(adPath && adPath !== ''){
            return adDivs[0].getAttribute("path");
        }
    }
}

//expose adPath as global variable
jQuery(function(){
    if(typeof window.adPath === 'undefined'){
        var adPath = findAdPath();
        if(adPath){
            window.adPath = adPath;
        }
    }
});

//set platform value based on window size
function calculatePlatform() {

    var platform,
        tabletMaxWidth = 1199,
        mobileMaxWidth = 767;

    switch (true) {
        case (window.innerWidth > tabletMaxWidth) :
            platform = 'desktop'
            break;
        case (window.innerWidth > mobileMaxWidth) :
            platform = 'tablet'
            break;
        case (window.innerWidth <= mobileMaxWidth) :
            platform = 'mobile'
            break;
        default:
            platform = false //no parametrs will be added if some problems with calculating
        }

    return platform;
}

window.renderAds = function (){
    x04kill();
    var theDivList = jQuery("div .adtag");
    for (var x = 0; x < theDivList.length; x++){
        renderAd(theDivList[x], "adtag");
    }
    renderElectronicsHeaderJsAd("x95",window.adPath);
};



function renderElectronicsHeaderJsAd(adId,adPath){
    if(adPath){
        if(adPath.indexOf(buildHostPath() + "/cro/electronics-computers/")==0
            || adPath.indexOf(buildHostPath() + "/cro/home-garden/")==0
            || adPath.indexOf(buildHostPath() + "/cro/appliances/")==0) {
            addJsAdToHeadSection(adId,adPath);
        }
    } else if(document.location.pathname.indexOf("/cro/electronics-computers/")==0
                || document.location.pathname.indexOf("/cro/home-garden/")==0
                || document.location.pathname.indexOf("/cro/appliances/")==0) {
        addJsToHeadSectionByPath(adId)
    }  else if(document.location.pathname.indexOf("/cro/electronics1/")==0
                || document.location.pathname.indexOf("/cro/appliances1/")==0
                || document.location.pathname.indexOf("/cro/homegarden1/")==0) {
        addJsToHeadSectionByPath(adId);
    }
}

//this random string has to be same for all the adds in the page irrespective of their location & time of trigger.
var theRandomStr="";
function getRandomString(){
    if(theRandomStr==""){
        theRandomStr = randomString(9, "0123456789");
    }
    return theRandomStr;
}

var thePositionList="";
function getAdPositionList(){
    if(thePositionList==""){
        var theDivList = jQuery("div .adtag,div .adhomepagetag");
        if(theDivList.length > 0){
            thePositionList = theDivList[0].getAttribute("id");
            for (var x = 1; x < theDivList.length; x++){
                thePositionList += "," + theDivList[x].getAttribute("id");
            }
        }
    }
    return thePositionList;
}

function buildHostPath(){
    return document.location.hostname.match("consumer\.org|crinfra\.net|stage.*\.consumerreports\.org") && "cqauthor.consumer.org" != document.location.hostname ? 'test.cu' : 'cu';
}

function buildPath (){
    var hostName = buildHostPath();
    var pathName = document.location.pathname;
    if(pathName.charAt(pathName.length - 1) === '/'){
        pathName = pathName.substring(0, pathName.length - 1);
    }
    return hostName + pathName;
}

function addJsToHeadSectionByPath(id,includePlatform){
    addJsAdToHeadSection(id,buildPath(),includePlatform);
}

function addJsAdToHeadSection(id,adPath,includePlatform,type){
    var adsType = type ? '&type=' + type : '';
    var platformType = platform ? '&platform=' + platform : '';
    var theRandomNumber = getRandomString();
    var theAdScript = document.createElement("script");
    var platform = '';

    if (includePlatform && calculatePlatform()) { //no parametrs will be added  to url if some problems with calculating
        platform = '&platform=' + calculatePlatform();
    }

    if(thePositionList !== '') {
        thePositionList += ',' + id;
    }
    var theAdCookie = readCookie("userAds");
    if (!theAdCookie || !CRUserInfo.getUser())
    {
        theAdCookie = "";
    }
    else
    {
        if ((theAdCookie.length >= 1) && (theAdCookie.substr(theAdCookie.length - 1) != '&'))
        {
            theAdCookie = theAdCookie + "&";
        }
    }

    var theSrc = "//oascentral.consumerreports.org/RealMedia/ads/adstream_sx.ads/" +
        adPath + "/1"+theRandomNumber + "@" + (thePositionList ? thePositionList + "!" : "") + id + "?url=" + encodeURIComponent(window.location.href) + "&" +
        theAdCookie + "article=112345&XE&status=active" + OAS_rdl + "&if_nt_CookieAccept=" + OAS_CA + "&XE" + adsType + platform + "&tier=" + (CRUserInfo.getTier() ? CRUserInfo.getTier() : "anonymous");
    theAdScript.setAttribute("src",theSrc);
    document.getElementsByTagName('head')[0].appendChild(theAdScript);
}

function queryAd(adId, callbackName){
    queryAdForPath(adId, buildPath(), callbackName, false);
}

function queryPlatformAd(adId, callbackName){
    queryAdForPath(adId, buildPath(), callbackName, true);
}

function queryAdForPath(adId, adPath, callbackName, includePlatform) {
    var platform = (includePlatform && calculatePlatform()) ? '&platform=' + calculatePlatform() : '';
    var theAdCookie = readCookie("userAds");
    if (!theAdCookie || !CRUserInfo.getUser()) {
        theAdCookie = "";
    } else {
        if ((theAdCookie.length >= 1) && (theAdCookie.substr(theAdCookie.length - 1) != '&')) {
            theAdCookie = theAdCookie + "&";
        }
    }
    var adUrl = "//oascentral.consumerreports.org/jp/" + adPath + "/1" + getRandomString() + "@" + adId +
        "?_RM_HTML_CALLBACK_=" + callbackName + "&url=" + encodeURIComponent(window.location.href) +
        "&" + theAdCookie + "article=112345&XE&status=active" + OAS_rdl + "&if_nt_CookieAccept=" + OAS_CA + "&XE" + platform + "&tier=" + (CRUserInfo.getTier() ? CRUserInfo.getTier() : "anonymous");

    jQuery.ajax({ url: adUrl, dataType: "jsonp" });
}

function renderAd(theDiv, location){
    var theRandomOverride = getRandomString(); //using getRandomString() instead of randomString(9, "0123456789"); making sure to use: same random number for all ads irrespective of their location & time of trigger.
    var theAd = document.createElement("iframe");
    theAd.setAttribute("width", theDiv.getAttribute("width"));
    theAd.setAttribute("height", theDiv.getAttribute("height"));
    if (theDiv.getAttribute("passthrustyle"))
    {
        if (typeof(theAd.style.cssText) == 'string')
        {
            theAd.style.cssText = theDiv.getAttribute("passthrustyle");
        }
        theAd.setAttribute("style", theDiv.getAttribute("passthrustyle"));
    }
    if (theDiv.getAttribute("passthruclass"))
    {
        theAd.className = theDiv.getAttribute("passthruclass");
        theAd.setAttribute("class", theDiv.getAttribute("passthruclass"));
    }
    if (theDiv.getAttribute("allowtransparency"))
    {
        theAd.allowtransparency = "true";
        theAd.setAttribute("allowtransparency", "true");
    }
    theAd.setAttribute("scrolling", "no");
    var theBrowser = navigator.appName;
    if(theBrowser == "Microsoft Internet Explorer"){
        theAd.frameBorder = theAd.marginWidth = theAd.marginHeight = 0;
    } else {
        theAd.setAttribute("frameborder", "0");
        theAd.setAttribute("marginwidth", "0");
        theAd.setAttribute("marginheight", "0");
    }
    theAd.setAttribute("id", theDiv.getAttribute("id"));
    theAd.setAttribute("name", theDiv.getAttribute("id"));
    var theAdCookie = readCookie("userAds");
    if (!theAdCookie || !CRUserInfo.getUser())
    {
        theAdCookie = "";
    }
    else
    {
        if ((theAdCookie.length >= 1) && (theAdCookie.substr(theAdCookie.length - 1) != '&'))
        {
            theAdCookie = theAdCookie + "&";
        }
    }
    var thePath = "";
    if(theDiv.getAttribute("path")){
        thePath = theDiv.getAttribute("path");
    }else{
        thePath = buildPath();
    }

    var theSrc = "//oascentral.consumerreports.org/RealMedia/ads/adstream_sx.ads/" + thePath +
        "/1" + theRandomOverride + "@" + getAdPositionList() + "!" + theDiv.getAttribute("id") + "?url=" + encodeURIComponent(window.location.href) + "&" +
        theAdCookie + "article=112345&XE&status=active" + OAS_rdl + "&if_nt_CookieAccept=" + OAS_CA + "&XE" + getHasBuyingGuideOnlyString(theDiv.getAttribute("id"))+ "&tier=" + (CRUserInfo.getTier() ? CRUserInfo.getTier() : "anonymous");
    // OAS_rdl and OAS_CA are set by https://oascentral.consumerreports.org/Scripts/oas_analytics.js
    theAd.setAttribute("src", theSrc);
    theDiv.parentNode.insertBefore(theAd, theDiv);
    theDiv.parentNode.removeChild(theDiv);
}

function getHasBuyingGuideOnlyString(id) {
    var checkBuyingGuideOnlyAds = ["x55", "x57", "x56", "Top"];
    return window.hasBuyingGuideOnly && checkBuyingGuideOnlyAds.indexOf(id) !== -1 && !CRUserInfo.hasSubscriptions() ? "&bg=y" : "";
}

function adjustIFrame(iframeId, height) {
    if(iframeId && height) {
        var frame = document.getElementById(iframeId)
        if (frame) {
            frame.height = height;
        }
    }
}

function getFrameId(url) {
    var frames = /@(.*[,!])+(.*)\?/.exec(url);
    return frames ? frames[2] : null;
}

function getHeaderDonateUrl() {
    var theCacheBuster = randomString(9,"0123456789");
    window.open("//oascentral.consumerreports.org/RealMedia/ads/click_lx.ads/cu/header/donateclicktracker/" + theCacheBuster + "/x75/Consumer/Donate_CU_ROS-Header_LI-LO_Clicks_x75/shim.gif/1", "_blank");
}

function getFooterDonateUrl() {
    var theCacheBuster = randomString(9,"0123456789");
    window.open("//oascentral.consumerreports.org/RealMedia/ads/click_lx.ads/cu/cro/footer/donateclicktracker/" + theCacheBuster + "/x76/Consumer/Donate_CU_ROS-Footer_LI-LO_Clicks_x76/shim.gif/1", "_blank");
}

if ((typeof window.postMessage != 'undefined') && (typeof window.addEventListener != 'undefined')) {
    window.addEventListener('message', function(event){
        adjustIFrame(getFrameId(event.data.url), event.data.height);
    }, false);
}

function renderAdSrcs()
{
    x04kill();
    var theRandomOverride = randomString(9, "0123456789");
    var theIFrameList = jQuery("iframe .adtag");
    var thePositionList = "";
    if(theIFrameList.length > 0)
    {
        thePositionList = theIFrameList[0].getAttribute("id");
        for (var x = 1; x < theIFrameList.length; x++)
        {
            thePositionList += "," + theIFrameList[x].getAttribute("id");
        }
    }
    for (var x = 0; x < theIFrameList.length; x++)
    {
        var theAd = theIFrameList[x];
        var theAdCookie = readCookie("userAds");
        if (!theAdCookie)
        {
            theAdCookie = "";
        }
        else
        {
            if ((theAdCookie.length >= 1) && (theAdCookie.substr(theAdCookie.length - 1) != '&'))
            {
                theAdCookie = theAdCookie + "&";
            }
        }
        var thePath = "";
        if(theAd.getAttribute("path")){
            thePath = theAd.getAttribute("path");
        }else{
            thePath = buildPath();
        }
        var theSrc = "//oascentral.consumerreports.org/RealMedia/ads/adstream_sx.ads/" + thePath +
            "/1" + theRandomOverride + "@" + thePositionList + "!" + theAd.getAttribute("id") + "?url=" + encodeURIComponent(window.location.href) + "&" +
            theAdCookie + "article=112345&XE&status=active" + OAS_rdl + "&if_nt_CookieAccept=" + OAS_CA + "&XE"+ "&tier=" + (CRUserInfo.getTier() ? CRUserInfo.getTier() : "anonymous");
        // OAS_rdl and OAS_CA are set by https://oascentral.consumerreports.org/Scripts/oas_analytics.js
        theAd.setAttribute("src", theSrc);
    }
}

/*
 Developed by Robert Nyman, http://www.robertnyman.com
 Code/licensing: http://code.google.com/p/getelementsbyclassname/
 */

var getElementsByClassName = function (className, tag, elm){
    if (document.getElementsByClassName) {
        getElementsByClassName = function (className, tag, elm) {
            elm = elm || document;
            var elements = elm.getElementsByClassName(className),
                nodeName = (tag)? new RegExp("\\b" + tag + "\\b", "i") : null,
                returnElements = [],
                current;
            for(var i=0, il=elements.length; i<il; i+=1){
                current = elements[i];
                if(!nodeName || nodeName.test(current.nodeName)) {
                    returnElements.push(current);
                }
            }
            return returnElements;
        };
    }
    else if (document.evaluate) {
        getElementsByClassName = function (className, tag, elm) {
            tag = tag || "*";
            elm = elm || document;
            var classes = className.split(" "),
                classesToCheck = "",
                xhtmlNamespace = "http://www.w3.org/1999/xhtml",
                namespaceResolver = (document.documentElement.namespaceURI === xhtmlNamespace)? xhtmlNamespace : null,
                returnElements = [],
                elements,
                node;
            for(var j=0, jl=classes.length; j<jl; j+=1){
                classesToCheck += "[contains(concat(' ', @class, ' '), ' " + classes[j] + " ')]";
            }
            try {
                elements = document.evaluate(".//" + tag + classesToCheck, elm, namespaceResolver, 0, null);
            }
            catch (e) {
                elements = document.evaluate(".//" + tag + classesToCheck, elm, null, 0, null);
            }
            while ((node = elements.iterateNext())) {
                returnElements.push(node);
            }
            return returnElements;
        };
    }
    else {
        getElementsByClassName = function (className, tag, elm) {
            tag = tag || "*";
            elm = elm || document;
            var classes = className.split(" "),
                classesToCheck = [],
                elements = (tag === "*" && elm.all)? elm.all : elm.getElementsByTagName(tag),
                current,
                returnElements = [],
                match;
            for(var k=0, kl=classes.length; k<kl; k+=1){
                classesToCheck.push(new RegExp("(^|\\s)" + classes[k] + "(\\s|$)"));
            }
            for(var l=0, ll=elements.length; l<ll; l+=1){
                current = elements[l];
                match = false;
                for(var m=0, ml=classesToCheck.length; m<ml; m+=1){
                    match = classesToCheck[m].test(current.className);
                    if (!match) {
                        break;
                    }
                }
                if (match) {
                    returnElements.push(current);
                }
            }
            return returnElements;
        };
    }
    return getElementsByClassName(className, tag, elm);
};

// Donate light-box
jQuery(function ($) {
    var buildUrl = function() {
        var hostName = document.location.hostname.match('consumer\.org|crinfra\.net') ? 'test.cu' : 'cu';
        var pathName = document.location.pathname;
        return hostName + pathName;
    }

    var DONATE_TAG_ID = "x20";
    var PATH = buildUrl();
    var STRUCTURE_TO_CREATE = '<div id="donate-iframe-div" class="donate-iframe-div" style="display:none;">' +
        '<img id="donate-lightbox-close-btn" class="donate-lightbox-close-btn" style="display:none;" src="/etc/designs/cro/application-resources/modules/ratingschart/b_close.png" alt="x">' +
        '<h3>Donate to Consumer Reports</h3>' +
        '<div id="' + DONATE_TAG_ID + '" class="adtag" passthruclass="donate-iframe" path="' + PATH + '" width="420" height="255"></div>' +
        '</div>';

    var animate = function(event) {
        var togglePopup = function() {
            $('#donate-iframe-div, #donate-lightbox-fade, #donate-lightbox-close-btn').toggle();
        }
        if(event.target.id=="donate-lightbox-close-btn") {
            togglePopup();
        } else {
            var s = s_gi(s_account),
                isHeader = jQuery(event.currentTarget).closest(".header").length > 0,
                overrides = {};
            overrides.linkTrackVars='prop48,eVar48';
            if (!!window.isDonatePopup) {
                if(!$('#donate-iframe-div').length) {
                    $(STRUCTURE_TO_CREATE).appendTo('body');
                    $('<div/>',{id:'donate-lightbox-fade'}).addClass('donate-lightbox-black_overlay').css('display', 'none').appendTo('body');
                    $('#donate-lightbox-close-btn').bind('click', animate);
                    if(thePositionList !== '') {
                        thePositionList += ',' + DONATE_TAG_ID;
                    }
                    renderAd(document.getElementById(DONATE_TAG_ID));
                }
                overrides.prop48 = overrides.eVar48 = s.pageName + " | donate_lightbox";
                if(isHeader) {
                    overrides.linkTrackEvents = 'event24,event32';
                    overrides.events='event24,event32';
                }else{
                    overrides.linkTrackEvents = 'event24,event33';
                    overrides.events='event24,event33';
                }
                s.tl(event.target, 'o', 'Donate', overrides);
                togglePopup();
            } else {
                overrides.prop48 = overrides.eVar48 = s.pageName + " | donate_default";
                if(isHeader) {
                    overrides.linkTrackEvents = 'event24,event32';
                    overrides.events='event24,event32';
                    s.tl(event.target, 'o', 'Donate', overrides);
                    getHeaderDonateUrl();
                }else{
                    overrides.linkTrackEvents = 'event24,event33';
                    overrides.events='event24,event33';
                    s.tl(event.target, 'o', 'Donate', overrides);
                    getFooterDonateUrl();
                }
            }
            event.preventDefault();
        }
    }
    $('.donate-lightbox-btn').attr("href","javascript:void()");
    $('.donate-lightbox-btn').bind('click', animate);
});


//collapsibleText component script
jQuery(function($){
    $('.collapsible-text-content').removeClass('hidden').addClass('collapse');
    $('.collapsible-text-label.collapsible').click(function(){
        $(this).siblings('.collapsible-text-content').toggle(200).end().toggleClass('collapsed');

        var target = event.currentTarget || event;
        var sourceTarget = event.target || target;
        var track = jQuery(target).text().trim();
        var s = s_gi(s_account);
        var overrides = {};
        var pattern = /collapsed$/ig;
        var classValue = (sourceTarget != null) ? $(this).attr("class") : null;

        if(track!=undefined && (classValue != null && pattern.test(classValue))){
            overrides.linkTrackVars='prop48,eVar48';
            overrides.prop48 = overrides.eVar48 = s.pageName + " | " + track;
            s.tl(sourceTarget, 'o', track, overrides);
        }

        return false;
    });
});

//predictive search tracking on mobile
jQuery(function($){
    var isMobile = (typeof CUMobileSettings === "object");

    if(isMobile){
        $('.results-outer-box').on("click", ".results-inner-box > a", function (e) {
                                        var keyword = $(e.currentTarget).text();
                                        CUMobile.setCookie('predictSearch', keyword, 1);
                                  });
    }
});
var headerElementsTracking = function(source){
    if (typeof window.s_account === 'undefined') {
        console.warn('Tracking has been suppressed: s_account is not defined');
        return;
    }

    var s = s_gi(s_account),
        overrides = {},
        target = source.currentTarget || source,
        sourceTarget = source.target || target,
        track = jQuery(sourceTarget).data("track") || jQuery(target).data("track"),
        trackTitle = jQuery(sourceTarget).data("track-title") || jQuery(target).data("track-title");
    if(track!=undefined){
        overrides.linkTrackVars='prop48,eVar48,eVar22';
        overrides.linkTrackEvents = 'event32';
        overrides.events='event32';
        overrides.prop48 = overrides.eVar48 = s.pageName + " | " + track;
        overrides.eVar22 = "+1";
        if (trackTitle === 'Search' || trackTitle === 'Search_mobile') {
            var searchElementId = jQuery(sourceTarget).data("search-element") || jQuery(target).data("search-element") || 'search';
            var searchKeyword = jQuery(sourceTarget).data("search-keyword") || jQuery(target).data("search-keyword") || jQuery('#'+searchElementId).val();
            if (searchKeyword) searchKeyword = searchKeyword.toLowerCase();
            overrides.prop10 = overrides.eVar2 = searchKeyword;
            overrides.prop11 = s.pageName;
            overrides.linkTrackVars += ',prop10,prop11,eVar2';
            if (trackTitle === 'Search_mobile') {
                markAsMobile(overrides, sourceTarget);
            }
            //for responsive CFA pages
            if (( typeof CUMobile === "object") && (CUMobile.getDeviceViewByScreenSize() === 'mobile')) {
                markAsMobile(overrides, sourceTarget);
                trackTitle = 'Search_mobile';
            }
            overrides.linkTrackEvents += ',event2';
            overrides.events += ',event2';
        }
        s.tl(sourceTarget, 'o', trackTitle, overrides);
    }
};
//this file at times is enclosed in closure (e.g. globalNavigationStandalone.js), export headerElementsTracking as global var
if(typeof window.headerElementsTracking === 'undefined'){
    window.headerElementsTracking = headerElementsTracking;
}

var markAsMobile = function( overrides, sourceTarget ) {
     delete overrides.eVar67;
     overrides.linkTrackVars = overrides.linkTrackVars.replace(',eVar67','');
     overrides.linkTrackVars += ',prop58,eVar58';
     overrides.prop58 = overrides.eVar58 = 'Mobile';
}
var footerElementsTracking = function(event){
    var s = s_gi(s_account),
        overrides = {},
        track = "-footer",
        trackTitle = "Footer";
    if(jQuery(event.currentTarget).prop("tagName") == "IMG"){
        trackTitle = jQuery.trim(jQuery(event.currentTarget).attr("alt"));
    }else{
        trackTitle = jQuery.trim(jQuery(event.currentTarget).text());
    }
    track = trackTitle.toLowerCase().replace(/\s/g, "_") + "-footer";
    overrides.linkTrackVars='prop48,eVar48';
    overrides.linkTrackEvents = 'event33';
    overrides.events='event33';
    overrides.prop48 = overrides.eVar48 = s.pageName + " | " + track;
    if ( ( typeof CUMobile === "object") && (CUMobile.getDeviceViewByScreenSize() === 'mobile'))  {
        overrides['prop58'] = overrides['eVar58'] = "Mobile";
        overrides.linkTrackVars += ',prop58,eVar58';
    }
    if(trackTitle != "Donate")
        s.tl(event.target, 'o', trackTitle, overrides);
};

//header/footer track events
jQuery(function($){
    $("#sign-up-myaccount, #sign-up-cservice, #sign-up-sitefeatures, #sign-in-link, #sign-in-link2, #sign-in-btn," +
        " .sign-in-link, #AZ-link a, .nvm-nav-item a, .logo a, div#main_nav a, #subscribe-link,"+
        ".ifCRO a, .ifHealth a, .ifCBDP a, .ifAPSNew a, .ifAPSUsed a, .ifCRMag a,"+
        ".ifOnHealth a, .ifMoneyAdviser a, .ifShopSmart a, #user_acount_button, #sign-in-manage-account, #main-sub-nav a,"+
        "#typeahead .input-box-button").click(headerElementsTracking);

    if ($.fn.live)
        $(".results-inner-box a, .results-inner-box-active a").live("click", headerElementsTracking);
    else
        $(".results-inner-box a, .results-inner-box-active a").on("click", headerElementsTracking);
    $(".footer .buttons img").click(footerElementsTracking);
});

(function () {
    var loadAd = function(ad) {
        if (CRUserInfo && typeof CRUserInfo.ready === "function") {
            CRUserInfo.ready().then(function () {
                if(window.adPath){
                    addJsAdToHeadSection(ad.id, window.adPath, ad.platform, ad.type);
                }else{
                    addJsToHeadSectionByPath(ad.id, ad.platform);
                }
            });
        } else {
            //fallback logic, for with current front-end architecture there is no guarantee that CURsorInfo is defined at this point
            jQuery(document).bind('userInfo_ready', function () {
                if(window.adPath){
                    addJsAdToHeadSection(ad.id, window.adPath, ad.platform, ad.type);
                }else{
                    addJsToHeadSectionByPath(ad.id, ad.platform);
                }
            });
        }
    };
    if(isShowAds()){
        var ads = [
            { id: 'x53' }, // Global special offer sticky footer
            { id: 'x97' }, // Global alerts
            { id: 'x14', platform: 'mobile' } //CTA alert
        ];
        ads.forEach(loadAd);
    }
}) ();
$(document).on('click', '.TRUSTeWidget_Tab_link', function () {

    if (!window.TRUSTeWidget) {
        $.getScript('https://preferences.truste.com/webservices/js?domain=consumersunion.org&type=consumer', function () {
            window.TRUSTeWidget.Tab.link();
        });

    } else {
        window.TRUSTeWidget.Tab.link();
    }
    return false;
});
//show logout button for mobile footer

(function ($) {
    if (CRUserInfo && typeof CRUserInfo.ready === 'function') {
            CRUserInfo.ready()
              .then(buttonsHandler)

    } else {
        $(document).on('userInfo_ready', buttonsHandler);
    }

    function showLogoutButton() {
        var buttons = $('.footer-mobile-account-btn');
        if (CRUserInfo.hasData() && CRUserInfo.getName()) {
            buttons.find('.global-footer-subscribe-btn').hide().end()
                .find('.global-footer-account-btn').hide().end()
                .find('.global-footer-account-btn.logOut').show();
        }
    }
    function hideSubscribeButton() {
        if(CRUserInfo && CRUserInfo.hasAtLeastMembershipTier('EMAIL')) {
            $('.subscribe-footer-btn, .subscribe-footer-btn + br').hide();
        }
    }

    function buttonsHandler () {
        showLogoutButton();
        hideSubscribeButton();
    }


    $(window).bind("pageshow", function (event) {
        var isSafari9 = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor) && /Version\/(9|10)\./.test(navigator.userAgent);
        if(!isSafari9) return;

        if ( (typeof event.originalEvent === 'object') && (event.originalEvent.persisted) ) {
            window.location.reload()
        }
    });
})(jQuery);
