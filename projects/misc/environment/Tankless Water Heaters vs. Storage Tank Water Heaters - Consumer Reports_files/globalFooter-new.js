//show logout button for mobile footer

(function ($) {
    if (CRUserInfo && typeof CRUserInfo.ready === 'function') {
            CRUserInfo.ready()
              .then(buttonsHandler)

    } else {
        $(document).on('userInfo_ready', buttonsHandler);
    }

    function toggleButtons() {
        if(CRUserInfo && CRUserInfo.itsJson.tier === 'ANONYMOUS') {
            $('.footer__join').show();
        } else {
          $('.footer__donate').show();
        }
    }

    function buttonsHandler () {
      $('.footer__join, .footer__donate').hide();
      toggleButtons();
    }


    $(window).bind("pageshow", function (event) {
        var isSafari9 = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor) && /Version\/(9|10)\./.test(navigator.userAgent);
        if(!isSafari9) return;

        if ( (typeof event.originalEvent === 'object') && (event.originalEvent.persisted) ) {
            window.location.reload()
        }
    });


    $(document).ready(function() {
        if (window.innerWidth < 768) {
          function toggleButton( item ) {
            $(item).toggleClass('global-footer-col-name--top');
            $(item).next('div').find('ul').toggle();
          }

          $('.global-footer-col-name').on('click', function() {toggleButton(this)});
          toggleButton('.footer__columns .col-xs-12:first-child .global-footer-col-name');
        }
        if (window.innerWidth <= 768) {
            var mobile_donation = 'https://donateconsumers.org/ea-action/action?ea.client.id=1926&ea.campaign.id=55501&sourcecode=9021000145&en_txn6=9021000145';
            $('.footer__donate, a[title="Make a Donation"]').attr('href', mobile_donation)
        }
    })

})(jQuery);
var ErrorTracker = ErrorTracker || {};
ErrorTracker.ERROR_TYPE = ErrorTracker.ERROR_TYPE || {};
ErrorTracker.ERROR_TYPE.CHECKED = "CHECKED";
ErrorTracker.ERROR_TYPE.UNCHECKED = "UNCHECKED";
ErrorTracker.ERROR_TYPE.COOKIE_KEY = "ERROR_REPORT";

/* Start Global footer scripts */
(function ($) {
  ErrorTracker.setCookie = function(_n, _v, _d) { var _e = new Date();_e.setDate(_e.getDate() + _d);_c = escape(_v) + ((_e == null) ? "" : ";domain=" + '.'+/[^\.]+\.(\w+)$/.exec(document.domain)[0] + ";path=/;expires=" + _e.toUTCString());document.cookie = _n + "=" + _c;}
  ErrorTracker.getCookie = function(_n) {var _v = document.cookie;_s = _v.indexOf(" " + _n + "=");if (_s == -1) {_s = _v.indexOf(_n + "=");}if (_s == -1) {_v = null;return null;} else {_s = _v.indexOf("=", _s) + 1;_e = _v.indexOf(";", _s);if (_e == -1) {_e = _v.length;}_v = unescape(_v.substring(_s, _e));{return _v;}}}
  /*
   * Error information will be sent using the following request paramaters
   * type = CHECKED|UNCHECKED
   * message= if CHECKED enumeration of case
   * event= the user event that resulted in the error (top of the stack) | PAGE_LOAD
   * uri= current uri
   * */

  ErrorTracker.sendErrorReport = function(type, message, event, uri){
    // try{
    //   var errorReport = ErrorTracker.getCookie(ErrorTracker.ERROR_TYPE.COOKIE_KEY);
    //   errorReport = errorReport == null ? 0 : parseInt(errorReport);
    //   if(errorReport <= 10){
    //     $.ajax({
    //       url: (window.globalNavDomain ? window.globalNavDomain : '') + '/mweb-x-monitor.html',
    //       data: {
    //         type: type,
    //         message: !!message ? message : "",
    //         event: !!event ? event : "",
    //         uri: !!uri ? uri : window.location.pathname
    //       }
    //     });
    //     ErrorTracker.setCookie(ErrorTracker.ERROR_TYPE.COOKIE_KEY, ++errorReport ,1);
    //   }
    // }catch(e){
    //   console.error('error[ '+ e.name +']: '+ e.message);
    // }
  };

  window.onerror = function(errorMsg, url, lineNumber){
    ErrorTracker.sendErrorReport(ErrorTracker.ERROR_TYPE.UNCHECKED, "", errorMsg+" ["+url+"]["+lineNumber+"]");
  };
})(jQuery);
/* End Global footer scripts */
// moved from globalHeader.js to preserve compatibility
(function(global, factory) {
	factory(
		global,
		global.document,
		global.jQuery,
		function fallback() {
			return {
				trackPageLink: function(linkName, linkLocation, linkReferrer, linkPrefix, prop72) {
					if (typeof global.s_gi == 'undefined' || global.s_account === 'undefined') {
						console.warn('s_gi or s_account is not defined');
						return;
					}
					var s = global.s_gi(s_account);
					var prop58 = s.prop58;
					prop72 || (prop72 = s.prop72);
					var prop48 = linkName;
					if (linkPrefix != null) {
						if (linkPrefix == 'social') {
							this.trackEvent(s, 'event3');
							s.eVar5 = linkName;
						} else {
							prop48 = linkPrefix + '-' + prop48;
						}
					}

					if (linkLocation != null)
						prop48 = prop48 + '-' + linkLocation;
					if (linkReferrer != null)
						prop48 = prop48 + '^' + linkReferrer;
					prop48 = s.pageName + ' | ' + prop48;
					s.prop48 = s.eVar48 = prop48;

					s.linkTrackVars = 'prop48,eVar48';
					if(prop72 != null){
						s.prop72 = s.eVar72 = prop72;
					}
					if (s.prop72)
						s.linkTrackVars += ',prop72,eVar72';

					if (linkName == 'donate')
						this.trackEvent(s, 'event24');

					if (linkLocation == 'footer')
						this.trackEvent(s, 'event33');
					if (prop58) {
						s.prop58 = s.eVar58 = prop58;
						s.linkTrackVars += ',prop58,eVar58';
					}
					s.tl(this, 'o', linkName);
				},
				trackEvent: function(s, event) {
					if (s.linkTrackVars.indexOf('events') == -1)
						s.linkTrackVars += s.linkTrackVars.length > 0 ? ',events' : 'events';
					if (s.linkTrackEvents.indexOf(event) == -1)
						s.linkTrackEvents += s.linkTrackEvents.length > 0 ? ',' + event : event;
					if (s.events.indexOf(event) == -1)
						s.events = s.apl(s.events, event, ',');
				}
			}
		}
	);
}(typeof window !== 'undefined' ? window : this, function(_w, _d, $, fallback) {

	if (!$) return;

	var trackPageLinkHandler = function () {
		var tracking = new Function('return ' + $(this).attr('data-trackPageLink'))();

		if (typeof _w.trackPageLink === 'function') {
			_w.trackPageLink.apply(_w, tracking);
		} else {
			var utils = fallback();
				utils.trackPageLink.apply(utils, tracking);
		}
	};

	$(_d).on('click', '[data-trackPageLink]', trackPageLinkHandler);

	$(function() {
		$('select').each(function() {
			var $select = $(this),
				$options = $select.find('[data-trackPageLink]');
			if($options.length) {
				$options.unbind('click', trackPageLinkHandler);
				$select.change(function () {
					trackPageLinkHandler.apply($select.find("option:selected"));
				});
			}
		});
	});

}));
