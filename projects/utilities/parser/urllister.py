from sgmllib import SGMLParser

class URLLister(SGMLParser):
    def reset(self):                              
        SGMLParser.reset(self)
        self.urls = []

    def start_a(self, attrs, mytag='href'):
        href = [v for k, v in attrs if k==mytag]
        if href:
            self.urls.extend(href)
