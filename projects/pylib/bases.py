'''Class for arbitrary base arithmetic.  Does division in the case of prime bases'''

class Digit(object):
    class IncompatibleBases(Exception):
        pass
    def __init__(self, s, base):
        self.n = s % base
        self.b = base
    def __add__(self, other):
        try:
            if self.base != other.base:
                raise IncompatibleBases
        except AttributeError:
            pass
        try:
            return Digit(self.n + other.n, self.b)
        except AttributeError:
            pass
        return Digit(self.n + other, self.b)
    def __mul__(self, other):
        try:
            if self.base != other.base:
                raise IncompatibleBases
        except AttributeError:
            pass
        try:
            return Digit(self.n * other.n, self.b)
        except AttributeError:
            pass
        return Digit(self.n * other, self.b)

    def __sub__(self, other):
        try:
            if self.base != other.base:
                raise IncompatibleBases
        except AttributeError:
            pass
        try:
            return Digit(self.n - other.n, self.b)
        except AttributeError:
            pass
        return Digit(self.n - other, self.b)

    
    def __str__(self):
        return str(self.n)
    def __repr__(self):
        return "Digit(" + self.__str__() + ", " + str(self.b) + ")"
    def write(self):
        print(self.__str__())
    __rmul__ = __mul__
    __radd__ = __add__



class Base(object):
    
