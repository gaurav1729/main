def Prime_Factors(n):
    """returns a dict whose keys are the prime factors, whose values
    are their powers in the factorization.  returns an empty dict for
    an input of unity.  need to raise exceptions for input < 1 or non-int"""
    if n < 1:
        raise TypeError("input must be an int greater than or equal to 1")
    
    out_dict = dict()
    i = 2
    while (n > 1):
        if n % i == 0:
            try:
                out_dict[i] += 1
            except KeyError, ke:
                out_dict[i] = 1
            n = n/i
        else:
            i += 1
    return out_dict
