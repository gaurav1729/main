class Weighted_Choice(object):
    class MissedTarget(Exception):
        pass

    def __init__(self, in_dict):
        """the dict is from a set of elements to their relative probability
        for being chosen"""
        weights = [w for o,w in in_dict.items()]
        self.ritems  = [o for o,w in in_dict.items()]
        self.tot_weight = sum(weights)
        self.cum_weight = [sum(weights[:i+1]) for i in range(len(weights))]

    def choose(self):
        """choose randomly with probability given in the constructor"""
        import random
        import itertools
        random.seed()
        rand = random.uniform(0, self.tot_weight)

        for i in range(len(self.cum_weight)):
            if self.cum_weight[i] >= rand:
                return self.ritems[i]
        # should never get here:
        raise(MissedTarget("shouldn't reach this"))
