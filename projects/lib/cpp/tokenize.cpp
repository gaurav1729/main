#include "/home/gaurav/lib/cpp/tokenize.h"

vector<string> tokenize(const char c, const string & s)
{
  vector<string> tokens;

  string::size_type start = 0;
  string::size_type finish;

  for(;;)
    {
      start = s.find_first_not_of(c, start);
      if ( start == string::npos )
        break;

      finish = s.find_first_of(c, start);
      if ( finish == string::npos )
        {
          tokens.push_back( s.substr(start, s.length()-start) );
          break;
        }
      tokens.push_back( s.substr(start, finish-start) );
      start = finish;
      
    } //   for(;;)

  return tokens;
} // vector<string> tokenize(char c, string s)
