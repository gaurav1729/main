#!/usr/bin/env python

## see writeup.txt
## this particular one currently only calculates the 2-level imbalance
##::::::::::::::::::::
##:: setup

import sys
import numpy as np
import math
import pylab as pl
from optparse import OptionParser

printp = False


##::::::::::::::::::::
##:: subroutines

def calc_prime_factors(n, pfdict = None):
    print("n is : " + str(n))
    print(str(pfdict))
    print("")
    if pfdict == None:
        pfdict = dict()
    if n == 1:
        print(" n is 1 ")
        return pfdict
    
    for i in range(2,n+1):
        print("    i is : " + str(i))
        if n%i == 0:
            try:
                pfdict[i] += 1
            except KeyError, ke:
                pfdict[i] = 1

            pfdict = calc_prime_factors(n/i)
            break
    print("before return")
    return pfdict
##:::::::::

def closest_factors(n):
    sqroot = math.sqrt(n)
    i  = int(math.ceil(sqroot))
    
    while(n%i != 0):
        i += 1
    return(n/i, i)
        

##::::::::::::::::::::
##:: main loop

def calc_imbalances(nmax):

    imbalances = np.zeros(nmax+1)

    for i in range(2, nmax+1):
        if printp:
            print("running for " + str(i))
        lower, higher = closest_factors(i)
        if printp: 
            print("  " + str(lower) + " " + str(higher))
        imbalances[i] = (float(higher)-float(lower)) / i
        # or float(higher)/(float(lower) * i)
        # of (  float(higher)/float(lower) - 1 )/ (i-1)
        if printp:
            print("  " + str(imbalances[i]))
    # end of for loop
    pl.plot(imbalances)
    pl.show()
    return imbalances

if __name__ == '__main__':
    print("in main")
    print(str(sys.argv))
    nmax = int(sys.argv[1])

    calc_imbalances(nmax)

##::::::::::::::::::::
    
    
## parsing
## zooming in and set x y limits
## heat map

## difference between higher and lower

## non 2-level (), consider (a^2 + b^2) or sqrt(a^2 + b^2) as a measure
## try product of the differences between each factor and the nth root of i
##   with perhaps an nth root of the product, but would then have to normalize it somehow.
##   also, it would be nice to be able to see 2 * p, 3 * p, etc.
## 

## is it fractal if you change x and y limits together in some way?
