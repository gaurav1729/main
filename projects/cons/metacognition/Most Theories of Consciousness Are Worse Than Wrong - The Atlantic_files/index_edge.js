function getParameterByName(name){
              name = name.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
              var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
              return results === null ? "" :
                decodeURIComponent(results[1].replace(/\+/g, " "));
          }

var clickTag = getParameterByName("exit");

function clickthrough() {
    window.open(window.clickTag)
     
}

(function(compId) {
    var _ = null,
        y = true,
        n = false,
        x43 = '109px',
        e22 = '${copy1}',
        x35 = '176px',
        x47 = '202px',
        x49 = 'copy1_13-134',
        e20 = '${bgF3}',
        cl = 'clip',
        x69 = 'steph3_136-0',
        x4 = 'rgba(255,255,255,1.00)',
        e15 = '${graphic1}',
        e19 = '${paint2}',
        i = 'none',
        x12 = 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)',
        x27 = '241px',
        x31 = '341px',
        lf = 'left',
        x34 = '179px',
        e18 = '${steph2}',
        x28 = 'auto',
        x59 = 'graphic2_67-72',
        x33 = 'steph1_101-15',
        e16 = '${graphic2}',
        x50 = '112px',
        x10 = 'rgba(255,255,255,1)',
        tp = 'top',
        x70 = '170px',
        xc = 'rgba(0,0,0,1)',
        e14 = '${copy2}',
        x26 = '400px',
        x68 = '300px',
        x1 = '6.0.0',
        x2 = '5.0.0',
        x5 = 'rgba(0,0,0,0)',
        x32 = '191px',
        x9 = 'rgba(192,192,192,0.00)',
        e11 = '${paint1}',
        x77 = 'cta',
        x79 = '30px',
        x78 = '166px',
        x29 = 'f1-f2Background',
        x75 = '71px',
        m = 'rect',
        x24 = '0px',
        x74 = '190px',
        x72 = '164px',
        x45 = 'steph2_147-minus9',
        x61 = 'copy2_15-134',
        x73 = 'copy3_13-109',
        x37 = 'graphic1_0-86',
        x66 = 'backgroundF3',
        x55 = '161px',
        x65 = '330px',
        o = 'opacity',
        x3 = '6.0.0.400',
        g = 'image',
        x62 = '126px',
        x38 = '272px',
        x42 = '199px',
        x58 = '93px',
        x57 = '233px',
        e13 = '${f1-2background}',
        x54 = '254px',
        x63 = '43px',
        e23 = '${steph1}',
        x25 = '-50px',
        e21 = '${copy3}',
        x51 = '46px',
        e17 = '${steph3}',
        x53 = 'paint2_0-18',
        x46 = '148px',
        x41 = 'paint1_0-62',
        x39 = '84px';
    var g44 = 'paint1_0-62.png',
        g40 = 'graphic1_0-86.png',
        g67 = 'backgroundF3.jpg',
        g6 = 'product_232-137.png',
        g36 = 'steph1_101-15.png',
        g60 = 'graphic2_67-72.png',
        g48 = 'steph2_147-minus9.png',
        g80 = 'cta_15x206.png',
        g76 = 'copy3_13-109.png',
        g71 = 'steph3_136-0.png',
        g7 = 'border.png',
        g56 = 'paint2_0-18.png',
        g8 = 'logo_15-15.png',
        g52 = 'copy1_13-134.png',
        g30 = 'f1-f2Background.jpg',
        g64 = 'copy2_15-134.png';
    var im = '',
        aud = '',
        vid = '',
        js = '',
        fonts = {},
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [],
        scripts = [],
        symbols = {
            "stage": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        id: 'f1-2background',
                        symbolName: 'f1-2background',
                        t: m,
                        r: ['-100px', '9px', '341', '191', 'auto', 'auto'],
                        o: '1'
                    }, {
                        id: 'bgF3',
                        symbolName: 'bgF3',
                        t: m,
                        r: ['-15px', '0', '300', '191', 'auto', 'auto'],
                        o: '0'
                    }, {
                        id: 'steph1',
                        symbolName: 'steph1',
                        t: m,
                        r: ['-189px', '15', '179', '176', 'auto', 'auto']
                    }, {
                        id: 'graphic1',
                        symbolName: 'graphic1',
                        t: m,
                        r: ['-17px', '86', '272', '84', 'auto', 'auto'],
                        cl: 'rect(48.10546875px 17.49609375px 84px 0px)'
                    }, {
                        id: 'paint1',
                        symbolName: 'paint1',
                        t: m,
                        r: ['-200px', '62', '199', '109', 'auto', 'auto'],
                        cl: 'rect(0px -0.25px 109px -2px)'
                    }, {
                        id: 'copy1',
                        symbolName: 'copy1',
                        t: m,
                        r: ['13', '134', '112', '46', 'auto', 'auto'],
                        o: '0'
                    }, {
                        id: 'paint2',
                        symbolName: 'paint2',
                        t: m,
                        r: ['0px', '18', '254', '161', 'auto', 'auto'],
                        cl: 'rect(0px -1.12890625px 161px -5.484375px)'
                    }, {
                        id: 'steph2',
                        symbolName: 'steph2',
                        t: m,
                        r: ['-150px', '86px', '148', '202', 'auto', 'auto'],
                        autoOrient: false
                    }, {
                        id: 'graphic2',
                        symbolName: 'graphic2',
                        t: m,
                        r: ['59px', '80px', '233', '93', 'auto', 'auto'],
                        cl: 'rect(2px 199.8984375px 14.642578125px 181.65625px)'
                    }, {
                        id: 'copy2',
                        symbolName: 'copy2',
                        t: m,
                        r: ['15', '134', '126', '43', 'auto', 'auto'],
                        o: '0'
                    }, {
                        id: 'steph3',
                        symbolName: 'steph3',
                        t: m,
                        r: ['130px', '0', '164', '191', 'auto', 'auto']
                    }, {
                        id: 'copy3',
                        symbolName: 'copy3',
                        t: m,
                        r: ['13', '109', '190', '71', 'auto', 'auto'],
                        o: '0'
                    }, {
                        id: 'whitebox',
                        t: m,
                        r: ['0px', '191px', '300px', '59px', 'auto', 'auto'],
                        f: [x4],
                        s: [0, xc, i]
                    }, {
                        id: 'product',
                        t: g,
                        r: ['232px', '137px', '52px', '113px', 'auto', 'auto'],
                        f: [x5, im + g6, '0px', '0px']
                    }, {
                        id: 'border',
                        t: g,
                        r: ['0', '0', '300px', '250px', 'auto', 'auto'],
                        f: [x5, im + g7, '0px', '0px']
                    }, {
                        id: 'logo',
                        t: g,
                        r: ['15px', '15px', '46px', '54px', 'auto', 'auto'],
                        f: [x5, im + g8, '0px', '0px']
                    }, {
                        id: 'CTA',
                        symbolName: 'CTA',
                        t: m,
                        r: ['15', '206', '166', '30', 'auto', 'auto'],
                        cu: 'pointer'
                    }, {
                        id: 'clickTag',
                        t: m,
                        r: ['0px', '0px', '300px', '250px', 'auto', 'auto'],
                        cu: 'pointer',
                        f: [x9],
                        s: [0, xc, i]
                    }],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            r: ['null', 'null', '300px', '250px', 'auto', 'auto'],
                            overflow: 'hidden',
                            f: [x10]
                        }
                    }
                },
                tt: {
                    d: 11600,
                    a: y,
                    data: [
                        ["eid9", cl, 350, 306, "easeInQuad", e11, [0, -6.835937976837158, 109, 0],
                            [0, 199.671875, 109, 0], {
                                vt: x12
                            }
                        ],
                        ["eid166", cl, 3749, 157, "easeOutCirc", e11, [0, 199.671875, 109, 0],
                            [0, -0.25, 109, -2], {
                                vt: x12
                            }
                        ],
                        ["eid138", tp, 4656, 3000, "linear", e13, '0px', '9px'],
                        ["eid99", o, 3906, 155, "easeInQuad", e14, '0', '1'],
                        ["eid108", o, 7406, 250, "easeOutQuad", e14, '1', '0'],
                        ["eid118", o, 7494, 162, "easeInQuad", e13, '1', '0'],
                        ["eid130", cl, 350, 0, "linear", e15, [48.10546875, 17.49609375, 84, 0],
                            [48.10546875, 17.49609375, 84, 0], {
                                vt: x12
                            }
                        ],
                        ["eid37", cl, 656, 112, "easeInQuad", e15, [48.10546875, 17.49609375, 84, 0],
                            [48.10546875, 226.96875, 84, 0], {
                                vt: x12
                            }
                        ],
                        ["eid38", cl, 768, 93, "easeInQuad", e15, [48.10546875, 226.96875, 84, 0],
                            [-1.890624761581421, 276.1875, 84, 0], {
                                vt: x12
                            }
                        ],
                        ["eid78", lf, 4347, 3059, "easeOutQuad", e16, '59px', '67px'],
                        ["eid106", lf, 7406, 250, "easeOutQuad", e16, '67px', '378px'],
                        ["eid79", tp, 4347, 3059, "easeOutQuad", e16, '80px', '72px'],
                        ["eid57", lf, 350, 0, "easeOutQuad", e11, '0px', '0px'],
                        ["eid129", lf, 7494, 162, "easeInQuad", e17, '303px', '136px'],
                        ["eid154", lf, 7656, 3444, "easeInSine", e17, '136px', '130px'],
                        ["eid34", lf, 600, 3056, "easeOutCirc", e15, '-17px', '0px'],
                        ["eid50", lf, 3656, 261, "easeOutCirc", e15, '0px', '302px'],
                        ["eid111", lf, 401, 3893, "linear", e13, '0px', '-27px'],
                        ["eid155", lf, 4294, 3362, "linear", e13, '-27px', '-100px'],
                        ["eid145", "location", 3906, 388, "easeInCirc", e18, [
                            [-76, 187, 0, 0, 0, 0, 0],
                            [78.07, 160.71, 217.36, -65.13, 213.1, -63.86, 156.56],
                            [204, 101, 0, 0, 0, 0, 296.42]
                        ]],
                        ["eid147", "location", 4294, 3112, "easeOutCirc", e18, [
                            [204, 101, 0, 0, 0, 0, 0],
                            [221, 91, 0, 0, 0, 0, 19.72]
                        ]],
                        ["eid149", "location", 7406, 250, "easeOutCubic", e18, [
                            [221, 91, 0, 0, 0, 0, 0],
                            [530, 91, 0, 0, 0, 0, 309]
                        ]],
                        ["eid73", cl, 4100, 301, "easeOutQuad", e19, [0, -1.128906011581421, 161, -5.484375],
                            [0, 256.6015625, 161, -5.484375], {
                                vt: x12
                            }
                        ],
                        ["eid172", cl, 7406, 148, "easeOutQuad", e19, [0, 256.6015625, 161, -5.484375],
                            [0, -0.71484375, 161, -5.484375], {
                                vt: x12
                            }
                        ],
                        ["eid122", o, 7494, 162, "easeInQuad", e20, '0.000000', '1'],
                        ["eid112", cl, 656, 3540, "easeInQuad", e16, [2, 199.8984375, 14.642578125, 181.65625],
                            [2, 199.8984375, 15.23828125, 181.4375], {
                                vt: x12
                            }
                        ],
                        ["eid92", cl, 4196, 98, "easeInQuad", e16, [2, 199.8984375, 15.23828125, 181.4375],
                            [2, 235.0498046875, 34.8759765625, 174.671875], {
                                vt: x12
                            }
                        ],
                        ["eid93", cl, 4294, 53, "easeInQuad", e16, [2, 235.0498046875, 34.8759765625, 174.671875],
                            [-0.380126953125, 235.0498046875, 93.150146484375, -1.595703125], {
                                vt: x12
                            }
                        ],
                        ["eid126", o, 7656, 162, "linear", e21, '0.000000', '1'],
                        ["eid44", o, 350, 166, "easeInCirc", e22, '0', '1'],
                        ["eid47", o, 3749, 157, "easeInQuad", e22, '1', '0'],
                        ["eid159", lf, 7656, 3944, "easeInSine", e20, '-30px', '-15px'],
                        ["eid7", lf, 350, 421, "easeInQuad", e23, '-189px', '89px'],
                        ["eid5", lf, 771, 2885, "easeOutCirc", e23, '89px', '101px'],
                        ["eid54", lf, 3656, 261, "easeOutCirc", e23, '101px', '403px']
                    ]
                }
            },
            "f1-2background": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        r: [x24, x25, x26, x27, x28, x28],
                        id: x29,
                        t: g,
                        f: [x5, im + g30, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x31, x32]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "steph1": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x33,
                        r: [x24, x24, x34, x35, x28, x28],
                        f: [x5, im + g36, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x34, x35]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "graphic1": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x37,
                        r: [x24, x24, x38, x39, x28, x28],
                        f: [x5, im + g40, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x38, x39]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "paint1": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x41,
                        r: [x24, x24, x42, x43, x28, x28],
                        f: [x5, im + g44, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x42, x43]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "steph2": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x45,
                        r: [x24, x24, x46, x47, x28, x28],
                        f: [x5, im + g48, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x46, x47]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "copy1": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x49,
                        r: [x24, x24, x50, x51, x28, x28],
                        f: [x5, im + g52, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x50, x51]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "paint2": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x53,
                        r: [x24, x24, x54, x55, x28, x28],
                        f: [x5, im + g56, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x54, x55]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "graphic2": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        r: [x24, x24, x57, x58, x28, x28],
                        id: x59,
                        t: g,
                        f: [x5, im + g60, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x57, x58]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "copy2": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x61,
                        r: [x24, x24, x62, x63, x28, x28],
                        f: [x5, im + g64, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x62, x63]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "bgF3": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        r: [x24, x24, x65, x32, x28, x28],
                        id: x66,
                        t: g,
                        f: [x5, im + g67, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x68, x32]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "steph3": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x69,
                        r: [x24, x24, x70, x32, x28, x28],
                        f: [x5, im + g71, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x72, x32]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "copy3": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x73,
                        r: [x24, x24, x74, x75, x28, x28],
                        f: [x5, im + g76, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x74, x75]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            },
            "CTA": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: g,
                        id: x77,
                        r: [x24, x24, x78, x79, x28, x28],
                        f: [x5, im + g80, x24, x24]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x78, x79]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            }
        };
    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);
})("EDGE-18193410");
(function($, Edge, compId) {
    var Composition = Edge.Composition,
        Symbol = Edge.Symbol;
    Edge.registerEventBinding(compId, function($) {
        //Edge symbol: 'stage'
        (function(symbolName) {
            Symbol.bindElementAction(compId, symbolName, "${CTA}", "click", function(sym, e) {});
            //Edge binding end
            Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {});
            //Edge binding end
            Symbol.bindElementAction(compId, symbolName, "${clickTag}", "click", function(sym, e) {});
            //Edge binding end
        })("stage");
        //Edge symbol end:'stage'

        //=========================================================

        //Edge symbol: 'f1-2background'
        (function(symbolName) {})("f1-2background");
        //Edge symbol end:'f1-2background'

        //=========================================================

        //Edge symbol: 'steph1'
        (function(symbolName) {})("steph1");
        //Edge symbol end:'steph1'

        //=========================================================

        //Edge symbol: 'graphic1'
        (function(symbolName) {})("graphic1");
        //Edge symbol end:'graphic1'

        //=========================================================

        //Edge symbol: 'paint1'
        (function(symbolName) {})("paint1");
        //Edge symbol end:'paint1'

        //=========================================================

        //Edge symbol: 'steph2'
        (function(symbolName) {})("steph2");
        //Edge symbol end:'steph2'

        //=========================================================

        //Edge symbol: 'copy1'
        (function(symbolName) {})("copy1");
        //Edge symbol end:'copy1'

        //=========================================================

        //Edge symbol: 'paint2'
        (function(symbolName) {})("paint2");
        //Edge symbol end:'paint2'

        //=========================================================

        //Edge symbol: 'graphic2'
        (function(symbolName) {})("graphic2");
        //Edge symbol end:'graphic2'

        //=========================================================

        //Edge symbol: 'copy2'
        (function(symbolName) {})("copy2");
        //Edge symbol end:'copy2'

        //=========================================================

        //Edge symbol: 'bgF3'
        (function(symbolName) {})("bgF3");
        //Edge symbol end:'bgF3'

        //=========================================================

        //Edge symbol: 'steph3'
        (function(symbolName) {})("steph3");
        //Edge symbol end:'steph3'

        //=========================================================

        //Edge symbol: 'copy3'
        (function(symbolName) {})("copy3");
        //Edge symbol end:'copy3'

        //=========================================================

        //Edge symbol: 'CTA'
        (function(symbolName) {})("CTA");
        //Edge symbol end:'CTA'
    })
})(AdobeEdge.$, AdobeEdge, "EDGE-18193410");