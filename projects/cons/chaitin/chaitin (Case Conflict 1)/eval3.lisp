[[[ LISP semantics defined in LISP ]]]
[
  Permissive LISP:
  head & tail of atom = atom,
  join of x with nonzero atom = x,
  initially all atoms evaluate to self,
  only depth exceeded failure!

  (Vsed) =
  value of S-expression s in environment e within depth d.
  If a new environment is created it is displayed.

  d is a natural number which must be decremented
  at each call.  And if it reaches zero, evaluation aborts.
  If depth is exceeded, V returns a special failure value $.
  Evaluation cannot fail any other way!
  Normally, when get value v, if bad will return it as is:
  /=$vv
  To stop unwinding,
  one must convert $ to ? & wrap good v in ()'s.
]
& (Vsed)
  /. s : (Ae) /.e s /=s+e+-e (A--e)
                 [ A is "Assoc" ]
        (Ae)     [ evaluate atom; if not in e, evals to self ]
  : f (V+sed)    [ evaluate the function f ]
  /=$ff          [ if evaluation of function failed, give up ]
  /=f"' +-s      [ do "quote" ]
  /=f"/ : p (V+-sed) /=$pp /=0p (V+---sed) (V+--sed)
                 [ do "if" ]
  : (Wl) /.ll : x (V+led) /=$xx : y (W-l) /=$yy *xy
                 [ W is "Evalst" ]
  : a (W-s)      [ a is the list of argument values ]
  /=$aa          [ evaluation of arguments failed, give up ]
  : x +a         [ pick up first argument ]
  : y +-a        [ pick up second argument ]
  /=f". .x       [ do "atom" ]
  /=f"+ +x       [ do "head" ]
  /=f"- -x       [ do "tail" ]
  /=f", ,x       [ do "out"  ]
  /=f"= =xy      [ do "eq"   ]
  /=f"* *xy      [ do "join" ]
  /.d   $        [ fail if depth already zero ]
  : d   -d       [ decrement depth ]
  /=f"! (Vx()d)  [ do "eval"; use fresh environment ]
  /=f"?          [ do "depth-limited eval" ]
     : (Lij) /.i1 /.j0 (L-i-j)
                 [ natural # i is less than or equal to j ]
     /(Ldx) : v (Vy()d) /=$vv *v()
                 [ old depth more limiting; keep unwinding ]
            : v (Vy()x) /=$v"? *v()
                 [ new depth limit more limiting;
                   stop unwinding ]
                 [ do function definition ]
  : (Bxa) /.xe *+x*+a(B-x-a)
                 [ B is "Bind" ]
  (V+--f,(B+-fa)d) [ display new environment ]

[ Test function (Cxy) = concatenate list x and list y. ]

[ Define environment for concatenation. ]
& E '( C &(xy) /.xy *+x(C-xy) )
(V '(C'(ab)'(cd)) E '())
(V '(C'(ab)'(cd)) E '(1))
(V '(C'(ab)'(cd)) E '(11))
(V '(C'(ab)'(cd)) E '(111))

