(***** EQ.M *****)

fulloutput = If[ fulloutput, True, False, False ]
fn = InputString["fn of fn.xrm file = "]
t0 = SessionTime[]
p = Get[fn<>".xrm"] (* read in program *)
o = OpenWrite[fn<>".eq",PageWidth->62]
Format[LineBreak[_]] = ""
Format[Continuation[_]] = " "
print[x_] := (Print@ x; Write[o,OutputForm@ x])

print@
 "********** program"
print@
 Short[InputForm@ p,10]

(* get set of labels of all instructions in program *)

labels = #[[1]]& /@ p

If[
 Length@ Union@ labels != Length@ p,
 print@
 "Duplicate labels!"
]

(* get set of all registers in program *)

registers = Union@ Flatten@ (Drop[#,2]& /@ p)
registers = Cases[registers,_Symbol]
registers = Complement[registers,labels]

eqs = {}
put[x_] := (Write[o,x]; eqs = {eqs,x};)
Write[o,OutputForm@
 "********** <='s & =='s as they are generated"
 ]

{
 (* generate equations for base q *)
 totalinput == Plus@@ (input[#]& /@ registers),
 numberofinstructions == Length@ p,
 longestlabel == (* with ( ) around label for jump's *)
 Max@ (StringLength["("<>ToString[#]<>")"]& /@ labels),
 q == 256^
 (totalinput+ time+ numberofinstructions+ longestlabel+ 1),
 qminus1 + 1 == q,
 1 + q i == i + q^time,
 (* label equations *)
 (# <= i)& /@ labels,
 i == Plus@@ labels,
 (* equations for starting & halting *)
 1 <= p[[1,1]],
 q^time == q Plus@@ Cases[p,{l_,halt}->l]
} // put

(* generate flow equations *)

Evaluate[ next /@ labels ] = RotateLeft@ labels

{
 Cases[ p, {l_,goto,l2_} -> q l <= l2 ],
 Cases[ p, {l_,jump,a_,l2_} -> q l <= l2 ],
 Cases[ p, {l_,goback,a_} ->
 (
 { goback <= x,
 goback <= qminus1 l,
 x <= goback + qminus1 (i-l)
 } /.
 goback -> goback[l] /.
 { {x -> a}, {x -> nextic} }
 )
 ],
 Cases[ p, {l_,eq|eqi,a_,b_,l2_} ->
 {
 q l <= next[l] + l2,
 q l <= next[l] + q eq[a,b]
 }
 ],
 Cases[ p, {l_,neq|neqi,a_,b_,l2_} ->
 {
 q l <= next[l] + l2,
 q l <= l2 + q eq[a,b]
 }
 ],
 Cases[
 DeleteCases[ p,
 {_,halt|goto|jump|goback|eq|eqi|neq|neqi,___}
 ],
 {l_,__} -> q l <= next[l]
 ],
 {
 ic == Plus@@ ((# "("<>ToString[#]<>")")& /@ labels),
 q nextic <= ic,
 ic <= q nextic + qminus1
 }
} // put

(* generate compare equations *)

(
 Cases[ p, {l_,eq|neq,a_,b_,_} ->
 compare[a,b,char[a],char[b]]
 ]
 ~Union~
 Cases[ p, {l_,eqi|neqi,a_,b_,_} ->
 compare[a,b,char[a],b i]
 ]
) /.
 compare[a_,b_,charA_,charB_] ->
{
 {
 eq[a,b] <= i,
 2 eq[a,b] <= ge[a,b] + ge[b,a],
 ge[a,b] + ge[b,a] <= 2 eq[a,b] + i
 },
 {
 geXY <= i,
 256 geXY <= 256 i + charX - charY,
 256 i + charX - charY <= 256 geXY + 255 i
 } /.
 {
 {geXY -> ge[a,b], charX -> charA, charY -> charB},
 {geXY -> ge[b,a], charX -> charB, charY -> charA}
 }
} // put

(* generate auxiliary register equations *)

(* set target t to source s at label l *)
set[t_,s_,l_] :=
 {
 set <= s,
 set <= qminus1 l,
 s <= set + qminus1 (i - l)
 } /.
 set -> set[t,l]

{
 Cases[ p, {l_,set,a_,b_} ->
 set[a,b,l]
 ],
 Cases[ p, {l_,seti,a_,b_} ->
 set[a,b i,l]
 ],
 Cases[ p, {l_,left,a_,b_} ->
 {
 set[a,256a+char[b],l],
 set[b,shift[b],l]
 }
 ],
 Cases[ p, {l_,lefti,a_,b_} ->
 set[a,256a+i b,l]
 ],
 Cases[ p, {l_,right,a_} ->
 set[a,shift[a],l]
 ],
 Cases[ p, {l_,jump,a_,_} :>
 set[a,i "("<>ToString[next[l]]<>")",l]
 ]
} // put

(* generate main register equations *)

defs[r_] := defs[r] = Cases[ p,
 {l_,set|seti|left|lefti|right|jump,r,___} |
 {l_,left,_,r}
 -> l ]

(
 Function[ r,
 {
 r <= qminus1 i,
 r + output q^time ==
 input + q (dontset + Plus@@ (set2[r,#]& /@ defs[r])),
 set == Plus@@ defs[r],
 dontset <= r,
 dontset <= qminus1 (i - set),
 r <= dontset + qminus1 set,
 256 shift <= r,
 256 shift <= i (qminus1 - 255),
 r <= 256 shift + 255 i,
 r == 256 shift + char
 } /. ((# -> #[r])& /@
 {input,output,set,dontset,shift,char}) /.
 set2 -> set
 ] /@ registers
) // put

(* all equations and inequalities are now in eqs; *)
(* start processing *)

eqs = Flatten[eqs]

print@
 "********** combined list of <='s & =='s"
print@
 Short[InputForm@ eqs,10]

(* how many ='s, <='s, registers, labels, variables ? *)

print@StringForm[
 "********** `` =='s, `` <='s, `` total",
 neq = Count[eqs,_==_], nle = Count[eqs,_<=_], Length@ eqs
 ]
print@
 "********** now counting variables"

variables =
 eqs /. Plus|Times|Power|Equal|LessEqual -> List

variables =
 DeleteCases[ Flatten@ variables, _String|_Integer ] // Union

print@StringForm[
"********** `` registers, `` labels, `` variables altogether",
Length@ registers, Length@ labels, nvar = Length@ variables
]
Write[o,variables]

(* convert strings to integers *)

alphabet = "\000()" ~StringJoin~
 "ABCDEFGHIJKLMNOPQRSTUVWXYZ" ~StringJoin~
 "abcdefghijklmnopqrstuvwxyz" ~StringJoin~
 "0123456789_+-.',!=*&?/:\"$"

bitmap =
 MapThread[
 #1 -> StringJoin[
 Rest@ IntegerDigits[256 + #2, 2] /.
 {0 -> "0", 1 -> "1"}
 ] & ,
 { Characters@ alphabet, Range[0, StringLength@ alphabet -1] }
 ]

s2i[x_] :=
 ToExpression[
 "2^^" <> StringReverse@ StringReplace[x,bitmap]
 ]

print@
 "********** now converting strings to integers"

eqs = eqs /.
 {eq[x__] -> eq[x], ge[x__] -> ge[x], x_String :> s2i@x}

(* transpose negative terms from rhs to lhs of equation *)

negterms[ (term:(x_Integer _. /; x < 0)) + rest_. ] :=
 term + negterms@ rest

negterms[ _ ] := 0

fix[x_] :=
 (
 x /. l_ == r_ :> l == Expand @ r
 ) /. l_ == r_ :> ( (l - # == r - #)&@ negterms@ r )

(* expand each implication into 7 equations & *)
(* add 9 variables *)

print@
 "********** now expanding <='s"
If[ fulloutput,
 Write[o,OutputForm@
 "********** expand each <="
 ]
]

eqs = eqs /. a_ <= b_ :>
(
 If[ fulloutput, Write[o,a<=b]; Write[o,#]; #, # ]& @
 Module[ {r,s,t,u,v,w,x,y,z},
 {
 fix[r == a],
 fix[s == b],
 t == 2^s,
 (1+t)^s == v t^(r+1) + u t^r + w,
 w + x + 1 == t^r,
 u + y + 1 == t,
 u == 2 z + 1
 }
 ]
)

eqs = Flatten[eqs]

print@
 "********** <='s expanded into =='s"
print@
 Short[InputForm@ eqs,10]
print@
 "********** each <= became 7 =='s and added 9 variables"
print@StringForm[
 "********** so should now have `` =='s and `` variables",
 neq + 7 nle, nvar + 9 nle
 ]
print@StringForm[
 "********** actually there are now `` =='s",
 Length@ eqs
 ]

(* combine all equations into one equation *)

ClearAttributes[ {Plus,Times}, {Orderless,Flat} ]

print@
"********** now combining equations"

eqn =
(
 Plus@@ ( eqs /. l_ == r_ -> (l^2 + r^2) ) ==
 Plus@@ ( eqs /. l_ == r_ -> 2 l r )
)

(***
(* Check that no =='s or <='s have become True or False, *)
(* that no <='s are left, that there are no minus signs, *)
(* and that there is just one == *)
If[ fulloutput,
 trouble[] := (Print@"trouble!"; Abort[]);
 print@
 "********** now checking combined equation";
 eqn /. True :> trouble[];
 eqn /. False :> trouble[];
 eqn /. _<=_ :> trouble[];
 eqn /. x_Integer /; x < 0 :> trouble[];
 eqn[[1]] /. _==_ :> trouble[];
 eqn[[2]] /. _==_ :> trouble[];
]
***)

print@
"********** combined equation"
print@
 Short[InputForm@ eqn,10]
print@StringForm[
 "********** `` terms on lhs, `` terms on rhs",
 Length@ eqn[[1]], Length@ eqn[[2]]
 ]
Write[o,OutputForm@
 "********** combined equation 2"
 ]
Write[o,OutputForm@
 Short[InputForm@ eqn,100]
 ]
Write[o,OutputForm@
 "********** left side"
 ]
Write[o,OutputForm@
 Short[InputForm@ eqn[[1]],50]
 ]
Write[o,OutputForm@
 "********** right side"
 ]
Write[o,OutputForm@
 Short[InputForm@ eqn[[2]],50]
 ]
Write[o,OutputForm@
 "********** first 50 terms"
 ]
Write[o,
 Take[eqn[[1]],+50]
 ]
Write[o,OutputForm@
 "********** last 50 terms"
 ]
Write[o,
 Take[eqn[[2]],-50]]
If[ fulloutput,
 print@
 "********** now writing full equation";
 Write[o,OutputForm@
 "********** combined equation in full"
 ];
 Write[o,
 eqn
 ],
 print@
 "********** now determining size of equation";
 print@StringForm[
 "********** size of equation `` characters",
 StringLength@ ToString@ InputForm@ eqn
 ]
]
print@StringForm[
 "********** elapsed time `` seconds",
 Round[SessionTime[]-t0]
 ]
Print@
 "********** list of =='s left in variable eqs"
Print@
 "********** combined == left in variable eqn"
Print@
 "********** warning: + * noncommutative nonassociative!"
Print@
 "********** (to preserve order of terms & factors in eqn)"
Close@ o

