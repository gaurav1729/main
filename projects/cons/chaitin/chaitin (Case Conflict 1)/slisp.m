(***** SLISP.M *****)

<<frontend.m

(* string lisp interpreter *)

at[x_] := StringLength@ x == 1 || x === "()"

hd[x_] :=
(If[ at@ x, Return@ x ];
 Block[ {p = 0},
 Do[
 p += Switch[ StringTake[x,{i}], "(", +1, ")", -1, _, 0 ];
 If[ p == 0, Return@ StringTake[x,{2,i}] ],
 {i, 2, StringLength@ x}
 ]
 ]
)

tl[x_] :=
(If[ at@ x, Return@ x ];
 Block[ {p = 0},
 Do[
 p += Switch[ StringTake[x,{i}], "(", +1, ")", -1, _, 0 ];
 If[ p == 0, Return[ "("<>StringDrop[x,i] ] ],
 {i, 2, StringLength@ x}
 ]
 ]
)

jn[x_,y_] :=
 If[ StringLength@ y == 1, x, "("<>x<>StringDrop[y,1] ]

eval[e_,,d_] := eval[e,"()",d]

eval[e2_,a_,d2_] :=

Block[ {e = e2, d = d2, f, args, x, y},
 If[
 at@ e,
 Return@
 Which[
 e === hd@ a, hd@tl@ a,
 at@ a, e,
 True, eval[ e, tl@tl@ a, ]
 ]
 ];
 f = eval[ hd@ e, a, d ];
 e = tl@ e;
 Switch[
 f,
 "'", Return@ hd@ e,
 "/", Return@
 If[
 eval[hd@ e,a,d] =!= "0",
 eval[hd@tl@ e,a,d],
 eval[hd@tl@tl@ e,a,d]
 ]
 ];
 args = evlst[e,a,d];
 x = hd@ args;
 y = hd@tl@ args;
 Switch[
 f,
 "+", Return@ hd@ x,
 "-", Return@ tl@ x,
 "*", Return@ jn[x,y],
 ".", Return@ If[ at@ x, "1", "0" ],
 "=", Return@ If[ x === y, "1", "0" ],
 ",", Return@ (print[ "display", output@ x ]; x)
 ];
 If[ d == 0, Throw@ "?" ];
 d--;
 Switch[
 f,
 "!", Return@ eval[x,,d],
 "?", Return@
 If[ size@x < d,
 Catch[ "("<>eval[y,,size@x]<>")" ],
 Catch[ "("<>eval[y,,d]<>")" ] //
 If[ # === "?", Throw@ #, # ] &
 ]
 ];
 f = tl@ f;
 eval[ hd@tl@ f, bind[hd@ f,args,a], d ]
]

size[x_?at] := 0
size[x_] := 1 + size@ tl@ x

evlst[e_?at,a_,d_] := e
evlst[e_,a_,d_] := jn[ eval[hd@ e,a,d], evlst[tl@ e,a,d] ]

bind[vars_?at,args_,a_] := a
bind[vars_,args_,a_] :=
 jn[hd@ vars, jn[hd@ args, bind[tl@ vars,tl@ args,a]]]

eval[e_] :=
(
 print[ "expression", output@ e ];
 eval[ output@ wrap@ e,,Infinity ]
)

run[fn_] := run[fn, "slisp.m", ".srun"]

