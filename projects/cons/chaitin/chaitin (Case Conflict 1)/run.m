(***** RUN.M *****)

(* handle {dd} chars *)
t[x_] := StringReplace[x,convertmap]
convertmap =
 ( FromCharacterCode@ # -> ToString@ {#-128} )& /@
 Range[128,255]
convertmap2 = convertmap /. (l_->r_)->(r->l)

chr3[]:=
Block[ {c},
While[
 StringLength@ line == 0,
 line = Read[i,Record];
 If[ line == EndOfFile, Abort[] ];
 Print@ line;
 WriteString[o,line,"\n"];
 (* keep only non-blank printable ASCII codes *)
 line = FromCharacterCode@
 Cases[ ToCharacterCode@ line, n_Integer /; 32 < n < 127 ]
];
c = StringTake[line,1];
line = StringDrop[line,1];
c
]

chr2[] :=
Block[ {c},
 c = chr3[];
 If[ c =!= "{", Return@ c ];
 While[ StringTake[c,-1] =!= "}", c = c<>chr3[] ];
 c = StringReplace[c,convertmap2];
 If[ StringLength@ c == 1, Return@ c ];
 StringReplace["{0}",convertmap2]
]

chr[] :=
Block[ {c},
While[ True,
 c = chr2[];
 If[ c =!= "[", Return@ c ];
 While[ chr[] =!= "]" ]
]
]

get[sexp_:False,rparenokay_:False] :=

Block[ {c = chr[], d, l ={}, name, def, body, varlist},
 Switch[
 c,
 ")", Return@ If[rparenokay,")",{}],
 "(",
 While[ ")" =!= (d = get[sexp,True]),
 AppendTo[l,d]
 ];
 Return@ l
 ];
 If[ sexp, Return@ c ];
 Switch[
 c,
 "\"", get[True],
 ":",
 {name,def,body} = {get[],get[],get[]};
 If[
 !MatchQ[name,{}|_String],
 varlist = Rest@ name;
 name = First@ name;
 def = {"'",{"&",varlist,def}}
 ];
 {{"'",{"&",{name},body}},def},
 "+"|"-"|"."|"'"|","|"!", {c,get[]},
 "*"|"="|"&"|"?", {c,get[],get[]},
 "/"|":", {c,get[],get[],get[]},
 _, c
 ]
]

(* output S-exp *)
output[x_String] := x
output[{x___}] := StringJoin["(", output /@ {x}, ")"]

blanks = StringJoin@ Table[" ",{12}]

print[x_,y_] := print1[t@ x,t@ y]
print1[x_,y_] := (print2[x,StringTake[y,50]];
 print1["",StringDrop[y,50]]) /; StringLength[y] > 50
print1[x_,y_] := print2[x,y]
print2[x_,y_] := print3[StringTake[x<>blanks,12]<>y]
print3[x_] := (Print[x]; WriteString[o,x,"\n"])

wrap[e_] :=
If[ names === {}, e, {{"'",{"&",names,e}}} ~Join~ defs ]

let[n_,d_] :=
(
 print[ output@ n<> ":", output@ d ];
 names = {n} ~Join~ names;
 defs = {{"'",d}} ~Join~ defs;
)

run[fn_,whoami_,outputsuffix_] :=
(
 line = "";
 names = defs = {};
 t0 = SessionTime[];
 o = OpenWrite[fn<>outputsuffix];
 i = OpenRead[fn<>".lisp"];
 print3["Start of "<>whoami<>" run of "<>fn<>".lisp"];
 print3@ "";
 CheckAbort[
 While[True,
(print3@ "";
 clock = 0;
 Replace[#,{
 {"&",{func_,vars___},def_} :> let[func,{"&",{vars},def}],
 {"&",var_,def_} :> let[var,eval@ def],
 _ :> print[ "value", output@ eval@ # ]
 }];
 If[clock != 0, print["cycles",ToString@clock]]
)& @ get[];
 print3@ ""
 ],
 ];
 print3@ StringForm[
 "Elapsed time `` seconds",
 Round[SessionTime[]-t0]
 ];
 Close@ i;
 Close@ o
)

runall := run /@ {"test","eval","eval2","eval3","omega"}

$RecursionLimit = $IterationLimit = Infinity
SetOptions[$Output,PageWidth->63];

