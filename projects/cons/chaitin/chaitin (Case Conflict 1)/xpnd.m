(***** XPND.M *****)

Off[ General::spell, General::spell1 ]

run[fn_String] := Module[ {p, o},

(* program p is list of instructions of form: l, op[r,s], *)
p = Get[fn<>".rm"];

SetOptions[$Output,PageWidth->62];
Format[LineBreak[_]] = "";
Format[Continuation[_]] = " ";
Print@ "(**** before ****)";
Print@ Short[InputForm@p,10];

p = p //. {
set[x_,x_] ->
 {},
split[h_,t_,s_] ->
 {set[source,s], jump[linkreg3,split$routine],
 set[h,target], set[t,target2]},
hd[t_,s_] ->
 split[t,target2,s],
tl[t_,s_] ->
 split[target,t,s],
empty[r_] ->
 {set[r,")"], left[r,"("]},
atom[r_,l_] ->
 {neq[r,"(",l], set[work,r], right[work], eq[work,")",l]},
jn[i_,x_,y_] ->
 {set[source,x], set[source2,y], jump[linkreg3,jn$routine],
 set[i,target]},
push[x_] ->
 {set[source,x], jump[linkreg2,push$routine]},
pop[x_] ->
 {jump[linkreg2,pop$routine], set[x,target]},
popl[x_,y_] ->
 split[x,y,y]
};

p = Flatten@ p;

p = p /. op_[l___, x_String, r___]
 :> ( ToExpression[ ToString@ op<> "i" ] )[l,x,r];

p = p //. {l___, x_Symbol, y_, r___}
 -> {l, label[x,y], r};

labels =
 ( ToExpression[ "l"<> ToString@ # ] )& /@ Range@ Length@ p;

p = MapThread[ Replace[#1,
 {label[x__] -> label[x], x_ -> label[#2,x]} ]&,
 {p,labels} ];

p = p /. label[x_,op_[y___]] -> {x,op,y};

r[x_] := ToExpression["reg$"<> ToString@ x]; (* register *)
l[x_] := ToExpression["lab$"<> ToString@ x]; (* label *)
i[x_] := x; (* immediate field *)

t[x_] := x /. {
 {a_,op:halt|dump} :> {l@ a, op},
 {a_,op:goto,b_} :> {l@ a, op, l@ b},
 {a_,op:jump,b_,c_} :> {l@ a, op, r@ b, l@ c},
 {a_,op:goback|right|out,b_} :> {l@ a, op, r@ b},
 {a_,op:eq|neq,b_,c_,d_} :> {l@ a, op, r@ b, r@ c, l@ d},
 {a_,op:eqi|neqi,b_,c_,d_} :> {l@ a, op, r@ b, i@ c, l@ d},
 {a_,op:left|set,b_,c_} :> {l@ a, op, r@ b, r@ c},
 {a_,op:lefti|seti,b_,c_} :> {l@ a, op, r@ b, i@ c} };

p = t /@ p;

Print@ "(**** after ****)";
Print@ Short[InputForm@p,10];

o = OpenWrite[fn<>".xrm",PageWidth->62];
Write[o,p];
Close@ o

]

runall := run /@ {"example","test","lisp"}

