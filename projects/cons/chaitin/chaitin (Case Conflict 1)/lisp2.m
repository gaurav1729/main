(***** LISP2.M *****)

<<frontend.m

(* "procedural" lisp interpreter *)

identitymap =
 ( FromCharacterCode /@ Range[0,255] ) ~Join~ {{},}

pos[c_String] :=
 ( If[ # <= 256, #, Abort[] ] )& @
 ( 1 + First@ ToCharacterCode@ c )
pos[{}] :=
 257
pos[_] :=
 258

at[x_] :=
 MatchQ[ x, {}|_String ]
hd[x_] :=
 If[ at@ x, x, First@ x ]
tl[x_] :=
 If[ at@ x, x, Rest@ x ]
jn[x_,y_] :=
 If[ MatchQ[y,_String], x, Prepend[y,x] ]

eval[e_,,d_] := eval[e,identitymap,d]

eval[e2_,a_,d2_] :=

Block[ {e = e2, d =d2, f, args, x, y},
 If[ at@ e, Return@ a[[ pos@ e ]] ];
 f = eval[hd@ e,a,d];
 e = tl@ e;
 Switch[
 f,
 "'", Return@ hd@ e,
 "/", Return@
 If[
 eval[hd@ e,a,d] =!= "0",
 eval[hd@tl@ e,a,d],
 eval[hd@tl@tl@ e,a,d]
 ]
 ];
 args = eval[#,a,d]& /@ e;
 x = hd@ args;
 y = hd@tl@ args;
 Switch[
 f,
 "+", Return@ hd@ x,
 "-", Return@ tl@ x,
 "*", Return@ jn[x,y],
 ".", Return@ If[ at@ x, "1", "0" ],
 "=", Return@ If[ x === y, "1", "0" ],
 ",", Return@ (print[ "display", output@ x ]; x)
 ];
 If[ d == 0, Throw@ "?" ];
 d--;
 Switch[
 f,
 "!", Return@ eval[x,,d],
 "?", Return@
 If[
 Length@x < d,
 Catch@ {eval[y,,Length@x]},
 Catch@ {eval[y,,d]} //
 If[ # === "?", Throw@ #, # ] &
 ]
 ];
 f = tl@ f;
 eval[ hd@tl@ f, bind[hd@ f,args,a], d ]
]

bind[vars_?at,args_,a_] :=
 a

bind[vars_,args_,a_] :=
ReplacePart[
 bind[ tl@ vars, tl@ args, a ],
 hd@ args,
 pos@ hd@ vars
]

eval[e_] :=
(
 print[ "expression", output@ e ];
 eval[ wrap@ e,,Infinity ]
)

run[fn_] := run[fn, "lisp2.m", ".2run"]

