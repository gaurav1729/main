(* CLISPM.M *)

<<frontend.m

(* driver for C lisp machine *)

eval[e_] :=
(
 print[ "expression", output@ e ];
 t1 = "tmp1"<>ToString@ Random[Integer,10^10];
 t2 = "tmp2"<>ToString@ Random[Integer,10^10];
 tmp1 = OpenWrite@ t1;
 (* should check that input has no \n or \0 characters *)
 WriteString[tmp1, StringReverse@ output@ wrap@ e,"\n"];
 Close@ tmp1;
 Run["lispm","<",t1,">",t2];
 tmp2 = ReadList[t2,Record];
 Run["rm",t1];
 Run["rm",t2];
 clock = ToExpression@ tmp2[[-1]];
 tmp2 = StringReverse /@ Drop[tmp2,-1];
 print["display",#]& /@ Drop[tmp2,-1];
 tmp2[[-1]]
)

run[fn_] := run[fn, "clispm.m", ".cmrun"]

