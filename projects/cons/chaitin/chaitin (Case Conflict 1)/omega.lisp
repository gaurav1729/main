[
  Make a list of strings into a prefix-free set
  by removing duplicates.  Last occurrence is kept.
]
& (Rx)
[ P-equiv: are two bit strings prefixes of each other ? ]
: (Pxy) /.x1 /.y1 /=+x+y (P-x-y) 0
[ is x P-equivalent to a member of l ? ]
: (Mxl) /.l0 /(Px+l) 1 (Mx-l)
[ body of R follows: ]
/.xx : r (R-x) /(M+xr) r *+xr

[
  K th approximation to Omega for given U.
]
& (WK)
: (Cxy) /.xy *+x(C-xy)           [ concatenation (set union) ]
: (B)
: k ,(*"&*()*,'k())                    [ write k & its value ]
: s (R(C(Hk)s))   [ add to s programs not P-equiv which halt ]
: s ,(*"&*()*,'s())                    [ write s & its value ]
/=kK (Ms)                [ if k = K, return measure of set s ]
: k *1k                                         [ add 1 to k ]
 (B)
: k ()                                [ initialize k to zero ]
: s ()               [ initialize s to empty set of programs ]
 (B)

[
  Subset of computer programs of size up to k
  which halt within time k when run on U.
]
& (Hk)
[ quote all elements of list ]
: (Qx) /.xx **"'*+x()(Q-x)
[ select elements of x which have property P ]
: (Sx) /.xx /(P+x) *+x(S-x) (S-x)
[ property P
  is that program halts within time k when run on U ]
: (Px) =0.?k(Q*U*x())
[ body of H follows:
  select subset of programs of length up to k ]
(S(Xk))

[
  Produce all bit strings of length less than or equal to k.
  Bigger strings come first.
]
& (Xk)
/.k '(())
: (Zy) /.y '(()) **0+y **1+y (Z-y)
(Z(X-k))

& (Mx)   [ M calculates measure of set of programs ]
[ S = sum of three bits ]
: (Sxyz) =x=yz
[ C = carry of three bits ]
: (Cxyz) /x/y1z/yz0
[ A = addition (left-aligned base-two fractions)
  returns carry followed by sum ]
: (Axy) /.x*0y /.y*0x : z (A-x-y) *(C+x+y+z) *(S+x+y+z) -z
[ M = change bit string to 2**-length of string
  example: (111) has length 3, becomes 2**-3 = (001) ]
: (Mx) /.x'(1) *0(M-x)
[ P = given list of strings,
  form sum of 2**-length of strings ]
: (Px)
   /.x'(0)
   : y (A(M+x)(P-x))
   : z /+y ,'(overflow) 0     [ if carry out, overflow ! ]
   -y                         [ remove carry ]
[ body of definition of measure of a set of programs follows:]
: s (Px)
*+s *". -s                    [ insert binary point ]

[
  If k th bit of string x is 1 then halt, else loop forever.
  Value, if has one, is always 0.
]
& (Oxk) /=0.,k (O-x-k)                                [ else ]
        /.x (Oxk)   [ string too short implies bit = 0, else ]
        /+x 0 (Oxk)

[[[ Universal Computer ]]]

& (Us)

[
  Alphabet:
]
: A '"
((((((((leftparen)(rightparen))(AB))((CD)(EF)))(((GH)(IJ))((
KL)(MN))))((((OP)(QR))((ST)(UV)))(((WX)(YZ))((ab)(cd)))))(((
((ef)(gh))((ij)(kl)))(((mn)(op))((qr)(st))))((((uv)(wx))((yz
)(01)))(((23)(45))((67)(89))))))((((((_+)(-.))((',)(!=)))(((
*&)(?/))((:")(${0}))))(((({1}{2})({3}{4}))(({5}{6})({7}{8}))
)((({9}{10})({11}{12}))(({13}{14})({15}{16})))))((((({17}{18
})({19}{20}))(({21}{22})({23}{24})))((({25}{26})({27}{28}))(
({29}{30})({31}{32}))))(((({33}{34})({35}{36}))(({37}{38})({
39}{40})))((({41}{42})({43}{44}))(({45}{46})({47}{48})))))))
[
  Read 7-bit character from bit string.
  Returns character followed by rest of string.
  Typical result is (A 1111 000).
]
: (Cs)
/.--- ---s (Cs)    [ undefined if less than 7 bits left ]
: (Rx) +-x         [ 1 bit: take right half ]
: (Lx) +x          [ 0 bit: take left half ]
*
 (/+s R L
 (/+-s R L
 (/+--s R L
 (/+---s R L
 (/+----s R L
 (/+-----s R L
 (/+------s R L
  A)))) )))
---- ---s
[
  Read zero or more s-exp's until get to a right parenthesis.
  Returns list of s-exp's followed by rest of string.
  Typical result is ((AB) 1111 000).
]
: (Ls)
: c (Cs)                        [ c = read char from input s ]
/=+c'(right paren) *()-c                       [ end of list ]
: d (Es)                       [ d = read s-exp from input s ]
: e (L-d)                 [ e = read list from rest of input ]
   **+d+e-e                              [ add s-exp to list ]
[
  Read single s-exp.
  Returns s-exp followed by rest of string.
  Typical result is ((AB) 1111 000).
]
: (Es)
: c (Cs)                        [ c = read char from input s ]
/=+c'(right paren) *()-c    [ invalid right paren becomes () ]
/=+c'(left  paren) (L-c)      [ read list from rest of input ]
c                 [ otherwise atom followed by rest of input ]

                    [ end of definitions; body of U follows: ]

: x (Es)   [ split bit string into function followed by data ]
! *+x**"'*-x()()    [ apply unquoted function to quoted data ]


[ Omega ! ]
(W'(1111 111 111))

