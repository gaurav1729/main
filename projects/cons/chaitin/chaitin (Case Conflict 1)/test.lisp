[ LISP test run ]
'(abc)
+'(abc)
-'(abc)
*'(ab)'(cd)
.'a
.'(abc)
='(ab)'(ab)
='(ab)'(ac)
-,-,-,-,-,-,'(abcdef)
/0'x'y
/1'x'y
!,'/1'x'y
(*"&*()*,'/1'x'y())
('&(xy)y 'a 'b)
: x 'a : y 'b *x*y()
[ first atom ]
: (Fx)/.,xx(F+x) (F'((((a)b)c)d))
[ concatenation ]
:(Cxy) /.,xy *+x(C-xy) (C'(ab)'(cd))
?'()'
:(Cxy) /.,xy *+x(C-xy) (C'(ab)'(cd))
?'(1)'
:(Cxy) /.,xy *+x(C-xy) (C'(ab)'(cd))
?'(11)'
:(Cxy) /.,xy *+x(C-xy) (C'(ab)'(cd))
?'(111)'
:(Cxy) /.,xy *+x(C-xy) (C'(ab)'(cd))
?'(1111)'
:(Cxy) /.,xy *+x(C-xy) (C'(ab)'(cd))
[ d: x goes to (xx) ]
& (dx) *,x*x()
[ e really doubles length of string each time ]
& (ex) *,xx
(d(d(d(d(d(d(d(d()))))))))
(e(e(e(e(e(e(e(e()))))))))

