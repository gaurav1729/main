/* high speed LISP interpreter */

#include <stdio.h>

#define SIZE 10000000 /* numbers of nodes of tree storage */
#define LAST_ATOM 255 /* highest integer value of character */
#define nil 0 /* null pointer in tree storage */

long hd[SIZE], tl[SIZE]; /* tree storage */
long vlst[LAST_ATOM]; /* bindings of each atom */
long next = LAST_ATOM+1; /* next free cell in tree storage */

void initialize_atoms(void); /* initialize atoms */
void clean_env(void); /* clean environment */
void restore_env(void); /* restore dirty environment */
long eval(long e, long d); /* evaluate expression */
/* evaluate list of expressions */
long evalst(long e, long d);
/* bind values of arguments to formal parameters */
void bind(long vars, long args);
long at(long x); /* atomic predicate */
long jn(long x, long y); /* join head to tail */
long eq(long x, long y); /* equal predicate */
long cardinality(long x); /* number of elements in list */
long out(long x); /* output expression */
void out2(long x); /* really output expression */
long in(); /* input expression */

main() /* lisp main program */
{
 long d = 999999999; /* "infinite" depth limit */
 initialize_atoms();
 /* read in expression, evaluate it, & write out value */
 out(eval(in(),d));
}

void initialize_atoms(void) /* initialize atoms */
{
 long i;
 for (i = 0; i <= LAST_ATOM; ++i) {
 hd[i] = tl[i] = i; /* so that hd & tl of atom = atom */
 /* initially each atom evaluates to self */
 vlst[i] = jn(i,nil);
 }
}

long jn(long x, long y) /* join two lists */
{
 /* if y is not a list, then jn is x */
 if ( y != nil && at(y) ) return x;

 if (next > SIZE) {
 printf("Storage overflow!\n");
 exit(0);
 }

 hd[next] = x;
 tl[next] = y;

 return next++;
}

long at(long x) /* atom predicate */
{
 return ( x <= LAST_ATOM );
}

long eq(long x, long y) /* equal predicate */
{
 if (x == y) return 1;
 if (at(x)) return 0;
 if (at(y)) return 0;
 if (eq(hd[x],hd[y])) return eq(tl[x],tl[y]);
 return 0;
}

long eval(long e, long d) /* evaluate expression */
{
/*
 e is expression to be evaluated
 d is permitted depth - integer, not pointer to tree storage
*/
 long f, v, args, x, y, vars, body;

 /* find current binding of atomic expression */
 if (at(e)) return hd[vlst[e]];

 f = eval(hd[e],d); /* evaluate function */
 e = tl[e]; /* remove function from list of arguments */
 if (f == ')') return ')'; /* function = error value? */

 if (f == '\'') return hd[e]; /* quote */

 if (f == '/') { /* if then else */
 v = eval(hd[e],d);
 e = tl[e];
 if (v == ')') return ')'; /* error? */
 if (v == '0') e = tl[e];
 return eval(hd[e],d);
 }

 args = evalst(e,d); /* evaluate list of arguments */
 if (args == ')') return ')'; /* error? */

 x = hd[args]; /* pick up first argument */
 y = hd[tl[args]]; /* pick up second argument */

 switch (f) {
 case '+': return hd[x];
 case '-': return tl[x];
 case '.': return (at(x) ? '1' : '0');
 case ',': return out(x);
 case '=': return (eq(x,y) ? '1' : '0');
 case '*': return jn(x,y);
 }

 if (d == 0) return ')'; /* depth exceeded -> error! */
 d--; /* decrement depth */

 if (f == '!') {
 clean_env(); /* clean environment */
 v = eval(x,d);
 restore_env(); /* restore unclean environment */
 return v;
 }

 if (f == '?') {
 x = cardinality(x); /* convert s-exp into number */
 clean_env();
 v = eval(y,(d <= x ? d : x));
 restore_env();
 if (v == ')') return (d <= x ? ')' : '?');
 return jn(v,nil);
 }

 f = tl[f];
 vars = hd[f];
 f = tl[f];
 body = hd[f];

 bind(vars,args);

 v = eval(body,d);

 /* unbind */
 while (at(vars) == 0) {
 if (at(hd[vars]))
 vlst[hd[vars]] = tl[vlst[hd[vars]]];
 vars = tl[vars];
 }

 return v;
}

void clean_env(void) /* clean environment */
{
 long i;
 for (i = 0; i <= LAST_ATOM; ++i)
 vlst[i] = jn(i,vlst[i]); /* clean environment */
}

void restore_env(void) /* restore unclean environment */
{
 long i;
 for (i = 0; i <= LAST_ATOM; ++i)
 vlst[i] = tl[vlst[i]]; /* restore unclean environment */
}

long cardinality(long x) /* number of elements in list */
{
 if (at(x)) return 0;
 return 1+cardinality(tl[x]);
}

/* bind values of arguments to formal parameters */
void bind(long vars, long args)
{
 if (at(vars)) return;
 bind(tl[vars],tl[args]);
 if (at(hd[vars]))
 vlst[hd[vars]] = jn(hd[args],vlst[hd[vars]]);
}

long evalst(long e, long d) /* evaluate list of expressions */
{
 long x, y;
 if (at(e)) return nil;
 x = eval(hd[e],d);
 if (x == ')') return ')';
 y = evalst(tl[e],d);
 if (y == ')') return ')';
 return jn(x,y);
}

long out(long x) /* output expression */
{
 out2(x);
 putchar('\n');
 return x;
}

void out2(long x) /* really output expression */
{
 if ( at(x) && x != nil ) {putchar(x); return;}
 putchar('(');

 while (at(x) == 0) {
 out2(hd[x]);
 x = tl[x];
 }

 putchar(')');
}

long in() /* input expression */
{
 long c = getchar(), first, last, next;
 if (c != '(') return c;
 /* list */
 first = last = jn(nil,nil);
 while ((next = in()) != ')')
 last = tl[last] = jn(next,nil);
 return tl[first];
}

