[[[ LISP semantics defined in LISP ]]]

[ (Vse) = value of S-expression s in environment e.
  If a new environment is created it is displayed. ]
& (Vse)
  /.s /.es /=s+e+-e (Vs--e)
  ('&(f) [ f is the function ]
   /=f"' +-s
   /=f". .(V+-se)
   /=f"+ +(V+-se)
   /=f"- -(V+-se)
   /=f", ,(V+-se)
   /=f"= =(V+-se)(V+--se)
   /=f"* *(V+-se)(V+--se)
   /=f"/ /(V+-se)(V+--se)(V+---se)
     (V+--f,(N+-f-se)) [ display new environment ]
 (V+se)) [ evaluate function f ]

[ (Nxae) = new environment created from list of
  variables x, list of unevaluated arguments a, and
  previous environment e. ]
& (Nxae) /.xe *+x*(V+ae)(N-x-ae)

[ Test function (Fx) = first atom in the S-expression x. ]
& (Fx)/.xx(F+x)        [ end of definitions ]

(F'(((ab)c)d))           [ direct evaluation ]

(V'(F'(((ab)c)d))*'F*F()) [ same thing but using V ]

