[[[ Normal LISP semantics defined in "Sub-Atomic" LISP ]]]

[ (Vse) = value of S-expression s in environment e.
  If a new environment is created it is displayed. ]
& (Vse)
  /.+s         /=s+e+-e (Vs--e)
  /=+s'(QUOTE) +-s
  /=+s'(ATOM)  /.+(V+-se)'(T)'(NIL)
  /=+s'(CAR)   +(V+-se)
  /=+s'(CDR)   : x -(V+-se) /.x'(NIL)x
  /=+s'(OUT)   ,(V+-se)
  /=+s'(EQ)    /=(V+-se)(V+--se)'(T)'(NIL)
  /=+s'(CONS)  : x (V+-se) : y (V+--se) /=y'(NIL) *x() *xy
  /=+s'(COND)  /='(NIL)(V++-se) (V*+s--se) (V+-+-se)
  : f  /.++s(V+se)+s       [ f is ((LAMBDA)((X)(Y))(BODY)) ]
  (V+--f,(N+-f-se))        [ display new environment ]


[ (Nxae) = new environment created from list of
  variables x, list of unevaluated arguments a, and
  previous environment e. ]
& (Nxae) /.xe *+x*(V+ae)(N-x-ae)

[ FIRSTATOM
    ( LAMBDA  ( X )
       ( COND  (( ATOM    X )  X )
               (( QUOTE   T ) ( FIRSTATOM  ( CAR   X )))))
]
& F '
((FIRSTATOM)
    ((LAMBDA) ((X))
       ((COND) (((ATOM)  (X)) (X))
               (((QUOTE) (T)) ((FIRSTATOM) ((CAR) (X))))))
)

[ APPEND
    ( LAMBDA  ( X  Y ) ( COND  (( ATOM   X )  Y )
     (( QUOTE   T ) ( CONS  ( CAR   X )
                            ( APPEND  ( CDR   X )  Y )))))
]
& C '
((APPEND)
    ((LAMBDA) ((X)(Y)) ((COND) (((ATOM) (X)) (Y))
     (((QUOTE) (T)) ((CONS) ((CAR) (X))
                            ((APPEND) ((CDR) (X)) (Y))))))
)

(V'
((FIRSTATOM) ((QUOTE) ((((A)(B))(C))(D))))
F)

(V'
((APPEND) ((QUOTE)((A)(B)(C))) ((QUOTE)((D)(E)(F))))
C)

