(***** LISP.M *****)

<<frontend.m

(* "nonprocedural" lisp interpreter *)

identitymap =
 ( FromCharacterCode /@ Range[0,255] ) ~Join~ {{},}

pos[c_String] :=
 ( If[ # <= 256, #, Abort[] ] )& @
 ( 1 + First@ ToCharacterCode@ c )
pos[{}] :=
 257
pos[_] :=
 258

eval[e_,,d_] :=
 eval[e,identitymap,d]

eval[(e:({}|_String)),a_,_] :=
 a[[ pos@ e ]]

eval[e_,a_,d_] :=
 eval[ eval[ First@ e,a,d ], Rest@ e, a, d ]

eval["'",{e_:{},___},_,_] :=
 e

eval["/",{p_:{},q_:{},r_:{},___},a_,d_] :=
If[
 eval[p,a,d] =!= "0",
 eval[q,a,d],
 eval[r,a,d]
]

eval[f_,e_,a_,d_] :=
 apply[ f, eval[#,a,d]& /@ e, a, d ]

apply["+",{},_,_] := {}
apply["+",{{},___},_,_] := {}
apply["+",{x_String,___},_,_] := x
apply["+",{x_,___},_,_] := First@ x

apply["-",{},_,_] := {}
apply["-",{{},___},_,_] := {}
apply["-",{x_String,___},_,_] := x
apply["-",{x_,___},_,_] := Rest@ x

apply["*",{x_,_String,___},_,_] := x
apply["*",{x_:{},y_:{},___},_,_] := {x} ~Join~ y

apply[".",{},_,_] := "1"
apply[".",{{},___},_,_] := "1"
apply[".",{_String,___},_,_] := "1"
apply[".",_,_,_] := "0"

apply["=",{x_:{},y_:{},___},_,_] :=
 If[ x === y, "1", "0" ]

apply[",",{x_:{},___},_,_] :=
 (print[ "display", output@ x ]; x)

apply["!",_,_,d_] :=
 Throw@ "?" /; d == 0
apply["!",{x_:{},___},_,d_] :=
 eval[x,,d-1]

apply["?",_,_,d_] :=
 Throw@ "?" /; d == 0
apply["?",{}|{_},_,_] :=
 {}
apply["?",{_String,y_,___},_,d_] :=
 apply["?",{{},y},,d]
apply["?",{x_,y_,___},_,d_] :=
 Catch@ {eval[y,,Length@x]} /; Length@x < d-1
apply["?",{x_,y_,___},_,d_] :=
 Catch@ {eval[y,,d-1]} // If[ # === "?", Throw@ #, # ] &

(* If not a primitive function: *)
apply[_,_,_,d_] :=
 Throw@ "?" /; d == 0
apply[(b:({}|_String)),_,a_,_] :=
 a[[ pos@ b ]]
apply[{_,_String,b_:{},___},_,a_,d_] :=
 eval[b,a,d-1]
apply[{_,x_:{},b_:{},___},v_,a_,d_] :=
 eval[ b, bind[x,v,a], d-1 ]

bind[{},v_,a_] :=
 a

bind[x_,{},a_] :=
 bind[x,{{}},a]

bind[x_,v_,a_] :=
ReplacePart[
 bind[ Rest@ x, Rest@ v, a ],
 First@ v,
 pos@ First@ x
]

eval[e_] :=
(
 print[ "expression", output@ e ];
 eval[ wrap@ e,,Infinity ]
)

run[fn_] := run[fn, "lisp.m", ".run"]

