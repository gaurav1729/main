(***** LISPM.M *****)

<<frontend.m

(* lisp machine interpreter *)

p = << lisp.xrm

labels = Cases[p, {l_,__} -> l]

If[
 Length@ Union@ labels != Length@ p,
 Print@ "Duplicate labels!!!"
]

registers = Cases[p, {_,_,r__} -> r] // Flatten // Union
registers = Cases[registers, r_Symbol -> r]
registers = Complement[registers,labels]

Evaluate[ next /@ labels ] = RotateLeft@ labels
Evaluate[ #[]& /@ registers ] = {}& /@ registers
Evaluate[ #[]& /@ labels ] =
 Cases[p, {l_,op_,x___} -> op[next[l],x]]

first[x_] := If[ x === {}, "\0", x[[1]] ]

out[n_,r_] :=
(
 print[ "display", StringJoin@@ Flatten@ r[] ];
 n
)

dump[n_] :=
(
 print[ ToString@ #, StringJoin@@ Flatten@ #[] ] & /@
 registers;
 n
)

eqi[n_,r_,i_,l_] := If[ first[r[]] === i, l, n ]
neqi[n_,r_,i_,l_] := If[ first[r[]] =!= i, l, n ]
eq[n_,r_,s_,l_] := If[ first[r[]] === first[s[]], l, n ]
neq[n_,r_,s_,l_] := If[ first[r[]] =!= first[s[]], l, n ]

lefti[n_,r_,i_] :=
If[
 i === "\0", error[],
 r[] = {i, r[]};
 n
]

left[n_,r_,s_] :=
If[
 s[] === {}, error[],
 r[] = {s[][[1]], r[]};
 s[] = s[][[2]];
 n
]

right[n_,r_] :=
If[
 r[] === {}, error[],
 r[] = r[][[2]];
 n
]

seti[n_,r_,"\0"] := (r[] = {}; n)
seti[n_,r_,i_] := (r[] = {i, {}}; n)
set[n_,r_,s_] := (r[] = s[]; n)

goto[n_,l_] := l
halt[n_] := halt
error[] := (Print@ "ERROR!!!"; Abort[])

ravel[c_,r___] := {c, ravel[r]}
ravel[] := {}

jump[n_,r_,l_] :=
(
 r[] = ravel@@ Characters[ "("<>ToString[n]<>")" ];
 l
)

goback[n_,r_] :=
ToExpression[
 StringJoin@@ Drop[ Drop[ Flatten@ r[], 1], -1]
]

eval[e_] :=
(
 print[ "expression", output@ e ];
 reg$expression[] = ravel@@ Characters@ output@ wrap@ e;
 loc = lab$l1;
 While[ loc =!= halt, clock++; loc = loc[] ];
 StringJoin@@ Flatten@ reg$value[]
)

run[fn_] := run[fn, "lispm.m", ".mrun"]

