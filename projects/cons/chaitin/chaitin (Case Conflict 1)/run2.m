(***** RUN2.M *****)

(* handle let/m-exp/s-exp/comments/funny chars/blanks *)
input[x_] := l[m@@ s@@ c@@ Characters@ f@ b@ StringJoin@ x]

(* keep only non-blank printable ASCII codes *)
b[x_] := FromCharacterCode@
 Cases[ ToCharacterCode@ x, n_Integer /; 32 < n < 127 ]

(* handle {dd} chars *)
f[x_] := StringReplace[x,convertmap2]
t[x_] := StringReplace[x,convertmap]
convertmap =
 ( FromCharacterCode@ # -> ToString@ {#-128} )& /@
 Range[128,255]
convertmap2 = convertmap /. (l_->r_)->(r->l)

(* remove comments *)
c["[",x__] := Replace[c@ x,{___,"]",y___}->{y}]
c[x_,y___] := {x} ~Join~ c@ y
c[] := {}

(* handle explicit parens (s-exp) *)
s["(",x__] := Replace[s@ x,{y___,")",z___}->{{y},z}]
s[x_,y___] := {x} ~Join~ s@ y
s[] := {}

(* handle implicit parens (m-exp) *)
get[c_,i_,x_] := {{c}~Join~Take[x,i]} ~Join~ Drop[x,i]
m[c:("+"|"-"|"."|"'"|","|"!"),x__] := get[c,1,m@ x]
m[c:("*"|"="|"&"|"?"),x__] := get[c,2,m@ x]
m[c:("/"|":"),x__] := get[c,3,m@ x]
m[")",y___] := {{}} ~Join~ m@ y
m["\"",")",y___] := {{}} ~Join~ m@ y
m["\"",x_,y___] := {x} ~Join~ m@ y
m[{x___},y___] := {m@ x} ~Join~ m@ y
m[x_,y___] := {x} ~Join~ m@ y
m[] := {}

(* handle definitions (let) *)
l[x_] := x //. {":",{func_,vars___},def_,body_} ->
 {{"'",{"&",{func},body}},{"'",{"&",{vars},def}}} \
 //. {":",var_,def_,body_} ->
 {{"'",{"&",{var},body}},def}

(* output S-exp *)
output[x_String] := x
output[{x___}] := StringJoin["(", output /@ {x}, ")"]

blanks = StringJoin@ Table[" ",{12}]

print[x_,y_] := print1[t@ x,t@ y]
print1[x_,y_] := (print2[x,StringTake[y,50]];
 print1["",StringDrop[y,50]]) /; StringLength[y] > 50
print1[x_,y_] := print2[x,y]
print2[x_,y_] := print3[StringTake[x<>blanks,12]<>y]
print3[x_] := (Print[x]; WriteString[o,x,"\n"])

wrap[e_] :=
 If[ names === {}, e, {{"'",{"&",names,e}}} ~Join~ defs ]

let[n_,d_] :=
(
 print[ output@ n<> ":", output@ d ];
 names = {n} ~Join~ names;
 defs = {{"'",d}} ~Join~ defs;
)

run[fn_,whoami_,outputsuffix_] :=
(
 names = defs = {};
 t0 = SessionTime[];
 o = OpenWrite[fn<>outputsuffix];
 print3["Start of "<>whoami<>" run of "<>fn<>".lisp"];
(
 print3@ "";
 clock = 0;
 Replace[#,{
 {"&",{func_,vars___},def_} :> let[func,{"&",{vars},def}],
 {"&",var_,def_} :> let[var,eval@ def],
 _ :> print[ "value", output@ eval@ #]
 }];
 If[clock != 0, print[ "cycles", ToString@ clock ]];
)& /@ (input@ ReadList[fn<>".lisp",Record]);
 print3@ "";
 print3@ StringForm[
 "Elapsed time `` seconds",
 Round[SessionTime[]-t0]
 ];
 Close@ o
)

runall := run /@ {"test","eval","eval2","eval3","omega"}

$RecursionLimit = $IterationLimit = Infinity
SetOptions[$Output,PageWidth->63];

