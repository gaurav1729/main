(* RM2C.M *)

p = <<lisp.xrm
p = (ToString /@ #)& /@ p
p = p /. {"'" -> "\\'", "\0" -> "\\0"}
labels = #[[1]]& /@ p
Evaluate[ next /@ labels ] = RotateLeft@ labels
registers =
 Select[ Union@ Flatten@ p, StringMatchQ[#,"reg$*"]& ]

o = OpenWrite@ "lispm.c"
put[x_] := WriteString[o,StringReplace[x,map],"\n"]

map = {}

put@ "/* LISP interpreter running on register machine */"
put@ "#include <stdio.h>"
put@ "#define size 100000"
put@ ""
put@ "main() /* lisp main program */"
put@ "{"
put@ "static char *label[] = {"
(
 map = {"R" -> #};
 put@ "\"(R)\","
)& /@ labels
put@ "\"\"}; /* end of label table */"
put@ ""
put@ "char c, *i, *j, *k;"
put@ "long n;"
put@ "double cycles = 0.0;"
put@ ""
(
 map = "R" -> #;
 put@ "char $R[size] = \"\", *R = $R;"
)& /@ registers
put@ ""
put@ "while ((c = getchar()) != '\\n') *++reg$expression = c;"
put@ ""

Cases[p,
 {l_,op_,a_:"",b_:"",c_:""} :>
 (map =
 {
 "L" -> l, "O" -> op, "A" -> a, "B" -> b, "C" -> c,
 "N" -> StringReverse@ next@ l
 };
 put@ ("/* L: O A,B,C */");
 put@ "L: cycles += 1.0;";
 put@ Switch[
 ToExpression@op,
 dump, "/* not supported */",
 halt, "goto termination_routine;",
 goto, "goto A;",
 goback, "k = A;\ngoto goback_routine;",
 eqi, "if (*A == 'B') goto C;",
 neqi, "if (*A != 'B') goto C;",
 eq, "if (*A == *B) goto C;",
 neq, "if (*A != *B) goto C;",
 right, "if (A != $A) --A;",
 lefti,
 "if (A == ($A+size)) goto storage_full;"
 ~StringJoin~ "\n*++A = 'B';",
 left,
 "if (A == ($A+size)) goto storage_full;"
 ~StringJoin~ "\n*++A = *B;\nif (B != $B) --B;",
 seti,
 If[ b === "\\0",
 "A = $A;",
 "*(A = ($A+1)) = 'B';"
 ],
 set,
 "A = $A;\ni = $B;\nwhile (i < B) *++A = *++i;",
 out,
 "i = $A;\nwhile (i < A) putchar(*++i);\nputchar('\\n');",
 jump,
 "A = $A;\ni = \")N(\";\nwhile ((*++A = *i++) != '(');"
 ~StringJoin~ "\ngoto B;"
 ]
 )
]

put@ ""
put@ ("goto termination_routine; " ~StringJoin~
 "/* in case fell through without halting */")
put@ ""
put@ "goback_routine: n = 0;\n"
put@ "bump_label: i = k;\nj = label[n++];"
put@ "while (*j != '\\0') if (*i-- != *j++) goto bump_label;"
put@ ""
put@ "switch (n) {"
MapThread[
 (
 map = {"L" -> #1, "I" -> #2};
 put@ "case I: goto L;"
 )&,
 {labels,ToString /@ Range[1,Length@labels]}
]

put@ "default:"
put@ "printf(\"!retsasid kcabog\");\ngoto finish;"
put@ "} /* end of switch */"
put@ ""
put@ "storage_full:"
put@ "printf(\"!lluf egarots\");"
put@ "goto finish;"
put@ ""
put@ "termination_routine:"
put@ "i = $reg$value;"
put@ "while (i < reg$value) putchar(*++i);"
put@ "finish:"
put@ "printf(\"\\n%.0f\\n\",cycles);"
put@ ""
put@ "} /* end of lisp machine! */"

Close@ o

(* compile resulting C program *)
Print@ "!cc -O -olispm lispm.c"
!cc -O -olispm lispm.c

