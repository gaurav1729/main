(* CLISP.M *)

<<frontend.m

(* driver for C lisp interpreter *)

eval[e_] :=
(
 print[ "expression", output@ e ];
 t1 = "tmp1"<>ToString@ Random[Integer,10^10];
 t2 = "tmp2"<>ToString@ Random[Integer,10^10];
 tmp1 = OpenWrite@ t1;
 (* should check that input has no \0 characters *)
 (* and also no characters above hex FF *)
 WriteString[tmp1, output@ wrap@ e,"\n"];
 Close@ tmp1;
 Run["lisp","<",t1,">",t2];
 tmp2 = ReadList[t2,Record];
 Run["rm",t1];
 Run["rm",t2];
 print["display",#]& /@ Drop[tmp2,-1];
 tmp2[[-1]]
)

run[fn_] := run[fn, "clisp.m", ".crun" ]

