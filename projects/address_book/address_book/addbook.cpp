// addbook.cpp

#include <iostream>
#include <string>
#include <vector>
#include <map>
using namespace std;

bool Is_Good_Email(const string);
void Print_User(const string in);
string lower_case(const string in);

template<class type, class container>
Super<type, container> Initialize_From_Data_File(string datafile);

template<class type>
int Create_Main_Menu(&vector<type>);

// This is to be an addressbook/databook:
// Functionality:
//      be able to search a given field for a given phrase
//      be able to choose a set of personal entries with fields matching the phrase
//         and add and subtract entries from it.
//      have a separate classification of data (passport numbers, credit card
//         numbers, etc.)
//      be able to print all chosen data (personal entries, data and other header info)
//         to a page and create a latex document and then a pdf/ps file.
//      search for all people with birthdays between two given dates.
//          
//
//  Each personal entry should have flexible number of home, work, cell phones
//  and comments for each of them.
//
//  Each personal entry should have possibility of multiple people in a family,
//  with sub-division of home, work, cell phones to each person and common stuff
//  also marked.
//
//  Each data entry should have the option of being a credit card or other type,
//  and has an owner.  If it is a credit card, it should be encoded before
//  getting written in.
//
//  There should be a compact view and an expanded view.
//
//  Be able to import and export data.
//  Have appropriate choices on each screen.


// there has to be a header (with commands that are always available), a mode
// line that tells us what is in focus and if it has been selected or not, the
// actual stuff being viewed, a set of view specific options, and a footer to
// input the option to.

// make a base class that has all the things that we require from all classes.  


//Different types of screens are main screen, list (list of names that have fit
//some criterion), command (ask for further details regarding some command),
//edit (edit an entry).
//
//After the edit screen, we get some further screens that ask about the fields
//to fill up in the entry.
//
//Command screens lead to list screens.
//
//At each screen, have the option to display the selected buffer.
//
// Selected buffer is a special case of list screen with the added option of
// printing.
//
// At each list screen, have the option of choosing which entries to add to the
// selected buffer.


//Different screens:
//a) main menu -- choices are
//    i) search for a given string
//    -> ask for the name of the field to be searched, or search all fields.
//    ii) subsume one entry into another (maybe implement this later).
//



/* ::::::::::::::::::::::::::::::::::::: */

//work questions: how do we know what fields each class/subclass has, so that we
//can display them?

// maybe we should have each structure know what it's parents are and if their
// subclasses are selected or not


// have a menu class that can be printed on screen, with members that describe
// the different fields and the actions for each field, the chosen field, the
// next screen, the state of the code.  At each class, have a make_menu() method
// that automatically takes the data and creates a menu.

// Are the other classes also derived from the menu class?

// creditcards: phonenumbers,pins,cardnumbers (with appropriate changes)

#define CONSTRUCTOR
#define DESTRUCTOR
#define FUNCTOR

const int ALIKE            =  0; // for strcmp
const int UNALIKE          =  1; // for strcmp
const int NOT_IN_FOCUS     = -1; // which member of the vector is in focus

enum Email_Type {home_email, work_email};
enum Phone_Type {cell_phone, home_phone, work_phone};

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

struct p // parameters for the project.
{
  const string file = "~/.addbook/addbook_data.txt";
  
}; //  struct p

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

// create a new class, which is a vector of fields, and use that as the base
// case instead of the Field class?  have information inside that one about how
// to display it, whether to check for equality or inclusion, etc.

// add a new member, focus, to tell us which one is being edited (although that
// should be in the main rather than in the class, i think).

// 


//::::::::::::::::::::::::::::::::::::::::::::::::::::::

class All // base class, whether a field or a node, things that are required by both
{
protected:
  bool selected; // this tells us whether we have put this in the selected buffer or not.
  bool this_focus = false; // are we in the focus chain?
  virtual bool last_focus = false; //are any subs in focus? always zero for fields. maybe should be function?
  int indent; // how much to indent this--any more printing info required?
  string name; // is this a family entry, phone number, what? set only by constructor
  Super * const owner; // address of owner, constructor sets this
public:  
  int set_focus()
  { this_focus = true; }

  int unset_focus() {this_focus = false; } //also should unset focus all the way down the chain
  virtual bool has(); //contains, but is '==' for phone, email, etc.
  virtual void print() const; // overload Screen & operator<< (Screen&, Base_Class&)
                         // will depend on value of 'selected'
  string get_name(){ return name; }
  void select(); // push this into superclass's select list, set selected
} // class All

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

template <class subclass_type, class container>
class Super // base class for nodes that have subclasses; should be derived from all???
{
protected:
  bool selected_all; // are *all* subs selected? (maybe should be in Super) .. this should propagate down
  container<subclass_type> subs; //the subclasses have to know their owner; will be constructed w/ 'this' arg
  container<subclass_type &> select_list; // list of addresses or indices of all subs that are selected.
  int sub_focus = NOT_IN_FOCUS; // index of the subclass that's in focus
public:
  virtual int set_sub_focus(int ind) // at the top level, there is no 'this' --or, at top level, create with null instead of addr
  {
    subs.at(ind).set_focus();
    return 0;
  }
  int select_all(); //select all subfields, recursively
  // constructor will call subclass_type constructor with args this and subclass name
};



//::::::::::::::::::::::::::::::::::::::::::::::::::::::

// This is the lowest level class. It can be for example, address, name, tel
// number (derived from it), email, credit card number, etc.  no subclasses
// beneath it! opposite of a Super class

class Field
{
protected:
  string val;
  string tag; // tells us what kind of field it is, probably not necessary; if it is necessary, set up a local enum
  int indent; // how many spaces do we indent this when printing?
  mutable bool selected; // tells us if this has been selected to be displayed (maybe one for screen, one for printing?)
public:
  virtual void setval(string in) { val = in; }
  string getval() { return val; }
  CONSTRUCTOR Field() { val = "hello akash"; }
  CONSTRUCTOR Field(string in) {val = in;}
  virtual bool contains(const string search_string) const; // for email, phone numbers we might want equality
                                                          // rather than 'contains' (but overload '==' instead is better!)
  virtual bool contains_find(const string search_string) const;
  void select() { selected = true; }
  void unselect() { selected = false; }
  bool get_select() { return selected; }


}; //   struct field

bool Field::contains(const string search_string) const //get this to handle upper/lower case
                                                       //  -- look at transform tolower in <cctype>
{
  int len_search_str = search_string.length();
  int len_field      = val.length();
  int comp;
  for (int i = 0; i + len_search_str <= len_field; i++)
    {
      if ( val.compare(i, len_search_str, search_string) == ALIKE )
        return true;
    }
  return false;
} // bool Field::contains(string search_string)


bool Field::contains_find(const string search_string) const // just for practice
{
  string lower_search_string = lower_case(search_string);
  string lower_val = lower_case(val);
  if (lower_val.find(lower_search_string, 0) == string::npos)
    return false;
  else
    return true;

}

FUNCTOR class to_lower
{
public:
  char operator() (char c) const
  {
    return ('A' <= c && c <= 'Z') ? (c-'A'+'a') : c  ;
  } //    char operator() (char c) const
}; //  FUNCTOR class to_lower


string lower_case(const string in)
{
  string out = in;
  transform(in.begin(), in.end(), out.begin(), to_lower());

  return out;
} //  string lower_case(string in)

//::::::::::::::::::::::::::::::::::::::::::::::::::::::



class Date
{
  friend ostream& operator<< (ostream&os, Date d);
protected:
  int date;
  int month;
  int year;
  mutable bool selected; // has this been selected for display?
  static int indent; // what's the default indent for printing this?  how do we set this if it is private?
                     // instead of this, could have nesting level.
public:
  void print() { cout << month << '/' << date << '/' << year << endl; }
  void set_date(int in)  {date = in;};  int get_date() {return date;}
  void set_month(int in) {month = in;}; int get_month(){return month;}
  void set_year(int in)  {year = in;};  int get_year() {return year;}
  void select() { selected = true; }
  void unselect() { selected = false; }
  bool get_select() { return selected; }
  void get_input_date_from_user()
  {
    string input_str;
    cout << "input the date (yyyymmdd)";
    cin >> input_str;
    set_date (atoi(input_str.substr(0,4).c_str()));
    set_month(atoi(input_str.substr(4,2).c_str()));
    set_year (atoi(input_str.substr(6,2).c_str()));
  }
  CONSTRUCTOR Date() {date = 26; month = 12; year = 2004; }
  CONSTRUCTOR Date(int in1, int in2, int in3)
  {
    date = in1;
    month = in2;
    // if the year is two digits, add the appropriate century
    if ( 0 < in3 <= 10 )
      year = 2000 + in3;
    else if (in3 < 100)
      year = 1900 + in3;
    else
      year = in3;
  }
}; // class Date

Date::indent = 5;

ostream& operator<< (ostream& os, Date d)
{
  return os << d.month << '/' << d.date << '/' << d.year;
}


//::::::::::::::::::::::::::::::::::::::::::::::::::::::


enum locale {us, foreign}; // good place to try polymorphism on us or foreign addresses.

class Address
{
private:
  mutable bool selected; // we want to select/deselect all of them simultaneously
public:
  Field street_address;
  Field city;
  Field state;
  Field country;
  Field code;
  void select() { selected = true; } 
  void unselect() { selected = false; }
  bool get_select() { return selected; }

} //  class Address:Field

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

class Phone_Number:Field
{
  friend ostream& operator<<(ostream &, Phone_Number);
private:
  Phone_Type type;
public:
  CONSTRUCTOR Phone_Number() { val = ""; type = home_phone; }
  CONSTRUCTOR Phone_Number(string in_val, Phone_Type in_type = home_phone)
  {
    val = in_val; type = in_type;
  }
  void print()
  {
    if (val.length() == 10) // it's an american phone number
      { cout << "(" << val.substr(0,3) << ") " << val.substr(3,3) << "-" << val.substr(6,4)<<endl; }
    else
      { cout << val << endl; }
  } //   void print()

}; // class Phone_Number


  ostream & operator<< (ostream& os, Phone_Number pn) // get this to also print type
  {
    string type_str;
    switch (pn.type)
      {
      case home_phone: type_str = "home"; break;
      case cell_phone: type_str = "cell"; break;
      case work_phone: type_str = "work"; break;
      default: type_str = ""; break;
      }
    if (pn.val.length() == 10)
      { os << type_str<< ": ("<<pn.val.substr(0,3)<<") "<<pn.val.substr(3,3)<<"-"<<pn.val.substr(6,4)<<endl; }
    else
      { os << type_str << ": " << pn.val << endl; }

    return os;
  }

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

class Email:Field
{
private:
  Email_Type type;
  string description; // one word description eg. yahoo, etc.
public:
  CONSTRUCTOR Email() { val = ""; type = home_email; description = ""; }
  CONSTRUCTOR Email( string in_val, string in_desc, Email_Type in_type = home_email)
  {
    if (! Is_Good_Email(in_val))
      {
        Print_User("Email is not of appropriate type");
        val = "";
      }
    else
      val         = in_val;
    type        = in_type;
    description = in_desc;
  }
}; // class Email:Field

bool Is_Good_Email(const string in)
{
  if (in.find("@",0) == string::npos)
    return false;
  else
    return true;
} //  bool Is_Good_Email(const string in)


void Print_User(const string in)
{
  cout << in << endl;
} //  void Print_User(const string in)


//::::::::::::::::::::::::::::::::::::::::::::::::::::::

class Person
{
private:
  Field first_name;
  Field last_name;
  Date bday;
  vector<Email> emails;
  vector<Phone_Number> phones;
  Field comments;
  Field relationship; // relationship to primary person in the family_entry
public:

  // have set and get functions here...do we make them templates, or do we have
  // one for each field?

}; // class Person

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

class Family_Entry
{
private:
  enum Contact_Type_Enum { work, personal, info };
  Person main_person; // alphabetical ordering done on this name; required
  vector<Person> people;
  vector<Phone_Number> phones;
  vector<Email> emails;
  Contact_Type_Enum contact_type;
public:
  static int num_entries;
  CONSTRUCTOR Family_Entry() {num_entries++;};
  DESTRUCTOR ~Family_Entry() {num_entries--;};
  //  Person_Entry read_entry(int); //
}; //   struct Family_Entry

// initialize static element
int Family_Entry::num_entries = 0;

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

// make this a template so that we can go from vector to linked list easily -- and basically for practice.
template <class container>
class Screen
{
protected:
  static const container<string> header; // some commands we always want to see
  static const container<string> footer; // asking for command?
  container<string> line; // each line to be printed to the screen
public:
  void print()
  {
    container::iterator j = header.begin();
    for (  ; j != header.end(); j++)
      cout << *j << endl;
    container::iterator i = line.begin();
    for (  ; i != line.end(); i++)
      {
        cout << *i << endl;
      }
    container::iterator k = footer.begin();
    for (  ; k != footer.end(); k++)
      cout << *k;
  } //   void print()
  template <class type>
  void create(vector<type>)
  {
    
  }
}; // class Screen

template <class container> const container<string> Screen<container>::header[0] = "";
template <class container> const container<string> Screen<container>::footer[0] = "enter your choice : ";

//::::::::::::::::::::::::::::::::::::::::::::::::::::::

int main()
{
  /*
    Read the existing file.  Bring up the main menu.  Work from there.

    Save if there have been any changes to the file before quitting.
   */
  Super<Family_Entry, map> top_level;
  top_level = Initialize_From_Data_File(p.file);
  Screen<vector> screen;
  vector<string> main_menu;
  Create_Main_Menu(&main_menu);

  while(1) // main loop
    {
      screen.create(main_menu);
      screen.print();

      choice = screen.input();

    } //   while(1) -- main loop
  

  return 0;
} //   int main()


//:::::::::::::::::::::::::::::::::::::::::::::::::;

// read the data file, parse it, write it into the top level container.

template<class type, class container>
Super<type, container> Initialize_From_Data_File(string datafile)
{
  
}

//:::::::::::::::::::::::::::::::::::::::::::::::::;
