#!/usr/bin/env python3
import os
import sys

def Power():
  cstr = 'upower -i /org/freedesktop/UPower/devices/battery_BAT0| grep -E "state|time\ to|percentage"'
  p_stat_str = os.popen(cstr).read()
  p_stat_list = p_stat_str.split('\n')
  p_stat_dict = dict([[x.strip() for x in y.split(':')] for y in p_stat_list if len(y) > 0])
  return(p_stat_dict)


pstatus = Power()['state']
if pstatus == "discharging":
  pstatus_sym = "- "
else:
  pstatus_sym = "+ "
  
sys.stdout.write(pstatus_sym + Power()['percentage'] + "\n")
