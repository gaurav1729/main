(setq ip-indent-level 0)
(setq ip-indent-val   2)
(setq ip-space      " ")
(defun ip-drop-line-other-window ()
  (interactive)
  (let (start line tot-indent ret-func)
    (setq tot-indent (* ip-indent-level ip-indent-val))

    ;; we're in the first window
    (beginning-of-line-text)
    (setq start (point))
    (end-of-line)
    (setq line (buffer-substring start (point)))

    ;; move to the other buffer
    (other-window 1)
    (beginning-of-line)
    ;; indent:
    (dotimes (number tot-indent number)
      (insert ip-space))
    (insert line)
    ;; kludge: (because i don't know any better)
    (if (lookup-key (current-local-map) (kbd "RET"))
	(comint-send-input)
      (newline))

    ;; back to the first
    (other-window 1)
    (next-line)))

(defun ip-indent-increase ()
  (interactive)
  (setq ip-indent-level (1+ ip-indent-level))
  (message "indent level is %d" ip-indent-level))


(defun ip-indent-decrease ()
  (interactive)
  (setq ip-indent-level (1- ip-indent-level))
  (message "indent level is %d" ip-indent-level))

(defun ip-indent-reset ()
  (interactive)
  (setq ip-indent-level 0)
  (message "indent level is %d" ip-indent-level))


(defun ip-drop-line-other-window2 ()
  (interactive)
  (let (start line tot-indent ret-func-loc ret-func-glo)
    (setq tot-indent (* ip-indent-level ip-indent-val))

    ;; we're in the first window
    ;(beginning-of-line-text)
    (back-to-indentation)
    (setq start (point))
    (end-of-line)
    (setq line (buffer-substring start (point)))

    ;; move to the other buffer
    (other-window 1)
    (setq ret-func-loc (lookup-key (current-local-map)  (kbd "RET")))
    (setq ret-func-glo (lookup-key (current-global-map) (kbd "RET")))
    (beginning-of-line)
    ;; indent:
    (dotimes (number tot-indent number)
      (insert ip-space))
    (insert line)
    ;; kludge: (because i don't know any better)
    (if ret-func-loc
	(apply ret-func-loc 'nil)
      (apply ret-func-glo 'nil))

    ;; back to the first
    (other-window 1)
    (next-line)))

(defun ip-drop-ret-other-window ()
  (interactive)
  (let (ret-func-loc ret-func-glo)
    (other-window 1)
    (setq ret-func-loc (lookup-key (current-local-map)  (kbd "RET")))
    (setq ret-func-glo (lookup-key (current-global-map) (kbd "RET")))
    (if ret-func-loc
	(apply ret-func-loc 'nil)
      (apply ret-func-glo 'nil))
    (other-window 1)))
;; (define-key global-map [(control x) (control a) (control a)] 'ip-drop-line-other-window2)
;; (define-key global-map [(control x) (control a) 0] 'ip-indent-reset)
;; (define-key global-map [(control x) (control a) p] 'ip-indent-increase)
;; (define-key global-map [(control x) (control a) m] 'ip-indent-decrease)

(global-set-key (kbd "C-x C-a C-a") 'ip-drop-line-other-window2)
(global-set-key (kbd "C-x C-a 0")   'ip-indent-reset)
(global-set-key (kbd "C-x C-a p")   'ip-indent-increase)
(global-set-key (kbd "C-x C-a m")   'ip-indent-decrease)
(global-set-key (kbd "C-x C-a r")   'ip-drop-ret-other-window)


