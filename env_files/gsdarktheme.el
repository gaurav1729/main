(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(default ((t (:foreground "navajowhite1" :background "#254050" :family "lucidatypewriter")))) ; was gray25
 '(blue ((t (:foreground "skyblue"))))
 '(bold ((t (:bold t))))
 '(bold-italic ((t (:bold t :italic t))))
 '(cperl-array-face ((t (:bold t :foreground "navajowhite1"))))
 '(cperl-hash-face ((((class color) (background light)) (:foreground "navajowhite1" :italic t :bold t))))
 '(cperl-nonoverridable-face ((((class color) (background light)) (:foreground "palegoldenrod"))))
 '(custom-button-face ((t (:bold t))) t)
 '(custom-comment-face ((((class grayscale color) (background light)) (:foreground "" :background "black"))) t)
 '(custom-comment-tag-face ((((class color) (background light)) (:foreground "green"))) t)
 '(custom-group-tag-face ((((class color) (background light)) (:foreground "skyblue" :underline t))) t)
 '(custom-state-face ((((class color) (background light)) (:foreground "green3"))) t)
 '(custom-variable-button-face ((t (:bold t :underline t))) t)
 '(custom-variable-tag-face ((((class color) (background light)) (:foreground "skyblue" :underline t))) t)
 '(font-lock-comment-face ((t (:foreground "green"))))
 '(font-lock-doc-string-face ((((class color) (background light)) (:foreground "lightblue"))))
 '(font-lock-function-name-face ((t (:foreground "magenta"))))
 '(font-lock-keyword-face ((t (:foreground "red" :weight extra-bold))))
 '(font-lock-preprocessor-face ((((class color) (background light)) (:foreground "yellow" :underline t))))
 '(font-lock-reference-face ((((class color) (background light)) (:foreground "mediumaquamarine" :underline t))))
 '(font-lock-string-face ((t (:foreground "cyan"))))
 '(font-lock-type-face ((t (:foreground "chocolate1"))))
 '(font-lock-variable-name-face ((((class color) (background light)) (:foreground "navajowhite1"))))
 '(font-lock-warning-face ((t (:foreground "chocolate1" :weight bold))))
 '(highlight ((t (:background "midnightblue"))))
 '(hyper-apropos-documentation ((((class color) (background light)) (:foreground "red"))))
 '(hyper-apropos-hyperlink ((((class color) (background light)) (:foreground "lightblue"))))
 '(isearch ((t (:background "lightslateblue"))))
 '(italic ((t (:italic t))))
 '(message-cited-text ((t (:foreground "steelblue1"))))
 '(message-header-contents ((t (:foreground "red" :size "7" :italic t))))
 '(message-headers ((t (:foreground "green" :size "7" :bold t))))
 '(message-highlighted-header-contents ((t (:size "7" :bold t :italic t))))
 '(mode-line ((t (:box (:line-width 2 :color "red") :foreground "yellow" :background "slategray"))))
 '(mode-line-buffer-id ((t (:foreground "gray80"))))
 '(modeline-mousable ((t (:foreground "cyan"))))
 '(modeline-mousable-minor-mode ((t (:foreground "white"))))
 '(primary-selection ((t (:foreground "gray50" :background "steelblue1"))))
 '(region ((t (:background "blue"))))
 '(widget ((t (:foreground "black" :background "Gray80"))))
 '(widget-button-face ((t (:size "9" :bold t))) t)
 '(widget-documentation-face ((((class color) (background light)) (:foreground "white"))) t)
 '(widget-field-face ((((class grayscale color) (background light)) (:foreground "black" :background "grey"))) t)
 '(widget-inactive-face ((((class grayscale color) (background light)) (:foreground "gray"))) t)
 '(zmacs-region ((t (:background "blue")))))

;; '(mode-line ((t (:box (:line-width 2 :color "red") :foreground "darkorange" :background "slategray"))))
 ;; '(mode-line-buffer-id ((t (:foreground "gray80"))))

;; (set-face-background 'modeline "khaki4")
;; (set-face-foreground 'modeline "yellow")

;; kind of nice, but not worth it:
;; (custom-set-faces
;;  '(mode-line ((t (:box (:line-width 2 :color "red"))))))

;; ;(set-foreground-color "navajowhite3")
;; (set-background-color "gray25")
(set-cursor-color "skyblue3")
;; (setq load-path (append (list "/home/gshah/emacs/")
;;                         load-path))

;;(set-default-font "Consolas")
;;(set-default-font "ProggyCleanTT") ;; actually pretty good
(condition-case nil
    (set-default-font "Inconsolata-10")
    ;(error (set-default-font "Consolas-9"))
    ;(error set-default-font "DejaVu Sans Mono-10")
  (error nil)
  )
