###shrc

## list of commands that need to be used sometimes:

## git pull to update local from remote
## git add <filenames>; git commit -m <message>
## or git commit -a -m <message>
## then git push origin master

## chromebook: disable touchpad easily
# alias detouch='xinput set-prop 12 "Device Enabled" 0'
# alias retouch='xinput set-prop 12 "Device Enabled" 1'
# alias xterm='/usr/bin/xterm -bg darkslategray -fg navajowhite -cr red'
# alias run_ctags='cd /home/gshah/DQM/trunk;ctags -e pmr/*.py util/*.py lib/*.py lib/*/*.py; cd -'
# alias pdb='/usr/local/lib/python2.5/pdb.py'
# this should give line number, but doesn't work
# export PS1='$USER@$HOST:{!}=>'
# this should set the window title
# export PROMPT_COMMAND='echo -n "^[]1;$PWD^G"'
# alias q='QHOME=~/q rlwrap -r ~/q/l32/q'

# User specific aliases and functions


# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

alias l='ls -l'
alias lrt='ls -lrt'
alias lsd='ls -l | grep ^d'

alias up='cd ..'
alias up2='cd ../..'
alias up3='cd ../../..'
alias up4='cd ../../../..'
alias up5='cd ../../../../..'

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ln='ln -i'
alias mkdir='mkdir -p'
alias rrm='/bin/rm'

alias more='/usr/bin/less'
alias memacs='emacs -mm'

alias battery_level='upower -i /org/freedesktop/UPower/devices/battery_BAT0'

export PS1="\W $ "
export EDITOR='/usr/bin/emacs'
export ASYMPTOTE_PDFVIEWER='/usr/bin/evince'
case $TERM in
        xterm*)
	#            PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
	                PROMPT_COMMAND='echo -ne "\033]0;${PWD}\007"'

	    
            ;;
        *)
            ;;
     esac

# log into my bluehost account (edgeofthecircle.net)
alias ssheotc='ssh -p 22 edgeofu2@edgeofthecircle.net'

# find all files recursively under .; order them by date.
alias lslrt="find . -printf '%T@ %t %p\n' | sort -k 1 -n | cut -d' ' -f2-"
alias lsrt="find . -type f -printf '%T@ %P\n' | sort -n | awk '{print $2}'"

# Load pyenv automatically by adding
export PATH="/home/gaurav/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

#this is the last line of the file
