;; .emacs
(put 'dired-find-alternate-file 'disabled nil)
(require 'package)
;; (add-to-list 'package-archives
;; 	     '("melpa" . "http://melpa.milkbox.net/packages/")
;; 	     ;'("melpa-stable" . "http://stable.melpa.org/packages/")
;; 	     t)
(setq package-archives
        '(("Elpa"     . "https://elpa.gnu.org/packages/")
          ("Melpa Stable" . "https://stable.melpa.org/packages/")
          ("Melpa"        . "https://melpa.org/packages/")
          ("marmalade" . "http://marmalade-repo.org/packages/"))
        package-archive-priorities
        '(("MELPA Stable" . 10)
          ("GNU ELPA"     . 5)
          ("MELPA"        . 0)))
; package-refresh-contents
(add-to-list 'package-archives
  '("melpa" . "http://melpa.milkbox.net/packages/") t)

;; (add-to-list 'load-path
;;              "~/path-to-yasnippet")
;(require 'yasnippet)
;(yas-global-mode 1)

(add-to-list 'load-path "/home/gaurav/Dropbox/gaurav/mygit/main/env_files/")
(add-to-list 'load-path "/home/gaurav/Dropbox/gaurav/mygit/main/env_files/csharp-mode-master")
(load "~/Dropbox/gaurav/mygit/main/env_files/gsdarktheme")

;; turn on font-lock mode
(when (fboundp 'global-font-lock-mode)
  (global-font-lock-mode t))

;; buffer-menu opens in the same window, as opposed to list-buffers
(global-set-key "\C-x\C-b" 'buffer-menu)

;; default to better frame titles
(setq frame-title-format
      '("emacs   buffer: %b   pos: %p   %n"))
      ;(concat  "%b - emacs@" system-name))

;; ediff is in one frame rather than two
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; insist on line wrapping
(setq truncate-partial-width-windows nil)

;; If you set set-mark-command-repeat-pop to non-nil, then immediately after you
;; type C-u C-SPC, you can type C-SPC instead of C-u C-SPC to cycle through the
;; mark ring. By default, set-mark-command-repeat-pop is nil.
(setq set-mark-command-repeat-pop t)

;;;;;;;;;;;;;;;;
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(case-fold-search t)
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(current-language-environment "Latin-1")
 '(custom-safe-themes
   (quote
    ("fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" "04dd0236a367865e591927a3810f178e8d33c372ad5bfef48b5ce90d4b476481" "7153b82e50b6f7452b4519097f880d968a6eaf6f6ef38cc45a144958e553fbc6" default)))
 '(default-input-method "latin-1-prefix")
 '(fill-column 80)
 '(global-font-lock-mode t nil (font-lock))
 '(inhibit-startup-screen t)
 '(package-selected-packages (quote (treemacs magit dash-functional skewer-mode)))
 '(package-selected-packages
   (quote
    (realgud-ipdb realgud alect-themes treemacs magit dash-functional skewer-mode)))
 '(show-paren-mode t nil (paren))
 '(tool-bar-mode nil)
 '(transient-mark-mode t)
 '(uniquify-buffer-name-style (quote forward) nil (uniquify)))

(condition-case nil
    (set-default-font "Inconsolata-10")
    ;(error (set-default-font "Consolas-9"))
    ;(error set-default-font "DejaVu Sans Mono-10")
  (error nil)
  )

(defun scroll-up-one-line ()
  (interactive)
  (scroll-up 1))

(defun scroll-down-one-line ()
  (interactive)
  (scroll-down 1))

(defun scroll-left-one-char ()
  (interactive)
  (scroll-left 1))
(defun scroll-right-one-char ()
  (interactive)
  (scroll-right 1))

(global-set-key [f1] 'enlarge-window)
(global-set-key [f2] 'shrink-window)
(global-set-key [f3] 'enlarge-window-horizontally)
(global-set-key [f4] 'shrink-window-horizontally)
(global-set-key [f5] 'goto-line)


(defalias 'line-comment
  (read-kbd-macro "C-a C-SPC C-e M-x comme SPC reg SPC RET C-n C-a"))
(global-set-key [f11] 'line-comment)
(defalias 'line-uncomment
  (read-kbd-macro "C-a C-SPC C-e M-x uncomment- reg SPC RET TAB C-n C-a"))
(global-set-key [f12] 'line-uncomment)


;; (global-set-key [home] 'beginning-of-buffer)
;; (global-set-key [end] 'end-of-buffer)
(define-key global-map [(shift down)] 'scroll-up-one-line)
(define-key global-map [(shift up)] 'scroll-down-one-line)
(defun scroll-other-up-one-line ()
 (interactive)
 (scroll-other-window 1))

(defun scroll-other-down-one-line ()
 (interactive)
 (scroll-other-window -1))

(define-key global-map [(control prior)] 'scroll-other-window-down)
(define-key global-map [(control next)] 'scroll-other-window)

(define-key global-map [(control left)] 'beginning-of-buffer)
(define-key global-map [(control right)] 'end-of-buffer)
;; (define-key global-map [(shift down)] 'scroll-up-command)
;; (define-key global-map [(shift up)] 'scroll-down-command)
(define-key global-map [(control end)] 'end-of-buffer-other-window)
(define-key global-map [(control home)] 'beginning-of-buffer-other-window)

(global-set-key [?\C-\`]          'other-window)
(global-set-key [ (control tab) ] 'other-window)
(global-set-key [?\C-t]           'other-window)

;; Prevent newlines from being added at the end of a buffer when arrowing down.
(setq next-line-add-newlines nil)

; to make sure full lines get shown for both partial windows and full windows.
(setq truncate-partial-width-windows nil)

; pops mark from the stack!!!
(global-set-key [(shift f4)] (lambda () (interactive) (set-mark-command t)))

;; Define function to match a parenthesis otherwise insert a %
(defun my-match-paren (arg)
  "Go to the matching parenthesis if on parenthesis otherwise insert %."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        (t (self-insert-command (or arg 1)))))

(defun goto-match-paren (arg)
   "Go to the matching parenthesis if on parenthesis."
   (interactive "p")
   (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
         ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
         (t nil)))

(global-set-key "\M-o" 'goto-match-paren)


;; put the cursor on a line in the debugger.  the code we're editing is assumed to be in the other buffer. 
;; C-x C-a C-a then inserts that line into the code buffer.
(load "ipmode")
;;;;;;;;;;;;

;; multiple desktop management
(load "my-desktop")

(fset 'vertify
   [?\C-x ?1 ?\C-x ?3 C-tab ?\C-x ?b return])

(fset 'horify
   [?\C-x ?1 ?\C-x ?2 C-tab ?\C-x ?b return])

(defun refresh-file ()
  "load this file again; useful if it's changed; perhaps save the mark ring?"
  (interactive)
  (let ((filename (buffer-file-name))
	(pos (point))
	(buffer (current-buffer)))
    (kill-buffer buffer)
    (find-file filename)
    (goto-char pos)))

(defun ic10 ()
  "set the default font to inconsolata-10"
  (interactive)
  (set-default-font "Inconsolata-10"))

(defun ic11 ()
  "set the default font to inconsolata-11"
  (interactive)
  (set-default-font "Inconsolata-11"))

(defun consolify ()
  "set the default font to Consolas"
  (interactive)
  (set-default-font "Consolas-9"))


(defun dv10 ()
  "set the default font to DejaVu Sans Mono-10"
  (interactive)
  (set-default-font "DejaVu Sans Mono-10"))

(defun pcq ()
  "short for python clear queue, call after running python through shell"
  (interactive)
  (py-clear-queue))

  
    



;; some functions to write: refresh all files (test if they are files and have
;; changed recently first); find next long line; python indent line in other
;; buffer (right and left); take function call and function def and set
;; corresponding variables to each other; run a region in the other window; show
;; all encompassing scopes; begin and end in latex (or other languages);

(put 'narrow-to-region 'disabled nil)
(ido-mode t)

;; in text mode, word count mode and longlines mode
(add-hook 'text-mode-hook 'wc-mode)
(add-hook 'text-mode-hook 'visual-line-mode)

;; (setq load-path
;;       (append (list nil
;;                     "~/.emacs.d/python-mode-1.0/"
;;             "~/.emacs.d/pymacs/"
;;             "~/.emacs.d/ropemacs-0.6"
;;                     )
;;               load-path))



;; add line to latest value in kill ring

(defun append-line-yank ()
  "This copies the line to the most recent member of the
kill-ring and moves to the next line.  Using this repeatedly will
append all the lines to the same member of the kill-ring.  If you
do anything other than call this function, it will start a new
member of the kill-ring. NOTE: need to save the position of the
mark."
  (interactive)
  (let* ((old-mark (pop-mark))
	 (lin (progn
		(beginning-of-line nil)
		(set-mark-command nil)
		(end-of-line nil)
		(buffer-substring (mark) (point))))
	 (cur (car kill-ring)))
    (if (eq last-command 'append-line-yank)
	(kill-new (concat cur lin "\n") t)
      (kill-new (concat lin "\n") nil))
    (forward-line nil)
    (push-mark old-mark t)
    (deactivate-mark)))


(global-set-key (kbd "C-l")
		'append-line-yank)

;; save all ~ files in the temp directory
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(defun goto-percent (percent)
  "Goto PERCENT of buffer."
  (interactive "nGoto percent: ")
  (goto-char (/ (* percent (point-max)) 100)))

(global-set-key (kbd "M-g M-t")
		'goto-percent)

(defun gsdark ()
  """loads my much beloved dark theme"
  (interactive)
  (load "~/Dropbox/gaurav/mygit/main/env_files/gsdarktheme"))

(defun goto-percent (percent)
  "Goto PERCENT of buffer."
  (interactive "nGoto percent: ")
  (goto-char (/ (* percent (point-max)) 100)))
(global-set-key (kbd "M-g M-t")
		'goto-percent)

;; restart shell in current buffer
(defun restart-shell()
  (interactive)
  (shell
   (current-buffer)))

;; make transparent

(set-frame-parameter (selected-frame) 'alpha '(50 . 50))
(add-to-list 'default-frame-alist '(alpha . (50 . 50)))

;You can use the following snippet after you’ve set the alpha as above to assign a toggle to “C-c t”:

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
		    ((numberp (cdr alpha)) (cdr alpha))
		    ;; Also handle undocumented (<active> <inactive>) form.
		    ((numberp (cadr alpha)) (cadr alpha)))
	      100)
	 '(50 . 50) '(100 . 100)))))
(global-set-key (kbd "C-c t") 'toggle-transparency)

; A general transparency function:

;; Set transparency of emacs
(defun transparency (value)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nTransparency Value 0 - 100 opaque:")
  (set-frame-parameter (selected-frame) 'alpha value))

;; run pdflatex rather than latex
(setq latex-run-command "pdflatex")

;; light modes: spacemacs-light, alect-light
;; realgud, realgud:ipdb for debugging

;; magit reference:
;; Crash course
;; Getting started with Magit is really easy:
;; M-x magit-status to see git status, and in the status buffer:
;; s to stage files
;; c c to commit (type the message then C-c C-c to actually commit)
;; b b to switch to another branch
;; Other handy keys:
;; P u to do a git push
;; F u to do a git pull
;; try to press TAB

;;;;;;;;;;;;;;;;;;;
;; desktop saves the session
;; (desktop-load-default)
;; (add-to-list 'desktop-globals-to-save 'file-name-history)
;; (desktop-read)
;; (setq desktop-enable t)
;;(desktop-save-mode t)

;; some python stuff::::::::::::::::::::::::::::::
;; set the pdb variable, get it to use the current buffer as default
;; (setq pdb-path '/usr/local/lib/python2.5/pdb.py
      ;; gud-pdb-command-name (symbol-name pdb-path))
;; http://lists.gnu.org/archive/html/help-gnu-emacs/2003-10/msg00577.html
;; (defadvice pdb (before gud-query-cmdline activate)
;;   "Provide a better default command line when called interactively."
;;   (interactive
;;    (list (gud-query-cmdline pdb-path
;; 			    (file-name-nondirectory buffer-file-name)))))



