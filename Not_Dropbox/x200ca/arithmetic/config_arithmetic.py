username = "Akash"

operator_probs = {
    '+': 0.5,
    '-': 0.5,
    '*': 0.0,
    '/': 0.0,
    }

max_int = 12

# found in the results_dir, defined in arithmetic_caller (!)
results_file = 'arith_results.pkl'

# in scoring, how much weight to give previous score vs latest result
results_persistence = 0.5

# how long to run it if Image (PIL) cannot load
default_duration = 8

# frames per sec for a gif file, approx
frames_per_sec = 8.0


next_prob_delay_seconds = 1

is_prod = True

