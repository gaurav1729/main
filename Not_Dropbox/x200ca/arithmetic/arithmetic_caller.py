#!/usr/bin/env python
is_prod = False
import os, sys

proj_dir   = os.path.join('..', '..', 'Dropbox','gaurav',
                          'svnco','svn_repository','projects')
image_dir  = os.path.join('.', 'image_files')
utils_dir  = os.path.join(proj_dir, 'pylib')
source_dir = os.path.join(proj_dir, 'arithmetic')
results_dir = os.path.join('.', 'results')


sys.path.append(utils_dir)
sys.path.append(source_dir)
sys.path.append(os.getcwd())

import config_arithmetic as config

if is_prod:
    print("is prod")
    import pyqt4
else:
    print("is not prod")
    import pyqt4_working as pyqt4

pyqt4.main(image_dir, results_dir)

